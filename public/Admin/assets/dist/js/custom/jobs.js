$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});



// Jobs
$(document).on("click", "#AdminPostJobs", function(){
    var employer_id = $("#employer_id").val();
    var title = $("#title").val();
    var address = $("#address").val();
    var zipcode = $("#zipcode").val();
    var country = $("#country").val();
    var state = $("#state").val();
    var city = $("#city").val();
    var email = $("#email").val();
    var mobile = $("#mobile").val();
    var job_type = $("#job_type :selected").val();
    var industry = $("#industry :selected").val();
    var role = $("#role").val();
    var min_salary = $("#salary_min").val();
    var max_salary = $("#salary_max").val();
    var offered_salary = min_salary+ "-" + max_salary;
    var salary_type = $("#salary_type").val();
    var gender = $("#gender :selected").val();
    var career_level = $("#career_level :selected").val();
    var experience_years = $("#experience_year").val();
    var experience_months = $("#experience_month").val();
    var experience = experience_years+ "-" +experience_months;
    var last_date = $("#last_date").val();
    var qualification = $("#qualification").val();
    var positions = $("#positions").val();
    // var description = CKEDITOR.instances['description'].getData();
    var description = $("#description").val();


    if(title == ""){
        danger_msg("Title should not empty");
        $("#title").focus();
        return false;
    }

    if(address == ""){
        danger_msg("Address should not empty");
        $("#address").focus();
        return false;
    }

    if(zipcode == ""){
        danger_msg("Zipcode should not empty");
        $("#zipcode").focus();
        return false;
    }

    if(country == ""){
        danger_msg("Country should not empty");
        $("#country").focus();
        return false;
    }

    if(state == ""){
        danger_msg("State should not empty");
        $("#state").focus();
        return false;
    }

    if(city == ""){
        danger_msg("City should not empty");
        $("#city").focus();
        return false;
    }

    if(email == ""){
        danger_msg("Email should not empty");
        $("#email").focus();
        return false;
    }

    if(mobile == ""){
        danger_msg("Mobile should not empty");
        $("#mobile").focus();
        return false;
    }

    if(job_type == "Select job type"){
        danger_msg("Job type should not empty");
        $("#job_type").focus();
        return false;
    }

    if(industry == "Select industry"){
        danger_msg("Industry should not empty");
        $("#industry").focus();
        return false;
    }

    if(role == ""){
        danger_msg("Role should not empty");
        $("#role").focus();
        return false;
    }

    if(min_salary == ""){
        danger_msg("Salary min should not empty");
        $("#salary_min").focus();
        return false;
    }

    if(max_salary == ""){
        danger_msg("Salary max should not empty");
        $("#salary_max").focus();
        return false;
    }

    if(gender == "Select gender"){
        danger_msg("Gender should not empty");
        $("#gender").focus();
        return false;
    }

    if(career_level == "2"){
        if($("#experience_year").val() == ""){
            danger_msg("Experience year should not empty");
            $("#experience_year").focus();
            return false;
        }

        if($("#experience_month").val() == ""){
            danger_msg("Experience month should not empty");
            $("#experience_month").focus();
            return false;
        }
    }

    if(last_date == ""){
        danger_msg("Last date should not empty");
        $("#last_date").focus();
        return false;
    }

    if(qualification == ""){
        danger_msg("Qualification should not empty");
        $("#qualification").focus();
        return false;
    }

    if(positions == ""){
        danger_msg("Positions should not empty");
        $("#positions").focus();
        return false;
    }

    if(description == ""){
        danger_msg("Description should not empty");
        $("#description").focus();
        return false;
    }

    var JobsData = {
        employer_id: employer_id,
        title: title,
        address: address,
        zipcode: zipcode,
        country: country,
        state: state,
        city: city,
        email: email,
        mobile: mobile,
        job_type: job_type,
        industry: industry,
        role: role,
        offered_salary: offered_salary,
        salary_type: salary_type,
        gender: gender,
        career_level: career_level,
        experience: experience,
        last_date: last_date,
        qualification: qualification,
        positions: positions,
        description: description
    }


    $.ajax({
        type: "POST",
        url: "/Employer/AddJobs",
        data: JobsData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                success_msg(data.message);
                return false;
            }
        }
    });

});

function CheckZipCode(){
    var zipcode = $("#zipcode").val();

    $.ajax({
        type: "GET",
        url: "https://api.postalpincode.in/pincode/"+zipcode,
        data: {zipcode: zipcode},
        dataType: "JSON",
        success: function (data) {
            
            if(data.error){
                console.log(data);
                return false;
            }else{
                console.log(data);
                $.each(data, function (key, val) { 
                    // console.log(val.PostOffice);
                    $.each(val.PostOffice, function (keys, i) { 
                        console.log(i.Country);
                        console.log(i.State);
                        $("#country").val(i.Country);
                        $("#state").val(i.State);
                        $("#city").val(i.District);
                        // console.log(i.District);
                    });
                });
                return false;
            }
            
        }
    });
}

function SalaryType(){
    var SalaryType = $("#salary_type").val();
    if(SalaryType == 1){
        $("#salary_min").val("");
        $("#salary_max").val("");
        $("#salary_min").attr("maxlength", 5);
        $("#salary_max").attr("maxlength", 5);
    }else{
        $("#salary_min").val("");
        $("#salary_max").val("");
        $("#salary_min").attr("maxlength", 6);
        $("#salary_max").attr("maxlength", 6);
    }
}   

function CheckCareerLevel(){
    if($("#career_level").val() == 2){
        // $("#exp_show").show();
        $("#experience_year").css("background","#fff");
        $('#experience_year').attr("disabled", false);
        $("#experience_month").css("background","#fff");
        $('#experience_month').attr("disabled", false);
    }else{
        // $("#exp_show").hide();
        $("#experience_year").css("background","#8080803b");
        $('#experience_year').attr("disabled", true);
        $("#experience_month").css("background","#8080803b");
        $('#experience_month').attr("disabled", true);
    }
}