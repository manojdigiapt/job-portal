$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});


$(document).on("click", "#AddFaculties", function(){
    var name = $("#name").val();
    var email = $("#email").val();
    var password = $("#password").val();

    if(name == ""){
        danger_msg("Name should not be empty");
        $("#name").focus();
        return false;
    }

    if(email == ""){
        danger_msg("Email should not be empty");
        $("#email").focus();
        return false;
    }


    if(password == ""){
        danger_msg("Password should not be empty");
        $("#password").focus();
        return false;
    }

    var AdminUsersData = {
        name: name,
        email: email,
        password: password
    }

    $.ajax({
        type: "POST",
        url: "/AddAdminFaculty",
        data: AdminUsersData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                success_msg(data.message);
                location.reload();
                return false;
            }
        }
    });
});



$(document).on("click", "#UpdateFacultyUsers", function(){
    var id = $("#EditId").val();
    var name = $("#Editname").val();
    var email = $("#Editemail").val();
    var password = $("#Editpassword").val();

    if(name == ""){
        danger_msg("Name should not be empty");
        $("#name").focus();
        return false;
    }

    if(email == ""){
        danger_msg("Email should not be empty");
        $("#email").focus();
        return false;
    }


    if(password == ""){
        danger_msg("Password should not be empty");
        $("#password").focus();
        return false;
    }

    var AdminUsersData = {
        id: id,
        name: name,
        email: email,
        password: password
    }

    $.ajax({
        type: "POST",
        url: "/UpdateFaculty",
        data: AdminUsersData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                success_msg(data.message);
                location.reload();
                return false;
            }
        }
    });
});





$(document).on("click", "#AddWebinars", function(){

    var web_topic = $("#web_topic").val();
    var speaker = $("#speaker_name").val();
    var web_date = $("#web_date").val();
    var web_type = $("#web_type :selected").val();
    var price = $("#price").val();
    var web_category = $("#web_category").val();
    var web_duration = $("#web_duration").val();
    var start_timing = $("#start_timing").val();
    var end_timing = $("#end_timing").val();
    // var web_description = $("#web_description").val();
    var web_description = CKEDITOR.instances['web_description'].getData();


    if(web_topic == ""){
        danger_msg("Webinar topic should not be empty");
        $("#web_topic").focus();
        return false;
    }

    if(speaker == ""){
        danger_msg("Speaker should not be empty");
        $("#speaker_name").focus();
        return false;
    }

    if(web_date == ""){
        danger_msg("Webinar date should not be empty");
        $("#web_date").focus();
        return false;
    }

    if(web_type == "Select Type"){
        danger_msg("Webinar type should not be empty");
        $("#web_type").focus();
        return false;
    }

    if(web_type == 2){
        if(price == ""){
            danger_msg("Price should not be empty");
            $("#price").focus();
            return false;
        }
    }

    if(web_category == "Select Category"){
        danger_msg("Webinar category should not be empty");
        $("#web_category").focus();
        return false;
    }

    

    if(start_timing == ""){
        danger_msg("Start timing should not be empty");
        $("#start_timing").focus();
        return false;
    }

    if(end_timing == ""){
        danger_msg("End timing should not be empty");
        $("#end_timing").focus();
        return false;
    }

    if(web_duration == ""){
        danger_msg("Webinar date should not be empty");
        $("#web_duration").focus();
        return false;
    }

    var WebinarData = new FormData();

    WebinarData.append('web_topic', web_topic);
    WebinarData.append('speaker', speaker);
    WebinarData.append('web_date', web_date);
    WebinarData.append('web_type', web_type);
    WebinarData.append('price', price);
    WebinarData.append('web_category', web_category);
    WebinarData.append('web_duration', web_duration);
    WebinarData.append('start_timing', start_timing);
    WebinarData.append('end_timing', end_timing);
    WebinarData.append('web_description', web_description);
    WebinarData.append('file', $('#inputFile')[0].files[0]);

    $.ajax({
        type: "POST",
        url: "/PostWebinars",
        data: WebinarData,
        dataType: "JSON",
        contentType: false,
        processData: false,
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                success_msg(data.message);
                // location.reload();
                return false;
            }
        }
    });

});



$(document).on("click", "#UpdateWebinars", function(){
    var WebId = $("#WebId").val();

    var web_topic = $("#web_topic").val();
    var speaker = $("#speaker_name").val();
    var web_date = $("#web_date").val();
    var web_type = $("#web_type :selected").val();
    var price = $("#price").val();
    var web_category = $("#web_category").val();
    var web_duration = $("#web_duration").val();
    var start_timing = $("#start_timing").val();
    var end_timing = $("#end_timing").val();
    // var web_description = $("#web_description").val();
    var web_description = CKEDITOR.instances['web_description'].getData();


    if(web_topic == ""){
        danger_msg("Webinar topic should not be empty");
        $("#web_topic").focus();
        return false;
    }

    if(speaker == ""){
        danger_msg("Speaker should not be empty");
        $("#speaker_name").focus();
        return false;
    }

    if(web_date == ""){
        danger_msg("Webinar date should not be empty");
        $("#web_date").focus();
        return false;
    }

    if(web_type == "Select Type"){
        danger_msg("Webinar type should not be empty");
        $("#web_type").focus();
        return false;
    }

    if(web_type == 2){
        if(price == ""){
            danger_msg("Price should not be empty");
            $("#price").focus();
            return false;
        }
    }

    if(web_category == "Select Category"){
        danger_msg("Webinar category should not be empty");
        $("#web_category").focus();
        return false;
    }

    

    if(start_timing == ""){
        danger_msg("Start timing should not be empty");
        $("#start_timing").focus();
        return false;
    }

    if(end_timing == ""){
        danger_msg("End timing should not be empty");
        $("#end_timing").focus();
        return false;
    }

    if(web_duration == ""){
        danger_msg("Webinar date should not be empty");
        $("#web_duration").focus();
        return false;
    }

    var WebinarData = new FormData();

    WebinarData.append('WebId', WebId);
    WebinarData.append('web_topic', web_topic);
    WebinarData.append('speaker', speaker);
    WebinarData.append('web_date', web_date);
    WebinarData.append('web_type', web_type);
    WebinarData.append('price', price);
    WebinarData.append('web_category', web_category);
    WebinarData.append('web_duration', web_duration);
    WebinarData.append('start_timing', start_timing);
    WebinarData.append('end_timing', end_timing);
    WebinarData.append('web_description', web_description);
    WebinarData.append('image', $('#inputFile')[0].files[0]);

    // console.log(WebinarData);
    // return false;
    
    $.ajax({
        type: "POST",
        url: "/UpdateWebinars",
        data: WebinarData,
        dataType: "JSON",
        contentType: false,
        processData: false,
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                success_msg(data.message);
                // location.reload();
                return false;
            }
        }
    });

});


function CheckPaymentType(){
    var web_type = $("#web_type").val();

    if(web_type == 1){
        $("#ShowPrice").hide();
        return false;
    }else{
        $("#ShowPrice").show();
        return false;
    }
}



function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#image_upload_preview').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}



function DeleteWebinars(id){
    swal({
        title: 'Are you sure?',
        text: 'You won\'t be able to revert this!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then(function (isConfirm) {
        if (isConfirm) {
            $.ajax({
                type: "GET",
                url: "/Admin/DeleteWebinars/"+id,
                dataType: "JSON",
                success: function (data) {
                    if(data.error){
                        danger_msg(data.message);
                        return false;
                    }else{
                        swal("Deleted!", "Data deleted successfully", "success");
                        location.reload();
                        return false;
                    }
                }
            });
        }
    }).catch(swal.noop)
}





function CheckParticipants(id){
    
    $.ajax({
        type: "POST",
        url: "/Admin/GetViewParticipants/"+id,
        // data: "data",
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                return false;
            }else{
                $("#Name").text("");
                $("#Email").text("");
                $("#Mobile").text("");
                $.each(data, function (i, key) { 

                    // $("#Name").text(key.name);
                    // $("#Email").text(key.email.replace(/(\w{1})(.*)(\w{1})@(.*)/, '$1******$3@$4'));
                    // $("#Mobile").text(key.mobile.replace(/(\d{1})(.*)(\d{3})/, '$1******$3'));
                    // console.log(key.email);
                    var GetParticpates = "<tr><td>"+key.name+"</td><td>"+key.email.replace(/(\w{1})(.*)(\w{1})@(.*)/, '$1******$3@$4')+"</td><td>"+key.mobile.replace(/(\d{1})(.*)(\d{3})/, '$1******$3')+"</td></tr>";

                    $("#ShowParticipates").append(GetParticpates);
                });
                $("#ViewParticipants").modal("show");
                return false;
            }
        }
    });
}



function calculateTime(){
    var day = '1 1 1970 ',  // 1st January 1970
        start = $('#start_timing').val(),   //eg "09:20 PM"
        end = $('#end_timing').val(),   //eg "10:00 PM"
        diff_in_min = ( Date.parse(day + end) - Date.parse(day + start) ) / 1000 / 60;
    $("#web_duration").val(diff_in_min + ' min');
}




$("#inputFile").change(function () {
    readURL(this);
});

function danger_msg(data){
    $.notify(data, {type:"danger"});
}

function success_msg(data){
    $.notify(data, {type:"success"});
}
