$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});


$(document).on("click", "#AddTitle", function(){
    var name = $("#title").val();

    if(name == ""){
        danger_msg("Title should not be empty");
        $("#title").focus();
        return false;
    }
    

    var AdminUsersData = {
        name: name
    }

    $.ajax({
        type: "POST",
        url: "/Admin/AddJobTitle",
        data: AdminUsersData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                success_msg(data.message);
                location.reload();
                return false;
            }
        }
    });
});


function EditTitle(id){
    $.ajax({
        type: "POST",
        url: "/Admin/GetJobTitlebyId/"+id,
        dataType: "JSON",
        success: function (data) {
            $("#EditId").val(data.id);
            $("#Editname").val(data.name);

            $("#EditTitle").modal("show");
        }
    });
}


$(document).on("click", "#UpdateTitle", function(){
    var id = $("#EditId").val();
    var name = $("#Editname").val();

    if(name == ""){
        danger_msg("Title should not be empty");
        $("#Editname").focus();
        return false;
    }

    var AdminUsersData = {
        id: id,
        name: name
    }

    $.ajax({
        type: "POST",
        url: "/Admin/UpdateJobTitle",
        data: AdminUsersData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                success_msg(data.message);
                location.reload();
                return false;
            }
        }
    });
});


function DeleteTitle(id){
    swal({
        title: 'Are you sure?',
        text: 'You won\'t be able to revert this!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then(function (isConfirm) {
        if (isConfirm) {
            $.ajax({
                type: "GET",
                url: "/Admin/DeleteJobTitle/"+id,
                dataType: "JSON",
                success: function (data) {
                    if(data.error){
                        danger_msg(data.message);
                        return false;
                    }else{
                        swal("Deleted!", "Data deleted successfully", "success");
                        location.reload();
                        return false;
                    }
                }
            });
        }
    }).catch(swal.noop)
}


// Job Type

$(document).on("click", "#AddJobType", function(){
    var name = $("#job_type").val();

    if(name == ""){
        danger_msg("Job type should not be empty");
        $("#job_type").focus();
        return false;
    }

    var AdminUsersData = {
        name: name
    }

    $.ajax({
        type: "POST",
        url: "/Admin/AddJobType",
        data: AdminUsersData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                success_msg(data.message);
                location.reload();
                return false;
            }
        }
    });
});


function EditJobType(id){
    $.ajax({
        type: "POST",
        url: "/Admin/GetJobTypebyId/"+id,
        dataType: "JSON",
        success: function (data) {
            $("#EditId").val(data.id);
            $("#EditType").val(data.job_type);

            $("#EditJobType").modal("show");
        }
    });
}



$(document).on("click", "#UpdateJobType", function(){
    var id = $("#EditId").val();
    var name = $("#EditType").val();

    if(name == ""){
        danger_msg("Job Type should not be empty");
        $("#EditType").focus();
        return false;
    }

    var AdminUsersData = {
        id: id,
        name: name
    }

    $.ajax({
        type: "POST",
        url: "/Admin/UpdateJobType",
        data: AdminUsersData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                success_msg(data.message);
                location.reload();
                return false;
            }
        }
    });
});

function DeleteJobType(id){
    swal({
        title: 'Are you sure?',
        text: 'You won\'t be able to revert this!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then(function (isConfirm) {
        if (isConfirm) {
            $.ajax({
                type: "GET",
                url: "/Admin/DeleteJobType/"+id,
                dataType: "JSON",
                success: function (data) {
                    if(data.error){
                        danger_msg(data.message);
                        return false;
                    }else{
                        swal("Deleted!", "Data deleted successfully", "success");
                        location.reload();
                        return false;
                    }
                }
            });
        }
    }).catch(swal.noop)
}


// Qualification

$(document).on("click", "#AddIndustries", function(){
    var name = $("#industry").val();

    if(name == ""){
        danger_msg("Industries should not be empty");
        $("#industry").focus();
        return false;
    }

    var AdminUsersData = {
        name: name
    }

    $.ajax({
        type: "POST",
        url: "/Admin/AddIndustries",
        data: AdminUsersData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                success_msg(data.message);
                location.reload();
                return false;
            }
        }
    });
});


function EditIndustries(id){
    $.ajax({
        type: "POST",
        url: "/Admin/GetIndustrybyId/"+id,
        dataType: "JSON",
        success: function (data) {
            $("#EditId").val(data.id);
            $("#EditIndustries").val(data.name);

            $("#EditIndustry").modal("show");
        }
    });
}



$(document).on("click", "#UpdateIndustries", function(){
    var id = $("#EditId").val();
    var name = $("#EditIndustries").val();

    if(name == ""){
        danger_msg("Qualification should not be empty");
        $("#EditIndustries").focus();
        return false;
    }

    var AdminUsersData = {
        id: id,
        name: name
    }

    $.ajax({
        type: "POST",
        url: "/Admin/UpdateIndustries",
        data: AdminUsersData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                success_msg(data.message);
                location.reload();
                return false;
            }
        }
    });
});

function DeleteIndustries(id){
    swal({
        title: 'Are you sure?',
        text: 'You won\'t be able to revert this!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then(function (isConfirm) {
        if (isConfirm) {
            $.ajax({
                type: "GET",
                url: "/Admin/DeleteIndustries/"+id,
                dataType: "JSON",
                success: function (data) {
                    if(data.error){
                        danger_msg(data.message);
                        return false;
                    }else{
                        swal("Deleted!", "Data deleted successfully", "success");
                        location.reload();
                        return false;
                    }
                }
            });
        }
    }).catch(swal.noop)
}



// Qualification

$(document).on("click", "#AddQualification", function(){
    var name = $("#qualification").val();

    if(name == ""){
        danger_msg("Qualification should not be empty");
        $("#qualification").focus();
        return false;
    }

    var AdminUsersData = {
        name: name
    }

    $.ajax({
        type: "POST",
        url: "/Admin/AddQualification",
        data: AdminUsersData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                success_msg(data.message);
                location.reload();
                return false;
            }
        }
    });
});


function EditQualification(id){
    $.ajax({
        type: "POST",
        url: "/Admin/GetQualificationbyId/"+id,
        dataType: "JSON",
        success: function (data) {
            $("#EditId").val(data.id);
            $("#EditQualifications").val(data.qualification);

            $("#EditQualification").modal("show");
        }
    });
}



$(document).on("click", "#UpdateQualification", function(){
    var id = $("#EditId").val();
    var name = $("#EditQualifications").val();

    if(name == ""){
        danger_msg("Qualification should not be empty");
        $("#EditQualifications").focus();
        return false;
    }

    var AdminUsersData = {
        id: id,
        name: name
    }

    $.ajax({
        type: "POST",
        url: "/Admin/UpdateQualification",
        data: AdminUsersData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                success_msg(data.message);
                location.reload();
                return false;
            }
        }
    });
});

function DeleteQualification(id){
    swal({
        title: 'Are you sure?',
        text: 'You won\'t be able to revert this!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then(function (isConfirm) {
        if (isConfirm) {
            $.ajax({
                type: "GET",
                url: "/Admin/DeleteQualification/"+id,
                dataType: "JSON",
                success: function (data) {
                    if(data.error){
                        danger_msg(data.message);
                        return false;
                    }else{
                        swal("Deleted!", "Data deleted successfully", "success");
                        location.reload();
                        return false;
                    }
                }
            });
        }
    }).catch(swal.noop)
}



// Functional Areas

$(document).on("click", "#AddFunctionalAreas", function(){
    var name = $("#functional_area").val();

    if(name == ""){
        danger_msg("Functional area should not be empty");
        $("#qualification").focus();
        return false;
    }

    var AdminUsersData = {
        name: name
    }

    $.ajax({
        type: "POST",
        url: "/Admin/AddFunctionalAreas",
        data: AdminUsersData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                success_msg(data.message);
                location.reload();
                return false;
            }
        }
    });
});


function EditFunctionalAreas(id){
    $.ajax({
        type: "POST",
        url: "/Admin/GetFunctionalAreasbyId/"+id,
        dataType: "JSON",
        success: function (data) {
            $("#EditId").val(data.id);
            $("#EditFunctionalAreas").val(data.functional_areas);

            $("#EditFunctionalAreasModel").modal("show");
        }
    });
}



$(document).on("click", "#UpdateFunctionalAreas", function(){
    var id = $("#EditId").val();
    var name = $("#EditFunctionalAreas").val();

    if(name == ""){
        danger_msg("FunctionalAreas should not be empty");
        $("#EditFunctionalAreas").focus();
        return false;
    }

    var AdminUsersData = {
        id: id,
        name: name
    }

    $.ajax({
        type: "POST",
        url: "/Admin/UpdateFunctionalAreas",
        data: AdminUsersData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                success_msg(data.message);
                location.reload();
                return false;
            }
        }
    });
});

function DeleteFunctionalAreas(id){
    swal({
        title: 'Are you sure?',
        text: 'You won\'t be able to revert this!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then(function (isConfirm) {
        if (isConfirm) {
            $.ajax({
                type: "GET",
                url: "/Admin/DeleteFunctionalAreas/"+id,
                dataType: "JSON",
                success: function (data) {
                    if(data.error){
                        danger_msg(data.message);
                        return false;
                    }else{
                        swal("Deleted!", "Data deleted successfully", "success");
                        location.reload();
                        return false;
                    }
                }
            });
        }
    }).catch(swal.noop)
}



// Training Completion

$(document).on("click", "#AddTrainingCompletion", function(){
    var name = $("#training_completion").val();

    if(name == ""){
        danger_msg("Training completion should not be empty");
        $("#training_completion").focus();
        return false;
    }

    var AdminUsersData = {
        name: name
    }

    $.ajax({
        type: "POST",
        url: "/Admin/AddTrainingCompletion",
        data: AdminUsersData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                success_msg(data.message);
                location.reload();
                return false;
            }
        }
    });
});


function EditTrainingCompletion(id){
    $.ajax({
        type: "POST",
        url: "/Admin/GetTrainingCompletionbyId/"+id,
        dataType: "JSON",
        success: function (data) {
            $("#EditId").val(data.id);
            $("#Editname").val(data.training_completion);

            $("#EditTrainingModel").modal("show");
        }
    });
}



$(document).on("click", "#UpdateTrainingCompletion", function(){
    var id = $("#EditId").val();
    var name = $("#Editname").val();

    if(name == ""){
        danger_msg("Training completion should not be empty");
        $("#Editname").focus();
        return false;
    }

    var AdminUsersData = {
        id: id,
        name: name
    }

    $.ajax({
        type: "POST",
        url: "/Admin/UpdateTrainingCompletion",
        data: AdminUsersData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                success_msg(data.message);
                location.reload();
                return false;
            }
        }
    });
});

function DeleteTrainingCompletion(id){
    swal({
        title: 'Are you sure?',
        text: 'You won\'t be able to revert this!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then(function (isConfirm) {
        if (isConfirm) {
            $.ajax({
                type: "GET",
                url: "/Admin/DeleteTrainingCompletion/"+id,
                dataType: "JSON",
                success: function (data) {
                    if(data.error){
                        danger_msg(data.message);
                        return false;
                    }else{
                        swal("Deleted!", "Data deleted successfully", "success");
                        location.reload();
                        return false;
                    }
                }
            });
        }
    }).catch(swal.noop)
}



// Career Level

$(document).on("click", "#AddCareerLevel", function(){
    var name = $("#career_level").val();

    if(name == ""){
        danger_msg("Career level should not be empty");
        $("#career_level").focus();
        return false;
    }

    var AdminUsersData = {
        name: name
    }

    $.ajax({
        type: "POST",
        url: "/Admin/AddCareerLevel",
        data: AdminUsersData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                success_msg(data.message);
                location.reload();
                return false;
            }
        }
    });
});


function EditCareerLevel(id){
    $.ajax({
        type: "POST",
        url: "/Admin/GetCareerLevelbyId/"+id,
        dataType: "JSON",
        success: function (data) {
            $("#EditId").val(data.id);
            $("#Editname").val(data.career_level);

            $("#EditCareerModel").modal("show");
        }
    });
}



$(document).on("click", "#UpdateCareerLevel", function(){
    var id = $("#EditId").val();
    var name = $("#Editname").val();

    if(name == ""){
        danger_msg("Career level should not be empty");
        $("#Editname").focus();
        return false;
    }

    var AdminUsersData = {
        id: id,
        name: name
    }

    $.ajax({
        type: "POST",
        url: "/Admin/UpdateCareerLevel",
        data: AdminUsersData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                success_msg(data.message);
                location.reload();
                return false;
            }
        }
    });
});

function DeleteCareerLevel(id){
    swal({
        title: 'Are you sure?',
        text: 'You won\'t be able to revert this!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then(function (isConfirm) {
        if (isConfirm) {
            $.ajax({
                type: "GET",
                url: "/Admin/DeleteCareerLevel/"+id,
                dataType: "JSON",
                success: function (data) {
                    if(data.error){
                        danger_msg(data.message);
                        return false;
                    }else{
                        swal("Deleted!", "Data deleted successfully", "success");
                        location.reload();
                        return false;
                    }
                }
            });
        }
    }).catch(swal.noop)
}



// Training Duration

$(document).on("click", "#AddTeamSize", function(){
    var name = $("#team_size").val();

    if(name == ""){
        danger_msg("Team size should not be empty");
        $("#team_size").focus();
        return false;
    }

    var AdminUsersData = {
        name: name
    }

    $.ajax({
        type: "POST",
        url: "/Admin/AddTeamSize",
        data: AdminUsersData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                success_msg(data.message);
                location.reload();
                return false;
            }
        }
    });
});


function EditTeamSize(id){
    $.ajax({
        type: "POST",
        url: "/Admin/GetTeamSizebyId/"+id,
        dataType: "JSON",
        success: function (data) {
            $("#EditId").val(data.id);
            $("#Editname").val(data.team_size);

            $("#EditTeamSizeModal").modal("show");
        }
    });
}



$(document).on("click", "#UpdateTeamSize", function(){
    var id = $("#EditId").val();
    var name = $("#Editname").val();

    if(name == ""){
        danger_msg("Team size should not be empty");
        $("#Editname").focus();
        return false;
    }

    var AdminUsersData = {
        id: id,
        name: name
    }

    $.ajax({
        type: "POST",
        url: "/Admin/UpdateTeamSize",
        data: AdminUsersData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                success_msg(data.message);
                location.reload();
                return false;
            }
        }
    });
});

function DeleteTeamSize(id){
    swal({
        title: 'Are you sure?',
        text: 'You won\'t be able to revert this!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then(function (isConfirm) {
        if (isConfirm) {
            $.ajax({
                type: "GET",
                url: "/Admin/DeleteTeamSize/"+id,
                dataType: "JSON",
                success: function (data) {
                    if(data.error){
                        danger_msg(data.message);
                        return false;
                    }else{
                        swal("Deleted!", "Data deleted successfully", "success");
                        location.reload();
                        return false;
                    }
                }
            });
        }
    }).catch(swal.noop)
}


// Marital status

$(document).on("click", "#AddMarital", function(){
    var name = $("#marital_status").val();

    if(name == ""){
        danger_msg("Marital status should not be empty");
        $("#marital_status").focus();
        return false;
    }

    var AdminUsersData = {
        name: name
    }

    $.ajax({
        type: "POST",
        url: "/Admin/AddMarital",
        data: AdminUsersData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                success_msg(data.message);
                location.reload();
                return false;
            }
        }
    });
});


function EditMarital(id){
    $.ajax({
        type: "POST",
        url: "/Admin/GetMaritalbyId/"+id,
        dataType: "JSON",
        success: function (data) {
            $("#EditId").val(data.id);
            $("#Editname").val(data.marital_status);

            $("#EditMaritalModal").modal("show");
        }
    });
}



$(document).on("click", "#UpdateMarital", function(){
    var id = $("#EditId").val();
    var name = $("#Editname").val();

    if(name == ""){
        danger_msg("Marital status should not be empty");
        $("#Editname").focus();
        return false;
    }

    var AdminUsersData = {
        id: id,
        name: name
    }

    $.ajax({
        type: "POST",
        url: "/Admin/UpdateMarital",
        data: AdminUsersData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                success_msg(data.message);
                location.reload();
                return false;
            }
        }
    });
});

function DeleteMarital(id){
    swal({
        title: 'Are you sure?',
        text: 'You won\'t be able to revert this!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then(function (isConfirm) {
        if (isConfirm) {
            $.ajax({
                type: "GET",
                url: "/Admin/DeleteMarital/"+id,
                dataType: "JSON",
                success: function (data) {
                    if(data.error){
                        danger_msg(data.message);
                        return false;
                    }else{
                        swal("Deleted!", "Data deleted successfully", "success");
                        location.reload();
                        return false;
                    }
                }
            });
        }
    }).catch(swal.noop)
}



// Experience Level

$(document).on("click", "#AddTrainingTitle", function(){
    var name = $("#title").val();

    if(name == ""){
        danger_msg("Training title should not be empty");
        $("#title").focus();
        return false;
    }

    var AdminUsersData = {
        name: name
    }

    $.ajax({
        type: "POST",
        url: "/Admin/AddTrainingTitle",
        data: AdminUsersData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                success_msg(data.message);
                location.reload();
                return false;
            }
        }
    });
});


function EditTrainingTitle(id){
    $.ajax({
        type: "POST",
        url: "/Admin/GetTrainingTitlebyId/"+id,
        dataType: "JSON",
        success: function (data) {
            $("#EditId").val(data.id);
            $("#Editname").val(data.title);

            $("#EditTrainingTitleModal").modal("show");
        }
    });
}



$(document).on("click", "#UpdateTrainingTitle", function(){
    var id = $("#EditId").val();
    var name = $("#Editname").val();

    if(name == ""){
        danger_msg("Training title should not be empty");
        $("#Editname").focus();
        return false;
    }

    var AdminUsersData = {
        id: id,
        name: name
    }

    $.ajax({
        type: "POST",
        url: "/Admin/UpdateTrainingTitle",
        data: AdminUsersData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                success_msg(data.message);
                location.reload();
                return false;
            }
        }
    });
});

function DeleteTrainingTitle(id){
    swal({
        title: 'Are you sure?',
        text: 'You won\'t be able to revert this!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then(function (isConfirm) {
        if (isConfirm) {
            $.ajax({
                type: "GET",
                url: "/Admin/DeleteTrainingTitle/"+id,
                dataType: "JSON",
                success: function (data) {
                    if(data.error){
                        danger_msg(data.message);
                        return false;
                    }else{
                        swal("Deleted!", "Data deleted successfully", "success");
                        location.reload();
                        return false;
                    }
                }
            });
        }
    }).catch(swal.noop)
}



// Languages

$(document).on("click", "#AddLanguages", function(){
    var name = $("#languages").val();

    if(name == ""){
        danger_msg("Languages should not be empty");
        $("#languages").focus();
        return false;
    }

    var AdminUsersData = {
        name: name
    }

    $.ajax({
        type: "POST",
        url: "/Admin/AddLanguages",
        data: AdminUsersData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                success_msg(data.message);
                location.reload();
                return false;
            }
        }
    });
});


function EditLanguages(id){
    $.ajax({
        type: "POST",
        url: "/Admin/GetLanguagesbyId/"+id,
        dataType: "JSON",
        success: function (data) {
            $("#EditId").val(data.id);
            $("#Editname").val(data.languages);

            $("#EditLanguagesModal").modal("show");
        }
    });
}



$(document).on("click", "#UpdateLanguages", function(){
    var id = $("#EditId").val();
    var name = $("#Editname").val();

    if(name == ""){
        danger_msg("Languages should not be empty");
        $("#Editname").focus();
        return false;
    }

    var AdminUsersData = {
        id: id,
        name: name
    }

    $.ajax({
        type: "POST",
        url: "/Admin/UpdateLanguages",
        data: AdminUsersData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                success_msg(data.message);
                location.reload();
                return false;
            }
        }
    });
});

function DeleteLanguages(id){
    swal({
        title: 'Are you sure?',
        text: 'You won\'t be able to revert this!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then(function (isConfirm) {
        if (isConfirm) {
            $.ajax({
                type: "GET",
                url: "/Admin/DeleteLanguages/"+id,
                dataType: "JSON",
                success: function (data) {
                    if(data.error){
                        danger_msg(data.message);
                        return false;
                    }else{
                        swal("Deleted!", "Data deleted successfully", "success");
                        location.reload();
                        return false;
                    }
                }
            });
        }
    }).catch(swal.noop)
}
