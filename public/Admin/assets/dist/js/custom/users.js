$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});



function ViewUserProfile(id){
    
    $.ajax({
        type: "POST",
        url: "/Admin/GetUserInfoById/"+id,
        // data: {},
        dataType: "html",
        success: function (data) {
            if(data.error){
                return false;
            }else{
                $("#GetUsersInfo").html(data);
                $.ajax({
                    type: "POST",
                    url: "/Admin/GetUserEducationInfoById/"+id,
                    // data: "data",
                    dataType: "html",
                    success: function (data) {
                        $("#EducationDetails").html(data);
                        // $("#AddUsers").modal("show");
                        // return false;
                    }
                });

                
                $.ajax({
                    type: "POST",
                    url: "/Admin/GetUserExperienceInfoById/"+id,
                    // data: "data",
                    dataType: "html",
                    success: function (data) {
                        $("#ExperienceDetails").html(data);
                        $("#AddUsers").modal("show");
                        return false;
                    }
                });
                
            }
        }
    });
}














function danger_msg(data){
    $.notify(data, {type:"danger"});
}

function success_msg(data){
    $.notify(data, {type:"success"});
}