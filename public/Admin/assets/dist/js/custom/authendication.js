$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$('#email').keypress(function(event){
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13'){
        var email = $("#email").val();
    var password = $("#password").val();

    var AuthendicationData = {
        email: email,
        password: password
    }

    $.ajax({
        type: "POST",
        url: "/AdminLogin",
        data: AuthendicationData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                if(data.type == 1){
                    success_msg(data.message);
                    window.location.href = "/Admin/Dashboard";
                    return false;
                }else if(data.type == 2){
                    success_msg(data.message);
                    alert("sub admin");
                    return false;
                    // window.location.href = "/Admin/Dashboard";
                    // return false;
                }else if(data.type == 3){
                    success_msg(data.message);
                    // alert("Faculty");
                    // return false;
                    window.location.href = "/Faculty/Dashboard";
                    return false;
                }
            }
        }
    }); 
    }
});


$('#password').keypress(function(event){
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13'){
        var email = $("#email").val();
    var password = $("#password").val();

    var AuthendicationData = {
        email: email,
        password: password
    }

    $.ajax({
        type: "POST",
        url: "/AdminLogin",
        data: AuthendicationData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                if(data.type == 1){
                    success_msg(data.message);
                    window.location.href = "/Admin/Dashboard";
                    return false;
                }else if(data.type == 2){
                    success_msg(data.message);
                    alert("sub admin");
                    return false;
                    // window.location.href = "/Admin/Dashboard";
                    // return false;
                }else if(data.type == 3){
                    success_msg(data.message);
                    // alert("Faculty");
                    // return false;
                    window.location.href = "/Faculty/Dashboard";
                    return false;
                }
            }
        }
    }); 
    }
});



$(document).on("click", "#AdminLogin", function(){
    var email = $("#email").val();
    var password = $("#password").val();

    var AuthendicationData = {
        email: email,
        password: password
    }

    $.ajax({
        type: "POST",
        url: "/AdminLogin",
        data: AuthendicationData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                if(data.type == 1){
                    success_msg(data.message);
                    window.location.href = "/Admin/Dashboard";
                    return false;
                }else if(data.type == 2){
                    success_msg(data.message);
                    alert("sub admin");
                    return false;
                    // window.location.href = "/Admin/Dashboard";
                    // return false;
                }else if(data.type == 3){
                    success_msg(data.message);
                    // alert("Faculty");
                    // return false;
                    window.location.href = "/Faculty/Dashboard";
                    return false;
                }
            }
        }
    });
});





$('#institute_email').keypress(function(event){
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13'){
        var email = $("#institute_email").val();
    var password = $("#institute_password").val();

    var AuthendicationData = {
        email: email,
        password: password
    }

    $.ajax({
        type: "POST",
        url: "/InstituteLogin",
        data: AuthendicationData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                if(data.type == 4){
                    success_msg(data.message);
                    window.location.href = "/Institute/CandidateList";
                    return false;
                }
            }
        }
    });
    }
});


$('#institute_password').keypress(function(event){
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13'){
        var email = $("#institute_email").val();
    var password = $("#institute_password").val();

    var AuthendicationData = {
        email: email,
        password: password
    }

    $.ajax({
        type: "POST",
        url: "/InstituteLogin",
        data: AuthendicationData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                if(data.type == 4){
                    success_msg(data.message);
                    window.location.href = "/Institute/CandidateList";
                    return false;
                }
            }
        }
    });
    }
});




$(document).on("click", "#InstituteLogin", function(){
    var email = $("#institute_email").val();
    var password = $("#institute_password").val();

    var AuthendicationData = {
        email: email,
        password: password
    }

    $.ajax({
        type: "POST",
        url: "/InstituteLogin",
        data: AuthendicationData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                if(data.type == 4){
                    success_msg(data.message);
                    window.location.href = "/Institute/CandidateList";
                    return false;
                }
            }
        }
    });
});



function danger_msg(data){
    $.notify(data, {type:"danger"});
}

function success_msg(data){
    $.notify(data, {type:"success"});
}