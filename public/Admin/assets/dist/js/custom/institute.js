$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});


$(document).on("click", "#AddInstitute", function(){
    var name = $("#name").val();
    var password = $("#password").val();
    var email = $("#email").val();
    var mobile = $("#mobile").val();
    var address = $("#address").val();

    if(name == ""){
        danger_msg("Name should not be empty");
        $("#name").focus();
        return false;
    }

    if(password == ""){
        danger_msg("Password should not be empty");
        $("#password").focus();
        return false;
    }

    if(email == ""){
        danger_msg("Email should not be empty");
        $("#email").focus();
        return false;
    }


    if(mobile == ""){
        danger_msg("Mobile should not be empty");
        $("#mobile").focus();
        return false;
    }

    if(address == ""){
        danger_msg("Address should not be empty");
        $("#address").focus();
        return false;
    }

    var InstituteData = {
        name: name,
        email: email,
        password: password,
        mobile: mobile,
        address: address
    }

    $.ajax({
        type: "POST",
        url: "/AddInstitute",
        data: InstituteData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                success_msg(data.message);
                location.reload();
                return false;
            }
        }
    });
});


function EditInstitute(id){
    
    $.ajax({
        type: "POST",
        url: "/GetInstitute/"+id,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                $("#EditId").val(data.id);
                $("#Editname").val(data.name);
                $("#Editemail").val(data.email);
                $("#Editmobile").val(data.mobile);
                $("#Editaddress").val(data.address);
                // $("#Editrole").val(data.role_id);
                $("#Editpassword").val(data.password);

                $("#EditInstitute").modal("show");
            }
        }
    });
}


$(document).on("click", "#UpdateInstitute", function(){
    var id = $("#EditId").val();
    var name = $("#Editname").val();
    var password = $("#Editpassword").val();
    var email = $("#Editemail").val();
    var mobile = $("#Editmobile").val();
    var address = $("#Editaddress").val();
    

    if(name == ""){
        danger_msg("Name should not be empty");
        $("#Editname").focus();
        return false;
    }

    // if(password == ""){
    //     danger_msg("Password should not be empty");
    //     $("#Editpassword").focus();
    //     return false;
    // }

    if(email == ""){
        danger_msg("Email should not be empty");
        $("#Editemail").focus();
        return false;
    }

    if(mobile == ""){
        danger_msg("Mobile should not be empty");
        $("#Editmobile").focus();
        return false;
    }

    if(address == ""){
        danger_msg("Address should not be empty");
        $("#Editaddress").focus();
        return false;
    }
    

    var InstituteData = {
        id: id,
        name: name,
        password: password,
        email: email,
        mobile: mobile,
        address: address
        
    }

    $.ajax({
        type: "POST",
        url: "/UpdateInstitute",
        data: InstituteData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                success_msg(data.message);
                location.reload();
                return false;
            }
        }
    });
});

function DeleteInstitute(id){
    swal({
        title: 'Are you sure?',
        text: 'You won\'t be able to revert this!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then(function (isConfirm) {
        if (isConfirm) {
            $.ajax({
                type: "GET",
                url: "/Admin/DeleteInstitute/"+id,
                dataType: "JSON",
                success: function (data) {
                    if(data.error){
                        danger_msg(data.message);
                        return false;
                    }else{
                        swal("Deleted!", "Data deleted successfully", "success");
                        location.reload();
                        return false;
                    }
                }
            });
        }
    }).catch(swal.noop)
}




function InstituteForgotPassword(){
    $("#LoginForm").hide();
    $("#ForgotPassword").show();
}

function BacktoLogin(){
    $("#LoginForm").show();
    $("#ForgotPassword").hide();
}


$(document).on('click', '#ForgotPasswordBtn', function(){
    var forgotEmail = $("#forgot_email").val();

    var ForgotEmailData = {
        forgotEmail: forgotEmail
    }

    $.ajax({
        type: "POST",
        url: "/InstituteForgotPassword",
        data: ForgotEmailData,
        dataType: "JSON",
        beforeSend: function(){
            $('#ForgotPasswordBtn').attr("disabled", true);
            // return false;
        },
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                success_msg(data.message);
                return false;
            }
        },
        complete:function(data){
            $('#ForgotPasswordBtn').attr("disabled", false);
        }
    });
});


$(document).on("click", "#ResetPassword", function(){
    var id = $("#id").val();
    var new_password = $("#new_password").val();
    var confirm_password = $("#confirm_password").val();

    if(new_password == ""){
        danger_msg("Please type new password");
        $("#new_password").focus();
        return false;
    }
    if(confirm_password == ""){
        danger_msg("Please type confirm password");
        $("#confirm_password").focus();
        return false;
    }


    var CandidateData = {
        id: id,
        new_password: new_password,
        confirm_password: confirm_password
    }

    $.ajax({
        type: "POST",
        url: "/Institute/ResetPassword",
        data: CandidateData,
        dataType: "JSON",
        beforeSend: function(){
            $('#ResetPassword').attr("disabled", true);
            // return false;
        },
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                success_msg(data.message);
                return false;
            }
        },
        complete:function(data){
            $('#ResetPassword').attr("disabled", false);
        }
    });
});