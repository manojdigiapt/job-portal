$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});




$(document).on("click", "#AddOrUpdateTraineeProfile", function(){
    // var fullname = $("#fullname").val();
    // var id = $("#id").val();
    var name = $("#name").val();
    var allow_search = $("#allow_search").val();
    var gender = $("#gender :selected").val();
    var aadhar = $("#aadhar").val();
    var pan = $("#pan").val();
    var facebook = $("#facebook").val();
    var twitter = $("#twitter").val();
    var googleplus = $("#googleplus").val();
    var linkedin = $("#linkedin").val();
    var mobile = $("#mobile").val();
    var email = $("#email").val();
    var address = $("#address").val();
    var zipcode = $("#zipcode").val();
    var country = $("#country").val();
    var state = $("#state").val();
    var city = $("#city").val();


    if(name == ""){
        danger_msg("First name should not empty");
        $("#name").focus();
        return false;
    }

    if(gender == "Please select gender"){
        danger_msg("Gender should not empty");
        $("#gender").focus();
        return false;
    }

    if(aadhar == ""){
        danger_msg("Aadhar no should not empty");
        $("#aadhar").focus();
        return false;
    }

    if(pan == ""){
        danger_msg("Pan number should not empty");
        $("#pan").focus();
        return false;
    }else if(!validatePAN(pan)){
        danger_msg("Invalid Pan Number");
        $("#pan").focus();
        return false;
    }

    if(mobile == ""){
        danger_msg("Mobile no should not empty");
        $("#mobile").focus();
        return false;
    }

    if(email == ""){
        danger_msg("Email should not empty");
        $("#email").focus();
        return false;
    }

    if(address == ""){
        danger_msg("Address should not empty");
        $("#address").focus();
        return false;
    }

    if(zipcode == ""){
        danger_msg("Zipcode should not empty");
        $("#zipcode").focus();
        return false;
    }

    if(country == ""){
        danger_msg("Country should not empty");
        $("#country").focus();
        return false;
    }

    if(state == ""){
        danger_msg("State should not empty");
        $("#state").focus();
        return false;
    }

    if(city == ""){
        danger_msg("City should not empty");
        $("#city").focus();
        return false;
    }


    var TraineeProfileData = new FormData();

    // TraineeProfileData.append('id', id);
    TraineeProfileData.append('name', name);
    TraineeProfileData.append('allow_search', allow_search);
    TraineeProfileData.append('gender', gender);
    TraineeProfileData.append('aadhar', aadhar);
    TraineeProfileData.append('pan', pan);
    TraineeProfileData.append('facebook', facebook);
    TraineeProfileData.append('twitter', twitter);
    TraineeProfileData.append('linkedin', linkedin);
    TraineeProfileData.append('googleplus', googleplus);
    TraineeProfileData.append('mobile', mobile);
    TraineeProfileData.append('email', email);
    TraineeProfileData.append('address', address);
    TraineeProfileData.append('zipcode', zipcode);
    TraineeProfileData.append('country', country);
    TraineeProfileData.append('state', state);
    TraineeProfileData.append('city', city);
    TraineeProfileData.append('_token', $("input[name=_token]").val());
    TraineeProfileData.append('file', $('#file')[0].files[0]);

    $.ajax({
        type: "POST",
        url: "/Trainee/AddOrUpdateTrainee",
        data: TraineeProfileData,
        dataType: "JSON",
        contentType: false,
        processData: false,
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return FALSE;
            }else{
                success_msg(data.message);
                return FALSE;
            }
        }
    });
});


$(document).on("click", "#SelectForTraining", function(){
    var start_date =$("#start_date").val();
    var CandidateId =$("#CandidateId").val();
    var TrainingId =$("#TrainingId").val();

    var SelectedTrainingData = {
        start_date: start_date,
        CandidateId: CandidateId,
        TrainingId: TrainingId
    }

    $.ajax({
        type: "POST",
        url: "/SelectTraineeForTraining",
        data: SelectedTrainingData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                success_msg(data.message);
                location.reload();
                return false;
            }
        }
    });
});



// Add to favourite trainings
$(function() {
    $(".training-heart").on("click", function() {
        var TrainingId = $("#TrainingId").val();
        if($(this).toggleClass("is-active")){
            $.ajax({
                type: "POST",
                url: "/AddToTrainingFavourite",
                data: {TrainingId: TrainingId},
                dataType: "JSON",
                success: function (data) {
                    if(data.error){
                        console.log(data);
                        return false;
                    }else{
                        console.log(data);
                        return false;
                    }
                }
            });
        }
    //   $(this).toggleClass("is-active");
    });
  });



// Aadhar Validation
$('[data-type="adhaar-number"]').keyup(function() {
    var value = $(this).val();
    value = value.replace(/\D/g, "").split(/(?:([\d]{4}))/g).filter(s => s.length > 0).join("-");
    $(this).val(value);
  });

  $('[data-type="adhaar-number"]').on("change, blur", function() {
    var value = $(this).val();
    var maxLength = $(this).attr("maxLength");
    if (value.length != maxLength) {
      $(this).addClass("highlight-error");
    } else {
      $(this).removeClass("highlight-error");
    }
  });



  $(document).on("click", "#LoadmoreTrainings", function(){
    var TrainingId = $(this).data('id');


    $.ajax({
        type: "POST",
        url: "/LoadmoreTrainings",
        data: {TrainingId: TrainingId},
        dataType: "text",
        beforeSend: function(){
            $("#ApplyBtnLoader").show();
        },
        success: function (data) {
            if(data != ''){
                console.log(data);
                $('#remove_training_load_more').remove();
                $("#SearchTrainingResults").append(data);
            }else{
                console.log(data);
                $("#LoadmoreTrainings").html("No trainings are found...");
                return false;
            }
        },
        complete:function(data){
            $("#ApplyBtnLoader").hide();
        }
    });
    
});


$(document).on("click", "#UpdateTraineePassword", function(){
    var OldPassword = $("#old_password").val();
    var NewPassword = $("#new_password").val();
    var VerifyPassword = $("#verify_password").val();

    if(OldPassword == ""){
        danger_toast_msg("Old password should not empty");
        $("#old_password").focus();
        return false;
    }

    if(NewPassword == ""){
        danger_toast_msg("New password should not empty");
        $("#new_password").focus();
        return false;
    }

    if(VerifyPassword == ""){
        danger_toast_msg("Verify password should not empty");
        $("#verify_password").focus();
        return false;
    }


    var CandidatePassword = {
        OldPassword: OldPassword,
        NewPassword: NewPassword,
        VerifyPassword: VerifyPassword
    }

    $.ajax({
        type: "POST",
        url: "/UpdateTraineePassword",
        data: CandidatePassword,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message);
                return false;
            }else{
                success_toast_msg(data.message);
                return false;
            }
        }
    });

});


/* Edit Candidate Profile  Popup */
$('.EditTraineeProfile').on('click', function(){
    $('.edit_trainee-profile-popup-box').fadeIn('fast');
    $('html').addClass('no-scroll');

    var id = $("#pro_id").val();

    $.ajax({
        type: "POST",
        url: "/Candidate/GetPersonalDetailsById/"+id,
        data: {id: id},
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                $("#Edit_experience option[value="+data.experience+"]").attr('selected', 'selected');

                $("#Edit_current_salary option[value="+data.gross_salary+"]").attr('selected', 'selected');

                $("#Edit_desired_location").val(data.desired_location);
                return false;
            }
        }
    });

});
$('.close-popup').on('click', function(){
    $('.edit_trainee-profile-popup-box').fadeOut('fast');
    $('html').removeClass('no-scroll');
});



function BrowseTraineePic(elemId) {
    var elem = document.getElementById(elemId);
    if(elem && document.createEvent) {
       var evt = document.createEvent("MouseEvents");
       evt.initEvent("click", true, false);
       elem.dispatchEvent(evt);
    }
    // readURL(this);
 }



 function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      
      reader.onload = function(e) {
        $('#TraineeProfilePic').attr('src', e.target.result);
        $('.profile-head-width').attr('src', e.target.result);
      }
      
      reader.readAsDataURL(input.files[0]);
    }
  }
  
  $("#trainee_file").change(function() {
    readURL(this);

    var CandidateProfileData = new FormData();
    CandidateProfileData.append('file', $('#trainee_file')[0].files[0]);

    $.ajax({
        type: "POST",
        url: "/Trainee/UpdateProfilePic",
        data: CandidateProfileData,
        dataType: "JSON",
        contentType: false,
        processData: false,
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message);
                return false;
            }else{
                success_toast_msg(data.message);
                location.reload();
                return false;
            }
        }
    });
  });



  // Update Trainee Profile
$(document).on("click", "#UpdateTraineeProfile", function(){
    // var fullname = $("#fullname").val();
    var id = $("#id").val();
    var name = $("#fullname").val();
    // var experience = $("#Edit_experience :selected").val();
    // var current_salary = $("#Edit_current_salary :selected").val();
    var desired_location = $("#Edit_desired_location").val();
    

    // if(experience == "Select Experience"){
    //     danger_toast_msg("Experience should not empty");
    //     $("#Edit_experience").focus();
    //     return false;
    // }

    // if(current_salary == "Please Select"){
    //     danger_toast_msg("Current salary should not empty");
    //     $("#Edit_current_salary").focus();
    //     return false;
    // }


    if(desired_location == ""){
        danger_toast_msg("Desired location should not empty");
        $("#Edit_desired_location").focus();
        return false;
    }

   
    var CandidateProfileData = {
        // name: name,
        // experience: experience,
        // current_salary: current_salary,
        desired_location: desired_location
    }

    $.ajax({
        type: "POST",
        url: "/Trainee/AddTraineeProfile",
        data: CandidateProfileData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message);
                return false;
            }else{
                success_toast_msg(data.message);
                location.reload();
                return false;
            }
        }
    });
});



function InsertOrUpdateTraineeCV(){
    var cv_id = $("#trainee_cv_id").val();
    var resume_title = CKEDITOR.instances['description'].getData();
    // var percentage = $("#percentage").val();

    var CVData = {
        cv_id: cv_id,
        resume_title: resume_title
    }
    

    $.ajax({
        type: "POST",
        url: "/Trainee/InsertOrUpdateCVTitle",
        data: CVData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message);
                return false;
            }else{
                success_toast_msg(data.message);
                return false;
            }
        }
    });
}


// Ecuation Details

$(document).on("click", "#AddTraineeEducation", function(){
    var board = $("#board").val();
    if((board == 1) || (board == 2)){
        var course_name = $("#board").val();
    }else{
        var course_name = $("#course_name").val();
    }

    var school_clg = $("#school_clg").val();
    var passed_out_year = $("#passed_out_year").val();
    var percentage = $("#percentage").val();
    
    // if(university == ""){
    //     danger_msg("University should not empty");
    //     $("#university").focus();
    //     return false;
        
    // }

    // if(school_clg == ""){
    //     danger_msg("School/Clg should not empty");
    //     $("#school_clg").focus();
    //     return false;
    // }

    // if(department == ""){
    //     danger_msg("Department should not empty");
    //     $("#department").focus();
    //     return false;
    // }

    // if(from_date == ""){
    //     danger_msg("From date should not empty");
    //     $("#from_date").focus();
    //     return false;
    // }

    // if(to_date == ""){
    //     danger_msg("To date should not empty");
    //     $("#to_date").focus();
    //     return false;
    // }

    var EducationData = {
        board: board,
        course_name: course_name,
        school_clg: school_clg,
        passed_out_year: passed_out_year,
        percentage: percentage
    }

    $.ajax({
        type: "POST",
        url: "/Trainee/InsertEducation",
        data: EducationData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message);
                return false;
            }else{
                success_toast_msg(data.message);
                location.reload();
                // return false;
            }
        }
    });
});

function EditTraineeEducation(id){
    $.ajax({
        type: "POST",
        url: "/Candidate/GetEducationById/"+id,
        data: {id: id},
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                $("#edu_id").val(data.id);

                if((data.title == 1) || (data.title == 2)){
                    $("#Edit_CourseNameRow").hide();
                }else{
                    $("#Edit_CourseNameRow").show();
                }
                $("#Edit_board option[value="+data.department+"]").attr('selected', 'selected');

                $("#edit_course_name").val(data.title);

                $("#Edit_school_clg").val(data.name);

                $("#Edit_passed_out_year option[value="+data.from_year+"]").attr('selected', 'selected');
                $("#Edit_percentage").val(data.percentage);

                $("#EditEducationModal").modal("show");
                return false;
            }
        }
    });
}

$(document).on("click", "#UpdateTraineeEducation", function(){
    var id = $("#edu_id").val();
    var course_name = $("#Edit_board").val();
    // if((board == 1) || (board == 2)){
    //     var course_name = $("#Edit_board").val();
    // }else{
    //     var course_name = $("#edit_course_name").val();
    // }

    var school_clg = $("#Edit_school_clg").val();
    var passed_out_year = $("#Edit_passed_out_year").val();
    var percentage = $("#Edit_percentage").val();
    
    // if(university == ""){
    //     danger_msg("University should not empty");
    //     $("#university").focus();
    //     return false;
        
    // }

    // if(school_clg == ""){
    //     danger_msg("School/Clg should not empty");
    //     $("#school_clg").focus();
    //     return false;
    // }

    // if(department == ""){
    //     danger_msg("Department should not empty");
    //     $("#department").focus();
    //     return false;
    // }

    // if(from_date == ""){
    //     danger_msg("From date should not empty");
    //     $("#from_date").focus();
    //     return false;
    // }

    // if(to_date == ""){
    //     danger_msg("To date should not empty");
    //     $("#to_date").focus();
    //     return false;
    // }

    var EducationData = {
        id: id,
        course_name: course_name,
        // course_name: course_name,
        school_clg: school_clg,
        passed_out_year: passed_out_year,
        percentage: percentage
    }

    $.ajax({
        type: "POST",
        url: "/Candidate/UpdateEducation",
        data: EducationData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message);
                return false;
            }else{
                success_toast_msg(data.message);
                location.reload();
                // return false;
            }
        }
    });
});

$(document).on("click", "#UpdateTrineePersonalDetails", function(){
    var dob = $("#Edit_dob").val();
    var gender = $("#Edit_gender").val();
    var marital_status = $("#Edit_marital_status").val();
    var father_name = $("#Edit_father_name").val();
    var address = $("#Edit_address").val();
    var state = $("#Edit_state").val();
    var city = $("#Edit_city").val();
    var zipcode = $("#Edit_zipcode").val();

    var PersonalDetails = {
        dob: dob,
        gender: gender,
        marital_status: marital_status,
        father_name: father_name,
        address: address,
        state: state,
        city: city,
        zipcode: zipcode
    }

    $.ajax({
        type: "POST",
        url: "/Trainee/UpdatePersonalDetails",
        data: PersonalDetails,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message);
                return false;
            }else{
                success_toast_msg(data.message);
                location.reload();
                return false;
            }
        }
    });

});

// DownloadCV
function DownloadTraineeCV(id, JobsId){
    $.ajax({
        type: "POST",
        url: "/DownloadTraineeCV/"+id+"/"+JobsId,
        data: {id: id},
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message);
                return false;
            }else{
                // success_toast_msg(data.message);
                window.open($("#DownloadCVPath").text(), '_blank')
                // return false;
            }
        }
    });
}








$(document).on("change", ".CheckDatePostedTrainingFilters", function(){
    // function CheckJobFilters(){
        var date_posted = $(this).val();
    
    
        var FiltersData = {
            date_posted: date_posted
        }
    
        $.ajax({
            type: "POST",
            url: "/FilterDatePostedTraining",
            data: FiltersData,
            dataType: "html",
            beforeSend: function(){
                // Show image container
                $("#AjaxLoader").show();
                $("#SearchTrainingResults").hide();
                // return false;
            },
            success: function (data) {
                if(data){
                    $("#SearchTrainingResults").html("");
                    $("#SearchTrainingResults").append(data);
                    
                    // $("#JobsCount").text(data.length)/1000;
                    return false;
                }else{
                    // $("#JobsCount").text('0');
                    $("#SearchTrainingResults").html("No results found...");
                    return false;
                }
            },
            complete:function(data){
            // Hide image container
                $("#AjaxLoader").hide();
                $("#SearchTrainingResults").show();
            }
        });
    // }
    });


    $(document).on("change", ".CheckDatePostedFresherJobsFilters", function(){
        // function CheckJobFilters(){
            var date_posted = $(this).val();
        
        
            var FiltersData = {
                date_posted: date_posted
            }
        
            $.ajax({
                type: "POST",
                url: "/FilterDatePostedFreshJobs",
                data: FiltersData,
                dataType: "html",
                beforeSend: function(){
                    // Show image container
                    $("#AjaxLoader").show();
                    $("#SearchCAFresherJobsResults").hide();
                    // return false;
                },
                success: function (data) {
                    if(data){
                        $("#SearchCAFresherJobsResults").html("");
                        $("#SearchCAFresherJobsResults").append(data);
                        
                        // $("#JobsCount").text(data.length)/1000;
                        return false;
                    }else{
                        // $("#JobsCount").text('0');
                        $("#SearchCAFresherJobsResults").html("No results found...");
                        return false;
                    }
                },
                complete:function(data){
                // Hide image container
                    $("#AjaxLoader").hide();
                    $("#SearchCAFresherJobsResults").show();
                }
            });
        // }
        });


    $(document).on("click", "#ClearAllTrainings", function(){
        $('input[type="checkbox"]').removeAttr('checked');
        $('#AppliedCount').text(1);
    
    
    
        $.ajax({
            type: "POST",
            url: "/ClearAllTrainings",
            dataType: "html",
            beforeSend: function(){
                // Show image container
                $("#AjaxLoader").show();
                $("#SearchTrainingResults").hide();
                // return false;
            },
            success: function (data) {
                if(data.error){
                    console.log(data);
                    return false;
                }else{
                    $("#SearchTrainingResults").html("");
                    $("#SearchTrainingResults").append(data);
                    return false;
                }
            },
            complete:function(data){
            // Hide image container
            $("#AjaxLoader").hide();
            $("#SearchTrainingResults").show();
            }
        });
    });
    

    function ChangeTraineeApplliedTrainingStatus(GetId, GetStatusId){
        var TrainingId = $("#TrainingId").val();

        var EmpId = $("#EmpId").val();

        // var EndDate = $("#training_end_date").val();
        $.ajax({
            type: "POST",
            url: "/ChangeAppliedTraineeTrainingStatus",
            data: {EmpId: EmpId, TrainingId: TrainingId, GetId: GetId, GetStatusId: GetStatusId},
            dataType: "JSON",
            success: function (data) {
                if(data.error){
                    danger_toast_msg(data.message);
                    location.reload();
                    return false;
                }else{
                    // $("#JobsCount").text('0');
                    success_toast_msg(data.message);
                    location.reload();
                    return false;
                }
            }
        });
    }


    function ChangeTrainingEndDate(){
        var TrainingId = $("#TrainingId").val();

        var GetId = $("#GetCandidateId").val();

        var EndDate = $("#training_end_date").val();
        $.ajax({
            type: "POST",
            url: "/ChangeTrainingEndDate",
            data: {TrainingId: TrainingId, GetId: GetId, EndDate: EndDate},
            dataType: "JSON",
            success: function (data) {
                if(data.error){
                    danger_toast_msg(data.message);
                    location.reload();
                    return false;
                }else{
                    // $("#JobsCount").text('0');
                    success_toast_msg(data.message);
                    location.reload();
                    return false;
                }
            }
        });
    }



// First Register Candidate
function BrowseFirstRegisterTraineeCV(elemId) {
    var elem = document.getElementById(elemId);
    if(elem && document.createEvent) {
       var evt = document.createEvent("MouseEvents");
       evt.initEvent("click", true, false);
       elem.dispatchEvent(evt);
    }

    // readURL(this);
 }

// $(document).on("click", ".Next_Trainee_Btn_Resume", function(){

$("#first_cv").change(function() {

    var cv_id = $("#cv_id").val();
    var CVData  = new FormData();

    CVData.append('cv_id', cv_id);
    CVData.append('cv', $('#first_cv')[0].files[0]);

    // console.log(CVData);
    // return false;
    $.ajax({
        type: "POST",
        url: "/Trainee/InsertCV",
        data: CVData,
        dataType: "JSON",
        contentType: false,
        processData: false,
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message);
                return false;
            }else{
                success_toast_msg(data.message);
                return false;
            }
        }
    });
 });


 $(document).on("click", ".AddTraineeEducation", function(){
    var board = $("#board").val();
    if((board == 1) || (board == 2)){
        var course_name = $("#board").val();
    }else{
        var course_name = $("#course_name").val();
    }

    var school_clg = $("#school_clg").val();
    var passed_out_year = $("#passed_out_year").val();
    var percentage = $("#percentage").val();
    
    // if(university == ""){
    //     danger_msg("University should not empty");
    //     $("#university").focus();
    //     return false;
        
    // }

    // if(school_clg == ""){
    //     danger_msg("School/Clg should not empty");
    //     $("#school_clg").focus();
    //     return false;
    // }

    // if(department == ""){
    //     danger_msg("Department should not empty");
    //     $("#department").focus();
    //     return false;
    // }

    // if(from_date == ""){
    //     danger_msg("From date should not empty");
    //     $("#from_date").focus();
    //     return false;
    // }

    // if(to_date == ""){
    //     danger_msg("To date should not empty");
    //     $("#to_date").focus();
    //     return false;
    // }

    var EducationData = {
        board: board,
        course_name: course_name,
        school_clg: school_clg,
        passed_out_year: passed_out_year,
        percentage: percentage
    }

    $.ajax({
        type: "POST",
        url: "/Trainee/InsertEducation",
        data: EducationData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message);
                return false;
            }else{
                success_toast_msg(data.message);
                $("#EducationDetails").load(location.href + " #EducationDetails");
                return false;
            }
        }
    });
});


$(document).on("click", ".UpdateTrineePersonalDetails", function(){
    var dob = $("#Edit_dob").val();
    var gender = $("#Edit_gender").val();
    var marital_status = $("#Edit_marital_status").val();
    var father_name = $("#Edit_father_name").val();
    var address = $("#Edit_address").val();
    var state = $("#Edit_state").val();
    var city = $("#Edit_city").val();
    var zipcode = $("#Edit_zipcode").val();

    var PersonalDetails = {
        dob: dob,
        gender: gender,
        marital_status: marital_status,
        father_name: father_name,
        address: address,
        state: state,
        city: city,
        zipcode: zipcode
    }

    $.ajax({
        type: "POST",
        url: "/Trainee/UpdatePersonalDetails",
        data: PersonalDetails,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message);
                return false;
            }else{
                success_toast_msg(data.message);
                window.location.href="/Trainee/Dashboard";
                return false;
            }
        }
    });

});







function validatePAN(pan){ 
    var regex = /[A-Z]{5}[0-9]{4}[A-Z]{1}$/; 
    return regex.test(pan); 
}


function success_msg(data){
    $.notify({
        title: '<strong>'+ data +'</strong>',
        message: ''
    },{
        type: 'success'
    });
}

function danger_msg(data){
    $.notify({
        title: '<strong>'+ data +'</strong>',
        message: ''
    },{
        type: 'danger'
    });
}
