// HOME_URL = "/";
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});



function CheckStipend(){
    if($("#stipend_type").val() == 1){
        // $("#exp_show").show();
        $("#ShowStipend").show();
        $("#SalaryShow").hide();
        return false;
    }else{
        $("#ShowStipend").hide();
        $("#SalaryShow").show();
        return false;
    }
}


function CheckEditStipend(){
    if($("#Edit_stipend_type").val() == 1){
        // $("#exp_show").show();
        // $("#ShowStipend").show();
        $("#Edit_SalaryShow").hide();
        return false;
    }else{
        // $("#ShowStipend").hide();
        $("#Edit_SalaryShow").show();
        return false;
    }
}





$("#Ca_salary_min").keypress(function (e) {
    //if the letter is not digit then display error and don't type anything
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
       //display error message
    //    $("#errmsg").html("Digits Only").show().fadeOut("slow");
              return false;
   }
  });

  $("#Ca_salary_max").keypress(function (e) {
    //if the letter is not digit then display error and don't type anything
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
       //display error message
    //    $("#errmsg").html("Digits Only").show().fadeOut("slow");
              return false;
   }
  });

  $("#percentage").keypress(function (e) {
    //if the letter is not digit then display error and don't type anything
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
       //display error message
    //    $("#errmsg").html("Digits Only").show().fadeOut("slow");
              return false;
   }
  });


  $("#Edit_percentage").keypress(function (e) {
    //if the letter is not digit then display error and don't type anything
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
       //display error message
    //    $("#errmsg").html("Digits Only").show().fadeOut("slow");
              return false;
   }
  });


  $("#notice_period").keypress(function (e) {
    //if the letter is not digit then display error and don't type anything
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
       //display error message
    //    $("#errmsg").html("Digits Only").show().fadeOut("slow");
              return false;
   }
  });

  $("#Edit_notice_period").keypress(function (e) {
    //if the letter is not digit then display error and don't type anything
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
       //display error message
    //    $("#errmsg").html("Digits Only").show().fadeOut("slow");
              return false;
   }
  });



// CA Jobs
$(document).on("click", "#PostCAJobs", function(){
    var description = $("#Ca_description").val();
    var qualification = $("#Ca_qualification").val();
    var stipend_type = $("#stipend_type").val();
    var stipend = $("#Ca_stipend").val();
    var address = $("#Ca_Edit_address").val();
    var zipcode = $("#Ca_Edit_zipcode").val();
    var country = $("#Ca_Edit_country").val();
    var state = $("#Ca_Edit_state").val();
    var city = $("#Ca_Edit_city").val();
    var salary_min = $("#Ca_salary_min").val();
    var salary_max = $("#Ca_salary_max").val();
    // var offered_salary = min_salary+ "-" + max_salary;
    var salary_type = $("#Ca_salary_type").val();
    var positions = $("#Ca_positions").val();
    // var description = CKEDITOR.instances['description'].getData();

    if(description == ""){
        danger_toast_msg("Description should not empty");
        $("#Ca_description").focus();
        return false;
    }

    if(qualification == ""){
        danger_toast_msg("Qualification should not empty");
        $("#Ca_qualification").focus();
        return false;
    }

    if(stipend_type == "2"){
        if(salary_min == ""){
            danger_toast_msg("Salary min should not empty");
            $("#Ca_salary_min").focus();
            return false;
        }
    
        if(salary_max == ""){
            danger_toast_msg("Salary max should not empty");
            $("#Ca_salary_max").focus();
            return false;
        }
    }

    if(address == ""){
        danger_toast_msg("Area should not empty");
        $("#Ca_Edit_address").focus();
        return false;
    }


    if(zipcode == ""){
        danger_toast_msg("Zipcode should not empty");
        $("#Ca_Edit_zipcode").focus();
        return false;
    }

    if(country == ""){
        danger_toast_msg("Country should not empty");
        $("#Ca_Edit_country").focus();
        return false;
    }

    if(state == ""){
        danger_toast_msg("State should not empty");
        $("#Ca_Edit_state").focus();
        return false;
    }

    if(city == ""){
        danger_toast_msg("City should not empty");
        $("#Ca_Edit_city").focus();
        return false;
    }


    if(positions == ""){
        danger_toast_msg("Positions should not empty");
        $("#Ca_positions").focus();
        return false;
    }

    

    var JobsData = {
        address: address,
        zipcode: zipcode,
        country: country,
        state: state,
        city: city,
        stipend_type: stipend_type,
        stipend: stipend,
        salary_min: salary_min,
        salary_max: salary_max,
        salary_type: salary_type,
        qualification: qualification,
        positions: positions,
        description: description
    }


    $.ajax({
        type: "POST",
        url: "/Article/AddCAJobs",
        data: JobsData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message);
                return false;
            }else{
                $('.post-ca-jobs-popup-box').fadeOut('fast');
                $('html').removeClass('no-scroll');
                success_toast_msg(data.message);
                location.reload();
                return false;
            }
        }
    });

});





$(document).on("change", ".CheckDatePostedCAJobsFilters", function(){
    // function CheckJobFilters(){
        var date_posted = $(this).val();
    
    
        var FiltersData = {
            date_posted: date_posted
        }
    
        $.ajax({
            type: "POST",
            url: "/FilterDatePostedCAJobs",
            data: FiltersData,
            dataType: "html",
            beforeSend: function(){
                // Show image container
                $("#AjaxLoader").show();
                $("#SearchCAJobsResults").hide();
                // return false;
            },
            success: function (data) {
                if(data){
                    $("#SearchCAJobsResults").html("");
                    $("#SearchCAJobsResults").append(data);
                    
                    // $("#JobsCount").text(data.length)/1000;
                    return false;
                }else{
                    // $("#JobsCount").text('0');
                    $("#SearchCAJobsResults").html("No results found...");
                    return false;
                }
            },
            complete:function(data){
            // Hide image container
                $("#AjaxLoader").hide();
                $("#SearchCAJobsResults").show();
            }
        });
    // }
    });



    $(document).on("keypress", "#SearchCAJobsLocationByKeypress", function(){
        var Location = $(this).val();
    
        $.ajax({
            type: "POST",
            url: "/SearchCAJobsLocationByKeypress",
            data: {Location: Location},
            dataType: "html",
            success: function (data) {
                if(data){
                    $("#AppendLocation").html(data);
                    return false;
                }else{
                    $("#AppendLocation").html("No results");
                    return false;
                }
            }
        });
    });



    $(document).on("change", ".CheckManageCAJobsFilters", function(){
        // function CheckJobFilters(){
            // var date_posted = $(this).val();
            var location = $('.Location:checked').val();
            // var qualification = $('.Qualification:checked').val();
            var Stipend = $('.JobType:checked').val();
        
            location = [];
        
            $('input[name="Location"]:checked').each(function()
            {
                location.push($(this).val());
            });
        
            Stipend = [];
        
            $('input[name="Stipend"]:checked').each(function()
            {
                Stipend.push($(this).val());
            });
        
        
            var FiltersData = {
                location: location,
                Stipend: Stipend
            }
        
            $.ajax({
                type: "POST",
                url: "/FilterManageCAJobs",
                data: FiltersData,
                dataType: "html",
                beforeSend: function(){
                    // Show image container
                    $("#AjaxLoader").show();
                    $("#SearchCAJobsResults").hide();
                    // return false;
                },
                success: function (data) {
                    if(data.error){
                        danger_msg(data.message);
                        return false;
                    }else{
                        $("#SearchCAJobsResults").html("");
                        $("#SearchCAJobsResults").append(data);
                        console.log(data);
                        return false;
                    }
                },
                complete:function(data){
                // Hide image container
                    $("#AjaxLoader").hide();
                    $("#SearchCAJobsResults").show();
                }
            });
        // }
        });



        $(document).on("click", "#ClearAllCAJobs", function(){
            $('input[type="checkbox"]').removeAttr('checked');
            $('#AppliedCount').text(1);
        
        
        
            $.ajax({
                type: "POST",
                url: "/ClearAllCAJobs",
                dataType: "html",
                beforeSend: function(){
                    // Show image container
                    $("#AjaxLoader").show();
                    $("#SearchCAJobsResults").hide();
                    // return false;
                },
                success: function (data) {
                    if(data.error){
                        console.log(data);
                        return false;
                    }else{
                        $("#SearchCAJobsResults").html("");
                        $("#SearchCAJobsResults").append(data);
                        return false;
                    }
                },
                complete:function(data){
                // Hide image container
                $("#AjaxLoader").hide();
                $("#SearchCAJobsResults").show();
                }
            });
        });



        $(document).on("keypress", "#SearchFresherJobsLocationByKeypress", function(){
            var Location = $(this).val();
        
            $.ajax({
                type: "POST",
                url: "/SearchFresherJobsLocationByKeypress",
                data: {Location: Location},
                dataType: "html",
                success: function (data) {
                    if(data){
                        $("#AppendLocation").html(data);
                        return false;
                    }else{
                        $("#AppendLocation").html("No results");
                        return false;
                    }
                }
            });
        });





// Close Jobs        
$(document).on("change", ".CheckDatePostedClosedCAJobsFilters", function(){
    // function CheckJobFilters(){
        var date_posted = $(this).val();
    
    
        var FiltersData = {
            date_posted: date_posted
        }
    
        $.ajax({
            type: "POST",
            url: "/FilterDatePostedClosedCAJobs",
            data: FiltersData,
            dataType: "html",
            beforeSend: function(){
                // Show image container
                $("#AjaxLoader").show();
                $("#SearchCAJobsResults").hide();
                // return false;
            },
            success: function (data) {
                if(data){
                    $("#SearchCAJobsResults").html("");
                    $("#SearchCAJobsResults").append(data);
                    
                    // $("#JobsCount").text(data.length)/1000;
                    return false;
                }else{
                    // $("#JobsCount").text('0');
                    $("#SearchCAJobsResults").html("No results found...");
                    return false;
                }
            },
            complete:function(data){
            // Hide image container
                $("#AjaxLoader").hide();
                $("#SearchCAJobsResults").show();
            }
        });
    // }
    });



    $(document).on("keypress", "#SearchClosedCAJobsLocationByKeypress", function(){
        var Location = $(this).val();
    
        $.ajax({
            type: "POST",
            url: "/SearchClosedCAJobsLocationByKeypress",
            data: {Location: Location},
            dataType: "html",
            success: function (data) {
                if(data){
                    $("#AppendLocation").html(data);
                    return false;
                }else{
                    $("#AppendLocation").html("No results");
                    return false;
                }
            }
        });
    });


    $(document).on("change", ".CheckCloseCAJobsFilters", function(){
        // function CheckJobFilters(){
            // var date_posted = $(this).val();
            var location = $('.Location:checked').val();
            // var qualification = $('.Qualification:checked').val();
            // var Stipend = $('.JobType:checked').val();
        
            location = [];
        
            $('input[name="Location"]:checked').each(function()
            {
                location.push($(this).val());
            });
        
            // Stipend = [];
        
            // $('input[name="Stipend"]:checked').each(function()
            // {
            //     Stipend.push($(this).val());
            // });
        
        
            var FiltersData = {
                location: location
                // Stipend: Stipend
            }
        
            $.ajax({
                type: "POST",
                url: "/FilterCloseCAJobs",
                data: FiltersData,
                dataType: "html",
                beforeSend: function(){
                    // Show image container
                    $("#AjaxLoader").show();
                    $("#SearchCAJobsResults").hide();
                    // return false;
                },
                success: function (data) {
                    if(data.error){
                        danger_msg(data.message);
                        return false;
                    }else{
                        $("#SearchCAJobsResults").html("");
                        $("#SearchCAJobsResults").append(data);
                        console.log(data);
                        return false;
                    }
                },
                complete:function(data){
                // Hide image container
                    $("#AjaxLoader").hide();
                    $("#SearchCAJobsResults").show();
                }
            });
        // }
        });


        $(document).on("click", "#ClearAllClosedCAJobs", function(){
            $('input[type="checkbox"]').removeAttr('checked');
            $('#AppliedCount').text(1);
        
            
            $.ajax({
                type: "POST",
                url: "/ClearAllClosedCAJobs",
                dataType: "html",
                beforeSend: function(){
                    // Show image container
                    $("#AjaxLoader").show();
                    $("#SearchCAJobsResults").hide();
                    // return false;
                },
                success: function (data) {
                    if(data.error){
                        console.log(data);
                        return false;
                    }else{
                        $("#SearchCAJobsResults").html("");
                        $("#SearchCAJobsResults").append(data);
                        return false;
                    }
                },
                complete:function(data){
                // Hide image container
                $("#AjaxLoader").hide();
                $("#SearchCAJobsResults").show();
                }
            });
        });