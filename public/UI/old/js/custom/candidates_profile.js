$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});



function CheckZipCode(){
    var zipcode = $("#zipcode").val();

    $.ajax({
        type: "GET",
        url: "https://api.postalpincode.in/pincode/"+zipcode,
        data: {zipcode: zipcode},
        dataType: "JSON",
        success: function (data) {
            
            if(data.error){
                console.log(data);
                return false;
            }else{
                console.log(data);
                $.each(data, function (key, val) { 
                    // console.log(val.PostOffice);
                    $.each(val.PostOffice, function (keys, i) { 
                        console.log(i.Country);
                        console.log(i.State);
                        $("#country").val(i.Country);
                        $("#state").val(i.State);
                        $("#city").val(i.District);
                        // console.log(i.District);
                    });
                });
                return false;
            }
            
        }
    });
}


$("#file").change(function() {
    readURL(this);
  });


function BrowseCandidatePic(elemId) {
    var elem = document.getElementById(elemId);
    if(elem && document.createEvent) {
       var evt = document.createEvent("MouseEvents");
       evt.initEvent("click", true, false);
       elem.dispatchEvent(evt);
    }
    // readURL(this);
 }



 function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      
      reader.onload = function(e) {
        $('#CandidateProfilePic').attr('src', e.target.result);
        $('.profile-head-width').attr('src', e.target.result);
      }
      
      reader.readAsDataURL(input.files[0]);
    }
  }
  
  

$(document).on("click", "#AddCandidateProfile", function(){
    // var fullname = $("#fullname").val();
    var id = $("#id").val();
    var name = $("#fullname").val();
    var industry = $("#industry").val();
    var functional_area = $("#functional_area").val();
    var role = $("#role").val();
    // var allow_search = $("#allow_search").val();
    var experience = $("#experience :selected").val();
    // var minimum_salary = $("#minimum_salary").val();
    var gender = $("#gender :selected").val();
    var dob = $("#dob").val();
    var age = $("#age").val();
    var current_salary = $("#current_salary :selected").val();
    var expected_salary = $("#expected_salary :selected").val();
    // var education_level = $("#education_level :selected").val();
    var job_type = $("#job_type :selected").val();
    var employee_type = $("#employee_type :selected").val();
    var desired_location = $("#desired_location").val();
    // var desired_shift = $("#desired_shift :selected").val();
    var marital_status = $("#marital_status :selected").val();
    var languages = $("#languages").val();
    var specialism = $("#specialism").val();
    var description = CKEDITOR.instances['description'].getData();
    var facebook = $("#facebook").val();
    var twitter = $("#twitter").val();
    var linkedin = $("#linkedin").val();
    var googleplus = $("#googleplus").val();
    // var phone = $("#phone").val();
    // var email = $("#email").val();
    // var website = $("#website").val();
    var address = $("#address").val();
    var zipcode = $("#zipcode").val();
    var country = $("#country").val();
    var state = $("#state").val();
    var city = $("#city").val();

    // Validation
    if(name == ""){
        danger_msg("Name should not empty");
        $("#fullname").focus();
        return false;
    }

    if(industry == ""){
        danger_msg("Industry should not empty");
        $("#industry").focus();
        return false;
    }

    if(functional_area == ""){
        danger_msg("Functional_area should not empty");
        $("#functional_area").focus();
        return false;
    }

    if(role == ""){
        danger_msg("Role should not empty");
        $("#role").focus();
        return false;
    }

    if(experience == "Select Experience"){
        danger_msg("Experience should not empty");
        $("#experience").focus();
        return false;
    }

    // if(minimum_salary == ""){
    //     danger_msg("Minimum salary should not empty");
    //     $("#minimum_salary").focus();
    //     return false;
    // }

    if(gender == "Select Gender"){
        danger_msg("Gender should not empty");
        $("#gender").focus();
        return false;
    }

    if(dob == ""){
        danger_msg("Date of birth should not empty");
        $("#dob").focus();
        return false;
    }

    if(age == ""){
        danger_msg("Age should not empty");
        $("#age").focus();
        return false;
    }

    if(current_salary == "Please Select"){
        danger_msg("Current salary should not empty");
        $("#current_salary").focus();
        return false;
    }

    if(expected_salary == "Please Select"){
        danger_msg("Expected salary should not empty");
        $("#expected_salary").focus();
        return false;
    }

    // if(education_level == "Please Select"){
    //     danger_msg("Education level should not empty");
    //     $("#education_level").focus();
    //     return false;
    // }

    // if(job_type == "Please Select"){
    //     danger_msg("Job type should not empty");
    //     $("#job_type").focus();
    //     return false;
    // }

    if(employee_type == "Please Select"){
        danger_msg("Employee type should not empty");
        $("#employee_type").focus();
        return false;
    }

    // if(desired_location == ""){
    //     danger_msg("Desired location should not empty");
    //     $("#desired_location").focus();
    //     return false;
    // }

    if(desired_shift == "Please Select"){
        danger_msg("Desired shift should not empty");
        $("#desired_shift").focus();
        return false;
    }

    if(marital_status == "Please Select"){
        danger_msg("Marital status should not empty");
        $("#marital_status").focus();
        return false;
    }

    if(languages == ""){
        danger_msg("Languages should not empty");
        $("#languages").focus();
        return false;
    }

    if(specialism == ""){
        danger_msg("Specialism should not empty");
        $("#specialism").focus();
        return false;
    }

    if(description == ""){
        danger_msg("Description should not empty");
        $("#description").focus();
        return false;
    }

    if(address == ""){
        danger_msg("Address should not empty");
        $("#address").focus();
        return false;
    }

    if(zipcode == ""){
        danger_msg("Zipcode should not empty");
        $("#zipcode").focus();
        return false;
    }
    
    if(country == ""){
        danger_msg("Country should not empty");
        $("#country").focus();
        return false;
    }

    if(state == ""){
        danger_msg("State should not empty");
        $("#state").focus();
        return false;
    }

    if(city == ""){
        danger_msg("City should not empty");
        $("#city").focus();
        return false;
    }




    var CandidateProfileData = new FormData();

    CandidateProfileData.append('id', id);
    CandidateProfileData.append('industry', industry);
    CandidateProfileData.append('functional_area', functional_area);
    CandidateProfileData.append('role', role);
    // CandidateProfileData.append('allow_search', allow_search);
    CandidateProfileData.append('experience', experience);
    // CandidateProfileData.append('minimum_salary', minimum_salary);
    CandidateProfileData.append('gender', gender);
    CandidateProfileData.append('dob', dob);
    CandidateProfileData.append('age', age);
    CandidateProfileData.append('current_salary', current_salary);
    CandidateProfileData.append('expected_salary', expected_salary);
    // CandidateProfileData.append('education_level', education_level);
    // CandidateProfileData.append('job_type', job_type);
    CandidateProfileData.append('employee_type', employee_type);
    CandidateProfileData.append('desired_location', desired_location);
    // CandidateProfileData.append('desired_shift', desired_shift);
    CandidateProfileData.append('marital_status', marital_status);
    CandidateProfileData.append('languages', languages);
    CandidateProfileData.append('specialism', specialism);
    CandidateProfileData.append('description', description);
    CandidateProfileData.append('facebook', facebook);
    CandidateProfileData.append('twitter', twitter);
    CandidateProfileData.append('linkedin', linkedin);
    CandidateProfileData.append('googleplus', googleplus);
    // CandidateProfileData.append('website', website);
    CandidateProfileData.append('address', address);
    CandidateProfileData.append('zipcode', zipcode);
    CandidateProfileData.append('country', country);
    CandidateProfileData.append('state', state);
    CandidateProfileData.append('city', city);
    CandidateProfileData.append('_token', $("input[name=_token]").val());
    CandidateProfileData.append('file', $('#file')[0].files[0]);

    $.ajax({
        type: "POST",
        url: "/Candidate/InsertCandidateProfile",
        data: CandidateProfileData,
        dataType: "JSON",
        contentType: false,
        processData: false,
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                success_msg(data.message);
                return false;
            }
        }
    });
});



// Add Shortlisted
$(document).on("click", "#AddShortlisted", function(){
   

    var CandidateId = $("#CandidateId").val();

    $.ajax({
        type: "POST",
        url: "/ShortlistCandidates",
        data: {CandidateId: CandidateId},
        dataType: "JSON",
        beforeSend: function(){
            // Show image container
            $("#ApplyBtnLoader").show();
            $("#AddShortlisted").hide();
        },
        success: function (data) {
            if(data.error){
                // danger_msg(data.message);
                return false;
            }else{
                // success_msg(data.message);
                return false;
            }
        },
        complete:function(data){
        // Hide image container
            $("#ApplyBtnLoader").hide();
            $("#AddShortlisted").hide();
            $("#JobsAppliedTxt").html("Candidate shortlisted.");
            $("#JobsAppliedTxt").show();
        }
    });
});



$(function() {
    $(".heart").on("click", function() {
        var JobsId = $("#JobsId").val();
        if($(this).toggleClass("is-active")){
            $.ajax({
                type: "POST",
                url: "/AddToFavourite",
                data: {JobsId: JobsId},
                dataType: "JSON",
                success: function (data) {
                    if(data.error){
                        console.log(data);
                        return false;
                    }else{
                        console.log(data);
                        return false;
                    }
                }
            });
        }
    //   $(this).toggleClass("is-active");
    });
  });


  function ChangeJob(){
    //   var status = "";
    //     if($('#ChangeJob').is(':checked')){
    //         status = 1;
    //     }else{
    //         status = 0;
    //     }

        $.ajax({
            type: "POST",
            url: "/ChangeJobChange",
            // data: {status: status},
            dataType: "JSON",
            success: function (data) {
                if(data.error){
                    danger_msg(data.message);
                    return false;
                }else{
                    success_msg(data.message);
                    return false;
                }
            }
        });
  }


  $(document).on("click", "#LoadmoreJobs", function(){
        var JobsId = $(this).data('id');

        $.ajax({
            type: "POST",
            url: "/LoadMoreJobs",
            data: {JobsId: JobsId},
            dataType: "text",
            beforeSend: function(){
                $("#ApplyBtnLoader").show();
            },
            success: function (data) {
                if(data != ''){
                    console.log(data);
                    $('#remove-row').remove();
                    $("#SearchJobsResults").append(data);
                }else{
                    console.log(data);
                    $("#LoadmoreJobs").html("No jobs found...");
                    return false;
                }
            },
            complete:function(data){
                $("#ApplyBtnLoader").hide();
            }
        });
        
  });



  $(document).on("click", "#UpdateCandidatePassword", function(){
        var OldPassword = $("#old_password").val();
        var NewPassword = $("#new_password").val();
        var VerifyPassword = $("#verify_password").val();

        var CandidatePassword = {
            OldPassword: OldPassword,
            NewPassword: NewPassword,
            VerifyPassword: VerifyPassword
        }

        $.ajax({
            type: "POST",
            url: "/UpdatePassword",
            data: CandidatePassword,
            dataType: "JSON",
            success: function (data) {
                if(data.error){
                    danger_msg(data.message);
                    return false;
                }else{
                    success_msg(data.message);
                    return false;
                }
            }
        });

  });



function success_msg(data){
    $.notify({
        title: '<strong>'+ data +'</strong>',
        message: ''
    },{
        type: 'success'
    });
}

function danger_msg(data){
    $.notify({
        title: '<strong>'+ data +'</strong>',
        message: ''
    },{
        type: 'danger'
    });
}
