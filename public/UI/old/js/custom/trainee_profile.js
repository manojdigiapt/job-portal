$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});




$(document).on("click", "#AddOrUpdateTraineeProfile", function(){
    // var fullname = $("#fullname").val();
    // var id = $("#id").val();
    var name = $("#name").val();
    var allow_search = $("#allow_search").val();
    var gender = $("#gender :selected").val();
    var aadhar = $("#aadhar").val();
    var pan = $("#pan").val();
    var facebook = $("#facebook").val();
    var twitter = $("#twitter").val();
    var googleplus = $("#googleplus").val();
    var linkedin = $("#linkedin").val();
    var mobile = $("#mobile").val();
    var email = $("#email").val();
    var address = $("#address").val();
    var zipcode = $("#zipcode").val();
    var country = $("#country").val();
    var state = $("#state").val();
    var city = $("#city").val();


    if(name == ""){
        danger_msg("First name should not empty");
        $("#name").focus();
        return false;
    }

    if(gender == "Please select gender"){
        danger_msg("Gender should not empty");
        $("#gender").focus();
        return false;
    }

    if(aadhar == ""){
        danger_msg("Aadhar no should not empty");
        $("#aadhar").focus();
        return false;
    }

    if(pan == ""){
        danger_msg("Pan number should not empty");
        $("#pan").focus();
        return false;
    }else if(!validatePAN(pan)){
        danger_msg("Invalid Pan Number");
        $("#pan").focus();
        return false;
    }

    if(mobile == ""){
        danger_msg("Mobile no should not empty");
        $("#mobile").focus();
        return false;
    }

    if(email == ""){
        danger_msg("Email should not empty");
        $("#email").focus();
        return false;
    }

    if(address == ""){
        danger_msg("Address should not empty");
        $("#address").focus();
        return false;
    }

    if(zipcode == ""){
        danger_msg("Zipcode should not empty");
        $("#zipcode").focus();
        return false;
    }

    if(country == ""){
        danger_msg("Country should not empty");
        $("#country").focus();
        return false;
    }

    if(state == ""){
        danger_msg("State should not empty");
        $("#state").focus();
        return false;
    }

    if(city == ""){
        danger_msg("City should not empty");
        $("#city").focus();
        return false;
    }


    var TraineeProfileData = new FormData();

    // TraineeProfileData.append('id', id);
    TraineeProfileData.append('name', name);
    TraineeProfileData.append('allow_search', allow_search);
    TraineeProfileData.append('gender', gender);
    TraineeProfileData.append('aadhar', aadhar);
    TraineeProfileData.append('pan', pan);
    TraineeProfileData.append('facebook', facebook);
    TraineeProfileData.append('twitter', twitter);
    TraineeProfileData.append('linkedin', linkedin);
    TraineeProfileData.append('googleplus', googleplus);
    TraineeProfileData.append('mobile', mobile);
    TraineeProfileData.append('email', email);
    TraineeProfileData.append('address', address);
    TraineeProfileData.append('zipcode', zipcode);
    TraineeProfileData.append('country', country);
    TraineeProfileData.append('state', state);
    TraineeProfileData.append('city', city);
    TraineeProfileData.append('_token', $("input[name=_token]").val());
    TraineeProfileData.append('file', $('#file')[0].files[0]);

    $.ajax({
        type: "POST",
        url: "/Trainee/AddOrUpdateTrainee",
        data: TraineeProfileData,
        dataType: "JSON",
        contentType: false,
        processData: false,
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return FALSE;
            }else{
                success_msg(data.message);
                return FALSE;
            }
        }
    });
});


$(document).on("click", "#SelectForTraining", function(){
    var start_date =$("#start_date").val();
    var CandidateId =$("#CandidateId").val();
    var TrainingId =$("#TrainingId").val();

    var SelectedTrainingData = {
        start_date: start_date,
        CandidateId: CandidateId,
        TrainingId: TrainingId
    }

    $.ajax({
        type: "POST",
        url: "/SelectTraineeForTraining",
        data: SelectedTrainingData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                success_msg(data.message);
                location.reload();
                return false;
            }
        }
    });
});



// Add to favourite trainings
$(function() {
    $(".training-heart").on("click", function() {
        var TrainingId = $("#TrainingId").val();
        if($(this).toggleClass("is-active")){
            $.ajax({
                type: "POST",
                url: "/AddToTrainingFavourite",
                data: {TrainingId: TrainingId},
                dataType: "JSON",
                success: function (data) {
                    if(data.error){
                        console.log(data);
                        return false;
                    }else{
                        console.log(data);
                        return false;
                    }
                }
            });
        }
    //   $(this).toggleClass("is-active");
    });
  });



// Aadhar Validation
$('[data-type="adhaar-number"]').keyup(function() {
    var value = $(this).val();
    value = value.replace(/\D/g, "").split(/(?:([\d]{4}))/g).filter(s => s.length > 0).join("-");
    $(this).val(value);
  });

  $('[data-type="adhaar-number"]').on("change, blur", function() {
    var value = $(this).val();
    var maxLength = $(this).attr("maxLength");
    if (value.length != maxLength) {
      $(this).addClass("highlight-error");
    } else {
      $(this).removeClass("highlight-error");
    }
  });



  $(document).on("click", "#LoadmoreTrainings", function(){
    var TrainingId = $(this).data('id');


    $.ajax({
        type: "POST",
        url: "/LoadmoreTrainings",
        data: {TrainingId: TrainingId},
        dataType: "text",
        beforeSend: function(){
            $("#ApplyBtnLoader").show();
        },
        success: function (data) {
            if(data != ''){
                console.log(data);
                $('#remove_training_load_more').remove();
                $("#SearchTrainingResults").append(data);
            }else{
                console.log(data);
                $("#LoadmoreTrainings").html("No trainings are found...");
                return false;
            }
        },
        complete:function(data){
            $("#ApplyBtnLoader").hide();
        }
    });
    
});


$(document).on("click", "#UpdateTraineePassword", function(){
    var OldPassword = $("#old_password").val();
    var NewPassword = $("#new_password").val();
    var VerifyPassword = $("#verify_password").val();

    var CandidatePassword = {
        OldPassword: OldPassword,
        NewPassword: NewPassword,
        VerifyPassword: VerifyPassword
    }

    $.ajax({
        type: "POST",
        url: "/UpdateTraineePassword",
        data: CandidatePassword,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                success_msg(data.message);
                return false;
            }
        }
    });

});


function validatePAN(pan){ 
    var regex = /[A-Z]{5}[0-9]{4}[A-Z]{1}$/; 
    return regex.test(pan); 
}


function success_msg(data){
    $.notify({
        title: '<strong>'+ data +'</strong>',
        message: ''
    },{
        type: 'success'
    });
}

function danger_msg(data){
    $.notify({
        title: '<strong>'+ data +'</strong>',
        message: ''
    },{
        type: 'danger'
    });
}
