$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

// Trainings

$(this).ready( function() {  
    $("#SearchTrainingTitleOrCompanyName").autocomplete({  
        minLength: 1,  
        source:   
        function(req, add){  
            $.ajax({  
                url: "/SearchTrainingTitle",  
                dataType: 'json',  
                type: 'POST',  
                data: req,  
                success:      
                function(data){  
                    if(data.response =="true"){  
                        $.each(data, function (i, key) { 
                            add(key);  
                            console.log(key);
                        });
                    }else{
                        $.each(data, function (i, key) { 
                            add(key);  
                            console.log(key);
                        });
                        
                    }
                },  
            });  
        },  
             
    });  
});  


$(document).on("click", "#SearchTrainings", function(){
    var title = $("#SearchTrainingTitleOrCompanyName").val();
    var state = $("#state :selected").val();
    var city = $("#city :selected").val();


    if(title == ""){
        danger_msg("Title should not empty");
        $("#SearchTrainingTitleOrCompanyName").focus();
        return false;
    }

    if(state == "Please select state"){
        danger_msg("State should not empty");
        $("#state").focus();
        return false;
    }

    if(city == "Please select city"){
        danger_msg("City should not empty");
        $("#city").focus();
        return false;
    }

    var SearchTrainingData = {
        title: title,
        state: state,
        city: city
    }

    $.ajax({
        type: "POST",
        url: "/SearchTrainings",
        data: SearchTrainingData,
        dataType: "JSON",
        beforeSend: function(){
            // Show image container
            $("#AjaxLoader").show();
        },
        success: function (data) {
            if(data.error){
                console.log(data);
                return false;
            }else{
                $("#SearchTrainingResults").html("");
                $.each(data, function (i, value) { 
                    var SearchTraining = "<div class='job-listing wtabs'><div class='job-title-sec'><div class='c-logo'> <img src='/employer_profile/"+value.profile_pic+"' alt='' /></div><h3><a href='/TrainingDetails/"+value.company_name+"/"+value.slug+"' target='_blank' title=''>"+value.title+"</a></h3><span>"+value.company_name+"</span><div class='job-lctn'><i class='la la-map-marker'></i>"+value.country+","+ value.city+"</div></div><div class='job-style-bx'><span class='job-is ft'>"+value.duration+" Days</span><span class='fav-job'><i class='la la-heart-o'></i></span><i>5 months ago</i></div></div>";
                    $("#SearchTrainingResults").append(SearchTraining);
                });
                return false;
            }
        },
        complete:function(data){
        // Hide image container
        $("#AjaxLoader").hide();
        }
    });
});













// Job Search
$(this).ready( function() {  
    $("#SearchJobsTitleOrCompanyName").autocomplete({  
        minLength: 1,  
        source:   
        function(req, add){  
            $.ajax({  
                url: "/SearchJobsTitle",  
                dataType: 'json',  
                type: 'POST',  
                data: req,  
                success:      
                function(data){  
                    if(data.response =="true"){  
                        $.each(data, function (i, key) { 
                            add(key);  
                            console.log(key);
                        });
                    }else{
                        $.each(data, function (i, key) { 
                            add(key);  
                            console.log(key);
                        });
                        
                    }
                },  
            });  
        },  
             
    });  
});  


$(document).on("click", "#SearchJobs", function(){
    var title = $("#SearchJobsTitleOrCompanyName").val();
    var state = $("#state :selected").val();
    var city = $("#city :selected").val();

    if(title == ""){
        danger_msg("Title should not empty");
        $("#SearchJobsTitleOrCompanyName").focus();
        return false;
    }

    if(state == "Please select state"){
        danger_msg("State should not empty");
        $("#state").focus();
        return false;
    }

    if(city == "Please select city"){
        danger_msg("City should not empty");
        $("#city").focus();
        return false;
    }

    var SearchJobsData = {
        title: title,
        state: state,
        city: city
    }

    $.ajax({
        type: "POST",
        url: "/SearchJobs",
        data: SearchJobsData,
        dataType: "JSON",
        beforeSend: function(){
            // Show image container
            $("#AjaxLoader").show();
        },
        success: function (data) {
            if(data.error){
                console.log(data);
                return false;
            }else{
                $("#SearchJobsResults").html("");
                $.each(data, function (i, value) { 
                    var CheckJobType = "";

                    if(value.job_type == 1){
                        CheckJobType = "<span class='job-is ft'>Full time</span>";
                    }else{
                        CheckJobType = "<span class='job-is pt'>Part time</span>";
                    }
                    var SearchJobs = "<div class='job-listing wtabs'><div class='job-title-sec'><div class='c-logo'> <img src='employer_profile/"+value.profile_pic+"' alt='' /> </div><h3><a href='/JobDetails/"+value.slug+"' target='_blank' title=''>"+value.title+"</a></h3><span>"+value.company_name+"</span><div class='job-lctn'><i class='la la-map-marker'></i>"+value.location+"</div></div><div class='job-style-bx'>"+CheckJobType+"<span class='fav-job'><i class='la la-heart-o'></i></span><i>5 months ago</i></div></div>";

                    $("#SearchJobsResults").append(SearchJobs);
                });
                return false;
            }
        },
        complete:function(data){
        // Hide image container
        $("#AjaxLoader").hide();
        }
    });
});










// Candidate Search
$(this).ready( function() {  
    $("#SearchCandidatesRole").autocomplete({  
        minLength: 1,  
        source:   
        function(req, add){  
            $.ajax({  
                url: "/SearchCandidatesPositions",  
                dataType: 'json',  
                type: 'POST',  
                data: req,  
                success:      
                function(data){  
                    if(data.response =="true"){  
                        $.each(data, function (i, key) { 
                            add(key);  
                            console.log(key);
                        });
                    }else{
                        $.each(data, function (i, key) { 
                            add(key);  
                            console.log(key);
                        });
                        
                    }
                },  
            });  
        },  
             
    });  
});  


$(document).on("click", "#SearchCandidates", function(){
    var role = $("#SearchCandidatesRole").val();
    var state = $("#state").val();
    var city = $("#city").val();


    var SearchCandidatesData = {
        role: role,
        state: state,
        city: city
    }

    $.ajax({
        type: "POST",
        url: "/SearchCandidates",
        data: SearchCandidatesData,
        dataType: "JSON",
        beforeSend: function(){
            // Show image container
            $("#AjaxLoader").show();
        },
        success: function (data) {
            if(data.error){
                console.log(data);
                return false;
            }else{
                $("#SearchCandidatesResults").html("");
                $.each(data, function (i, value) { 
                    
                    var SearchCandidates = "<div class='emply-resume-list square'><div class='emply-resume-thumb'><img src='candidate_profile/"+value.profile_pic+"' alt=''/></div><div class='emply-resume-info'><h3><a href='#' title=''>"+value.name+"</a></h3><span><i>"+value.role+"</i> at Atract Solutions</span><p><i class='la la-map-marker'></i>"+value.address+" / "+value.city+"</p></div><div class='shortlists'><a href='#'title=''>Shortlist <i class='la la-plus'></i></a></div></div>";

                    $("#SearchCandidatesResults").append(SearchCandidates);
                });
                return false;
            }
        },
        complete:function(data){
        // Hide image container
        $("#AjaxLoader").hide();
        }
    });
});