<div class="reviews-sec candiates-card-profile-top">
        <div class="col-lg-12">
            <div class="reviews candidates-profile-card-border">
                <div class="col-md-3 pull-left">
                        <div class="progress mx-auto" data-value='{{$CandidateProfile->profile_completion}}'>
                            <span class="progress-left">
                                            <span class="progress-bar border-primary"></span>
                            </span>
                            <span class="progress-right">
                                            <span class="progress-bar border-primary"></span>
                            </span>
                            <div class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
                                @if($CandidateProfile->profile_pic)
                                    <img class="candidate-profile-img" src="{{URL::asset('candidate_profile/')}}/{{$CandidateProfile->profile_pic}}" alt="" />
                                @else
                                    <img class="candidate-profile-img" src="{{URL::asset('UI/images/resource/r1.jpg')}}" alt="" />
                                @endif
                            </div>
                            <h5 class="profile-completion-percentage">{{$CandidateProfile->profile_completion}}%</h5>
                            <p class="profile-completion-p">Profile Completion</p>
                        </div>

                    {{-- <div class="progress blue">
                        <span class="progress-left">
                            <span class="progress-bar"></span>
                        </span>
                        <span class="progress-right">
                            <span class="progress-bar"></span>
                        </span>
                        <div class="progress-value">
                        @if($CandidateProfile->profile_pic)
                            <img class="candidate-profile-img" src="{{URL::asset('candidate_profile/')}}/{{$CandidateProfile->profile_pic}}" alt="" />
                        @else
                            <img class="candidate-profile-img" src="{{URL::asset('UI/images/resource/r1.jpg')}}" alt="" />
                        @endif
                        </div>
                        <h5 class="profile-completion-percentage">90%</h5>
                        <p class="profile-completion-p">Profile Completion</p>
                    </div> --}}
                </div>

                <div class="col-md-9 pull-left mobile-profile-center">
                    <h3 class="mobile-review-head">{{$Profile->name}} </h3>
                    <span class="candidate-profile-position">
                            @if($GetExperiencePosition)
                            {{$GetExperiencePosition->name}}
                          @else
                            Posiiton
                          @endif
                    </span>
                    <br>
                    <span class="candidate-profile-position">
                            @if($GetExperiencePosition)
                            {{$GetExperiencePosition->CompanyName}}
                          @else
                            Company name
                          @endif    
                    </span>  
                    <br>   
                    @if($CandidateProfile->profile_completion == 100)
                    <a href="/Candidate/Profile" class="profile-complete-link">Edit Profile</a>
                    @else
                    <a href="/Candidate/Profile" class="profile-complete-link">Complete Profile Now</a>
                    @endif
                </div>
            </div>
        </div>
    </div>
    
    <div class="widget border filter-card-candidate-profile filter-jobs-mar-bottom">
            <h3 class="sb-title open Filters col-md-4 text-left pad-left0 mobile-filter-float-left">Filters (<span class="filter-count-font" id="AppliedCount">1</span>)</h3>
            {{-- <h3 class="sb-title open Filters filter-applied col-md-4 mobile-filter-float-left" >Applied(<span id="AppliedCount">1</span>)</h3> --}}
            <a href="javascript:void(0);" class="pull-right open Filters col-md-4 filters-clearall mobile-filters-clear-all" id="ClearAll">Clear All</a>
            <br>
    </div>
    <div class="widget border filter-card-candidate-profile ">
            <h4 class="sb-title open">Date Posted</h4>
            <div class="posted_widget">
               {{--  <input type="radio" value="1" name="choose" class="CheckDatePostedJobFilters" id="232" checked><label for="232">Last Hour</label><br />  --}}
               <input type="radio" value="2" name="choose" class="CheckDatePostedJobFilters" id="wwqe"><label for="wwqe">Last 24 hours</label><br />
               <input type="radio" value="3" name="choose" class="CheckDatePostedJobFilters" id="erewr"><label for="erewr">Last 7 days</label><br />
               <input type="radio" value="4" name="choose" class="CheckDatePostedJobFilters" id="all" checked><label for="all">All</label><br />
            </div>
    </div>
    <div class="widget border filter-card-candidate-profile filter-jobs-mar-top">
        <div>
            <h3 class="sb-title open">Location</h3>
            <input type="text" class="search-input-bg" id="SearchJobsLocationByKeypress" placeholder="Search Location">

            <div class="type_widget pad-bottom10 managejobs-border-line" id="AppendLocation">
                {{-- <p><input type="checkbox" class="CheckManageJobFilters" class="" value="Bangalore" name="Location" id="blr"><label for="blr" class="CheckBoxFilter">Bangalore</label></p> --}}
            </div>
            {{-- <div class="type_widget">
                <p><input type="checkbox" class="CheckJobFilters" class="" value="Bangalore" name="Location" id="blr"><label for="blr" class="CheckBoxFilter">Bangalore</label></p>
            </div> --}}
        </div>
    </div>
    {{-- <div class="widget border filter-card-candidate-profile filter-jobs-mar-top">
        <div>
            <h3 class="sb-title open">Salary</h3>
            <div class="type_widget">
                @foreach($GetJobType as $JobType)
                    <p><input type="checkbox" value="{{$JobType->id}}" name="spealism" id="{{$JobType->id}}"><label for="{{$JobType->id}}" class="CheckBoxFilter">{{$JobType->job_type}}</label></p>
                @endforeach
            </div>
        </div>
    </div> --}}
    <div class="widget border filter-card-candidate-profile filter-jobs-mar-top">
        <div>
            <h3 class="sb-title open">Job Titles</h3>
            <div class="type_widget">
                @foreach($GetJobTitle as $Title)
                    <p><input type="checkbox" class="CheckJobFilters" value="{{$Title->id}}" name="Title" id="{{$Title->name}}"><label for="{{$Title->name}}" class="CheckBoxFilter">{{$Title->name}}</label></p>
                @endforeach
            </div>
        </div>
    </div>
    {{-- <div class="widget border filter-card-candidate-profile filter-jobs-mar-top">
        <div>
            <h3 class="sb-title open">Job Type</h3>
            <div class="type_widget">
                @foreach($GetJobType as $JobType)
                    <p><input type="checkbox" class="CheckJobFilters" value="{{$JobType->id}}" name="JobType" id="{{$JobType->job_type}}"><label for="{{$JobType->job_type}}" class="CheckBoxFilter">{{$JobType->job_type}}</label></p>
                @endforeach
            </div>
        </div>
    </div> --}}
    {{-- <div class="widget border filter-card-candidate-profile filter-jobs-mar-top">
        <h3 class="sb-title open">Industry</h3>
        <div class="type_widget">
            @foreach($GetIndustries as $Industries)
                <p><input type="checkbox" class="CheckJobFilters" value="{{$Industries->id}}" name="Industry" id="{{$Industries->name}}"><label for="{{$Industries->name}}" class="CheckBoxFilter">{{$Industries->name}}</label></p>
            @endforeach
        </div>
    </div> --}}