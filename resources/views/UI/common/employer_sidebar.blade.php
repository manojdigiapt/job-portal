<aside class="col-lg-3 column border-right">
        <div class="widget">
            <div class="tree_widget-sec">
                <ul>
                    <li><a href="/Employer/Profile" title=""><i class="la la-file-text"></i>Company Profile</a></li>
                    <li><a href="/Employer/ManageJobs" title=""><i class="la la-briefcase"></i>Manage Jobs</a></li>
                    <li><a href="/Employer/ManageTrainings" title=""><i class="la la-briefcase"></i>Manage Trainings</a></li>     
                    {{-- <li><a href="employer_resume.html" title=""><i class="la la-paper-plane"></i>Resumes</a></li> --}}
                    <li><a href="/Employer/Post_Jobs" title=""><i class="la la-file-text"></i>Post a New Job</a></li>
                    <li><a href="/Employer/Post_Training" title=""><i class="la la-file-text"></i>Post a New Training</a></li>
                    <li><a href="/ViewAllSelectedTrainees" title=""><i class="la la-file-text"></i>Check Selected Students</a></li>
                    <li><a href="employer_change_password.html" title=""><i class="la la-lock"></i>Change Password</a></li>
                    <li><a href="/Employer/logout" title=""><i class="la la-unlink"></i>Logout</a></li>
                </ul>
            </div>
        </div>
    </aside>