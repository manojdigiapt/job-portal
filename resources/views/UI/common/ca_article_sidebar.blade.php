<div class="reviews-sec candiates-card-profile-top">
        <div class="col-lg-12">
            <div class="reviews candidates-profile-card-border">
                <div class="col-md-3 pull-left">
                    
                    <div class="progress mx-auto" data-value='{{$ArticleProfile->profile_completion}}'>
                        <span class="progress-left">
                                        <span class="progress-bar border-primary"></span>
                        </span>
                        <span class="progress-right">
                                        <span class="progress-bar border-primary"></span>
                        </span>
                        <div class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
                            @if($ArticleProfile->profile_pic)
                                <img class="candidate-profile-img" src="{{URL::asset('trainee_profile/')}}/{{$ArticleProfile->profile_pic}}" alt="" />
                            @else
                                <img class="candidate-profile-img" src="{{URL::asset('UI/images/resource/r1.jpg')}}" alt="" />
                            @endif
                        </div>
                        <h5 class="profile-completion-percentage">{{$ArticleProfile->profile_completion}}%</h5>
                        <p class="profile-completion-p">Profile Completion</p>
                    </div>
                </div>

                <div class="col-md-9 pull-left mobile-profile-center">
                    <h3 class="mobile-review-head">{{$Profile->name}} </h3>
                    <span class="candidate-profile-position">
                        Ca Article
                    </span>
                    <br>
                    @if($ArticleProfile->profile_completion == 100)
                    <a href="/ArticleProfile" class="profile-complete-link">Edit Profile</a>
                    @else
                    <a href="/ArticleProfile" class="profile-complete-link">Complete Profile Now</a>
                    @endif
                </div>
            </div>
        </div>
    </div>
    
    <div class="widget border filter-card-candidate-profile filter-jobs-mar-bottom">
            <h3 class="sb-title open Filters col-md-4 text-left pad-left0 mobile-filter-float-left">Filters (<span class="filter-count-font" id="AppliedCount">1</span>)</h3>
            {{-- <h3 class="sb-title open Filters filter-applied col-md-4 mobile-filter-float-left" >Applied(<span id="AppliedCount">1</span>)</h3> --}}
            <a href="javascript:void(0);" class="pull-right open Filters col-md-4 filters-clearall mobile-filters-clear-all" id="ClearAllCAJobs">Clear All</a>
            <br>
    </div>
    @if((request()->is('Article/FresherJobs')) ? 'active' : '')
    <div class="widget border filter-card-candidate-profile ">
        <h4 class="sb-title open">Date Posted</h4>
        <div class="posted_widget">
           {{--  <input type="radio" value="1" name="choose" class="CheckDatePostedFresherJobsFilters" id="232" ><label for="232">Last Hour</label><br />  --}}
           <input type="radio" value="2" name="choose" class="CheckDatePostedFresherJobsFilters" id="wwqe"><label for="wwqe">Last 24 hours</label><br />
           <input type="radio" value="3" name="choose" class="CheckDatePostedFresherJobsFilters" id="erewr"><label for="erewr">Last 7 days</label><br />
           <input type="radio" value="4" name="choose" class="CheckDatePostedFresherJobsFilters" id="all" checked><label for="all">All</label><br />
        </div>
</div>
    @else
    <div class="widget border filter-card-candidate-profile ">
        <h4 class="sb-title open">Date Posted</h4>
        <div class="posted_widget">
           {{--  <input type="radio" value="1" name="choose" class="CheckDatePostedCAJobsFilters" id="232" ><label for="232">Last Hour</label><br />  --}}
           <input type="radio" value="2" name="choose" class="CheckDatePostedCAJobsFilters" id="wwqe"><label for="wwqe">Last 24 hours</label><br />
           <input type="radio" value="3" name="choose" class="CheckDatePostedCAJobsFilters" id="erewr"><label for="erewr">Last 7 days</label><br />
           <input type="radio" value="4" name="choose" class="CheckDatePostedCAJobsFilters" id="all" checked><label for="all">All</label><br />
        </div>
    </div>
    @endif


    @if((request()->is('Article/FresherJobs')) ? 'active' : '')
    <div class="widget border filter-card-candidate-profile filter-jobs-mar-top">
        <div>
            <h3 class="sb-title open">Location</h3>
            {{-- <div class="type_widget">
                <p><input type="checkbox" class="CheckFilterTrainings" class="" value="Bangalore" name="Location" id="blr"><label for="blr" class="CheckBoxFilter">Bangalore</label></p>
                <p><input type="checkbox" class="CheckFilterTrainings" class="" value="Chennai" name="Location" id="che"><label for="che" class="CheckBoxFilter">Chennai</label></p>
            </div> --}}
            <input type="text" class="search-input-bg" id="SearchFresherJobsLocationByKeypress" placeholder="Search Location">

            <div class="type_widget" id="AppendLocation">
                {{-- <p><input type="checkbox" class="CheckManageJobFilters" class="" value="Bangalore" name="Location" id="blr"><label for="blr" class="CheckBoxFilter">Bangalore</label></p> --}}
            </div>
        </div>
    </div>
    @else
    <div class="widget border filter-card-candidate-profile filter-jobs-mar-top">
        <div>
            <h3 class="sb-title open">Location</h3>
            {{-- <div class="type_widget">
                <p><input type="checkbox" class="CheckFilterTrainings" class="" value="Bangalore" name="Location" id="blr"><label for="blr" class="CheckBoxFilter">Bangalore</label></p>
                <p><input type="checkbox" class="CheckFilterTrainings" class="" value="Chennai" name="Location" id="che"><label for="che" class="CheckBoxFilter">Chennai</label></p>
            </div> --}}
            <input type="text" class="search-input-bg" id="SearchCAJobsLocationByKeypress" placeholder="Search Location">

            <div class="type_widget" id="AppendLocation">
                {{-- <p><input type="checkbox" class="CheckManageJobFilters" class="" value="Bangalore" name="Location" id="blr"><label for="blr" class="CheckBoxFilter">Bangalore</label></p> --}}
            </div>
        </div>
    </div>
    @endif


    
    
    
    @if(!(request()->is('Article/FresherJobs')) ? 'active' : '')
    <div class="widget border filter-card-candidate-profile filter-jobs-mar-top">
        <div>
            <h3 class="sb-title open">Stipend Type</h3>
            <div class="type_widget pad-bottom10 managejobs-border-line">
                <p><input type="checkbox" class="CheckManageCAJobsFilters" value="1" name="Stipend" id="sti"><label for="sti" class="CheckBoxFilter">Stipend</label></p>

                <p><input type="checkbox" class="CheckManageCAJobsFilters" value="2" name="Stipend" id="sal"><label for="sal" class="CheckBoxFilter">Salary</label></p>
            </div>
            {{-- <div class="type_widget">
                <p><input type="checkbox" class="CheckFilterTrainings" value="1" name="Duration" id="1"><label for="1" class="CheckBoxFilter">30 Days</label></p>
                <p><input type="checkbox" class="CheckFilterTrainings" value="2" name="Duration" id="2"><label for="2" class="CheckBoxFilter">60 Days</label></p>
            </div> --}}
        </div>
    </div>
    @endif
    {{-- <div class="widget border filter-card-candidate-profile filter-jobs-mar-top">
        <div>
            <h3 class="sb-title open">Stipend</h3>
            <div class="type_widget">
                @foreach($GetStipend as $Stipend)
                    <p><input type="checkbox" class="CheckFilterTrainings" value="{{$Stipend->id}}" name="Stipend" id="{{$Stipend->training_completion}}"><label for="{{$Stipend->training_completion}}" class="CheckBoxFilter">{{$Stipend->training_completion}}</label></p>
                @endforeach
            </div>
        </div>
    </div> --}}