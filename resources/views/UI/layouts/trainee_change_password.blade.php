@extends('UI.base')

@section('Content')
<section id="scroll-here" class="bg-grey">
    <div class="block">
        <div class="container">
            <div class="row candidate-row-mar-top">
                <div class="col-lg-12 col-md-6">
                    <div class="reviews-sec candiates-card-profile-top">
						<div class="row job-details-breadcrumb">
							<div class="col-lg-12 pad-top30">
								<p class="job-details-breadcrumb-p"><i class="fa fa-home clr-primary"></i> <i class="fa fa-caret-right"></i> Change password</p>
							</div>
						</div>


						<div class="reviews-sec applied-jobs-list">
							<div class="row">
									<div class="col-lg-12">
										<div class="reviews candidates-profile-card-border mar-top15 mobile-card-pad-right">
											<div class="row">
												<div class="col-md-12">
													<h5 class="text-center">Change Password</h5>
													<div class="col-md-12 change-password-column">
														<form class="mar-top15">
															{{-- <label class="pull-left remove-label">Name
															</label> --}}
															<div class="cfield">
																<input type="password" class="bg-white input-border" placeholder="Old Password" value="" id="old_password"/>
															</div>

															<div class="cfield">
																<input type="password" class="bg-white input-border" placeholder="New Password" value="" id="new_password"/>
															</div>

															<div class="cfield">
																<input type="password" class="bg-white input-border" placeholder="Confirm Password" value="" id="verify_password"/>
															</div>

															<button type="button" class="candidate-profile-save-btn col-md-6 col-xs-6 update-password-btn" id="UpdateTraineePassword">Update
																</button>
														</form>
													</div>
												</div>
												
											</div>
										</div>
									</div>
							</div>
						</div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
