@extends('UI.base')

@section('Content')
<section class="overlape">
		<div class="block no-padding">
			<div data-velocity="-.1" style="background: url({{URL::asset('UI/images/resource/mslider1.jpg')}}) repeat scroll 50% 422.28px transparent;" class="parallax scrolly-invisible no-parallax"></div><!-- PARALLAX BACKGROUND IMAGE -->
			<div class="container fluid">
				<div class="row">
					<div class="col-lg-12">
						<div class="inner-header">
							<h3>Welcome {{Session::get('EmployerName')}}</h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section>
		<div class="block no-padding">
			<div class="container">
				 <div class="row no-gape">
                    @include('UI.common.employer_sidebar')
				 	<div class="col-lg-9 column">
				 		<div class="padding-left">
					 		<div class="profile-title">
					 			<h3>Edit Job</h3>
					 		</div>
					 		<div class="profile-form-edit">
					 			<form>
					 				<div class="row">
					 					<div class="col-lg-12">
					 						<span class="pf-title">Job Title</span>
					 						<div class="pf-field">
                                                <input type="hidden" id="id" placeholder="Designer" value="{{$GetJobsById->id}}"/>

                                                <input type="text" id="title" placeholder="Designer" value="{{$GetJobsById->title}}"/>
					 						</div>
                                         </div>
                                         <div class="col-lg-6">
                                            <span class="pf-title">Address</span>
                                            <div class="pf-field">
                                                <input type="text" id="GetAddress" placeholder="Please type address..." value="{{$GetJobsById->location}}" style="display:none;"/>

                                            <input type="text" id="address" placeholder="Please type address..." value="{{$GetJobsById->location}}"/>
                                            </div>
                                            <p class="remember-label">
                                                    <input type="checkbox" name="cb" id="CheckSameAddress"><label for="CheckSameAddress">Same as office address</label>
                                                </p>
                                        </div>
                                        <div class="col-lg-6">
                                                <span class="pf-title">Zip code</span>
                                                <div class="pf-field">
                                                    <input type="text" id="GetZipcode" placeholder="Please type address..." value="{{$GetJobsById->zip_code}}" style="display:none;"/>

                                                    <input type="text" onchange="CheckZipCode()" id="zipcode" placeholder="Please type zipcode..." value="{{$GetJobsById->zip_code}}"/>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <span class="pf-title">Country</span>
                                                <div class="pf-field">
                                                    <input type="text" id="GetCountry" placeholder="Please type address..." value="{{$GetJobsById->country}}" style="display:none;"/>

                                                    <input type="text" id="country" placeholder="Eg: India" value="{{$GetJobsById->country}}"/>
                                                </div>
                                            </div>
                                            <div class="col-lg-3">
                                                <span class="pf-title">State</span>
                                                <div class="pf-field">
                                                    <input type="text" id="GetState" placeholder="Please type address..." value="{{$GetJobsById->state}}" style="display:none;"/>

                                                    <input type="text" id="state" placeholder="Eg: Karnataga" value="{{$GetJobsById->state}}"/>
                                                </div>
                                            </div>
                                            <div class="col-lg-3">
                                                <span class="pf-title">City</span>
                                                <div class="pf-field">
                                                    <input type="text" id="GetCity" placeholder="Please type address..." value="{{$GetJobsById->city}}" style="display:none;"/>

                                                    <input type="text" id="city" placeholder="Eg: Bangalore" value="{{$GetJobsById->city}}"/>
                                                </div>
                                            </div>

                                        <div class="col-lg-6">
                                            <span class="pf-title">Email</span>
                                            <div class="pf-field">
                                                
                                                <input type="text" id="email" placeholder="Please type email" value="{{Session::get('EmployerEmail')}}"/>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <span class="pf-title">Mobile</span>
                                            <div class="pf-field">
                                                <input type="text" id="mobile" placeholder="Please type mobile" value="{{$GetJobsById->mobile}}"/>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <span class="pf-title">Job Type</span>
                                            <div class="pf-field">
                                                <select data-placeholder="Please Select Job Type" id="job_type" class="chosen">
                                                    <option selected>Select job type</option>
                                                    <option value="1">Full Time</option>
                                                    <option value="2">Part time</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <span class="pf-title">Industry</span>
                                            <div class="pf-field">
                                                <select data-placeholder="Please Select Industry" id="industry" class="chosen">
                                                    <option selected>Select industry</option>
                                                    <option>Software Design</option>
                                                    <option>Software Developement</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <span class="pf-title">Role</span>
                                            <div class="pf-field">
                                                <input type="text" value="{{$GetJobsById->role}}" data-role="tagsinput" id="role"/>
                                                {{-- <select data-placeholder="Please Select Role" id="role" class="chosen" multiple>
                                                    <option>UI Designer</option>
                                                    <option>UI Developer</option>
                                                    <option>PHP</option>
                                                    <option>React</option>
                                                </select> --}}
                                            </div>
                                        </div>

                                        <div class="col-lg-3">
                                            <span class="pf-title">Type</span>
                                            <div class="pf-field">
                                                <select data-placeholder="Please Select Salary Type" class="chosen" id="salary_type">
                                                    <option value="1">Per month</option>
                                                    <option value="2">Per year</option>
                                                </select>
                                            </div>
                                        </div>
                                        
                                        <div class="col-lg-3">
                                            <span class="pf-title">Offered Salary - (Min)</span>
                                            <div class="pf-field">
                                            <input type="text" id="salary_min" value="{{$GetSalaries[0]}}">
                                            </div>
                                        </div>

                                        <div class="col-lg-3">
                                            <span class="pf-title">Max</span>
                                            <div class="pf-field">
                                                <input type="text" id="salary_max" value="{{$GetSalaries[1]}}">
                                            </div>
                                        </div>
                                        
                                        <div class="col-lg-3">
                                            <span class="pf-title">Gender</span>
                                            <div class="pf-field">
                                                <select data-placeholder="Please Select Gender" class="chosen" id="gender">
                                                    <option selected>Select gender</option>
                                                    <option value="Male">Male</option>
                                                    <option value="Female">Female</option>
                                                    <option value="Both">Both</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-lg-4">
                                            <span class="pf-title">Career Level</span>
                                            <div class="pf-field">
                                                <select data-placeholder="Please Select Career Level" id="career_level" class="chosen" onchange="CheckCareerLevel()">
                                                    <option value="1">Fresher</option>
                                                    <option value="2">Experience</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-2">
                                            <span class="pf-title">Experience (Years)</span>
                                            <div class="pf-field">
                                                <input type="text" id="experience_year" disabled style="background-color: #8080803b;">
                                            </div>
                                        </div>
                                        <div class="col-lg-2">
                                            <span class="pf-title">Months</span>
                                            <div class="pf-field">
                                                <input type="text" id="experience_month" disabled style="background-color: #8080803b;">
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <span class="pf-title">Applicant Last Date</span>
                                            <div class="pf-field">
                                                <input type="date" placeholder="01.11.207"  class="form-control datepicker" id="last_date" value="{{$GetJobsById->job_expires}}"/>
                                            </div>
                                        </div>
                                        <div class="col-lg-10">
                                            <span class="pf-title">Qualification</span>
                                            <div class="pf-field">
                                                <input type="text" value="{{$GetJobsById->qualification}}" data-role="tagsinput" id="qualification"/>
                                                {{-- <select data-placeholder="Please Select Qualification" id="qualification" class="chosen" multiple>
                                                    <option>BE</option>
                                                    <option>M.sc</option>
                                                    <option>BCA</option>
                                                    <option>MCA</option>
                                                </select> --}}
                                            </div>
                                        </div>
                                        <div class="col-lg-2">
                                            <span class="pf-title">Job Positions</span>
                                            <div class="pf-field">
                                                <input type="text" id="positions" value="{{$GetJobsById->total_positions}}">
                                            </div>
                                        </div>
					 					<div class="col-lg-12">
					 						<span class="pf-title">Description</span>
					 						<div class="pf-field">
					 							<textarea id="description">Spent several years working on sheep on Wall Street. Had moderate success investing in Yugos on Wall Street. Managed a small team buying and selling pogo sticks for farmers. Spent several years licensing licorice in West Palm Beach, FL. Developed severalnew methods for working with banjos in the aftermarket. Spent a weekend importing banjos in West Palm Beach, FL.In this position, the Software Engineer ollaborates with Evention's Development team to continuously enhance our current software solutions as well as create new solutions to eliminate the back-office operations and management challenges present</textarea>
					 						</div>
					 					</div>
					 					

					 				</div>
					 			</form>
					 		</div>
					 		<div class="contact-edit">
					 			<form>
					 				<div class="row">
					 					<div class="col-lg-12">
					 						<button type="button" id="UpdateJobs">Update a job</button>
					 					</div>
					 				</div>
					 			</form>
					 		</div>
					 	</div>
					</div>
				 </div>
			</div>
		</div>
	</section>
@endsection