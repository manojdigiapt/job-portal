@extends('UI.base')

@section('Content')
<section id="scroll-here" class="bg-grey">
    <div class="block">
        <div class="container">
            <div class="row candidate-row-mar-top">
                <div class="col-lg-12 col-md-6">
                    <div class="reviews-sec candiates-card-profile-top">
						<div class="row job-details-breadcrumb">
							<div class="col-lg-12 pad-top30">
								<p class="job-details-breadcrumb-p"><i class="fa fa-home clr-primary"></i> <i class="fa fa-caret-right"></i> Applied jobs</p>
							</div>
						</div>

						<div class="row">
							<div class="col-lg-12 candidate-card-pad-left-right">
								<div class="reviews candidates-profile-card-border mar-top15 mobile-card-pad-right">
									<div class="row">
										
										<div class="col-md-3 pull-left mobile-job-details-align-center candidate-details-mar-top">
											<h3 class="mobile-review-head applied-jobs-head"> <img src="{{URL::asset('UI/tick.svg')}}" class="applied-jobs-img" alt=""> {{count($GetAppliedJobs)}} </h3>
											<p class="applied-jobs-total">Total Applied</p>
										</div>

										<div class="col-md-3 pull-left mobile-job-details-align-center candidate-details-mar-top">
											<h3 class="mobile-review-head applied-jobs-head"> <img src="{{URL::asset('UI/eye.svg')}}" class="applied-jobs-img" alt=""> {{$GetRecruiterViewed}} </h3>
											<p class="applied-jobs-total">Recruiter Viewed</p>
										</div>

										<div class="col-md-3 pull-left mobile-job-details-align-center candidate-details-mar-top">
											<h3 class="mobile-review-head applied-jobs-head"> <img src="{{URL::asset('UI/download.svg')}}" class="applied-jobs-img" alt=""> {{$GetApplicationDownload}} </h3>
											<p class="applied-jobs-total">Resume Downloaded</p>
										</div>

										<div class="col-md-3 pull-left mobile-job-details-align-center candidate-details-mar-top">
											<p class="applied-jobs-total-days">Jobs Applied Last 90 Days</p>
										</div>
									</div>
									
								</div>
							</div>
						</div>

						<div class="filterbar applied-jobs-head-sortby mar-bottom0">
							{{-- <div class="sortby-sec candidate-profile-sortby">
								<select data-placeholder="20 Per Page" class="chosen">
									<option selected disabled>Sort by</option>
									<option>40 Per Page</option>
									<option>50 Per Page</option>
									<option>60 Per Page</option>
								</select>
							</div> --}}
							<h5>Applied Jobs List</h5>
						</div>

						<div class="reviews-sec applied-jobs-list">
							<div class="row">
								@foreach($GetAppliedJobs as $Jobs)
									<div class="col-lg-12">
										<div class="reviews candidates-profile-card-border mar-top15 mobile-card-pad-right">
											<div class="row">
												
												<div class="col-md-12 pull-left pad-left30 mobile-job-details-align-center">
													<h3 class="mobile-review-head">{{$Jobs->title}} </h3>
													<span class="candidate-profile-position">
															{{$Jobs->company_name}}
													</span> 
													<br>
													<p class="job-details-attributes applied-jobs-attributes-width"><i class="fa fa-briefcase"> {{$Jobs->experience}} Years</i></p>
				
													<p class="job-details-attributes applied-jobs-attributes-width"><i class="fa fa-briefcase"> 
														@if($Jobs->job_type == 1)
															Part time
														@else
															Full time
														@endif
													</i></p>
				
													<p class="job-details-attributes applied-jobs-attributes-width"><i class="fa fa-map-marker"> {{$Jobs->city}}</i></p>

													{{-- <p class="job-details-attributes applied-jobs-attributes-width"><i class="fa fa-industry"> {{$Jobs->name}}</i></p> --}}
												</div>
												
												<div class="row pull-left pad-left30 mobile-job-details-align-center">
													<div class="@if($Jobs->recruiter_status == 0)
													col-md-8
													@elseif($Jobs->recruiter_status == 1 && $Jobs->download_cv_status == 1)
													col-md-2
													@else
													col-md-3
													@endif
													applied-jobs-recruiter-status-pad pad-right0">
														<span class="applied-jobs-recruiter-dot">.</span> <span class="applied-jobs-border">Applied</span>
														{{--  <hr class="applied-jobs-line">  --}}
													<p class="applied-jobs-date">{{date('d M Y', strtotime($Jobs->created_at))}}</p>
													</div>
													@if($Jobs->recruiter_status == 1)
													<div class="@if($Jobs->recruiter_status == 1 && $Jobs->download_cv_status == 0)
													col-md-2 
													@else
													col-md-1
													@endif
													applied-jobs-recruiter-status-pad applied-jobs-border-line ">
														<p class="applied-jobs-border-width"></p>
													</div>

													<div class="@if($Jobs->recruiter_status == 1 && $Jobs->download_cv_status == 0)
													col-md-6 
													@else
													col-md-4
													@endif applied-jobs-recruiter-status-pad pad-right0">
														<span class="applied-jobs-recruiter-dot">.</span> <span class="applied-jobs-border">Application Viewed</span>
														{{--  <hr class="applied-jobs-line">  --}}
														<p class="applied-jobs-date">{{date('d M Y', strtotime($Jobs->application_view_date))}}</p>
													</div>
													@endif

													@if($Jobs->download_cv_status == 1)
													<div class="col-md-1 applied-jobs-recruiter-status-pad applied-jobs-border-line application-viewed-mar-left">
														<p class="applied-jobs-border-width"></p>
													</div>

													<div class="col-md-4 applied-jobs-recruiter-status-pad">
														<span class="applied-jobs-recruiter-dot">.</span> <span class="applied-jobs-border">Resume Downloaded</span>

														<p class="applied-jobs-date">{{date('d M Y', strtotime($Jobs->download_cv_date))}}</p>
													</div>
													@endif
												</div>
											</div>
											
											
										</div>
									</div>
								@endforeach
							</div>
						</div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
