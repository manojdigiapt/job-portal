@extends('UI.base')

@section('Content')
<section>
    <div class="block no-padding">
        <div class="container fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="main-featured-sec style2 mobile-height-sec">
                        <ul class="main-slider-sec style2 text-arrows home-img candidate-dashboard-banner-height mobile-height-slider">
                            <li class="slideHome"><img src="{{URL::asset('UI/images/resource/mslider3.jpg')}}" alt="" /></li>
                        </ul>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="scroll-here" class="bg-grey">
    <div class="block">
        <div class="container">
            <div class="row candidate-row-mar-top">
                <div class="col-lg-4 col-md-6 search-jobs-left-pad-top">
                    
                    <div class="widget border filter-card-candidate-profile filter-jobs-mar-bottom">
                            <h3 class="sb-title open Filters col-md-4 text-left pad-left0 mobile-filter-float-left">Filters (<span class="filter-count-font" id="AppliedCount">1</span>)</h3>
                            {{-- <h3 class="sb-title open Filters filter-applied col-md-4 mobile-filter-float-left" >Applied(<span id="AppliedCount">1</span>)</h3> --}}
                            <a href="javascript:void(0);" class="pull-right open Filters col-md-4 filters-clearall mobile-filters-clear-all" id="ClearAll">Clear All</a>
                            <br>
                    </div>
                    <div class="widget border filter-card-candidate-profile ">
                            <h4 class="sb-title open">Date Posted</h4>
                            <div class="posted_widget">
                               {{--  <input type="radio" value="1" name="choose" class="CheckDatePostedJobFilters" id="232" checked><label for="232">Last Hour</label><br />  --}}
                               <input type="radio" value="2" name="choose" class="CheckDatePostedJobFilters" id="wwqe"><label for="wwqe">Last 24 hours</label><br />
                               <input type="radio" value="3" name="choose" class="CheckDatePostedJobFilters" id="erewr"><label for="erewr">Last 7 days</label><br />
                               <input type="radio" value="4" name="choose" class="CheckDatePostedJobFilters" id="all"><label for="all">All</label><br />
                            </div>
                    </div>
                    <div class="widget border filter-card-candidate-profile filter-jobs-mar-top">
                        <div>
                            <h3 class="sb-title open">Location</h3>
                            <input type="text" class="search-input-bg" id="SearchJobsLocationByKeypress" placeholder="Search Location">
                
                            <div class="type_widget pad-bottom10 managejobs-border-line" id="AppendLocation">
                                {{-- <p><input type="checkbox" class="CheckManageJobFilters" class="" value="Bangalore" name="Location" id="blr"><label for="blr" class="CheckBoxFilter">Bangalore</label></p> --}}
                            </div>
                            {{-- <div class="type_widget">
                                <p><input type="checkbox" class="CheckJobFilters" class="" value="Bangalore" name="Location" id="blr"><label for="blr" class="CheckBoxFilter">Bangalore</label></p>
                            </div> --}}
                        </div>
                    </div>
                    {{-- <div class="widget border filter-card-candidate-profile filter-jobs-mar-top">
                        <div>
                            <h3 class="sb-title open">Salary</h3>
                            <div class="type_widget">
                                @foreach($GetJobType as $JobType)
                                    <p><input type="checkbox" value="{{$JobType->id}}" name="spealism" id="{{$JobType->id}}"><label for="{{$JobType->id}}" class="CheckBoxFilter">{{$JobType->job_type}}</label></p>
                                @endforeach
                            </div>
                        </div>
                    </div> --}}
                    <div class="widget border filter-card-candidate-profile filter-jobs-mar-top">
                        <div>
                            <h3 class="sb-title open">Job Titles</h3>
                            <div class="type_widget">
                                @foreach($GetJobTitle as $Title)
                                    <p><input type="checkbox" class="CheckJobFilters" value="{{$Title->id}}" name="Title" id="{{$Title->name}}"><label for="{{$Title->name}}" class="CheckBoxFilter">{{$Title->name}}</label></p>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-md-6 recommended-jobs-list">
                    <div class="filterbar candidate-profile-filter-pad">
                        {{-- <div class="sortby-sec candidate-profile-sortby">
                            <select data-placeholder="20 Per Page"  class="SortByJobs chosen">
                                <option selected disabled>Sort by</option>
                                <option value="1">Date</option>
                                <option value="2">Relevance</option>
                            </select>
                        </div>
                        <h5><span id="JobsCount">{{count($GetJobsBySlug)}}</span> recommended jobs</h5> --}}
                    </div>

                    <div class="job-list-modern text-center">
                            <img id="AjaxLoader" class="jobs-search-loader" src="{{URL::asset('UI/ajax_loader.gif')}}" alt="" style="display:none;">
                            <div class="job-listings-sec no-border text-center" id="SearchJobsResults">
                                @foreach($GetJobsBySlug as $Jobs)
                                
                               <div class="job-listing wtabs recommented-jobs">
                                   <div class="job-title-sec recommended-jobs-pad-left">
                                        <h3><a href="/JobDetails/{{$Jobs->CompanySlug}}/{{$Jobs->id}}/{{$Jobs->slug}}" target="_blank" title="">{{$Jobs->title}}</a></h3>
                                       <p class="recommended-jobs-company-name">{{$Jobs->company_name}}</p>
                                       <div class="job-lctn job-attributes-recommended-jobs"><i class="fa fa-briefcase"></i>{{$Jobs->experience}} Years</div>
                                       <div class="job-lctn job-attributes-recommended-jobs"><i class="fa fa-user-circle"></i>
                                            @if($Jobs->job_type == 1)
                                                Part time
                                            @else
                                                Full time
                                            @endif
                                        </div>
                                       <div class="job-lctn job-attributes-recommended-jobs"><i class="fa fa-map-marker" ></i> {{$Jobs->city}}</div>
                                       
                                    </div>
                                <div class="recommended-jobs-description">{!!implode(' ', array_slice(explode(' ', $Jobs->job_description), 0, 20))!!}</div>
                                    <p class="recommended-jobs-posted-on">Posted on: 
                                            @php 
                                            $now = new DateTime;
$full = false;	
$ago = new DateTime($Jobs->created_at);
$diff = $now->diff($ago);

$diff->w = floor($diff->d / 7);
$diff->d -= $diff->w * 7;

$string = array(
'y' => 'year',
'm' => 'month',
'w' => 'week',
'd' => 'day',
'h' => 'hour',
'i' => 'minute',
's' => 'second',
);
foreach ($string as $k => &$v) {
if ($diff->$k) {
$v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
} else {
unset($string[$k]);
}
}

if (!$full) $string = array_slice($string, 0, 1);
echo $string ? implode(', ', $string) . ' ago' : 'just now';
                                        @endphp
                                    </p>
                               </div>
                               @endforeach
                           </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection