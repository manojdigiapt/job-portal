@extends('UI.base')

@section('Content')
<section class="overlape">
    <div class="block no-padding">
        <div data-velocity="-.1" style="background: url({{URL::asset('UI/images/resource/mslider1.jpg')}} repeat scroll 50% 422.28px transparent;" class="parallax scrolly-invisible no-parallax"></div><!-- PARALLAX BACKGROUND IMAGE -->
        <div class="container fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="inner-header">
                        <h3>Welcome {{Session::get('EmployerName')}}</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="block no-padding">
        <div class="container">
             <div class="row no-gape">
                {{-- @include('UI.common.employer_sidebar') --}}
                 <div class="col-lg-12 column">
                     <div class="padding-left">
                            <div class="contact-edit">
                                    <form>
                                        <div class="col-lg-4">
                                            <button type="button" onclick="window.location.href='/Employer/Dashboard'" style="float:left;">Back to dashboard</button>
                                        </div>
                                        <div class="col-lg-8">
                                        </div>
                                    </form>
                                </div>
                         <div class="manage-jobs-sec">
                             <h3>Selected Trainees</h3>
                             <div class="extra-job-info">
                             <span><i class="la la-clock-o"></i><strong>{{count($GetTrainingByEmployerId)}}</strong> Training Posted</span>
                             <span><i class="la la-file-text"></i><strong>{{count($GetCandidates)}}</strong> Selected Students</span>
                                 <span><i class="la la-users"></i><strong>
                                    {{count($GetActiveTrainingsCount)}}
                                </strong> Completed Training</span>
                             </div>
                             <table>
                                 <thead>
                                     <tr>
                                         <td>Title</td>
                                         <td>Trainee Name</td>
                                         <td>Start date</td>
                                         <td>End date</td>
                                         <td>Training Status</td>
                                         <td>Add Reviews</td>
                                         <td>Action</td>
                                     </tr>
                                 </thead>
                                 <tbody>
                                     @foreach($GetCandidates as $Trainings)
                                     <tr>
                                         <td>
                                             <div class="table-list-title">
                                             <h3><a href="#" title="">{{$Trainings->title}}</a></h3>
                                            <span><i class="la la-clock-o"></i>{{$Trainings->duration}} days</span>
                                             </div>
                                         </td>
                                         <td>
                                         <h5>{{$Trainings->name}}</h5>
                                         </td>
                                         <td>
                                             {{--  <span>October 27, 2017</span><br />  --}}
                                         <span>{{date('m-d-Y', strtotime($Trainings->training_start_date))}}</span>
                                         </td>
                                         <td>
                                            <span>{{date('m-d-Y', strtotime($Trainings->training_end_date))}}</span>
                                        </td>
                                        <td>
                                            <span class="status active">
                                                @if($Trainings->training_status == 1)
                                                <span class="badge badge-success badge-font">Completed</span>
                                                @else
                                                <span class="badge badge-danger badge-font">Not Completed</span>
                                                @endif
                                            </span>
                                        </td>

                                        <td>
                                            <div class="resumeadd-form add-reviews">
                                                <button type="button" onclick="AddReviews({{$Trainings->CandidateId}}, {{$Trainings->TrainingId}})">Add</button>
                                            </div>
                                        </td>
                                         
                                         <td>
                                             <ul class="action_job">
                                             <li><span>View Training</span><a href="/TrainingDetails/{{$Trainings->CompanySlug}}/{{$Trainings->slug}}" target="_blank" title=""><i class="la la-eye"></i></a></li>
                                    
                                            {{-- <li><span>Delete</span><a href="#" title=""><i class="la la-trash-o"></i></a></li> --}}
                                             </ul>
                                         </td>
                                     </tr>
                                     @endforeach
                                     
                                 </tbody>
                             </table>
                         </div>
                     </div>
                </div>
             </div>
        </div>
    </div>
</section>

<div id="AddReviews" class="modal fade" role="dialog">
        <div class="modal-dialog modal-md">
      
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              {{-- <h4 class="modal-title">Modal Header</h4> --}}
            </div>
            <div class="modal-body">
                    <div class="account-popup select-trining-popup ratings-popup">
                            <form>
                                <p class="text-left">Select Ratings</p>
                                <div class="rating-star">
                                    <span data-id="001" data-val="1"></span>
                                    <span data-id="002" data-val="2"></span>
                                    <span data-id="003" data-val="3"></span>
                                    <span data-id="004" data-val="4"></span>
                                    <span data-id="005" data-val="5"></span>
                                </div>
                                <input type="hidden" id="RatingStars">
                                @foreach($GetCandidates as $Trainings)
                                <input type="hidden" id="TrainingId" value="{{$Trainings->TrainingId}}">
                                <input type="hidden" id="CandidateId" value="{{$Trainings->CandidateId}}"> 
                                @endforeach
                                <br>
                                <div class="cfield">
                                    <textarea id="feedback" placeholder="Type Feedback"></textarea>
                                    <i class="la la-user"></i>
                                </div>
                                <button type="button" id="SubmitRatings">Submit</button>
                            </form>
                        </div>
            </div>
          </div>
      
        </div>
      </div>
@endsection


@section('JSScript')
<script src="https://www.jqueryscript.net/demo/Product-Rating-ArtaraxRatingStar/artarax-rating-star/jquery.artarax.rating.star.js"></script>

<script>
$(function () {

var artaraxRatingStar = $.artaraxRatingStar({
    onClickCallBack: onRatingStar
});

function onRatingStar(rate, id) {
    // alert("data-val(rate)=" + rate + " data-id(ProductId)=" + id);
    $("#RatingStars").val(rate);
}

});
</script>
@endsection