@extends('UI.base')

@section('Content')
<section id="scroll-here" class="bg-grey">
    <div class="block">
        <div class="container">
            <div class="row candidate-row-mar-top">
                <div class="col-lg-12 col-md-6">
                    <div class="reviews-sec candiates-card-profile-top">
						<div class="row job-details-breadcrumb">
							<div class="col-lg-12 pad-top30">
									<p class="job-details-breadcrumb-p"><i class="fa fa-home clr-primary"></i> <i class="fa fa-caret-right"></i> <a href="/Employer/Dashboard" class="">Dashboard</a></p>
							</div>
						</div>

						<div class="row">
							<div class="col-lg-2">

							</div>
							<div class="col-lg-8">
								<div class="reviews candidates-profile-card-border jobs-description-padding job-description-mar-top30 applied-jobs-card-width-pad recent-candidates-row-width96 review-position">
									<div class="row candidate-border-bottom">
										<p class="candidate-applications-pad col-md-6 text-center"><a href="/ViewAllCandidates" class="training-completion-head">Candidate Applications </a>({{count($GetAppliedCandidates)}})</p>

										<p class="candidate-applications-pad col-md-6"><a href="/ViewAllTrainings" >Training Applications  </a></p>
										
										<p class="training-completion-border-bottom candidates-view-all-border-bottom"></p>
									</div>

									<div class="row">
										{{--  <div class="posted_widget">
											<input type="text" class="search-input-bg view-all-search-input" id="JobsName" placeholder="Search Keyword">

											<a href="javascript:void(0);" onclick="SearchAppliedCandidates()"><i class="fa fa-search view-all-input-search-icon"></i></a>
										</div>  --}}

										<div class="text-center loader-center-mar0auto">
											<img id="AjaxLoader" class="appliedtrainees-loader-width" src="{{URL::asset('UI/ajax_loader.gif')}}" alt="" style="display:none;">
										</div>

										<div class="col-md-12 view-all-candidate-top-pad" id="AppliedCandidates">
											@foreach($GetAppliedCandidates as $Candidates)
												<div class="col-md-1 pull-left">
												<img src="{{URL::asset('candidate_profile')}}/{{$Candidates->profile_pic}}" alt="">
												</div>
												
												<div class="col-md-8 pull-left">
												<h4 class="recent-candidate-name">{{$Candidates->name}}</h4>
													<p class="pad-left10 col-md-6 max-width100">Job title : {{$Candidates->title}}</p>
												</div>
	
												<div class="col-md-3 pull-left">
												<p class="col-md-6 text-right recent-candidates-date view-all-training-date">{{date('d M Y', strtotime($Candidates->created_at))}}</p>
												</div>
	
												<hr class="view-all-border-bottom">
	
											@endforeach
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-2">
							
							</div>
						</div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</section>


{{--  Post New Jobs  --}}
<div class="account-popup-area post-jobs-popup-box">
		<div class="account-popup post_new_jobs_popup">
			<span class="close-popup" id="register_close"><i class="la la-close"></i></span>
			<h3>Post a New Job</h3>
			<form class="mar-top15">
				<div class="row">
					<div class="dropdown-field col-md-5 pad-left0 pad-right0 mar-right10">
						<label class="pull-left remove-label">Industry *
						</label>
						<select data-placeholder="Please Select Industries" class="chosen" id="industry">
							<option selected>Select industry
								</option>
							<option value="1">Demo
							</option>
						</select>
					</div>
	
					<div class="dropdown-field col-md-5 pad-left0 pad-right0">
						<label class="pull-left remove-label">Job Type *
							</label>
						<select  data-placeholder="Please Select Functional Areas" class="chosen" id="job_type">
							<option selected>Select job type</option>
							<option value="1">Part time
							</option>
							<option value="2">Full time
								</option>
						</select>
					</div>
				</div>
				
				<div class="row">
					<div class="dropdown-field col-md-12 pad-left0 pad-right0 mar-right10">
						<label class="pull-left remove-label">Job Title *
							</label>
						<div class="cfield">
							<input type="text" placeholder="UI Designer" value="" class="bg-white " id="title"/>
						</div>
					</div>
				</div>
	
				<div class="row">
					<div class="col-md-12 pad-left0 pad-right0 mar-right10">
						<label class="pull-left remove-label">Job Description
							</label>
						<div class="cfield">
							<textarea name="" id="description" class="bg-white"></textarea>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="dropdown-field col-md-12 pad-left0 pad-right0 mar-right10">
						<label class="pull-left remove-label">Qualification
							</label>
						<div class="cfield">
							<input type="text" placeholder="Qualification" value="" class="bg-white " id="qualification"/>
						</div>
					</div>
				</div>
	
				<h4 class="location-head">Location --------------------------------------------------</h4>
	
				<div class="row">
					<div class="col-md-4 pad-left0 pad-right0 mar-right5">
						<label class="pull-left remove-label">Zip Code
							</label>
						<div class="cfield">
							<input type="text" placeholder="Zipcode" value="" class="bg-white " id="zipcode"/>
						</div>
					</div>
	
					<div class="col-md-7 pad-left0 pad-right0">
						<label class="pull-left remove-label">Country
							</label>
						<div class="cfield">
							<input type="text" placeholder="Country" value="" class="bg-white " id="country"/>
						</div>
					</div>
				</div>
	
				<div class="row">
					<div class="col-md-5 pad-left0 pad-right0 mar-right5">
						<label class="pull-left remove-label">State
							</label>
						<div class="cfield">
							<input type="text" placeholder="State" value="" class="bg-white " id="state"/>
						</div>
					</div>
	
					<div class="col-md-5 pad-left0 pad-right0">
						<label class="pull-left remove-label">City
							</label>
						<div class="cfield">
							<input type="text" placeholder="City" value="" class="bg-white " id="city"/>
						</div>
					</div>
				</div>
	
				<h4 class="location-head">Salary and Experience -----------------------</h4>
	
				<div class="row">
					<div class="col-md-4 pad-left0 pad-right0 salary-column-width">
						<label class="pull-left remove-label">Salary Min
							</label>
						<div class="cfield">
							<input type="text" placeholder="Salary Min" value="" class="bg-white " id="salary_min"/>
						</div>
					</div>
	
					<div class="col-md-4 pad-left0 pad-right0 salary-column-width">
						<label class="pull-left remove-label">Salary Max
							</label>
						<div class="cfield">
							<input type="text" placeholder="Salary Max" value="" class="bg-white " id="salary_max"/>
						</div>
					</div>
					<div class="col-md-4 pad-left0 pad-right0 salary-column-width">
						<label class="pull-left remove-label">Salary Type *
							</label>
						<select  data-placeholder="Please Select Functional Areas" class="chosen" id="salary_type">
							<option value="1">Monthly
							</option>
							<option value="2">Yearly
							</option>
						</select>
					</div>
				</div>
	
				<div class="row">
					<div class="col-md-7 pad-left0 pad-right0 mar-right5">
						<label class="pull-left remove-label">Experience (Years)
							</label>
						<div class="cfield">
							<input type="text" placeholder="Experience" value="" class="bg-white " id="experience"/>
						</div>
					</div>
	
					<div class="col-md-4 pad-left0 pad-right0">
						<label class="pull-left remove-label">Position
							</label>
						<div class="cfield">
							<input type="text" placeholder="Position" value="" class="bg-white " id="positions"/>
						</div>
					</div>
				</div>
	
				<div class="row">
					<div class="col-md-7">
						<button type="button" class="btn btn-default candidate-profile-cancel-btn text-right" data-dismiss="modal">Close
							</button>
					</div>
					<div class="col-md-5">
						<button type="button" id="PostJobs" class="btn btn-default candidate-profile-save-btn">Save
							</button>
					</div>
	
				</div>
			  </form>
		</div>
	</div>
	<!-- Post New Jobs POPUP -->
@endsection
