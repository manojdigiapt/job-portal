@extends('UI.base')
@section('Content')
<section id="scroll-here">
  <div class="block caniddate-profile-pad-lef-right-top">
    <div class="container">
      <div class="row">
        <div class="col-lg-4 col-md-6 candidate-profile-card">
          <div class="col-md-3 pull-left pad-left5">
            @if($CandidateProfile['profile_pic'])
            <img id="CandidateProfilePic" src="{{URL::asset('candidate_profile/')}}/{{$CandidateProfile->profile_pic}}" class="candidate-profile-pic" alt="" />
            @else
            <img id="CandidateProfilePic" src="{{URL::asset('UI/images/resource/r1.jpg')}}" class="candidate-profile-pic" alt="" />
            @endif
            <input type="file" name="file" id="file" value="" style="display:none;">
            <p class="camera-candidate-profile-bg">
              <a href="javascript:void(0);" onclick="BrowseCandidatePic('file');">
                <i class="fa fa-camera candidate-profile-camera-icon" aria-hidden="true">
                </i>
              </a>
            </p>
          </div>
          <div class="col-md-9 candidate-profile-card-pad-left">
            <h5 class="candidate-name">{{$Profile->name}}
            </h5>
            @if($CandidateProfile->id)
              <input type="hidden" id="pro_id" value="{{$CandidateProfile->id}}">
            <a href="javascript:void(0);" class="clr-white candidate-profile-card-pencil EditCandidateProfile">
              <i class="fa fa-pencil">
              </i>
            </a>
            @else
            <a href="javascript:void(0);" class="clr-white candidate-profile-card-pencil CandidateProfile">
                <i class="fa fa-pencil">
                </i>
              </a>
            @endif
            <p class="candidate-profile-card-position">
              @if($GetExperiencePosition)
                {{$GetExperiencePosition->name}}
              @else
                <a href="javascript:void(0);" class="EditCandidateProfile">Update Desgnation</a>
              @endif

            </p>
            <p class="candidate-profile-card-position">
                @if($GetExperiencePosition)
                {{$GetExperiencePosition->CompanyName}}
              @else
              <a href="javascript:void(0);" class="EditCandidateProfile">Update Company name </a>
              @endif
            </p>
            <p class="candidate-profile-completion-p">Profile Completion
            </p>
            <p class="candiate-profile-percentage">
              @if($CandidateProfile->profile_completion)
                {{$CandidateProfile->profile_completion}}%
              @else
                0%
              @endif
            </p>
            <div id="progressbar">
              <div style="width: @if($CandidateProfile->profile_completion)
                  {{$CandidateProfile->profile_completion}}%
                @else
                  
                @endif;">
                </div>
              
            </div>
            <br>
            <p class="clr-white candidate-profile-card-details">
              <i class="fa fa-map-marker pad-right5" aria-hidden="true">
              </i>
              @if($CandidateProfile->desired_location)
              {{$CandidateProfile->desired_location}}
              @else
              <a href="javascript:void(0);" class="EditCandidateProfile">Update Location</a>
              @endif
            </p>
            <p class="clr-white candidate-profile-card-details">
              <i class="fa fa-briefcase pad-right5" aria-hidden="true">
              </i> 
              @if(isset($CandidateProfile->experience))
                @php
                    $TotalExperience = $CandidateProfile->experience;
                    $GetTotalExperience = explode(',', $TotalExperience);

                    echo $GetTotalExperience[0].' Years '.$GetTotalExperience[1].' Months';
                @endphp

                @else
                <a href="javascript:void(0);" class="EditCandidateProfile">Update Experience</a>
              @endif
            </p>
            <p class="clr-white candidate-profile-card-details">
              <i class="fa fa-money pad-right5" aria-hidden="true">
              </i> 
              @if($CandidateProfile->gross_salary)
              Rs. {{$CandidateProfile->gross_salary}}
              @else
              <a href="javascript:void(0);" class="EditCandidateProfile">Update Salary</a>
              @endif
            </p>
            <br>
            <p class="clr-white candidate-profile-card-details">
              <i class="fa fa-phone pad-right5" aria-hidden="true">
              </i> {{$Profile->mobile}}
            </p>
            <p class="clr-white candidate-profile-card-details">
              <i class="fa fa-envelope pad-right5" aria-hidden="true">
              </i> {{$Profile->email}}
            </p>
            {{--  <ul class="social candidate-profile-card-social-padtop">
              <li class="social-fb"> 
                <a href="#"> 
                  <i class=" fa fa-facebook">   
                  </i> 
                </a> 
              </li>
              <li class="social-twitter"> 
                <a href="#"> 
                  <i class="fa fa-twitter">   
                  </i> 
                </a> 
              </li>
              <li class="social-instagram"> 
                <a href="#"> 
                  <i class="fa fa-instagram">   
                  </i> 
                </a> 
              </li>
            </ul>  --}}
          </div>
        </div>
        <div class="col-lg-8 col-md-6">
          <div class="job-list-modern">
            <div class="caniddate-profile-resume">
              <h3 class="candidate-profile-head">My Resume (pdf file only)
              </h3>
              @if($GetCV)
              <a href="/candidate_cv/{{$GetCV->cv}}" target="_blank" class="pull-right preview-cv-link">
                <i class="fa fa-eye">
                </i> Preview CV
              </a>
              @endif
              <div class="uploadbox">
                @if($GetCV)
                <a href="javascript:void(0);" onclick="BrowseCandidateCV('cv')">
                  <label for="file-upload" class="custom-file-upload">
                    <i class="la la-cloud-upload">
                    </i> 
                    <span>Edit Resume
                    </span>
                  </label>
                </a>
                @else
                <a href="javascript:void(0);" onclick="BrowseCandidateCV('cv')">
                    <label for="file-upload" class="custom-file-upload">
                      <i class="la la-cloud-upload">
                      </i> 
                      <span>Upload Resume
                      </span>
                    </label>
                  </a>
                @endif
                @if($GetCV)
                <input id="cv_id" type="text" value="{{$GetCV["id"]}}" style="display: none;">
                @endif
                <input id="cv" type="file" style="display: none;">
              </div>
              {{-- <h3 class="candidate-profile-sub-head">Resume Headline
              </h3>
              @if($GetCV["id"])
              <a href="javascript:void(0);" onclick="EditResumeHeadline({{$GetCV["id"]}})">
                <i class="fa fa-pencil candidate-profile-pencil">
                </i>
              </a>
              @else
              <a href="javascript:void(0);">
                  <i class="fa fa-pencil candidate-profile-pencil">
                  </i>
                </a>
              @endif
              @if($GetCV["id"])
              <div class="candidate-profile-p">{!!$GetCV->title!!}
              </div>
              @else
              <p class="candidate-profile-p">Resume is the most important document recruiters look for. Recruiters generally do not look at profiles without resumes.
              </p>
              @endif --}}
            </div>
            <br>
            <div class="caniddate-profile-resume">
              <h3 class="candidate-profile-head">Skills
              </h3>
              {{-- <a href="javascript:void(0);" onclick="AddOrEditCareerProfile()">
                <i class="fa fa-pencil candidate-profile-head-pencil">
                </i>
              </a> --}}
              <div class="row">
                @foreach($GetIndustries as $SkillNames)
                <div class="col-md-3">
                <p><input type="checkbox" class="SkillNames" class="" value="{{$SkillNames->name}}" name="Location" id="{{$SkillNames->id}}"
                  @if(isset($GetSkillNames))
                  @for($i = 0; $i < count($GetSkillNames); $i++) 
                    @if($GetSkillNames[$i] == $SkillNames->name)
                      checked
                    @else

                    @endif
                  @endfor
                @endif 
                  ><label for="{{$SkillNames->id}}" class="CheckBoxFilter">{{$SkillNames->name}} </label></p>
                </div>
                @endforeach
              </div>
              
              <div class="row">
                  <div class="col-md-12">
                    <h5 class="candidate-career-profile-table-head">Skill names
                    </h5>
                    <div class="skills-badge mar-bottom0 mar-top0">
                    <input type='text' class="bg-white input-height44 input-border" id='SkillNameInput' value='{{$CandidateProfile->skills}}'/>
                    </div>
                    <br>
                    <a href="javascript:void(0);" id="UpdateCareerProfile" title="" class="for-employers-btn post-a-job-home text-center download-cv-mar pull-right" download="">Update Skills</a>
                  </div>
                </div>
            </div>
            <br>
            <div class="caniddate-profile-resume">
              <h3 class="candidate-profile-head">Employment
              </h3>
              <a href="javascript:void(0);" class="pull-right preview-cv-link"  data-toggle="modal" data-target="#AddWorkExperience">
                <i class="fa fa-plus">
                </i> Add Employment
              </a>
        
        <table class="table table-hover">
            <thead>
              <tr>
                <th>Designation</th>
                <th>Company name</th>
                <th>Experience</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
                @foreach($GetExperience as $Experience)
              <tr>
                <td>{{$Experience->JobTitle}}</td>
                <td>{{$Experience->name}}</td>
                <td><p class="candidate-work-experience-p">
                    (
                    @php
            
            if($Experience->from_year && $Experience->from_month){
              $d1 = new DateTime($Experience->from_year."-".$Experience->from_month);

              if($Experience->to_year && $Experience->to_month){
              $d2 = new DateTime($Experience->to_year."-".$Experience->to_month);
              }
              $diff = $d2->diff($d1);
                      
            }

            @endphp

        @if($Experience->from_year && $Experience->from_month)
        @if($Experience->to_year && $Experience->to_month)
              {{$diff->y}} Years {{$diff->m}} Months
            
        @endif
        @endif
            
                    )
                    {{-- @if($Experience->worked_till == 1)
                  <p class="candidate-work-experience-p">Available to join in @if($Experience->notice_period == 1)
                    {{15}}
                    @else
                    {{30}}
                    @endif
                    Days</p>
                  @endif --}}
                </td>
                <td><a href="javascript:void(0);" onclick="EditWorkExperience({{$Experience->id}})">
                    <i class="fa fa-pencil">
                    </i>
                  </a>
    
                  <a href="javascript:void(0);" onclick="DeleteExperience({{$Experience->id}})">
                      <i class="fa fa-trash">
                      </i>
                    </a></td>
              </tr>
              @endforeach
            </tbody>
          </table>

			  {{-- <div class="pad-top10">
              <h4 class="candidate-work-experience-head">{{$Experience["department"]}}
              </h4>
              <a href="javascript:void(0);" onclick="EditWorkExperience({{$Experience["id"]}})">
                <i class="fa fa-pencil candidate-profile-head-pencil candidate-profile-work-experience-pencil">
                </i>
              </a>

              <a href="javascript:void(0);" onclick="DeleteExperience({{$Experience["id"]}})">
                  <i class="fa fa-trash candidate-profile-head-pencil candidate-profile-work-experience-trash delete-mar-top-right">
                  </i>
                </a>

              <p class="candidate-work-experience-p">{{$Experience["name"]}}
              </p>
              <p class="candidate-work-experience-p">
                {{$Experience["from_month"]}} {{$Experience["from_year"]}} to {{$Experience["to_month"]}} {{$Experience["to_year"]}}
                (
                @php
				
				$d1 = new DateTime($Experience['from_year']."-".$Experience['from_month']);
				$d2 = new DateTime($Experience['to_year']."-".$Experience['to_month']);

				$diff = $d2->diff($d1);
                @endphp
				{{$diff->y}} Years {{$diff->m}} Months
                )
                @if($Experience->worked_till == 1)
              <p class="candidate-work-experience-p">Available to join in @if($Experience->notice_period == 1)
                {{15}}
                @else
                {{30}}
                @endif
                Days</p>
              @endif
              <p class="candidate-work-experience-p candidate-work-experience-p-description">{{$Experience->description}}
			  
			</div> --}}
			
			{{-- <div class="work-experience-border"></div> --}}
              
			</div>
			
			<br>
            <div class="caniddate-profile-resume">
              <h3 class="candidate-profile-head">Education
              </h3>
              <a href="javascript:void(0);" class="pull-right preview-cv-link"  data-toggle="modal" data-target="#AddEducationModal">
                <i class="fa fa-plus">
                </i> Add Education
              </a>
              
              <table class="table table-hover">
                  <thead>
                    <tr>
                      <th>Degree</th>
                      <th>University / Board</th>
                      <th>Year of passed</th>
                      <th>Percentage</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                      @foreach($GetEducation as $Education)
                    <tr>
                      <td>{{$Education->qualification}}</td>
                      <td>{{$Education->name}}</td>
                      <td>{{$Education->from_year}}</td>
                      <td>{{$Education->percentage}}</td>
                      <td><a href="javascript:void(0);" onclick="EditEducation({{$Education->id}})">
                          <i class="fa fa-pencil">
                          </i>
                        </a>
        
                        <a href="javascript:void(0);" onclick="DeleteEducation({{$Education->id}})">
                            <i class="fa fa-trash">
                            </i>
                          </a></td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>

              {{-- <div class="pad-top20">
                <h4 class="candidate-work-experience-head">
                  {{$Education->qualification}}
                </h4>
                <a href="javascript:void(0);" onclick="EditEducation({{$Education->id}})">
                  <i class="fa fa-pencil candidate-profile-head-pencil candidate-profile-work-experience-pencil">
                  </i>
                </a>

                <a href="javascript:void(0);" onclick="DeleteEducation({{$Education->id}})">
                    <i class="fa fa-trash candidate-profile-head-pencil candidate-profile-work-experience-pencil delete-mar-top-right">
                    </i>
                  </a>
                <p class="candidate-work-experience-p">{{$Education->name}}
                </p>
                <p class="candidate-work-experience-p">{{$Education->from_year}} (
                  @if($Education->course_type == 1)
                    Part time
                  @else
                    Full time
                  @endif
                )
                </p>
              </div>
              <div class="work-experience-border pad-bottom10"></div> --}}
              
              {{-- 
              <p class="candidate-work-experience-p">Available to join in 2 Months
              </p> --}}
			</div>
			
			<br>
            <div class="caniddate-profile-resume">
              <h3 class="candidate-profile-head">Personal Details
        </h3>
        @if($CandidateProfile["id"])
        <a href="javascript:void(0);" onclick="EditPersonalDetailsModal({{$CandidateProfile["id"]}})">
            <i class="fa fa-pencil candidate-profile-head-pencil candidate-profile-work-experience-pencil candidate-profile-personal-details-pencil">
            </i>
          </a>
          @else
			  <a href="javascript:void(0);" data-toggle="modal" data-target="#AddPersonalDetailsModal">
					<i class="fa fa-pencil candidate-profile-head-pencil candidate-profile-work-experience-pencil candidate-profile-personal-details-pencil">
					</i>
				</a>
        @endif

			<div class="row">
				<div class="col-md-3">
					<h4 class="candidate-work-experience-head">Date of birth
            </h4>
            @if($CandidateProfile["date_of_birth"])
					<p class="candidate-work-experience-p">{{date('d M Y', strtotime($CandidateProfile->date_of_birth))}}
          </p>
          @endif
				</div>
				<div class="col-md-3">
					<h4 class="candidate-work-experience-head">Gender
            </h4>
            @if($CandidateProfile["gender"])
					<p class="candidate-work-experience-p">
            @if($CandidateProfile->gender == 1)
              Male
            @else
              Female
            @endif
          </p>
          @endif
				</div>
				<div class="col-md-3">
					<h4 class="candidate-work-experience-head">Marital Status
            </h4>
            @if($CandidateProfile["marital_status"])
					<p class="candidate-work-experience-p">
              @if($CandidateProfile->marital_status == 1)
              Married
            @else
              Unmarried
            @endif
          </p>
          @endif
				</div>
				<div class="col-md-3">
					<h4 class="candidate-work-experience-head">Father's Name
            </h4>
            @if($CandidateProfile["father_name"])
					<p class="candidate-work-experience-p">{{$CandidateProfile->father_name}}
          </p>
          @endif
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-md-6">
					<h4 class="candidate-work-experience-head">Address
          </h4>
          @if($CandidateProfile["address"])
					<p class="candidate-work-experience-p">{{$CandidateProfile->address}}
          </p>
          @endif
				</div>
      </div>
      <br>
      <div class="row">
        <div class="col-md-4">
          <h4 class="candidate-work-experience-head">State
          </h4>
          @if($CandidateProfile["state"])
          <p class="candidate-work-experience-p">{{$CandidateProfile->state}}
          </p>
          @endif
        </div>
        <div class="col-md-4">
            <h4 class="candidate-work-experience-head">City
            </h4>
            @if($CandidateProfile["city"])
            <p class="candidate-work-experience-p">{{$CandidateProfile->city}}
            </p>
            @endif
          </div>
          <div class="col-md-4">
              <h4 class="candidate-work-experience-head">Zip code
              </h4>
              @if($CandidateProfile["zip_code"])
              <p class="candidate-work-experience-p">{{$CandidateProfile->zip_code}}
              </p>
              @endif
            </div>
      </div>
			  
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Profile Edit -->
<div class="account-popup-area candidate-profile-popup-box">
  <div class="account-popup candidate-profile-popup">
    <span class="close-popup">
      <i class="la la-close">
      </i>
    </span>
    <h3 class="candidate-profile-details-popup">Profile Details
    </h3>
    <form class="mar-top15">
      <label class="pull-left remove-label">Name
      </label>
      <div class="cfield">
        <input type="text" placeholder="Username" value="{{$Profile->name}}" id=""/>
      </div>
      <label class="pull-left remove-label">Total Experience *
      </label>
      <div class="dropdown-field col-md-8 pad-left0 width50">
        <select data-placeholder="Please Select Candidate Type" class="chosen" id="experience">
          <!-- <option selected disabled>Please Select Candidate Type</option> -->
          <option  value="1">2-6 years
          </option>
          <option  value="2">6-12 years
          </option>
        </select>
      </div>
      <label class="pull-left remove-label">Current Salary *
      </label>
      <div class="dropdown-field col-md-8 pad-left0 width50">
        <select data-placeholder="Please Select Candidate Type" class="chosen" id="current_salary">
          <!-- <option selected disabled>Please Select Candidate Type</option> -->
          <option  value="1">50k-60k
          </option>
          <option  value="2">60k-70k
          </option>
        </select>
      </div>
      
      <label class="pull-left remove-label">Current Location
      </label>
      <div class="cfield">
        <input type="text" placeholder="Bangalore" value="" id="desired_location"/>
        {{-- 
        <a href="javascript:void(0);" id="Currentlocation">
          <i class="fa fa-arrows">
          </i>
        </a> --}}
      </div>
      <label class="pull-left remove-label">Mobile Number *
      </label>
      <div class="cfield">
        <input type="text" placeholder="9876543210" value="{{$Profile->mobile}}" id="" disabled/>
      </div>
      <label class="pull-left remove-label">Email Address
      </label>
      <div class="cfield">
        <input type="text" placeholder="demo@gmail.com" value="{{$Profile->email}}" id="" disabled/>
      </div>
      <button type="button" class="candidate-profile-cancel-btn col-md-6 col-xs-6 text-right width50">Cancel
      </button>
      <button type="button" class="candidate-profile-save-btn col-md-6 col-xs-6" id="AddCandidateProfile">Save
      </button>
    </form>
    <img id="CandidateOtpLoader" class="signup-otp-width" src="{{URL::asset('UI/otp_verify_loader.gif')}}" alt="" style="display:none;">
  </div>
</div>

{{-- Edit Profile Details --}}
<div class="account-popup-area edit_candidate-profile-popup-box">
    <div class="account-popup candidate-profile-popup">
      <span class="close-popup">
        <i class="la la-close">
        </i>
      </span>
      <h3 class="candidate-profile-details-popup">Profile Details
      </h3>
      <form class="mar-top15">
        <label class="pull-left remove-label">Name
        </label>
        <div class="cfield">
          <input type="text" placeholder="Username" value="{{$Profile->name}}" id="" disabled>
        </div>
        
        <div class="dropdown-field col-md-8 pad-left0 width50">
            <label class="pull-left remove-label">Total Experience (Years)*
              </label>
          <select data-placeholder="Please Select Candidate Type" class="dropdown-custom-clr-pad" id="Edit_experience">
            <!-- <option selected disabled>Please Select Candidate Type</option> -->
            {{-- <option  value="1">1
            </option>
            <option  value="2">2
            </option> --}}
          </select>
        </div>


        <div class="dropdown-field col-md-8 pad-left0 width50">
            <label class="pull-left remove-label">(Months)*
              </label>
          <select data-placeholder="Please Select Candidate Type" class="dropdown-custom-clr-pad" id="Edit_experience_months">
            <!-- <option selected disabled>Please Select Candidate Type</option> -->
            {{-- <option  value="1">2
            </option>
            <option  value="2">4
            </option> --}}
          </select>
        </div>
        
        <label class="pull-left remove-label">Current Gross Salary *
        </label>
        <div class="dropdown-field col-md-12 pad-left0">
          
          <div class="cfield">
              <input type="text" placeholder="Salary (Lacs)" value="" id="Edit_current_salary" maxlength="7"/>
            </div>
        </div>

        {{-- <div class="dropdown-field col-md-8 pad-left0 width50">
            
            <div class="cfield">
                <input type="text" placeholder="Salary (Thousand)" value="" id="Edit_current_salary_thousand"/>
              </div>
          </div> --}}

        <label class="pull-left remove-label">Current Location
        </label>
        <div class="cfield">
          <input type="text" placeholder="Bangalore" value="" id="Edit_desired_location"/>
          {{-- 
          <a href="javascript:void(0);" id="Currentlocation">
            <i class="fa fa-arrows">
            </i>
          </a> --}}
        </div>
        <label class="pull-left remove-label">Mobile Number *
        </label>
        <div class="cfield">
          <input type="text" placeholder="9876543210" value="{{$Profile->mobile}}" id="" disabled/>
        </div>
        <label class="pull-left remove-label">Email Address
        </label>
        <div class="cfield">
          <input type="text" placeholder="demo@gmail.com" value="{{$Profile->email}}" id="" disabled/>
        </div>
        <button type="button" class="candidate-profile-cancel-btn col-md-6 col-xs-6 text-right width50">Cancel
        </button>
        <button type="button" class="candidate-profile-save-btn col-md-6 col-xs-6" id="UpdateCandidateProfile">Save
        </button>
      </form>
      <img id="CandidateOtpLoader" class="signup-otp-width" src="{{URL::asset('UI/otp_verify_loader.gif')}}" alt="" style="display:none;">
    </div>
  </div>

{{-- Resume headline edit --}}
<div id="ResumeHeadline" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header pad-bottom0">
		  <h5>Add Resume Headline</h5>
        <button type="button" class="close" data-dismiss="modal">&times;
        </button>
      </div>
      <div class="modal-body pad-top0">
        <label class="pull-left remove-label">Resume Headline
		</label>
		<p class="resume-headline-p">Resume is most important document recruiter look for. Recruiters generally do not look at profiles without resumes.</p>
        <div class="cfield">
          <textarea id="description">
          </textarea>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default candidate-profile-cancel-btn" data-dismiss="modal">Close
        </button>
        <button type="button" onclick="InsertOrUpdateCV()" class="btn btn-default candidate-profile-save-btn">Save
        </button>
      </div>
    </div>
  </div>
</div>
{{-- Career Profile edit --}}
<div id="CareerProfile" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header pad-bottom0">
		  <h5>Add Career Profile</h5>
        <button type="button" class="close" data-dismiss="modal">&times;
        </button>
      </div>
      <div class="modal-body pad-top0">
        <form class="mar-top15">
          <label class="pull-left remove-label">Industry *
          </label>
          <div class="dropdown-field col-md-12 pad-left0 pad-right0">
            <select data-placeholder="Please Select Industries" class="chosen" id="industry">
              <!-- <option selected disabled>Please Select Candidate Type</option> -->
              @foreach($GetIndustries as $Industries)
              <option value="{{$Industries->id}}">{{$Industries->name}}
              </option>
              @endforeach
            </select>
          </div>
          <label class="pull-left remove-label">Functional area *
          </label>
          <div class="dropdown-field col-md-12 pad-left0 pad-right0">
            <select  data-placeholder="Please Select Functional Areas" class="chosen" id="functional_area">
              <!-- <option selected disabled>Please Select Candidate Type</option> -->
              @foreach($GetFunctionalAreas as $Functional)
              <option value="{{$Functional["id"]}}">{{$Functional->functional_areas	}}
              </option>
              @endforeach
            </select>
          </div>
          <label class="pull-left remove-label">Role *
          </label>
          <div class="cfield">
            @if($CandidateProfile)
            <input type="text" placeholder="UI Designer" class="bg-white input-border" value="{{$CandidateProfile->role}}" id="role"/>
            @else
            <input type="text" placeholder="UI Designer" value="" class="bg-white input-border" id="role"/>
            @endif
          </div>

          <label class="pull-left remove-label">Skill *
            </label>
            <div class="cfield Skills">
              @if($CandidateProfile)
              <input type="text" placeholder="Type your skills..." data-role="tagsinput" class="bg-white input-border" value="{{$CandidateProfile->skills}}" id="skill"/>
              @else
              <input type="text" data-role="tagsinput" placeholder="HTML" value="" class="bg-white input-border" id="skill"/>
              @endif
            </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default candidate-profile-cancel-btn" data-dismiss="modal">Close
        </button>
        <button type="button" id="UpdateCareerProfile" class="btn btn-default candidate-profile-save-btn">Save
        </button>
      </div>
    </div>
  </div>
</div>
{{-- Work Experience --}}
<div id="AddWorkExperience" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header pad-bottom0">
		  <h5>Add Work Experience</h5>
        <button type="button" class="close" data-dismiss="modal">&times;
        </button>
      </div>
      <div class="modal-body pad-top0">
        <form class="mar-top15">
          <label class="pull-left remove-label candidate-profile-label">Your Designation *
          </label>
          {{-- <div class="cfield">
            <input type="text" placeholder="UI Designer" value="" class="bg-white input-border input-height44 mar-bottom25" id="designation"/>
          </div> --}}
          <div class="cfield ">
              <select data-placeholder="Please Select Job Title" class="chosen" id="designation">
                <option selected>Select Title</option>
                @foreach($GetJobTitle as $Title)
              <option value="{{$Title->id}}">{{$Title->name}}
                </option>
                @endforeach
              </select>
          </div>
          <label class="mar-top15 pull-left remove-label candidate-profile-label">Your Organization *
          </label>
          <div class="cfield">
            <input type="text" placeholder="Organization Name" value="" class="bg-white input-border input-height44 mar-bottom25" id="company_name"/>
		  </div>
		  <label class="pull-left remove-label candidate-profile-label">Is this your current company? 
			</label>
			<div class="cfield">
				<input type="radio" name="CheckWorkedTill" id="yes" value="1" checked><label class="pull-left pad-right45 mar-bottom25" for="yes">Yes</label><br />
				<input type="radio" name="CheckWorkedTill" value="2" id="no"><label for="no" class="mar-bottom25">No</label><br />
			</div>
			
          <label class="pull-left remove-label candidate-profile-label">Started Working From *
          </label>
          <div class="cfield dropdown-year-month-width">
              <select data-placeholder="Please Select Industries" class="dropdown-custom-clr-pad" id="exp_from_year">
                <option selected disabled>Select Year</option>
                {{--  <option value="2005">2005
                </option>
                <option value="2010">2010
                </option>
                <option value="2019">2019
                </option>  --}}
              </select>
          </div>
		  <div class="cfield dropdown-year-month-width">
				<select data-placeholder="Please Select Industries" class="chosen" id="exp_from_month">
          <option selected disabled>Select Month</option>
          <option value="01"> January </option>
          <option value="02"> February </option>
          <option value="03"> March </option>
          <option value="04"> April </option>
          <option value="05"> May </option>
          <option value="06"> June </option>
          <option value="07"> July </option>
          <option value="08"> August </option>
          <option value="09"> September </option>
          <option value="10"> October </option>
          <option value="11"> November </option>
          <option value="12"> December </option>
				</select>
			</div>

			<div id="WorkingTo" style="display: none;"> 
				<label class="pull-left remove-label candidate-profile-label">Started Working To *
				</label>
				<div class="cfield dropdown-year-month-width">
					<select data-placeholder="Please Select Industries" class="dropdown-custom-clr-pad" id="exp_to_year">
						<option selected disabled>Select Year</option>
						{{--  <option value="2005">2005
            </option>
            <option value="2010">2010
            </option>
            <option value="2019">2019
            </option>  --}}
					</select>
				</div>
				<div class="cfield dropdown-year-month-width">
						<select data-placeholder="Please Select Industries" class="chosen" id="exp_to_month">
							<option selected disabled>Select Month</option>
              <option value="01"> January </option>
              <option value="02"> February </option>
              <option value="03"> March </option>
              <option value="04"> April </option>
              <option value="05"> May </option>
              <option value="06"> June </option>
              <option value="07"> July </option>
              <option value="08"> August </option>
              <option value="09"> September </option>
              <option value="10"> October </option>
              <option value="11"> November </option>
              <option value="12"> December </option>
						</select>
					</div>
			</div>
			<div id="WorkedTill">
				<label class="pull-left remove-label candidate-profile-label">Worked Till *
				</label>
				<div class="cfield dropdown-year-month-width">
					<select class="chosen" id="worked_till">
						{{-- <option selected disabled>Please select</option> --}}
						<option value="1" selected>Present
						</option>
						{{-- <option value="0">No</option> --}}
					</select>
				</div>
			</div>

		  {{-- <div id="CurrentSalaryRow"> 
				<label class="pull-left remove-label candidate-profile-label">Current Salary *
				</label>
				<div class="cfield">
          <input type="text" placeholder="Gross salary" value="" id="current_salary_month" class="bg-white input-border input-height44" maxlength="7"/>
            </div>
			</div> --}}

          <label class="pull-left remove-label candidate-profile-label">Job Description *
          </label>
          <div class="cfield">
            <input type="text" placeholder="Description" value="" class="bg-white input-border input-height44 mar-bottom25" id="exp_description"/>
		  </div>
		
		  <div id="NoticePeriod">
			<label class="pull-left remove-label candidate-profile-label">Notice Period *
				</label>
				<div class="cfield dropdown-year-month-width">
				{{-- <select class="chosen" id="notice_period">
					<option selected disabled>Please select</option>
					<option value="1">15 Days
					</option>
					<option value="2">30 Days</option>
        </select> --}}
        
        <input type="text" placeholder="Notice period" value="" class="bg-white input-border input-height44 mar-bottom25" id="notice_period"/>
				</div>
			</div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default candidate-profile-cancel-btn" data-dismiss="modal">Close
        </button>
        <button type="button" id="AddExperience" class="btn btn-default candidate-profile-save-btn">Save
        </button>
      </div>
    </div>
  </div>
</div>

{{-- Edit Experience --}}
<div id="EditWorkExperience" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header pad-bottom0">
        <h5>Edit Work Employment</h5>
          <button type="button" class="close" data-dismiss="modal">&times;
          </button>
        </div>
        <div class="modal-body pad-top0">
          <form class="mar-top15">
            <label class="pull-left remove-label candidate-profile-label">Your Designation *
            </label>
            <div class="cfield">
                <input type="hidden" placeholder="UI Designer" value="" class="bg-white input-border input-height44 mar-bottom25" id="Edit_exp_id"/>

              {{-- <input type="text" placeholder="UI Designer" value="" class="bg-white input-border input-height44 mar-bottom25" id="Edit_designation"/> --}}

              <select data-placeholder="Please Select Job Title" class="dropdown-custom-clr-pad" id="Edit_designation">
                  <option selected>Select Title</option>
                  @foreach($GetJobTitle as $Title)
                <option value="{{$Title->id}}">{{$Title->name}}
                  </option>
                  @endforeach
                </select>

            </div>


            <label class="mar-top15 pull-left remove-label candidate-profile-label">Your Organization *
            </label>
            <div class="cfield">
              <input type="text" placeholder="Organization Name" value="" class="bg-white input-border input-height44 mar-bottom25" id="Edit_company_name"/>
        </div>
        <label class="pull-left remove-label candidate-profile-label">Is this your current company? 
        </label>
        <div class="cfield">
          <input type="radio" name="Edit_CheckWorkedTill" value="1" class="yes" id="Edit_yes" checked><label class="pull-left pad-right45 mar-bottom25" for="Edit_yes">Yes</label><br />
          <input type="radio" name="Edit_CheckWorkedTill" value="2" class="no" id="Edit_no"><label for="Edit_no" class="mar-bottom25">No</label><br />
        </div>
        
            <label class="pull-left remove-label candidate-profile-label">Started Working From *
            </label>
            <div class="cfield dropdown-year-month-width">
        <select data-placeholder="Please Select Industries" class="dropdown-custom-clr-pad" id="Edit_exp_from_year">
          <option selected disabled>Select Year</option>
          {{--  <option value="2005">2005
          </option>
          <option value="2010">2010
          </option>
          <option value="2019">2019
          </option>  --}}
        </select>
        </div>
        <div class="cfield dropdown-year-month-width">
          <select data-placeholder="Please Select Industries" class="dropdown-custom-clr-pad" id="Edit_exp_from_month">
            <option selected disabled>Select Month</option>
            <option value="01"> January </option>
              <option value="02"> February </option>
              <option value="03"> March </option>
              <option value="04"> April </option>
              <option value="05"> May </option>
              <option value="06"> June </option>
              <option value="07"> July </option>
              <option value="08"> August </option>
              <option value="09"> September </option>
              <option value="10"> October </option>
              <option value="11"> November </option>
              <option value="12"> December </option>
          </select>
        </div>
  
        <div id="Edit_WorkingTo" style="display: none;"> 
          <label class="pull-left remove-label candidate-profile-label">Started Working To *
          </label>
          <div class="cfield dropdown-year-month-width">
            <select data-placeholder="Please Select Industries" class="dropdown-custom-clr-pad" id="Edit_exp_to_year">
              <option selected disabled>Select Year</option>
              {{--  <option value="2005">2005
              </option>
              <option value="2010">2010
              </option>
              <option value="2019">2019
              </option>  --}}
            </select>
          </div>
          <div class="cfield dropdown-year-month-width">
              <select data-placeholder="Please Select Industries" class="dropdown-custom-clr-pad" id="Edit_exp_to_month">
                <option selected disabled>Select Month</option>
                <option value="01"> January </option>
                <option value="02"> February </option>
                <option value="03"> March </option>
                <option value="04"> April </option>
                <option value="05"> May </option>
                <option value="06"> June </option>
                <option value="07"> July </option>
                <option value="08"> August </option>
                <option value="09"> September </option>
                <option value="10"> October </option>
                <option value="11"> November </option>
                <option value="12"> December </option>
              </select>
            </div>
        </div>
        <div id="Edit_WorkedTill">
          <label class="pull-left remove-label candidate-profile-label">Worked Till *
          </label>
          <div class="cfield dropdown-year-month-width">
            <select class="dropdown-custom-clr-pad" id="Edit_worked_till">
              {{-- <option selected disabled>Please select</option> --}}
              <option value="1" selected>Present
              </option>
              {{-- <option value="0">No</option> --}}
            </select>
          </div>
        </div>
  
        {{-- <div id="Edit_CurrentSalaryRow"> 
          <label class="pull-left remove-label candidate-profile-label">Current Salary *
          </label>
          <div class="cfield">
            <div class="cfield">
                <input type="text" placeholder="Description" value="" class="bg-white input-border input-height44 mar-bottom25" id="Edit_current_salary_month"/>
          </div>

          </div>
        </div> --}}
  
            <label class="pull-left remove-label candidate-profile-label">Job Description *
            </label>
            <div class="cfield">
              <input type="text" placeholder="Description" value="" class="bg-white input-border input-height44 mar-bottom25" id="Edit_exp_description"/>
        </div>
      
        <div id="Edit_NoticePeriod">
        <label class="pull-left remove-label candidate-profile-label">Notice Period *
          </label>
          <div class="cfield dropdown-year-month-width">
          {{-- <select class="dropdown-custom-clr-pad" id="Edit_notice_period">
            <option selected disabled>Please select</option>
            <option value="1">15 Days
            </option>
            <option value="2">30 Days</option>
          </select> --}}

          <input type="text" placeholder="Notice period" value="" class="bg-white input-border input-height44 mar-bottom25" id="Edit_notice_period"/>
          </div>
        </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default candidate-profile-cancel-btn" data-dismiss="modal">Close
          </button>
          <button type="button" id="UpdateExperience" class="btn btn-default candidate-profile-save-btn">Save
          </button>
        </div>
      </div>
    </div>
  </div>




{{-- Education --}}
<div id="AddEducationModal" class="modal fade" role="dialog">
		<div class="modal-dialog">
		  <!-- Modal content-->
		  <div class="modal-content">
			<div class="modal-header pad-bottom0">
				<h5>Add Education Details</h5>
			  <button type="button" class="close" data-dismiss="modal">&times;
			  </button>
			</div>
			<div class="modal-body pad-top0">
			  <form class="mar-top15">

				{{-- <label class="pull-left remove-label candidate-profile-label">Board *
				</label> --}}
				{{-- <div class="cfield dropdown-year-month-width">
				  <select data-placeholder="Please Select Industries" class="chosen board" id="board" onchange="CheckBoard()">
					  <option selected disabled>Select Board</option>
            <option value="10th">10th
            </option>
            <option value="12th">12th
					  </option>
            <option value="3">Graduate
					  </option>
				  </select>
				</div> --}}
        
        <div id="CourseNameRow">
          <label class="pull-left remove-label candidate-profile-label">Degree *
          </label>
          {{-- <div class="cfield">
            <input type="text" placeholder="Course name" value="" id="course_name" class="bg-white input-border input-height44 mar-bottom25 course_name" id=""/>
          </div> --}}
          <select data-placeholder="Please Select Industries" class="chosen" id="board">
					  <option selected disabled>Select Degree</option>
              @foreach($GetQualification as $Qualification)
          <option value="{{$Qualification->id}}">{{$Qualification->qualification}}
              </option>
              @endforeach
				  </select>
        </div>

        <br>
				<label class="pull-left remove-label candidate-profile-label mar-top15">University / Board *
				</label>
				<div class="cfield">
				  <input type="text" placeholder="School name" value="" id="school_clg" class="bg-white input-border input-height44 mar-bottom25" id=""/>
				</div>
				
				<label class="pull-left remove-label candidate-profile-label">Passing Out Year *
				  </label>
				  <div class="cfield dropdown-year-month-width">
					<select class="dropdown-custom-clr-pad" id="passed_out_year">
						<option selected disabled>Select year</option>
						{{--  <option value="2014">2014
						</option>
						<option value="2018">2018</option>  --}}
					</select>
				  </div>

				  {{-- <div class="cfield dropdown-year-month-width">
					<select class="chosen" id="course_type">
						<option selected disabled>Select type</option>
						<option value="1">Part time
						</option>
						<option value="2">Full time</option>
					</select>
          </div> --}}
          {{-- <label class="pull-left remove-label candidate-profile-label mar-top15">Percentage
          </label> --}}
          <div class="cfield width50 pull-left">
            <input type="text" placeholder="Percentage" value="" id="percentage" class="bg-white input-border input-height44 mar-bottom25" id="" maxlength="2">
          </div>
			  </form>
			</div>
			<div class="modal-footer">
			  <button type="button" class="btn btn-default candidate-profile-cancel-btn" data-dismiss="modal">Close
			  </button>
			  <button type="button" id="AddEducation" class="btn btn-default candidate-profile-save-btn">Save
			  </button>
			</div>
		  </div>
		</div>
	  </div>


{{-- Edit Education --}}
<div id="EditEducationModal" class="modal fade" role="dialog">
		<div class="modal-dialog">
		  <!-- Modal content-->
		  <div class="modal-content">
			<div class="modal-header pad-bottom0">
				<h5>Edit Education Details</h5>
			  <button type="button" class="close" data-dismiss="modal">&times;
			  </button>
			</div>
			<div class="modal-body pad-top0">
			  <form class="mar-top15">

				{{-- <label class="pull-left remove-label candidate-profile-label">Board *
				</label> --}}
				<div class="cfield dropdown-year-month-width">
            <input type="hidden" placeholder="Course name" value="" id="edu_id" class="bg-white input-border input-height44 mar-bottom25 course_name"/>

				  {{-- <select data-placeholder="Please Select Industries" class="board dropdown-custom-clr-pad" id="Edit_board" onchange="Edit_CheckBoard()">
					  <option selected disabled>Select Board</option>
            <option value="1">10th
            </option>
            <option value="2">12th
					  </option>
            <option value="3">Graduate
					  </option>
				  </select> --}}
				</div>
        
        <div id="">
          <label class="pull-left remove-label candidate-profile-label">Degree *
          </label>
          {{-- <div class="cfield">
            <input type="text" placeholder="Course name" value="" id="edit_course_name" class="bg-white input-border input-height44 mar-bottom25 course_name" id=""/>
          </div> --}}
          <select data-placeholder="Please Select Industries" class="dropdown-custom-clr-pad" id="Edit_board">
					  <option selected disabled>Select Degree</option>
              @foreach($GetQualification as $Qualification)
          <option value="{{$Qualification->id}}">{{$Qualification->qualification}}
              </option>
              @endforeach
				  </select>
        </div>

				<label class="pull-left remove-label candidate-profile-label mar-top15">University / Board *
				</label>
				<div class="cfield">
				  <input type="text" placeholder="School name" value="" id="Edit_school_clg" class="bg-white input-border input-height44 mar-bottom25" id=""/>
				</div>
				
				<label class="pull-left remove-label candidate-profile-label">Passing Out Year *
				  </label>
				  <div class="cfield dropdown-year-month-width">
					<select class="dropdown-custom-clr-pad" id="Edit_passed_out_year">
						<option selected disabled>Select year</option>
						
					</select>
				  </div>

				  {{-- <div class="cfield dropdown-year-month-width">
					<select class="dropdown-custom-clr-pad" id="Edit_course_type">
						<option selected disabled>Select type</option>
						<option value="1">Part time
						</option>
						<option value="2">Full time</option>
					</select>
          </div> --}}
          <div class="cfield width50 pull-left">
            <input type="text" placeholder="Percentage" value="" id="Edit_percentage" class="bg-white input-border input-height44 mar-bottom25" id="" maxlength="2">
          </div>
			  </form>
			</div>
			<div class="modal-footer">
			  <button type="button" class="btn btn-default candidate-profile-cancel-btn" data-dismiss="modal">Close
			  </button>
			  <button type="button" id="UpdateEducation" class="btn btn-default candidate-profile-save-btn">Save
			  </button>
			</div>
		  </div>
		</div>
	  </div>




{{-- Personal Details --}}
<div id="AddPersonalDetailsModal" class="modal fade" role="dialog">
		<div class="modal-dialog">
		  <!-- Modal content-->
		  <div class="modal-content">
			<div class="modal-header pad-bottom0">
				<h5>Add Personal Details</h5>
			  <button type="button" class="close" data-dismiss="modal">&times;
			  </button>
			</div>
			<div class="modal-body pad-top0">
			  <form class="mar-top15">

					<label class="pull-left remove-label candidate-profile-label">Date of Birth 
						</label>
						<div class="cfield width50">
						  <input type="text" placeholder="Date of birth" id="dob" value="" class="bg-white input-border input-height44 mar-bottom25" id=""/>
						</div>
				
				<label class="pull-left remove-label candidate-profile-label">Gender 
					</label>
					<div class="cfield dropdown-year-month-width">
					  <select class="dropdown-custom-clr-pad" id="gender">
						  <option selected disabled>Select gender</option>
						  <option value="1">Male
						  </option>
						  <option value="2">Female</option>
					  </select>
					</div>
  
					<div class="cfield dropdown-year-month-width">
					  <select class="dropdown-custom-clr-pad" id="marital_status">
						  <option selected disabled>Marital Status</option>
						  <option value="1">Married
						  </option>
						  <option value="2">Un married</option>
					  </select>
					  </div>

				<label class="pull-left remove-label candidate-profile-label">Father's Name 
				</label>
				<div class="cfield">
				  <input type="text" placeholder="Father name" value="" class="bg-white input-border input-height44 mar-bottom25" id="father_name"/>
				</div>
				
				<label class="pull-left remove-label candidate-profile-label">Address 
				</label>
				<div class="cfield">
        <input type="text" placeholder="Address" value="" id="address" class="bg-white input-border input-height44 mar-bottom25" id=""/>
				</div>
			  </form>
			</div>
			<div class="modal-footer">
			  <button type="button" class="btn btn-default candidate-profile-cancel-btn" data-dismiss="modal">Close
			  </button>
			  <button type="button" id="AddCandidatePersonalDetails" class="btn btn-default candidate-profile-save-btn">Save
			  </button>
			</div>
		  </div>
		</div>
    </div>	  
    
    {{-- Edit Personal Details --}}
<div id="EditPersonalDetailsModal" class="modal fade" role="dialog">
		<div class="modal-dialog">
		  <!-- Modal content-->
		  <div class="modal-content">
			<div class="modal-header pad-bottom0">
				<h5>Add Personal Details</h5>
			  <button type="button" class="close" data-dismiss="modal">&times;
			  </button>
			</div>
			<div class="modal-body pad-top0">
			  <form class="mar-top15">

					<label class="pull-left remove-label candidate-profile-label">Date of Birth 
						</label>
						<div class="cfield width50">

						  <input type="date" placeholder="Date of birth" id="Edit_dob" value="" class="bg-white input-border input-height44 mar-bottom25" id=""/>
						</div>
				
				<label class="pull-left remove-label candidate-profile-label">Gender 
					</label>
					<div class="cfield dropdown-year-month-width">
					  <select class="dropdown-custom-clr-pad" id="Edit_gender">
						  <option selected disabled>Select gender</option>
						  <option value="1">Male
						  </option>
						  <option value="2">Female</option>
					  </select>
					</div>
  
					<div class="cfield dropdown-year-month-width">
					  <select class="dropdown-custom-clr-pad" id="Edit_marital_status">
						  <option selected disabled>Marital Status</option>
						  <option value="1">Married
						  </option>
						  <option value="2">Un married</option>
					  </select>
					  </div>

				<label class="pull-left remove-label candidate-profile-label">Father's Name 
				</label>
				<div class="cfield">
				  <input type="text" placeholder="Father name" value="" class="bg-white input-border input-height44 mar-bottom25" id="Edit_father_name"/>
				</div>
        
        <div id="GetProfileAddress">
				<label class="pull-left remove-label candidate-profile-label">Address 
				</label>
				<div class="cfield">
        <input type="text" placeholder="Address" data-geocomplete="street address" value="" id="Edit_address" class="bg-white input-border input-height44 mar-bottom25" id=""/>
        </div>
        
        <label class="pull-left remove-label candidate-profile-label">State 
          </label>
          <div class="cfield">
          <input type="text" placeholder="State" data-geocomplete="state" value="" id="Edit_state" class="bg-white input-border input-height44 mar-bottom25" id=""/>
          </div>

          <label class="pull-left remove-label candidate-profile-label">City 
            </label>
            <div class="cfield">
            <input type="text" placeholder="City" data-geocomplete="city" value="" id="Edit_city" class="bg-white input-border input-height44 mar-bottom25" id=""/>
            </div>

            <label class="pull-left remove-label candidate-profile-label">Zip code 
              </label>
              <div class="cfield">
              <input type="text" placeholder="Zip code" data-geocomplete="zip code" value="" id="Edit_zipcode" class="bg-white input-border input-height44 mar-bottom25" id=""/>
              </div>
          </div>
			  </form>
			</div>
			<div class="modal-footer">
			  <button type="button" class="btn btn-default candidate-profile-cancel-btn" data-dismiss="modal">Close
			  </button>
			  <button type="button" id="UpdateCandidatePersonalDetails" class="btn btn-default candidate-profile-save-btn">Save
			  </button>
			</div>
		  </div>
		</div>
	  </div>	  
@endsection
@section('JSScript')
{{-- 
<script src="https://code.jquery.com/jquery-1.12.4.js">
</script> --}}
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js">
</script>
<script>
  // $('#dob').datepicker({
  //   onSelect: function(value, ui) {
  //     var today = new Date(),
  //         dob = new Date(value),
  //         age = new Date(today - dob).getFullYear() - 1970;
  //     $('#age').val(age);
  //   }
  //   ,
  //   maxDate: '+0d',
  //   yearRange: '1960:2019',
  //   changeMonth: true,
  //   changeYear: true
  // }
  //           );
            
  //           $('#Edit_dob').datepicker({
  //   onSelect: function(value, ui) {
  //     var today = new Date(),
  //         dob = new Date(value),
  //         age = new Date(today - dob).getFullYear() - 1970;
  //     $('#age').val(age);
  //   }
  //   ,
  //   maxDate: '+0d',
  //   yearRange: '1960:2019',
  //   changeMonth: true,
  //   changeYear: true
  // }
	// 				  );
</script>

<script>
		$(document).ready(function () {
			var $select = $("#Edit_experience");
			for (i=1;i<=20;i++){
				$select.append($('<option value='+i+'></option>').val(i).html(i))
			}
    });
    
    $(document).ready(function () {
			var $select = $("#Edit_experience_months");
			for (i=1;i<=12;i++){
				$select.append($('<option value='+i+'></option>').val(i).html(i))
			}
    });
    
    $(document).ready(function () {
      for (i = new Date().getFullYear(); i > 1990; i--)
      {
          $('#exp_from_year').append($('<option />').val(i).html(i));
      }
    });

    $(document).ready(function () {
      for (i = new Date().getFullYear(); i > 1990; i--)
      {
          $('#exp_to_year').append($('<option />').val(i).html(i));
      }
    });


    $(document).ready(function () {
      for (i = new Date().getFullYear(); i > 1990; i--)
      {
          $('#Edit_exp_from_year').append($('<option />').val(i).html(i));
      }
    });

    $(document).ready(function () {
      for (i = new Date().getFullYear(); i > 1990; i--)
      {
          $('#Edit_exp_to_year').append($('<option />').val(i).html(i));
      }
    });

    $(document).ready(function () {
      for (i = new Date().getFullYear(); i > 1990; i--)
      {
          $('#passed_out_year').append($('<option />').val(i).html(i));
      }
    });

    $(document).ready(function () {
      for (i = new Date().getFullYear(); i > 1990; i--)
      {
          $('#Edit_passed_out_year').append($('<option />').val(i).html(i));
      }
    });
  </script>
  
  
@endsection 


