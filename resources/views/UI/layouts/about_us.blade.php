@extends('UI.base')

@section('Content')
<section class="home-section-height" 

@if((request()->is('AboutUs')) ? 'active' : '')
style="height: 100px;"
@endif
>
        <div class="block no-padding overlape">
            <div class="container fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="main-featured-sec style2">
                            <ul class="main-slider-sec style2 text-arrows home-img aboutus_height">
                                <li class="slideHome"><img src="{{URL::asset('UI/images/resource/mslider3.jpg')}}" alt="" /></li>
                            </ul>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<section>
		<div class="block">
			<div class="container">
				 <div class="row">
                     <div class="col-lg-12 
                     @if(!(request()->is('AboutUs')) ? 'active' : '')
                     pad-top8percentage
@endif
">
				 		<div class="about-us aboutus-pad-left-right">
				 			<div class="row">
				 				<div class="col-lg-12">
				 					<h3>Who we are</h3>
				 				</div>
				 				<div class="col-lg-12">
                                     <p>We firmly believe that Accounting and Finance is a back bone of any business for smooth operations that has to be anchored by an able Accountant with increasing globalization, technological advancement and statutory compliances complexity in business transactions has increased manifolds. </p>
                                     
                                     <p>Being in the industry for almost 20 years now, we have observed need to upskills entry level candidates in Accounting and Finance , enhance specialized skills of experienced professionals by webinars, workshops, meetups and at the same time connect experts in the field to the startups and small business entities.</p>
                                     
                                     <p>We have learnt over a period of time that, many of the startup corporates and small business entities are suffering managing their business operations at ease only due to not understanding the importance of day to day accounting and finance management and lack of clarity on book keeping , statutory and compliance, and, cash flow management. We aim to add value to the business entities by providing common platform for all accounting ,finance and taxation professionals and employers for job opportunities available at each level business level.</p>
				 				</div>
				 				
                             </div>
                             

                             <div class="row pad-bottom5percentage">
                                <h3 class="our-team-head-pad">Executive team</h3>

                                <div class="col-lg-6 text-center">
                                    <img src="{{URL::asset('UI/images/team/suresh.jpeg')}}" class="team-img" alt="">

                                    <h5>Suresh Vankar <br> <b> Founder</b></h5>
                                </div>

                                <div class="col-lg-6 text-center">
                                    <img src="{{URL::asset('UI/images/team/falgun.jpeg')}}" class="team-img" alt="">
                                    <h5>CA Falgun Shah <br> <b>Advisor</b></h5>
                                </div>
                            </div>

                            <div class="row">
    
                                    <div class="col-lg-6 text-center">
                                        <img src="{{URL::asset('UI/images/team/gaurav.jpeg')}}" class="team-img" alt="">
                                        <h4>Gaurav Kumar <br> <b>Placement officer</b></h4>
                                    </div>
    
                                    <div class="col-lg-6 text-center">
                                        <img src="{{URL::asset('UI/images/team/Neelima.jpg')}}" class="team-img" alt="">
                                        <h4>Neelima Joshi <br> <b>Business Development Manager</b></h4>
                                    </div>
                                </div>
				 		</div>
				 	</div>
				 </div>
			</div>
		</div>
	</section>
@endsection