@extends('UI.base')


@section('Content')
<section class="overlape">
		<div class="block no-padding">
			<div data-velocity="-.1" style="background: url({{URL::asset('UI/images/resource/mslider1.jpg')}}) repeat scroll 50% 422.28px transparent;" class="parallax scrolly-invisible no-parallax"></div><!-- PARALLAX BACKGROUND IMAGE -->
			<div class="container fluid">
				<div class="row">
					<div class="col-lg-12">
						<div class="inner-header">
							<h3>Welcome {{Session::get('CandidateName')}}</h3>
						</div>
					</div>
				</div>
			</div>
		</div>
    </section>



<section>
		<div class="block remove-top">
			<div class="container">
				 <div class="row no-gape">
				 	@include('UI.common.candidate_sidebar')
				 	<div class="col-lg-9 column">
				 		<div class="padding-left">
					 		<div class="manage-jobs-sec">
                                <div class="border-title"><h3>My Resume</h3></div>
                                <div class="resumeadd-form">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <p>Resume is the most important document recruiters look for. Recruiters generally do not look at profiles without resumes.</p>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="upload-portfolio">
                                                <div class="uploadbox">
                                                    <a href="javascript:void(0);" onclick="BrowseCandidateCV('file')">
                                                        <label for="file-upload" class="custom-file-upload">
                                                            <i class="la la-cloud-upload"></i> <span>Upload Resume</span>
                                                        </label>
                                                    </a>
                                                    <input id="file" type="file" style="display: none;"/>
                                                </div>
                                                <div class="uploadfield">
                                                    <span class="pf-title">Resume Headline</span>
                                                    <div class="pf-field">
                                                        @if($GetCV)
                                                        <input placeholder="Eg: My Resume" type="hidden" value="{{$GetCV->id}}" id="id">
                                                        
                                                        <input placeholder="Eg: My Resume" type="text" value="{{$GetCV->title}}" id="resume_title">
                                                        @else
                                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                        <input placeholder="Eg: My Resume" type="text" id="resume_title">
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="uploadbutton">
                                                    <button type="button" onclick="InsertOrUpdateCV()">Update</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                         </div>
                         <br>
                         @if(isset($GetCV->cv))
                         <div class="manage-jobs-sec">
                            <div class="border-title"><h3>Resume Preview</h3></div>
                         <embed src="/candidate_cv/{{$GetCV->cv}}#toolbar=0&navpanes=0&scrollbar=0" type="application/pdf" width="100%" height="600px" />
                         </div>
                         @endif
					</div>
                 </div>
                 
			</div>
		</div>
    </section>
    


@endsection

{{-- @section('JSScript')
    <script>
        $('a.media').media({width:500, height:400});
    </script>
@endsection      --}}