@extends('UI.base')

@section('Content')
{{-- @foreach($GetCandidateDetails as $GetCandidateDetails) --}}
<section id="scroll-here" class="bg-grey">
    <div class="block">
        <div class="container">
            <div class="row candidate-row-mar-top">
                <div class="col-lg-12 col-md-6">
                    <div class="reviews-sec candiates-card-profile-top">
						<div class="row job-details-breadcrumb">
							<div class="col-lg-12 pad-top30">
							<p class="job-details-breadcrumb-p"><i class="fa fa-home clr-primary"></i> > <a href="#" class="clr-primary">All candidates</a> > {{$GetCandidateDetails->name}}</p>
							</div>
						</div>

						<div class="row">
							<div class="col-lg-12 candidate-card-pad-left-right">
								<div class="reviews candidates-profile-card-border mar-top15 mobile-card-pad-right">
									<div class="row">
										<div class="col-md-1 col-xs-12 pull-left pad-left30">
										<img class="candidate-details-img" src="{{URL::asset('trainee_profile')}}/{{$GetCandidateDetails->profile_pic}}" alt="" />
										</div>
										
										<div class="col-md-5 pull-left pad-left30 mobile-job-details-align-center candidate-details-mar-top">
											<h3 class="mobile-review-head">{{$GetCandidateDetails->name}} </h3>
											<span class="candidate-profile-position">
												Student
											</span> 
											<br>
											<p class="candidate-details-attributes"><i class="fa fa-map-marker"> {{$GetCandidateDetails->desired_location}}</i></p>

										<p class="candidate-details-member-since">Member Since, {{date('Y', strtotime($GetCandidateDetails->created_at))}}</p>
										</div>
		
										<div class="col-md-6 pull-right mobile-profile-center">
											
											{{-- <img id="ApplyBtnLoader" class="candidate-shortlisted-loader" src="{{URL::asset('UI/otp_verify_loader.gif')}}" alt="" style="display:none;">
		
												<p class="shortlisted-success " id="ShortlistedSuccess" style="display:none;"><i class="fa fa-check"></i> Shortlisted</p>
												
												@if($CheckShortlisted)
												<p class="applied-trainee-shortlisted-success text-left" id="ShortlistedSuccess"><i class="fa fa-check"></i> Shortlisted</p>
												@else

													<a href="javascript:void(0);" id="AddShortlisted" onclick="AddShortlisted({{$GetCandidateDetails->id}})">
														<p class=" width100 shortlist-jobs col-md-3 col-xs-12 pad-top5">
																<span class="shortlist-icon">
																	<i class="fa fa-heart-o"> </i>
																</span>
														<span class="shortlist-bold">Shortlist</span></p>
													</a>
											@endif --}}

											{{--  <a href="tel:{{$GetCandidateDetails->mobile}}" title="" class="for-employers-btn post-a-job-home col-md-4 text-center candidate-details-contact-btn">CONTACT</a>  --}}

											@if($GetCV)
												<a href="/candidate_cv/{{$GetCV->cv}}" onclick="DownloadCV()" title="" class="for-employers-btn post-a-job-home col-md-4 text-center" download>Download CV</a>
											@else
												<a href="javascript:void(0);" title="" class="for-employers-btn post-a-job-home col-md-4 text-center" download>Download CV</a>
											@endif
										</div>
									</div>
									
								</div>
							</div>
						</div>
						<div class="row candidate-details-pad-left-right">
							<div class="col-lg-12">
									<div class="reviews candidates-profile-card-border jobs-description-padding mar-top0">
										<div class="row job-description-p candidate-details-career ">
											<div class="col-md-3">
												<i class="fa fa-briefcase canddiate-details-career-icon"></i> 4-6 Years
												<p class="candidate-details-career-p">Experience</p>
											</div>
											
											<div class="col-md-3">
												<i class="fa fa-briefcase canddiate-details-career-icon"></i> 55K - 60K / Month
												<p class="candidate-details-career-p">Salary</p>
											</div>

											<div class="col-md-3">
												<i class="fa fa-briefcase canddiate-details-career-icon"></i> Bachelor's Degree
												<p class="candidate-details-career-p">Qualification</p>
											</div>
										</div>
										
										
										<div class="edu-history-sec candidate-education-pad-left">
												<h2 class="candiates-education-head">Education</h2>
												@foreach($GetEducation as $Education)
												<div class="pad-top20">
													<h4 class="candidate-work-experience-head">
													  @if($Education->title == 1)
														10th
													  @elseif($Education->title == 2)
														12th
													  @else
														{{$Education->title}}
													  @endif
													</h4>

													<p class="candidate-work-experience-p">{{$Education->name}}
													</p>
													{{-- 
													<p class="candidate-work-experience-p">Jan 2018 to Present (1 year 7 months)
													</p> --}}
													<p class="candidate-work-experience-p">{{$Education->from_year}} (
													  @if($Education->course_type == 1)
														Part time
													  @else
														Full time
													  @endif
													)
													</p>
												  </div>
												  <div class="work-experience-border pad-bottom10 candidate-details-border"></div>*
												@endforeach
											</div>
										
									</div>
								</div>
		
								
						</div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</section>
{{-- @endforeach --}}
@endsection
