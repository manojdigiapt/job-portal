@extends('UI.base')

@section('Content')
<section id="scroll-here" class="bg-grey">
    <div class="block">
        <div class="container">
            <div class="row candidate-row-mar-top">
                <div class="col-lg-12 col-md-6">
                    <div class="reviews-sec candiates-card-profile-top">
						<div class="row job-details-breadcrumb">
							<div class="col-lg-12 pad-top30">
								<p class="job-details-breadcrumb-p"><i class="fa fa-home clr-primary"></i> <i class="fa fa-caret-right"></i> Applied webinars</p>
							</div>
						</div>

						<div class="row">
							<div class="col-lg-12 candidate-card-pad-left-right">
								<div class="reviews candidates-profile-card-border mar-top15 mobile-card-pad-right">
									<div class="row">
										
										<div class="col-md-3 pull-left mobile-job-details-align-center candidate-details-mar-top">
											<h3 class="mobile-review-head applied-jobs-head"> <img src="{{URL::asset('UI/tick.svg')}}" class="applied-jobs-img" alt=""> {{count($GetAppliedWebinars)}} </h3>
											<p class="applied-jobs-total">Upcoming Webinars</p>
										</div>

									</div>
									
								</div>
							</div>
						</div>

						<div class="filterbar applied-jobs-head-sortby mar-bottom0">
							{{-- <div class="sortby-sec candidate-profile-sortby">
								<select data-placeholder="20 Per Page" class="chosen">
									<option selected disabled>Sort by</option>
									<option>40 Per Page</option>
									<option>50 Per Page</option>
									<option>60 Per Page</option>
								</select>
							</div> --}}
							<h5>Applied Webinars List</h5>
						</div>

						<div class="reviews-sec applied-jobs-list">
							<div class="row">
								@foreach($GetAppliedWebinars as $Webinars)
								<div class="col-lg-12">
									<div class="reviews candidates-profile-card-border mar-top15 mobile-card-pad-right">
											
											<div class="col-md-12 pull-left pad-left30 mobile-job-details-align-center">
												<h3 class="mobile-review-head pull-left">{{$Webinars->topic_name}} </h3>

												<h3 class="mobile-review-head text-right applied-webinar-date-mar-top"><b>Date: {{date("d M Y", strtotime($Webinars->webinar_date))}} </b> </h3>

												<span class="candidate-profile-position">
														{{$Webinars->FacultyName}}
												</span> 
												<br>
												<p class="job-details-attributes applied-webinars-attributes-width">Duration: {{$Webinars->duration}}</p>
			
												<p class="job-details-attributes applied-webinars-attributes-width">Timing: {{$Webinars->start_time}} to {{$Webinars->end_time}}</p>
			

												{{-- <p class="job-details-attributes applied-jobs-attributes-width"><i class="fa fa-industry"> {{$Jobs->name}}</i></p> --}}
												<h3 class="mobile-review-head pull-left">Price: @if($Webinars->webinar_type == 2)
														₹ {{$Webinars->price}}
													@else
														Free
													@endif</h3>
												
												@php 
												$GetCurrentTime = date('h:i A', time());
												$GetCurrentDate = date('Y-m-d', time());

												if($Webinars->webinar_date == $GetCurrentDate){
													if($Webinars->start_time <= $GetCurrentTime && $Webinars->end_time >= $GetCurrentTime) {
														echo "<a href=$Webinars->host_url title='' target='_blank'  class='for-employers-btn post-a-job-home col-md-2 text-center pull-right'>Join Now</a>";
													}
												}
												@endphp

											
											
											<div class="row pull-left pad-left30 mobile-job-details-align-center">
												
											</div>
										</div>

								</div>
							</div>
								@endforeach
							
						</div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

