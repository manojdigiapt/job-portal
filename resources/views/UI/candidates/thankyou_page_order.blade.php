@extends('UI.base')

@section('Content')
<section id="scroll-here" class="bg-grey">
    <div class="block">
        <div class="container">
            <div class="row candidate-row-mar-top">
                <div class="col-lg-12 col-md-6">
                    <div class="reviews-sec candiates-card-profile-top">
						<div class="row job-details-breadcrumb">
							<div class="col-lg-12 pad-top30">
								<p class="job-details-breadcrumb-p"><i class="fa fa-home clr-primary"></i> <i class="fa fa-caret-right"></i> Webinars</p>
							</div>
						</div>

                    </div>
                    
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 text-center pad-top10">
                    <h3>Thank you For your purchasing</h3>
                    <br>
                    <h5>Transaction id: <b><span id="paymentID"></span></b></h5>
                    <h5>Payment Date: <b><span id="PaymentDate"></span></b></h5>
                    <h5>Webinar name: <b><span id="WebinarName"></span></b></h5>
                    
                    <h5>Price: <b>₹ <span id="Price"></span></b></h5>
                    <div class="gotomywebinars-padleft">
                        <button onclick="window.location.href='/GetAppliedWebinars'" class="add-to-cart btn btn-default pull-left" type="button"> Go To My Webinars</button>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('JSScript')
    <script>
    var PaymentId = localStorage.getItem("PaymentId");
    var PaymentDate = Cookies.get('PaymentDate');
    var WebinarName = Cookies.get('WebinarName');
    var Price = Cookies.get('Price');

    $("#paymentID").text(PaymentId);
    $("#PaymentDate").text(PaymentDate);
    $("#WebinarName").text(WebinarName);
    $("#Price").text(Price);
    </script>
@endsection