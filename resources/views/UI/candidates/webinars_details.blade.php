@extends('UI.base')

@section('Content')
<section id="scroll-here" class="bg-grey">
    <div class="block">
        <div class="container">
            <div class="row candidate-row-mar-top">
                <div class="col-lg-12 col-md-6">
                    <div class="reviews-sec candiates-card-profile-top">
						<div class="row job-details-breadcrumb">
							<div class="col-lg-12 pad-top30">
								<p class="job-details-breadcrumb-p"><i class="fa fa-home clr-primary"></i> <i class="fa fa-caret-right"></i> Webinars</p>
							</div>
						</div>


						{{-- <div class="filterbar applied-jobs-head-sortby mar-bottom0">
							
							<h5>Webinars list</h5>
						</div> --}}
                        <div class="reviews-sec webinar-details-bg pad-top0 mar-top10">
                            <div class="wrapper row pad-top0 pad-bottom10">
                                <div class="preview col-md-5 col-lg-5">
                                    
                                    <div class="preview-pic tab-content">
                                      <div class="tab-pane active" id="pic-1"><img src="{{URL::asset('webinar_featured_image/')}}/{{$GetWebinars->featured_image}}" /></div>
                                    </div>
                                    
                                </div>
                                <div class="details col-md-7 col-lg-7">
                                <h3 class="product-title webinar-details-head" id="WebinarName">{{$GetWebinars->topic_name}}</h3>
                                
                                <div class="webinars-date-bg webinar-details-date">
                                    <p class="clr-white">{{date('M d Y', strtotime($GetWebinars->webinar_date))}}</p>
                                </div>

                                <p class="webinar-details-timing">Timing: {{$GetWebinars->start_time}} to {{$GetWebinars->end_time}}</p>

                                <div class="webinar-details-speaker-box">
                                    <p class="vote webinar-details-speaker-name mar-bottom0"> {{$GetWebinars->speaker_name}}</p>

                                    {!!implode(' ', array_slice(explode(' ', $GetWebinars->description), 0, 20))!!}
                                </div>
                                    {{-- <div class="rating">
                                        <div class="stars">
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                        </div>
                                        <span class="review-no">41 reviews</span>
                                    </div> --}}
                                    {{-- <p class="vote"><strong>Faculty: </strong> {{$GetWebinars->name}}</p> --}}

                                    

                                    {{-- <h5>Short Description</h5> --}}
                                    
                                    
                                    
                                    

                                    <form id="rzp-footer-form" action="{!!route('dopayment')!!}" method="POST" style="width: 100%; text-align: center" >
                                        @csrf
                        
                                        {{-- <p><br/>Price: 2,475 INR </p> --}}
                                        <input type="hidden" name="amount" id="amount" value="2475"/>
                                        {{-- <div class="pay">
                                            <button class="razorpay-payment-button btn filled small" id="paybtn" type="button">Pay with Razorpay</button>                        
                                        </div> --}}
                                    </form>
                                </div>
                            </div>

                            <div class="wrapper row pad-top0">
                                <div class="col-md-8 col-lg-8 webinars-details-pad-left">
                                    <h5 class="webinar-details-description">Description</h5>
                                    <p class="webinars-details-pad-left">{!!$GetWebinars->description!!}</p>
                                </div>

                                <div class="col-md-4 col-lg-4">

                                    <div class="webinars-details-pad-left webinars-div-border">
                                    {{-- <p>Duration: <b>{{date("d M Y", strtotime($GetWebinars->webinar_date))}}</b></p> --}}

                                        <p class="webinar-details-duration-box">Duration: {{$GetWebinars->duration}}</p>

                                        <h4 class="webinar-details-price">₹<span id="Price">
                                            @if($GetWebinars->webinar_type == 2)
                                             {{$GetWebinars->price}}
                                            @else
                                                Free
                                            @endif
                                        </span></h4>
                                        
                                            <input type="hidden" id="WebinarId" value="{{$GetWebinars->id}}">
                                            <input type="hidden" id="FacultyId" value="{{$GetWebinars->faculty_id}}">
    
                                            @if($CheckWebinarApplied)
                                                <h5 class="webinars-already-applied">Already applied</h5>
                                            @else
                                            @if($GetWebinars->webinar_type == 1)
                                                <button class="add-to-cart btn btn-default pull-left webinar-details-enroll-now-btn" type="button" id="EnrollNow">Enroll now</button>
                                            @else
                                                <button class="add-to-cart btn btn-default pull-left webinar-details-enroll-now-btn" type="button" id="paybtn"> Enroll now</button>
                                            @endif
                                            @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{-- <div class="wrapper row">
                            
                        </div> --}}

                        <br>
                        <br>
                        <div class="wrapper row blog">
                            <h3 class="webinars-details-pad-left width100 pad-bottom30 similar-webinar-head">Similar Webinars</h3>
                            <br>

                            @foreach($GetSimilarWebinars as $Webinars)
                            <div class="col-md-4 mt-4 mar-top0">
                                <div class="card profile-card-5">
                                    {{-- <div class="card-img-block">
                                        
                                    </div> --}}
                                    <div class="card-body pt-0 pad-left-right0">
                                        <img class="card-img-top webinars-img-height webinar-date-mar-bottom" src="{{URL::asset('webinar_featured_image/')}}/{{$Webinars->featured_image}}" alt="Card image cap">

                                        <div class="webinars-date-bg webinar-date-position">
                                            <p class="clr-white">{{date('M d Y', strtotime($Webinars->webinar_date))}}</p>
                                        </div>
                                        

                                        <div class="padding30 pad-top20percentage">
                                            <h5 class="card-title webinar-title">{{$Webinars->topic_name}}</h5>
                                            <p class="mar-bottom0 webinars-faculty-bold webinar-speaker-name">{{$Webinars->speaker_name}}</p>
                                            {!!implode(' ', array_slice(explode(' ', $Webinars->description), 0, 10))!!}
    
                                            <p>Timing: {{$Webinars->start_time}} to {{$Webinars->end_time}}</p>
                                            <a href="/WebinarsDetails/{{$Webinars->id}}" class="btn btn-primary card-btn pull-left" target="_blank">REGISTER TODAY</a>
    
                                            {{-- <h6 class="pull-right">Price: <b>@if($Webinars->webinar_type == 2)
                                                    ₹ {{$Webinars->price}}
                                                @else
                                                    Free
                                                @endif</b></h6> --}}
                                        </div>
                                    
                                  </div>
                                </div>
                            </div>
                            @endforeach
                            
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</section>
@endsection


@section('JSScript')
<script>
	/*Tooltip*/
$(function () {
  $('[data-toggle="tooltip"]').tooltip();
});
</script>


<script>
    $('#rzp-footer-form').submit(function (e) {
        var button = $(this).find('button');
        var parent = $(this);
        button.attr('disabled', 'true').html('Please Wait...');
        $.ajax({
            method: 'get',
            url: this.action,
            data: $(this).serialize(),
            complete: function (r) {
                console.log('complete');
                console.log(r);
            }
        })
        return false;
    })
</script>

<script>
    function padStart(str) {
        return ('0' + str).slice(-2)
    }

    function demoSuccessHandler(transaction) {
        // You can write success code here. If you want to store some data in database.
        $("#paymentDetail").removeAttr('style');
        $('#paymentID').text(transaction.razorpay_payment_id);
        var paymentDate = new Date();
        $('#paymentDate').text(
                padStart(paymentDate.getDate()) + '.' + padStart(paymentDate.getMonth() + 1) + '.' + paymentDate.getFullYear() + ' ' + padStart(paymentDate.getHours()) + ':' + padStart(paymentDate.getMinutes())
                );

        var payment_id = transaction.razorpay_payment_id;
        // var payment_date = PaymentGetDate;
        var webinar_id = $("#WebinarId").val();

        var webinar_name = $("#WebinarName").text();
        var webinar_price = $("#Price").text();

        // var PaymentData = {
        //     payment_id: payment_id,
        //     // payment_date: payment_date,
        //     webinar_id: webinar_id,
        //     "_token": "{{ csrf_token() }}"
        // }
        // console.log(webinar_id);
        $.ajax({
            method: 'post',
            url: "{!!route('dopayment')!!}",
            data: {
                "_token": "{{ csrf_token() }}",
                "razorpay_payment_id": transaction.razorpay_payment_id,
                "payment_date": 
                padStart(paymentDate.getDate()) + '.' + padStart(paymentDate.getMonth() + 1) + '.' + paymentDate.getFullYear() + ' ' + padStart(paymentDate.getHours()) + ':' + padStart(paymentDate.getMinutes()),
                "webinar_id": webinar_id
            },
            success: function (r) {
                if (typeof r.razorpay_payment_id == 'undefined' ||  r.razorpay_payment_id < 1) {
                    success_toast_msg("Complete");
                    var WebinarId = $("#WebinarId").val();
    var FacultyId = $("#FacultyId").val();

    $.ajax({
        type: "POST",
        url: "/ApplyWebinar",
        data: {WebinarId: WebinarId, FacultyId: FacultyId},
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message);
                return false;
            }else{
                localStorage.setItem("PaymentId", transaction.razorpay_payment_id);

                Cookies.set('PaymentDate',  
                 padStart(paymentDate.getDate()) + '.' + padStart(paymentDate.getMonth() + 1) + '.' + paymentDate.getFullYear() + ' ' + padStart(paymentDate.getHours()) + ':' + padStart(paymentDate.getMinutes()));

                 Cookies.set('WebinarName', webinar_name);
                 Cookies.set('Price', webinar_price);
                // localStorage.setItem("PaymentDate", 
                // padStart(paymentDate.getDate()) + '.' + padStart(paymentDate.getMonth() + 1) + '.' + paymentDate.getFullYear() + ' ' + padStart(paymentDate.getHours()) + ':' + padStart(paymentDate.getMinutes()));
                // localStorage.setItem("WebinarName", webinar_name);
                // localStorage.setItem("Price", webinar_price);

                window.location.href = "/Thankyou";
                // success_toast_msg(data.message);
                return false;
            }
        }
    });
                    return false;
                } else {
                    danger_toast_msg("Failed");
                    return false;
                }
                // console.log('complete');
                // console.log(r);
            }
        })
    }
</script>
<script>
    var Price = $("#Price").text() * 100;
    // console.log(Price);
    var options = {
        key: "{{ env('RAZORPAY_KEY') }}",
        amount: Price,
        name: $("#WebinarName").text(),
        // description: 'TVS Keyboard',
        image: 'http://accountswale.in/UI/Logo.svg',
        handler: demoSuccessHandler
    }
</script>
<script>
    window.r = new Razorpay(options);
    document.getElementById('paybtn').onclick = function () {
        r.open()
    }
</script>

@endsection