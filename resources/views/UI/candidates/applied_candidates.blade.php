@extends('UI.base')

@section('Content')
{{-- @foreach($GetCandidateDetails as $GetCandidateDetails) --}}
<section id="scroll-here" class="bg-grey">
    <div class="block">
        <div class="container">
            <div class="row candidate-row-mar-top">
                <div class="col-lg-12 col-md-6">
                    <div class="reviews-sec candiates-card-profile-top">
						<div class="row job-details-breadcrumb">
							<div class="col-lg-12 pad-top30">
							<p class="job-details-breadcrumb-p"><i class="fa fa-home clr-primary"></i> <i class="fa fa-caret-right"></i> <a href="#" class="clr-primary">All candidates</a> <i class="fa fa-caret-right"></i> {{$GetCandidateDetails->name}}</p>
							</div>
						</div>

						<div class="row">
							<div class="col-lg-12 candidate-card-pad-left-right">
								<div class="reviews candidates-profile-card-border mar-top15 mobile-card-pad-right">
									<div class="row">
										<div class="col-md-12 col-xs-12 pull-left pad-left30">
										@if($GetCandidateDetails->type == 1)
										<img class="candidate-details-img" src="{{URL::asset('candidate_profile')}}/{{$GetCandidateDetails->profile_pic}}" alt="" />
										</div>
										@elseif($GetCandidateDetails->type == 2)
										<img class="candidate-details-img" src="{{URL::asset('trainee_profile')}}/{{$GetCandidateDetails->profile_pic}}" alt="" />
										</div>
										@endif
										
										<div class="col-md-5 pull-left pad-left30 mobile-job-details-align-center candidate-details-mar-top">
											<h3 class="mobile-review-head">{{$GetCandidateDetails->name}} </h3>
											<span class="candidate-profile-position">
												@if($GetProfessionalDetails)
													{{$GetProfessionalDetails->name}}
												@endif
											</span> 
											<br>
											<p class="candidate-details-attributes"><i class="fa fa-map-marker"> {{$GetCandidateDetails->city}}</i></p>

										<p class="candidate-details-member-since"></p>
										</div>
		
										<div class="col-md-6 pull-right mobile-profile-center">
											
											<img id="ApplyBtnLoader" class="candidate-shortlisted-loader" src="{{URL::asset('UI/otp_verify_loader.gif')}}" alt="" style="display:none;">
	
												<p class="shortlisted-success " id="ShortlistedSuccess" style="display:none;"><i class="fa fa-check"></i> Shortlisted</p>
												
											@if($GetCandidateDetails->type == 1)
											@if($CheckShortlisted)
													<p class="applied-trainee-shortlisted-success" id="ShortlistedSuccess"><i class="fa fa-check"></i> Shortlisted</p>
												@else
													<a href="javascript:void(0);" id="AddShortlisted" onclick="AddShortlisted({{$GetCandidateDetails->id}})">
														<p class=" width100 shortlist-jobs col-md-3 col-xs-12 pad-top5">
																<span class="shortlist-icon">
																	<i class="fa fa-heart-o"> </i>
																</span>
														<span class="shortlist-bold">Shortlist</span></p>
													</a>
												@endif
											@elseif($GetCandidateDetails->type == 3)
											
											@endif
												

											{{--  <a href="tel:{{$GetCandidateDetails->mobile}}" title="" class="for-employers-btn post-a-job-home col-md-4 text-center candidate-details-contact-btn">CONTACT</a>  --}}
											
											{{-- <p id="DownloadCVPath" style="display:none;">/candidate_cv/{{$GetCV->cv}}</p> --}}
											@if($GetCandidateDetails->type == 1)
											<p id="DownloadCVPath" style="display:none;">/candidate_cv/{{$GetCV->cv}}</p>
											@elseif($GetCandidateDetails->type == 2)
											<p id="DownloadCVPath" style="display:none;">/trainee_cv/{{$GetCV->cv}}</p>
											@elseif($GetCandidateDetails->type == 3)
											<p id="DownloadCVPath" style="display:none;">/article_cv/{{$GetCV->cv}}</p>
											@endif
											
											@if($GetCV)
											@if($GetCandidateDetails->type == 3)
												<a onclick="DownloadCV({{$GetCandidateDetails->id}}, {{$GetUpdateRecruiterStatus->jobs_id}})" title="" class="pull-right for-employers-btn post-a-job-home col-md-4 text-center" download>Download CV</a>
												
											@else
												<a onclick="DownloadCV({{$GetCandidateDetails->id}}, {{$GetUpdateRecruiterStatus->jobs_id}})" title="" class="download-cv-details for-employers-btn post-a-job-home col-md-4 text-center" download>Download CV</a>
											@endif
											@else
												<a href="javascript:void(0);" title="" class="for-employers-btn post-a-job-home col-md-4 text-center" download>Download CV</a>
											@endif
										</div>
									</div>
									
								</div>
							</div>
						</div>
						<br>
						<div class="col-md-12 candidate-details-pad-left-right mar-top10">
							<div class="col-lg-12 col-md-12">
									<div class="reviews candidates-profile-card-border jobs-description-padding mar-top0">
										<div class="row job-description-p candidate-details-career ">
											@if($GetCandidateDetails->experience)
											<div class="col-md-3 candidate-details-years">
												<i class="fa fa-briefcase canddiate-details-career-icon"></i>
												@php
													if($GetCandidateDetails->experience){
														$TotalExperience = $GetCandidateDetails->experience;
													$GetTotalExperience = explode(',', $TotalExperience);
								
													echo $GetTotalExperience[0].' Years '.$GetTotalExperience[1].' Months';
													}else{

													}
													
												@endphp
												<p class="candidate-details-career-p candidate-details-years-p-left">Experience</p>
											</div>
											@endif

											@if($GetCandidateDetails->experience)
											<div class="col-md-3 candidate-details-salary">
												<i class="fa fa-briefcase canddiate-details-career-icon"></i> 
												{{-- @php
													$TotalSalary = $GetCandidateDetails->gross_salary;
													$GetTotalSalary = explode(',', $TotalSalary);
								
													echo $GetTotalSalary[0].' Lacs '.$GetTotalSalary[1].' Thousand';
												@endphp --}}

												{{$GetCandidateDetails->gross_salary}}
												<p class="candidate-details-career-p">Salary</p>
											</div>
											@endif

											<div class="col-md-3 pad-top15">
												<i class="fa fa-briefcase canddiate-details-career-icon"></i> 
												@if(isset($GetQualification))
													{{$GetQualification->qualification}}
												@else
													{{$GetQualification['qualification']}}
												@endif
												<p class="candidate-details-career-p">Degree</p>
											</div>
										</div>
										
										<div class="row Skill-row">
											@if(isset($GetSkillNames))
											<h5 class="candidate-career-profile-table-head skills-head">Skills
											</h5>
											<div class="skills-badge mar-bottom0 mar-top0 skill-name">
												@if(isset($GetSkillNames))
													@for($i = 0; $i < count($GetSkillNames); $i++) 
														<span>{{$GetSkillNames[$i]}}</span>
													@endfor
												@endif 
											</div>
											@endif
										</div>
										<div class="candidate-details-about-top-border"></div>
										{{-- <div class="row">
											<h5 class="candidate-details-about">About Candidate</h5>

											<p class="candiadte-details-p">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
										</div> --}}

										<div class="edu-history-sec candidate-education-pad-left">
												<h2 class="candiates-education-head">Education</h2>
												@foreach($GetEducation as $Education)
												<div class="pad-top20">
													<h4 class="candidate-work-experience-head">
													  {{-- @if($Education->title == '10th')
														10th
													  @elseif($Education->title == '12th')
														12th
													  @else
														{{$Education->qualification}}
													  @endif --}}
													  {{$Education->qualification}}
													</h4>

													<p class="candidate-work-experience-p">{{$Education->name}}
													</p>
													{{-- 
													<p class="candidate-work-experience-p">Jan 2018 to Present (1 year 7 months)
													</p> --}}
													<p class="candidate-work-experience-p">{{$Education->from_year}} (
													  @if($Education->course_type == 1)
														Part time
													  @else
														Full time
													  @endif
													)
													</p>
												  </div>
												  <div class="work-experience-border pad-bottom10 candidate-details-border"></div>
												@endforeach
											</div>

											@if($GetCandidateDetails->experience)
											<div class="edu-history-sec candidate-education-pad-left">
												<h2 class="candiates-education-head">Work & Experience</h2>
												<br>
												@if(isset($GetExperience))
												@foreach($GetExperience as $Experience)
												<div class="pad-top10">
													<h4 class="candidate-work-experience-head">{{$Experience['JobTitle']}}
													</h4>
													<p class="candidate-work-experience-p">{{$Experience->name}}
													</p>
													{{-- 
													<p class="candidate-work-experience-p">Jan 2018 to Present (1 year 7 months)
													</p> --}}
													<p class="candidate-work-experience-p">
													  {{$Experience->from_month}} {{$Experience->from_year}} to {{$Experience->to_month}} {{$Experience->to_year}}
													  (
													  @php
													  
													  $d1 = new DateTime($Experience->from_year."-".$Experience->from_month);
													  $d2 = new DateTime($Experience->to_year."-".$Experience->to_month);
									  
													  $diff = $d2->diff($d1);
													  @endphp
													  {{$diff->y}} Years {{$diff->m}} Months
													  )
													  @if($Experience->worked_till == 1)
													<p class="candidate-work-experience-p mar-bottom0">Available to join in @if($Experience->notice_period == 1)
													  {{15}}
													  @else
													  {{30}}
													  @endif
													  Days</p>
													@endif
													<p class="candidate-work-experience-p candidate-work-experience-p-description">{{$Experience->description}}
													
												  </div>
												  
												  <div class="work-experience-border candidate-details-work-experience"></div>
												@endforeach
												@endif
											</div>
											@endif
									</div>
								</div>
		
								
						</div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</section>
{{-- @endforeach --}}
@endsection
