@extends('UI.base')

@section('Content')
<section id="scroll-here" class="bg-grey">
    <div class="block">
        <div class="container">
            <div class="row candidate-row-mar-top">
                <div class="col-lg-12 col-md-6">
                    <div class="reviews-sec candiates-card-profile-top">
						<div class="row job-details-breadcrumb">
							<div class="col-lg-12 pad-top30">
								<p class="job-details-breadcrumb-p"><i class="fa fa-home clr-primary"></i> <i class="fa fa-caret-right"></i> Webinars</p>
							</div>
						</div>


						<div class="filterbar applied-jobs-head-sortby mar-bottom0">
							{{-- <div class="sortby-sec candidate-profile-sortby">
								<select data-placeholder="20 Per Page" class="chosen">
									<option selected disabled>Sort by</option>
									<option>40 Per Page</option>
									<option>50 Per Page</option>
									<option>60 Per Page</option>
								</select>
							</div> --}}
							<h5>Webinars list</h5>
						</div>

                        <div class="reviews-sec applied-jobs-list">
							<div class="row">
                                @foreach($GetWebinars as $Webinars)
								<div class="col-md-4 mt-4">
                                    <div class="card profile-card-5">
                                        {{-- <div class="card-img-block">
                                            
                                        </div> --}}
                                        <div class="card-body pt-0 pad-left-right0">
                                            <img class="card-img-top webinars-img-height webinar-date-mar-bottom" src="{{URL::asset('webinar_featured_image/')}}/{{$Webinars->featured_image}}" alt="Card image cap">

                                            <div class="webinars-date-bg webinar-date-position">
                                                <p class="clr-white">{{date('M d Y', strtotime($Webinars->webinar_date))}}</p>
                                            </div>
                                            

                                            <div class="padding30 pad-top20percentage">
                                                <h5 class="card-title webinar-title">{{$Webinars->topic_name}}</h5>
                                                <p class="mar-bottom0 webinars-faculty-bold webinar-speaker-name">{{$Webinars->speaker_name}}</p>
                                                {!!implode(' ', array_slice(explode(' ', $Webinars->description), 0, 10))!!}
        
                                                <p>Timing: {{$Webinars->start_time}} to {{$Webinars->end_time}}</p>
                                                <a href="/WebinarsDetails/{{$Webinars->id}}" class="btn btn-primary card-btn pull-left" target="_blank">REGISTER TODAY</a>
        
                                                {{-- <h6 class="pull-right">Price: <b>@if($Webinars->webinar_type == 2)
                                                        ₹ {{$Webinars->price}}
                                                    @else
                                                        Free
                                                    @endif</b></h6> --}}
                                            </div>
                                        
                                      </div>
                                    </div>
                                </div>
                                @endforeach
                                
							</div>
						</div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
