@extends('UI.base')
@section('Content')
<section id="scroll-here">
  <div class="block caniddate-profile-pad-lef-right-top">
    <div class="container">
      <div class="row" id="ProfileCompletion">
            <div class="col-md-8">
                <h4>Hi, {{$Profile['name']}}</h4>
                <p>Complete your profile to get more attention by employer</p>
            </div>
            {{-- <div class="col-md-4 pad-top15" >
                <p class="candidate-profile-completion-p clr-black">Profile Completion
                </p>
                <p class="candiate-profile-percentage clr-black">
                  {{$TraineeProfile['profile_completion']}}%
                </p>
                <div id="progressbar" class="progress-bar-bg">
                  <div style="width: {{$TraineeProfile['profile_completion']}}%" class="progress-loader-clr">
                    </div>
                  
                </div>
            </div> --}}
      </div>

      <br>

      <div class="row">
          <div class="col-md-12 form-wizard-pad pad-left-right0">
            <div class="stepwizard form-wizard-border">
                <div class="stepwizard-row setup-panel">
                    <div class="stepwizard-step">
                        <a href="#step-1" type="button" class="btn btn-circle from-wizrd-success current step-1 cursor-not-allowed step-1-success" style="display:none;"> <i class="fa fa-check form-wizard-success-tick"></i> <span class="span-profile-sec">My Resume</span></a>
                        
                        <a href="#step-1" type="button" class="btn  btn-circle form-wizard-ongoing-bg pad0 current step-1 cursor-not-allowed step-1-failed"> <i class="fa fa-check form-wizard-success-tick" style="display:none;"></i> <span>1</span> <span class="form-ongoing-clr">My Resume</span></a>

                        {{-- <p>Step 1</p> --}}
                    </div>
                    <div class="stepwizard-step pad-left20">
                        <a href="#step-4" type="button" class="btn btn-circle from-wizrd-success step-4 cursor-not-allowed step-4-success" style="display:none;"> <i class="fa fa-check form-wizard-success-tick"></i> <span class="span-profile-sec">Education</span></a>
                        
                        <a href="#step-4"  type="button" class="btn btn-default btn-circle form-wizard-pending step-4 cursor-not-allowed step-4-failed" disabled="disabled"> <i class="fa fa-check form-wizard-success-tick" style="display:none;"></i> <span>2</span> <span class="pad-left15">Education</span></a>
                        {{-- <p>Step 4</p> --}}
                    </div>
                    <div class="stepwizard-step">
                        <a href="#step-5" type="button" class="btn btn-circle from-wizrd-success step-5 cursor-not-allowed step-5-success" style="display:none;"> <i class="fa fa-check form-wizard-success-tick"></i> <span class="span-profile-sec">Personal Details</span></a>
                        
                        <a href="#step-5" type="button" class="btn btn-default btn-circle form-wizard-pending step-5 cursor-not-allowed step-5-failed" disabled="disabled">  <span>3</span> <span class="pad-left15">Personal Details</span></a>
                        {{-- <p>Step 5</p> --}}
                    </div>
                </div>
            </div>
            <form role="form">
                <div class="row setup-content form-wizard-content-pad" id="step-1">
                    <div class="col-md-12">
                        <div class="uploadbox form-wizard-uploadbox-height">
                            <a href="javascript:void(0);" onclick="BrowseFirstRegisterTraineeCV('first_cv')">
                                <label for="file-upload" class="custom-file-upload pad-top30">
                                    <i class="la la-cloud-upload">
                                    </i> 
                                    <span>Upload Resume
                                    </span>
                                </label>
                            </a>
                            <input id="first_cv" type="file" style="display: none;">
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="col-md-4 pad-top5percentage pull-left">
                            {{-- <h5 class="form-wizard-back-btn BackBtn">< BACK</h5> --}}
                        </div>

                        <div class="col-md-4 pad-top5percentage pull-left">
                            <h5 class="form-wizard-back-btn form-wizard-skip-now-btn nextBtn">SKIP FOR NOW</h5>
                        </div>


                        <div class="col-md-4 pad-top5percentage pull-left">
                            <a href="javascript:void(0);" title="" class="for-employers-btn post-a-job-home text-center download-cv-mar pull-right form-wizard-next-btnpmar-top  nextBtn">NEXT</a>
                        </div>
                    </div>
                </div>
                

                <div class="row setup-content pad10 pad-top5percentage" id="step-4">
                    <div class="col-md-12">
                        <form class="mar-top15">

                                {{-- <label class="pull-left remove-label candidate-profile-label">Board *
                                </label> --}}
                                {{-- <div class="cfield dropdown-year-month-width">
                                    <select data-placeholder="Please Select Industries" class="chosen board" id="board" onchange="CheckBoard()">
                                        <option selected disabled>Select Board</option>
                            <option value="10th">10th
                            </option>
                            <option value="12th">12th
                                        </option>
                            <option value="3">Graduate
                                        </option>
                                    </select>
                                </div> --}}
                        
                        <div id="CourseNameRow">
                            <label class="pull-left remove-label candidate-profile-label">Degree *
                            </label>
                            {{-- <div class="cfield">
                            <input type="text" placeholder="Course name" value="" id="course_name" class="bg-white input-border input-height44 mar-bottom25 course_name" id=""/>
                            </div> --}}
                            <select data-placeholder="Please Select Industries" class="chosen" id="board">
                                        <option selected disabled>Select Degree</option>
                                @foreach($GetQualification as $Qualification)
                            <option value="{{$Qualification->id}}">{{$Qualification->qualification}}
                                </option>
                                @endforeach
                                    </select>
                        </div>
                
                        <br>
                                <label class="pull-left remove-label candidate-profile-label mar-top15">University / Board *
                                </label>
                                <div class="cfield">
                                    <input type="text" placeholder="School name" value="" id="school_clg" class="bg-white input-border input-height44 mar-bottom25" id=""/>
                                </div>
                                
                                <label class="pull-left remove-label candidate-profile-label">Passing Out Year *
                                    </label>
                                    <div class="cfield dropdown-year-month-width">
                                    <select class="dropdown-custom-clr-pad" id="passed_out_year">
                                        <option selected disabled>Select year</option>
                                        {{--  <option value="2014">2014
                                        </option>
                                        <option value="2018">2018</option>  --}}
                                    </select>
                                    </div>
                
                                    {{-- <div class="cfield dropdown-year-month-width">
                                    <select class="chosen" id="course_type">
                                        <option selected disabled>Select type</option>
                                        <option value="1">Part time
                                        </option>
                                        <option value="2">Full time</option>
                                    </select>
                            </div> --}}
                            {{-- <label class="pull-left remove-label candidate-profile-label mar-top15">Percentage
                            </label> --}}
                            <div class="cfield width50 pull-left">
                            <input type="text" placeholder="Percentage" value="" id="percentage" class="bg-white input-border input-height44 mar-bottom25" id="" maxlength="2">
                            </div>

                            <a href="javascript:void(0);" title="" class="for-employers-btn post-a-job-home text-center download-cv-mar pull-right AddTraineeEducation"> <i class="fa fa-plus"></i> Add Education</a>

                            <br>
                        </form>

                        <table class="table table-hover" id="EducationDetails">
                            <thead>
                              <tr>
                                <th>Degree</th>
                                <th>University / Board</th>
                                <th>Year of passed</th>
                                <th>Percentage</th>
                                <th>Action</th>
                              </tr>
                            </thead>
                            <tbody>
                                @foreach($GetEducation as $Education)
                              <tr>
                                <td>{{$Education->qualification}}</td>
                                <td>{{$Education->name}}</td>
                                <td>{{$Education->from_year}}</td>
                                <td>{{$Education->percentage}}</td>
                                <td><a href="javascript:void(0);" onclick="EditEducation({{$Education->id}})">
                                    <i class="fa fa-pencil">
                                    </i>
                                  </a>
                  
                                  <a href="javascript:void(0);" onclick="DeleteEducation({{$Education->id}})">
                                      <i class="fa fa-trash">
                                      </i>
                                    </a></td>
                              </tr>
                              @endforeach
                            </tbody>
                          </table>
                    </div>

                    <div class="col-md-12">
                        <div class="col-md-4 pad-top5percentage pull-left">
                            {{-- <h5 class="form-wizard-back-btn BackBtn">< BACK</h5> --}}
                        </div>

                        <div class="col-md-4 pad-top5percentage pull-left">
                            <h5 class="form-wizard-back-btn form-wizard-skip-now-btn nextBtn">SKIP FOR NOW</h5>
                        </div>
    
                            <div class="col-md-4 pad-top5percentage pull-left">
                                <a href="javascript:void(0);" title="" class="for-employers-btn post-a-job-home text-center download-cv-mar pull-right form-wizard-next-btnpmar-top nextBtn">NEXT</a>
                            </div>
                        </div>
                </div>


                <div class="row setup-content pad10 pad-top5percentage" id="step-5">
                    <div class="col-md-12">
                            <form class="mar-top15">

                                    <label class="pull-left remove-label candidate-profile-label">Date of Birth 
                                        </label>
                                        <div class="cfield width50">
                
                                          <input type="date" placeholder="Date of birth" id="Edit_dob" value="" class="bg-white input-border input-height44 mar-bottom25" id=""/>
                                        </div>
                                
                                <label class="pull-left remove-label candidate-profile-label">Gender 
                                    </label>
                                    <div class="cfield dropdown-year-month-width">
                                      <select class="dropdown-custom-clr-pad" id="Edit_gender">
                                          <option selected disabled>Select gender</option>
                                          <option value="1">Male
                                          </option>
                                          <option value="2">Female</option>
                                      </select>
                                    </div>
                  
                                    <div class="cfield dropdown-year-month-width">
                                      <select class="dropdown-custom-clr-pad" id="Edit_marital_status">
                                          <option selected disabled>Marital Status</option>
                                          <option value="1">Married
                                          </option>
                                          <option value="2">Un married</option>
                                      </select>
                                      </div>
                
                                <label class="pull-left remove-label candidate-profile-label">Father's Name 
                                </label>
                                <div class="cfield">
                                  <input type="text" placeholder="Father name" value="" class="bg-white input-border input-height44 mar-bottom25" id="Edit_father_name"/>
                                </div>
                        
                        <div id="GetProfileAddress">
                                <label class="pull-left remove-label candidate-profile-label">Address 
                                </label>
                                <div class="cfield">
                        <input type="text" placeholder="Address" data-geocomplete="street address" value="" id="Edit_address" class="bg-white input-border input-height44 mar-bottom25" id=""/>
                        </div>
                        
                        <label class="pull-left remove-label candidate-profile-label">State 
                          </label>
                          <div class="cfield">
                          <input type="text" placeholder="State" data-geocomplete="state" value="" id="Edit_state" class="bg-white input-border input-height44 mar-bottom25" id=""/>
                          </div>
                
                          <label class="pull-left remove-label candidate-profile-label">City 
                            </label>
                            <div class="cfield">
                            <input type="text" placeholder="City" data-geocomplete="city" value="" id="Edit_city" class="bg-white input-border input-height44 mar-bottom25" id=""/>
                            </div>
                
                            <label class="pull-left remove-label candidate-profile-label">Zip code 
                              </label>
                              <div class="cfield">
                              <input type="text" placeholder="Zip code" data-geocomplete="zip code" value="" id="Edit_zipcode" class="bg-white input-border input-height44 mar-bottom25" id=""/>
                              </div>
                          </div>
                        </form>

                        {{-- <a href="javascript:void(0);" title="" class="for-employers-btn post-a-job-home text-center download-cv-mar pull-right UpdateCandidatePersonalDetails">Update Personal Details</a> --}}
                    </div>

                    <div class="col-md-12">
                        <div class="col-md-4 pad-top5percentage pull-left">
                            {{-- <h5 class="form-wizard-back-btn BackBtn">< BACK</h5> --}}
                        </div>

                        <div class="col-md-4 pad-top5percentage pull-left">
                            <h5 class="form-wizard-back-btn form-wizard-skip-now-btn nextBtn">SKIP FOR NOW</h5>
                        </div>
    
                            <div class="col-md-4 pad-top5percentage pull-left">
                                <a href="javascript:void(0);" title="" class="for-employers-btn post-a-job-home text-center download-cv-mar pull-right form-wizard-next-btnpmar-top UpdateTrineePersonalDetails">FINISH</a>
                            </div>
                        </div>
                </div>
            </form>

            <div class="col-md-12 exit-dashboard">
               <a href="/Trainee/Dashboard"> <h4 class="text-center clr-grey">EXIT TO DASHBOARD <i class="fa fa-caret-right"></i></h4></a>
            </div>
          </div>
          
      </div>
    </div>
  </div>
  <div class="bottom-line bg-grey">
    <span>© 2019 Techtalents Software Technologies Pvt Ltd. All rights reserved.</span>
</div>
</section>

  
  
@endsection 




@section('JSScript')

<script>
$(document).ready(function () {
      for (i = new Date().getFullYear(); i > 1990; i--)
      {
          $('#exp_from_year').append($('<option />').val(i).html(i));
      }
    });

    $(document).ready(function () {
      for (i = new Date().getFullYear(); i > 1990; i--)
      {
          $('#exp_to_year').append($('<option />').val(i).html(i));
      }
    });

    $(document).ready(function () {
      for (i = new Date().getFullYear(); i > 1990; i--)
      {
          $('#passed_out_year').append($('<option />').val(i).html(i));
      }
    });
</script>
    
@endsection