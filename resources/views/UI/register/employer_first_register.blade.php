@extends('UI.base')
@section('Content')
<section id="scroll-here">
  <div class="block caniddate-profile-pad-lef-right-top">
    <div class="container">
      <div class="row" id="ProfileCompletion">
            <div class="col-md-8">
                <h4>Hi, {{$Profile['company_name']}}</h4>
                {{-- <p>Complete your profile to get more attention by employer</p> --}}
            </div>
            <div class="col-md-4 pad-top15" >
                {{-- <p class="candidate-profile-completion-p clr-black">Profile Completion
                </p>
                <p class="candiate-profile-percentage clr-black">
                  {{$CandidateProfile['profile_completion']}}%
                </p>
                <div id="progressbar" class="progress-bar-bg">
                  <div style="width: {{$CandidateProfile['profile_completion']}}%" class="progress-loader-clr">
                    </div>
                  
                </div> --}}
            </div>
      </div>

      <br>

      <div class="row">
          <div class="col-md-12 form-wizard-pad pad-left-right0">
            <div class="stepwizard form-wizard-border">
                
            </div>
            
            <form class="mar-top15 employer-first-register-pad-left-right">

                <div class="row">
                    <h3 class="text-center">Profile Details</h3>
                </div>

                <br>

				<div class="row">
					<div class="dropdown-field col-md-12 pad-left0 pad-right0 mar-right10">
						<label class="pull-left remove-label">Company Name *
							</label>
						<div class="cfield">
							<input type="text" placeholder="Company Name" value="" class="bg-white input-border-line" id="company_name"/>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="dropdown-field col-md-12 pad-left0 pad-right0 mar-right10">
						<label class="pull-left remove-label">Mobile *
							</label>
						<div class="cfield">
							<input type="text" placeholder="Mobile no" value="" class="bg-white input-border-line" id="mobile" maxlength="10"/>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="dropdown-field col-md-12 pad-left0 pad-right0 mar-right10">
						<label class="pull-left remove-label">Email *
							</label>
						<div class="cfield">
							<input type="text" placeholder="Email" value="" class="bg-white input-border-line" id="email"/>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="dropdown-field col-md-12 pad-left0 pad-right0 mar-right10">
						<label class="pull-left remove-label">Employer Type *
							</label>
						<div class="cfield">
							<select  data-placeholder="Please Select Functional Areas" class="dropdown-custom-clr-pad" id="Emp_type">
								<option value="1">CA Firms
								</option>
								<option value="2">CS Firms
								</option>
								<option value="3">Tax Consulting Firm
								</option>
								<option value="4">Corporate Lawyer
								</option>
								<option value="5">Private Limited Company
								</option>
								<option value="6">Others
								</option>
							</select>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="dropdown-field col-md-12 pad-left0 pad-right0 mar-right10">
						<label class="pull-left remove-label">Pan Number *
							</label>
						<div class="cfield">
							<input type="text" placeholder="Pan no" value="" class="bg-white input-border-line" id="pan_no"/>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="dropdown-field col-md-12 pad-left0 pad-right0 mar-right10">
						<label class="pull-left remove-label">GST Number *
							</label>
						<div class="cfield">
							<input type="text" placeholder="GST Number" value="" class="bg-white input-border-line" id="gst_number"/>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="dropdown-field col-md-12 pad-left0 pad-right0 mar-right10">
						<label class="pull-left remove-label">
							No of current Employees *
							</label>
						<div class="cfield">
							<input type="text" placeholder="No of current Employees" value="" class="bg-white input-border-line" id="team_size"/>
						</div>
					</div>
				</div>
				
				<h4 class="location-head">Location --------------------------------------------------</h4>

                    <div id="GetEmpAddress">

                        <div class="row">
                            <div class="col-md-4 pad-left0 pad-right0 mar-right5">
                                <label class="pull-left remove-label">Zip Code
                                    </label>
                                <div class="cfield">
                                    <input type="text" data-geocomplete="zip code" placeholder="Zipcode" onchange="CheckEmpZipCode()" value="" class="bg-white input-border-line" id="Emp_zipcode"/>
                                </div>
                            </div>
            
                            <div class="col-md-7 pad-left0 pad-right0">
                                <label class="pull-left remove-label">Country
                                    </label>
                                <div class="cfield">
                                    <input type="text" data-geocomplete="county" placeholder="Country" value="" class="bg-white input-border-line" id="Emp_country"/>
                                </div>
                            </div>
                        </div>
            
                        <div class="row">
                            <div class="col-md-5 pad-left0 pad-right0 mar-right5">
                                <label class="pull-left remove-label">State
                                    </label>
                                <div class="cfield">
                                    <input type="text" data-geocomplete="state" placeholder="State" value="" class="bg-white input-border-line" id="Emp_state"/>
                                </div>
                            </div>
            
                            <div class="col-md-5 pad-left0 pad-right0">
                                <label class="pull-left remove-label">City
                                    </label>
                                <div class="cfield">
                                    <input type="text" data-geocomplete="city" placeholder="City" value="" class="bg-white input-border-line" id="Emp_city"/>
                                </div>
                            </div>
						</div>
						
						<div class="row">
                            <div class="col-md-12 pad-left0">
                                <label class="pull-left remove-label">Area
                                    </label>
                                <div class="cfield">
                                    <input type="text" data-geocomplete="street address"  placeholder="Type Area" value="" class="bg-white input-border-line" id="Emp_address"/>
                                </div>
                            </div>
                        </div>
                    </div>

				<div class="row">
					<div class="dropdown-field col-md-12 pad-left0 pad-right0 mar-right10">
						<label class="pull-left remove-label">Facebook *
							</label>
						<div class="cfield">
							<input type="text" placeholder="Facebbok" value="" class="bg-white input-border-line" id="facebook"/>
						</div>
					</div>
				</div>
	
				<div class="row">
					<div class="dropdown-field col-md-12 pad-left0 pad-right0 mar-right10">
						<label class="pull-left remove-label">Twitter *
							</label>
						<div class="cfield">
							<input type="text" placeholder="Twitter" value="" class="bg-white input-border-line" id="twitter"/>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="dropdown-field col-md-12 pad-left0 pad-right0 mar-right10">
						<label class="pull-left remove-label">Linkedin *
							</label>
						<div class="cfield">
							<input type="text" placeholder="Linkedin" value="" class="bg-white input-border-line" id="linkedin"/>
						</div>
					</div>
				</div>


				<div class="row">
					<div class="col-md-7">
						<button type="button" class="btn btn-default candidate-profile-cancel-btn text-right close-popup" data-dismiss="modal">Close
                        </button>
					</div>
					<div class="col-md-5">
						<button type="button" id="AddEmployerProfileFirstRegister" class="btn btn-default candidate-profile-save-btn">Save
							</button>
					</div>
	
				</div>
			</form>

            <br>

            <div class="col-md-12 exit-dashboard">
               <a href="/Employer/Dashboard"> <h4 class="text-center clr-grey">EXIT TO DASHBOARD <i class="fa fa-caret-right"></i></h4></a>
            </div>
          </div>
          
      </div>
    </div>
  </div>
  <div class="bottom-line bg-grey">
    <span>© 2019 Techtalents Software Technologies Pvt Ltd. All rights reserved.</span>
</div>
</section>

  
  
@endsection 




@section('JSScript')

<script>
$(document).ready(function () {
      for (i = new Date().getFullYear(); i > 1990; i--)
      {
          $('#exp_from_year').append($('<option />').val(i).html(i));
      }
    });

    $(document).ready(function () {
      for (i = new Date().getFullYear(); i > 1990; i--)
      {
          $('#exp_to_year').append($('<option />').val(i).html(i));
      }
    });

    $(document).ready(function () {
      for (i = new Date().getFullYear(); i > 1990; i--)
      {
          $('#passed_out_year').append($('<option />').val(i).html(i));
      }
    });
</script>
    
@endsection