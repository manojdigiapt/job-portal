@extends('UI.base')

@section('Content')
<section>
    <div class="block no-padding">
        <div class="container fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="main-featured-sec style2 mobile-height-sec">
                        <ul class="main-slider-sec style2 text-arrows home-img candidate-dashboard-banner-height mobile-height-slider">
                            <li class="slideHome"><img src="{{URL::asset('Demo/images/resource/mslider3.jpg')}}" alt="" /></li>
                        </ul>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="scroll-here" class="bg-grey">
    <div class="block pad-top0 pad-bottom0">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="reviews-sec">
                        <div class="col-lg-12">
                            <div class="account-popup reset-mar-top-bottom">
                                <h3>Reset Password</h3>
                                <form>
                                    <div class="cfield">
                                    <input type="hidden" value="{{$GetId}}" placeholder="Id" id="id"/>

                                        <input type="password" placeholder="New Password" id="NewPassword"/>
                                        <i class="la la-key"></i>
                                    </div>
                                    <div class="cfield">
                                        <input type="password" placeholder="Confirm Password" id="ConfirmPassword"/>
                                        <i class="la la-key"></i>
                                    </div>
                                    <button type="button" id="ResetEmployerPassword" class="Login-clr">Reset Password</button>
                                </form>
                        
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
