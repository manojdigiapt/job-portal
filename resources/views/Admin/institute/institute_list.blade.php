@extends('Admin.base')

@section('Content')
<div id="page-content-wrapper">
        <div class="page-content">
            <!-- Content Header (Page header) -->
            {{-- <section class="content-header z-depth-1">
                <div class="header-icon">
                    <i class="fa fa-table"></i>
                </div>
                <div class="header-title">
                    <h1> Admin Users List</h1>
                    <ul class="link hidden-xs">
                        <li><a href="/Admin/Dashboard"><i class="fa fa-home"></i>Home</a></li>
                        <li><a href="table.html">Table</a></li>
                    </ul>
                </div>
            </section> --}}
            <!-- page section -->
            <br>
            <div class="container-fluid">
                <div class="row">
                    <div class="fixed-action-btn">
                        <a href="javascript:void(0);" class="btn-floating btn-large red" data-toggle="modal" data-target="#AddInstituteModel" title="Add new institute">
                            <i class="large material-icons">add</i>
                        </a>
                    </div>
                    <!-- bootstrap table -->
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="card-header">
                                <i class="fa fa-table fa-lg"></i>
                            <h2>{{$title}}</h2>
                            </div>
                            <div class="card-content">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                {{-- <th>Select</th> --}}
                                                <th>Name</th>
                                                <th>Email </th>
                                                <th>Referral Code </th>
                                                {{-- <th>Status </th> --}}
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($GetInstitute as $Institute)
                                            <tr>
                                                <td>{{$Institute->name}}</td>
                                                <td>{{$Institute->email}}</td>
                                                <td>{{$Institute->referral_code}}</td>
                                                {{-- <td>@if($Faculty->staus == 1)
                                                        Active
                                                    @else
                                                        In Active
                                                    @endif
                                                </td> --}}
                                                <td>
                                                    <a href="javascript:void(0);" class="btn btn-sm" data-toggle="tooltip" data-placement="left" title="Update" onclick="EditInstitute({{$Institute->id}});"><i class="fa fa-pencil" aria-hidden="true"></i></a>

                                                    <a href="javascript:void(0);" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="right" title="Delete " onclick="DeleteInstitute({{$Institute->id}});"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ./bootstrap table -->
                    <!-- ./data table -->
                </div>
                <!-- ./row -->
            </div>
            <!-- ./cotainer -->

        </div>
        <!-- ./page-content -->
    </div>




    {{-- Add user Modal --}}
    <div id="AddInstituteModel" class="modal fade" role="dialog">
            <div class="modal-dialog modal-md">
          
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Add Institute</h4>
                </div>
                <div class="modal-body">
                        <div class="card-content" id="color-button">
                                <div class="row">
                                    <form class="col s11 m-t-20">
                                        <div class="input-field form-input col s12">
                                            <i class="material-icons prefix">account_circle</i>
                                            <input id="name" type="tel" class="validate" placeholder="Institute name">
                                            {{--  <label for="icon_prefix">Name</label>  --}}
                                        </div>
                                        <div class="input-field form-input col s12">
                                            <i class="material-icons prefix">email</i>
                                            <input type="email" id="email" class="validate" placeholder="Email">
                                            {{--  <label>Email</label>  --}}
                                        </div>
                                        <div class="input-field form-input col s12">
                                            <i class="material-icons prefix">lock</i>
                                            <input type="password" id="password" class="validate" placeholder="Password">
                                            {{--  <label>Password</label>  --}}
                                        </div>

                                        <div class="input-field form-input col s12">
                                            <i class="material-icons prefix">phone</i>
                                            <input type="text" id="mobile" class="validate" placeholder="Mobile">
                                            {{--  <label>Email</label>  --}}
                                        </div>

                                        <div class="input-field form-input col s12">
                                            <i class="material-icons prefix">email</i>
                                            <input type="text" id="address" class="validate" placeholder="Address">
                                            {{--  <label>Email</label>  --}}
                                        </div>
                                        <a href="javascript:void(0);" id="AddInstitute" class="waves-effect waves-light btn teal pull-right"><i class="material-icons left">save</i>submit</a>

                                    </form>
                                </div>

                            </div>
                </div>
                {{--  <div class="modal-footer">
                  <button type="button" id="AddAdminUsers" class="btn btn-success modal-save-btn-mar-right">Save</button>  --}}
                </div>
              </div>
          
            </div>
          </div>


          {{-- Edit user Modal --}}
    <div id="EditInstitute" class="modal fade" role="dialog">
            <div class="modal-dialog modal-md">
          
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Edit Institute</h4>
                </div>
                <div class="modal-body">
                        <div class="card-content" id="color-button">
                                <div class="row">
                                    <form class="col s11 m-t-20">
                                        <div class="input-field form-input col s12">
                                            <i class="material-icons prefix">account_circle</i>
                                            <input id="EditId" type="hidden" class="validate" placeholder="">
                                            <input id="Editname" type="text" class="validate" placeholder="Institute name">
                                            {{--  <label for="icon_prefix">Name</label>  --}}
                                        </div>
                                        <div class="input-field form-input col s12">
                                            <i class="material-icons prefix">lock</i>
                                            <input type="password" id="Editpassword" class="validate" placeholder="Password" disabled>
                                            {{--  <label>Password</label>  --}}
                                        </div>
                                        <div class="input-field form-input col s12">
                                            <i class="material-icons prefix">email</i>
                                            <input type="email" id="Editemail" class="validate" placeholder="Email">
                                            {{--  <label>Email</label>  --}}
                                        </div>

                                        <div class="input-field form-input col s12">
                                            <i class="material-icons prefix">phone</i>
                                            <input type="email" id="Editmobile" class="validate" placeholder="Mobile">
                                            {{--  <label>Email</label>  --}}
                                        </div>

                                        <div class="input-field form-input col s12">
                                            <i class="material-icons prefix">email</i>
                                            <input type="email" id="Editaddress" class="validate" placeholder="Address">
                                            {{--  <label>Email</label>  --}}
                                        </div>
                                        
                                        <a href="javascript:void(0);" id="UpdateInstitute" class="waves-effect waves-light btn teal pull-right"><i class="material-icons left">refresh</i>Update</a>

                                    </form>
                                </div>

                            </div>
                </div>
                {{--  <div class="modal-footer">
                  <button type="button" id="AddAdminUsers" class="btn btn-success modal-save-btn-mar-right">Save</button>  --}}
                </div>
              </div>
          
            </div>
          </div>
@endsection


@section('JSScript')
<script>
    "use strict";
    $(function () {
        $('select').material_select();
        Materialize.updateTextFields();

        //datepicker
        $('.datepicker').pickadate({
            selectMonths: true, // Creates a dropdown to control month
            selectYears: 15 // Creates a dropdown of 15 years to control year
        });
    });
</script>
@endsection