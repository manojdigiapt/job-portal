@extends('Admin.base')

@section('Content')
<div id="page-content-wrapper">
        <div class="page-content">
            <!-- page section -->
            <br>
            <div class="container-fluid">
                <div class="row">

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="card-header">
                                <i class="fa fa-file-text-o fa-lg"></i>
                            <h2>{{$title}}</h2>
                            </div>
                            <div class="card-content">
                                <div class="row">
                                    <form class="col s12 m-t-20">
                                        <div class="input-field form-input form-input-select col s6">
                                            <i></i>
                                            <select class="icons" id="employer_id">
                                                <option value="" disabled selected>Choose company</option>
                                                @foreach($GetEmployer as $Employer)
                                                    <option value="{{$Employer->id}}" data-icon="{{URL::asset('employer_profile')}}/{{$Employer->profile_pic}}" class="left circle">{{$Employer->company_name}}</option>
                                                @endforeach
                                            </select>
                                            {{--  <label>Images in select</label>  --}}
                                        </div>
                                        <div class="input-field form-input col s6">
                                            <i class="material-icons prefix">account_circle</i>
                                            <input id="title" type="tel" class="validate" placeholder="Please enter your Job title">
                                            {{--  <label for="icon_prefix">First Name</label>  --}}
                                        </div>
                                        <div class="input-field form-input col s3">
                                            <i class="material-icons prefix">add_location</i>
                                            <input type="text" onchange="CheckZipCode()" id="zipcode" class="validate" placeholder="Please enter zipcode">
                                        </div>
                                        <div class="input-field form-input col s3">
                                            <i class="material-icons prefix">add_location</i>
                                            <input type="text" id="country" class="validate" placeholder="Please enter country">
                                        </div>
                                        <div class="input-field form-input col s3">
                                            <i class="material-icons prefix">add_location</i>
                                            <input type="text" id="state" class="validate" placeholder="Please enter state">
                                        </div>
                                        <div class="input-field form-input col s3">
                                            <i class="material-icons prefix">add_location</i>
                                            <input type="text" id="city" class="validate" placeholder="Please enter city">
                                        </div>
                                        <div class="input-field form-input form-input-select col s6">
                                            <select class="icons" id="job_type">
                                                <option value="" disabled selected>Select job type</option>
                                                <option value="1">Full Time</option>
                                                <option value="2">Part Time</option>
                                            </select>
                                        </div>
                                        <div class="input-field form-input form-input-select col s6">
                                            <select class="icons" id="industry">
                                                <option value="" disabled selected>Select industry</option>
                                                <option value="1">Software</option>
                                                <option value="2">Hardware</option>
                                            </select>
                                        </div>
                                        <div class="input-field form-input form-input-select col s6">
                                            <select class="icons" onchange="SalaryType()">
                                                <option value="" id="salary_type" disabled selected>Select salary type</option>
                                                <option value="1">Per month</option>
                                                <option value="2">Per year</option>
                                            </select>
                                        </div>
                                        <div class="input-field form-input col s3">
                                            <i class="material-icons prefix">email</i>
                                            <input type="text" id="salary_min" class="validate" placeholder="Enter offered minimum salary">
                                            {{--  <label>Email</label>  --}}
                                        </div>
                                        <div class="input-field form-input col s3">
                                            <i class="material-icons prefix">email</i>
                                            <input type="text" id="salary_max" class="validate" placeholder="Enter offered maximum salary">
                                            {{--  <label>Password</label>  --}}
                                        </div>
                                        <div class="input-field form-input form-input-select col s6">
                                            <select class="icons" onchange="CheckCareerLevel()" id="career_level">
                                                <option selected>Select career level</option>
                                                <option value="1">Fresher</option>
                                                <option value="2">Experince</option>
                                            </select>
                                        </div>
                                        <div class="input-field form-input col s3">
                                            <i class="material-icons prefix">phone</i>
                                            <input id="experience_year" type="text" class="validate" placeholder="Please enter experience(years)" style="background-color: #8080803b;">
                                            {{--  <label for="icon_telephone">Experience(Years)</label>  --}}
                                        </div>
                                        <div class="input-field form-input col s3">
                                            <i class="material-icons prefix">phone</i>
                                            <input id="experience_month" type="text" class="validate" placeholder="Please enter experience(months)" style="background-color: #8080803b;">
                                            {{--  <label for="icon_telephone">Telephone</label>  --}}
                                        </div>
                                        <div class="input-field form-input col s6">
                                            <i class="material-icons prefix">account_circle</i>
                                            <input id="last_date" type="text" class="validate" placeholder="Please enter application last date">
                                            {{--  <label for="icon_prefix">First Name</label>  --}}
                                        </div>
                                        <div class="input-field form-input col s6">
                                            <i class="material-icons prefix">account_circle</i>
                                            <input id="positions" type="text" class="validate" placeholder="Job positions">
                                            {{--  <label for="icon_prefix">First Name</label>  --}}
                                        </div>
                                        <div class="input-field form-input col s12">
                                            <i class="material-icons prefix">account_circle</i>
                                            <input id="qualification" type="text" class="validate" placeholder="Please enter qualification">
                                            {{--  <label for="icon_prefix">First Name</label>  --}}
                                        </div>
                                        
                                        <div class="input-field form-input col s6">
                                            <i class="material-icons prefix">account_circle</i>
                                            <input id="description" type="text" class="validate" placeholder="Job description">
                                        </div>
                                        <div id="color-button">
                                            <a class="waves-effect waves-light btn teal pull-right" id="AdminPostJobs"><i class="material-icons left">send</i>submit</a>
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- ./basic forms -->

                </div>
                <!-- ./bootstrap forms -->
            </div>
            <!-- ./row -->
        </div>
        <!-- ./cotainer -->
    </div>
@endsection


@section('JSScript')
<script>
    "use strict";
    $(function () {
        $('select').material_select();
        Materialize.updateTextFields();

        //datepicker
        $('.datepicker').pickadate({
            selectMonths: true, // Creates a dropdown to control month
            selectYears: 15 // Creates a dropdown of 15 years to control year
        });
    });
</script>
@endsection