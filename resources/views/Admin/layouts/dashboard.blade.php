@extends('Admin.base')

@section('Content')
<div id="page-content-wrapper">
        <div class="page-content">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="header-icon">
                    <i class="fa fa-tachometer"></i>
                </div>
                <div class="header-title">
                    <h1> Dashboard</h1>
                    <small> Dashboard features</small>
                    <ul class="link hidden-xs">
                        <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
                        <li><a href="#">Dashboard</a></li>
                    </ul>
                </div>
            </section>
            <!-- page section -->
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="panel cardbox bg-primary">
                            <div class="panel-body card-item panel-refresh">
                                {{-- <a class="refresh" href="#">
                                    <span class="fa fa-refresh"></span>
                                </a>  --}}
                                <div class="refresh-container"><i class="refresh-spinner fa fa-spinner fa-spin fa-5x"></i></div>
                            <div class="timer" data-to="{{$GetExperiencedUsersCount}}" data-speed="1500">0</div>
                                <div class="cardbox-icon">
                                    <i class="material-icons">directions_run</i>
                                </div>
                                <div class="card-details">
                                    <h4>Total Experienced Users</h4>
                                    {{-- <span>10% Higher than last week</span> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="panel cardbox bg-primary">
                            <div class="panel-body card-item panel-refresh">
                                {{-- <a class="refresh" href="#">
                                    <span class="fa fa-refresh"></span>
                                </a>  --}}
                                <div class="refresh-container"><i class="refresh-spinner fa fa-spinner fa-spin fa-5x"></i></div>
                            <div class="timer" data-to="{{$GetFresherUsersCount}}" data-speed="1500">0</div>
                                <div class="cardbox-icon">
                                    <i class="material-icons">directions_run</i>
                                </div>
                                <div class="card-details">
                                    <h4>Total Fresher Users</h4>
                                    {{-- <span>10% Higher than last week</span> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="panel cardbox bg-primary">
                            <div class="panel-body card-item panel-refresh">
                                {{-- <a class="refresh" href="#">
                                    <span class="fa fa-refresh"></span>
                                </a>  --}}
                                <div class="refresh-container"><i class="refresh-spinner fa fa-spinner fa-spin fa-5x"></i></div>
                            <div class="timer" data-to="{{$GetCAUsersCount}}" data-speed="1500">0</div>
                                <div class="cardbox-icon">
                                    <i class="material-icons">directions_run</i>
                                </div>
                                <div class="card-details">
                                    <h4>Total CA Users</h4>
                                    {{-- <span>10% Higher than last week</span> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="panel cardbox bg-success">
                            <div class="panel-body card-item panel-refresh">
                                {{-- <a class="refresh" href="#">
                                    <span class="fa fa-refresh"></span>
                                </a>  --}}
                                <div class="refresh-container"><i class="refresh-spinner fa fa-spinner fa-spin fa-5x"></i></div>
                            <div class="timer" data-to="{{$GetEmployerCount}}" data-speed="1500">0</div>
                                <div class="cardbox-icon">
                                    <i class="material-icons">business</i>
                                </div>
                                <div class="card-details">
                                    <h4>Total CA Firms</h4>
                                    {{-- <span>10% Higher than last week</span> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="panel cardbox bg-warning">
                            <div class="panel-body card-item panel-refresh">
                                {{-- <a class="refresh2" href="#">
                                    <span class="fa fa-refresh"></span>
                                </a>  --}}
                                <div class="refresh-container"><i class="refresh-spinner fa fa-spinner fa-spin fa-5x"></i></div>
                            <div class="timer" data-to="{{$GetFacultyCount}}" data-speed="1500">0</div>
                                <div class="cardbox-icon">
                                    <i class="material-icons">visibility</i>
                                </div>
                                <div class="card-details">
                                    <h4>Total Faculties</h4>
                                    {{-- <span>10% Higher than last week</span> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="panel cardbox bg-success">
                            <div class="panel-body card-item panel-refresh">
                                {{-- <a class="refresh" href="#">
                                    <span class="fa fa-refresh"></span>
                                </a>  --}}
                                <div class="refresh-container"><i class="refresh-spinner fa fa-spinner fa-spin fa-5x"></i></div>
                                @foreach($GetJobsAppliedCount as $JobsCount)
                            <div class="timer" data-to="{{$JobsCount->count}}"
                            @endforeach data-speed="1500">0</div>
                                <div class="cardbox-icon">
                                    <i class="material-icons">supervisor_account</i>
                                </div>
                                <div class="card-details">
                                    <h4>Total Jobs Applied Candidates</h4>
                                    {{-- <span>10% Higher than last week</span> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="panel cardbox bg-warning">
                            <div class="panel-body card-item panel-refresh">
                                {{-- <a class="refresh2" href="#">
                                    <span class="fa fa-refresh"></span>
                                </a>  --}}
                                <div class="refresh-container"><i class="refresh-spinner fa fa-spinner fa-spin fa-5x"></i></div>
                                @foreach($GetInternshipsAppliedCount as $InternshipsCount)
                            <div class="timer" data-to="{{$InternshipsCount->count}}"
                            @endforeach data-speed="1500">0</div>
                                <div class="cardbox-icon">
                                    <i class="material-icons">supervisor_account</i>
                                </div>
                                <div class="card-details">
                                    <h4>Total Internships Applied</h4>
                                    {{-- <span>10% Higher than last week</span> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ./counter Number -->
                    <!-- chart -->
                    {{-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="card-header">
                                <i class="fa fa-bar-chart fa-lg"></i>
                                <h2>Chart</h2>
                            </div>
                            <div class="card-content">
                                {{-- <canvas id="lineChart" height="150"></canvas> 
                                    <canvas id="canvas"></canvas>
                            </div>
                        </div>
                    </div> --}}

                    {{-- <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="card-header">
                                <i class="fa fa-bar-chart fa-lg"></i>
                                <h2>Chart</h2>
                            </div>
                            <div class="card-content">
                                    <canvas id="monthwise"></canvas>
                            </div>
                        </div>
                    </div> --}}
                    <!-- ./chart -->
                    <!-- ./Calender -->
                    <!-- Table content -->
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="card-header">
                                <i class="fa fa-table fa-lg"></i>
                                <h2>Experienced Candidate</h2>

                                <a href="/Admin/ExperiencedUsersList" class="btn btn-sm view-all-btn" data-toggle="tooltip" data-placement="left" title="Update">View All</a>
                            </div>
                            <div class="card-content">
                                <!-- Table content -->
                                <table class="responsive-table highlight bordered">
                                    <thead>
                                        <tr>
                                            <th data-field="id">Name</th>
                                            <th data-field="name">Email</th>
                                            <th data-field="price">Mobile</th>
                                            {{-- <th data-field="Status">Status</th> --}}
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($ExperienceCandidate as $Candidates)
                                        <tr>
                                            <td>{{$Candidates->name}}</td>
                                            <td>{{$Candidates->email}}</td>
                                            <td>{{$Candidates->mobile}}</td>
                                            {{-- <td><span class="label teal pull-right">Active</span></td> --}}
                                        </tr>
                                        @endforeach
                                    </tbody>
                                    
                                </table>
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="card-header">
                                <i class="fa fa-table fa-lg"></i>
                                <h2>Fresher Candidate</h2>

                                <a href="/Admin/FresherUsersList" class="btn btn-sm view-all-btn" data-toggle="tooltip" data-placement="left" title="Update">View All</a>
                            </div>
                            <div class="card-content">
                                <!-- Table content -->
                                <table class="responsive-table highlight bordered">
                                    <thead>
                                        <tr>
                                            <th data-field="id">Name</th>
                                            <th data-field="name">Email</th>
                                            <th data-field="price">Mobile</th>
                                            {{-- <th data-field="Status">Status</th> --}}
                                        </tr>
                                    </thead>
                                    <tbody>
                                            @foreach($FresherCandidate as $Candidates)
                                            <tr>
                                                <td>{{$Candidates->name}}</td>
                                                <td>{{$Candidates->email}}</td>
                                                <td>{{$Candidates->mobile}}</td>
                                                {{-- <td><span class="label teal pull-right">Active</span></td> --}}
                                            </tr>
                                            @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="card-header">
                                <i class="fa fa-table fa-lg"></i>
                                <h2>CA Candidate</h2>

                                <a href="/Admin/CAUsersList" class="btn btn-sm view-all-btn" data-toggle="tooltip" data-placement="left" title="Update">View All</a>
                            </div>
                            <div class="card-content">
                                <!-- Table content -->
                                <table class="responsive-table highlight bordered">
                                    <thead>
                                        <tr>
                                            <th data-field="id">Name</th>
                                            <th data-field="name">Email</th>
                                            <th data-field="price">Mobile</th>
                                            {{-- <th data-field="Status">Status</th> --}}
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($CACandidate as $Candidates)
                                        <tr>
                                            <td>{{$Candidates->name}}</td>
                                            <td>{{$Candidates->email}}</td>
                                            <td>{{$Candidates->mobile}}</td>
                                            {{-- <td><span class="label teal pull-right">Active</span></td> --}}
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="card-header">
                                <i class="fa fa-table fa-lg"></i>
                                <h2>Jobs Applied Candidates</h2>

                                <a href="/Admin/EmployerList" class="btn btn-sm view-all-btn" data-toggle="tooltip" data-placement="left" title="Update">View All</a>
                            </div>
                            <div class="card-content">
                                <!-- Table content -->
                                <table class="responsive-table highlight bordered">
                                    <thead>
                                        <tr>
                                            {{-- <th>Select</th> --}}
                                            <th>Name</th>
                                            <th>Email </th>
                                            {{-- <th>Status </th> --}}
                                            <th>Mobile</th>
                                            <th>User Type</th>
                                            <th>Created Date</th>
                                            <th>View Details</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($GetUsers as $Users)
                                            <tr>
                                                <td>{{$Users->name}}</td>
                                                <td>{{$Users->email}}</td>
                                                <td>{{$Users->mobile}}</td>
                                                <td>@if($Users->type == 1)
                                                    Experienced
                                                    @elseif($Users->type == 2)
                                                    Fresher
                                                    @else
                                                    CA 
                                                    @endif
                                                </td>
                                                <td>{{date('d M Y', strtotime($Users->created_at))}}</td>
                                                <td><a href="javascript:void(0);" onclick="ViewUserProfile({{$Users->id}})" class="btn btn-sm" data-toggle="tooltip" data-placement="left" title="Info"><i class="fa fa-info" aria-hidden="true"></i></a></td>
                                                
                                            </tr>
                                            @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- ./Table content -->
                    <!-- Google Map -->
                    <!-- ./Google Map -->
                    <!-- ./Messages -->
                </div>
                <!-- ./row -->
            </div>
            <!-- ./cotainer -->
        </div>
        <!-- ./page-content -->
    </div>
@endsection

@section('JSScript')
{{-- // var data_click = echo $click; ?>;
    // var data_viewer = echo $viewer; ?>; --}}
    
<script src="http://demo.itsolutionstuff.com/demoTheme/js/Chart.bundle.js"></script>

{{-- <script>
    var year = ['2019','2020','2021', '2022'];

    

    

    var barChartData = {
        labels: year,
        datasets: [{
            label: 'Candidate',
            backgroundColor: "rgba(220,220,220,0.5)",
            data: candidates
        }, {
            label: 'Employer',
            backgroundColor: "rgba(151,187,205,0.5)",
            data: employer
        }]
    };

    window.onload = function() {
        var ctx = document.getElementById("canvas").getContext("2d");
        window.myBar = new Chart(ctx, {
            type: 'bar',
            data: barChartData,
            options: {
                elements: {
                    rectangle: {
                        borderWidth: 2,
                        borderColor: 'rgb(0, 255, 0)',
                        borderSkipped: 'bottom'
                    }
                },
                responsive: true,
                title: {
                    display: true,
                    text: 'Yearly Accountswale Registers'
                }
            }
        });

    };

</script> --}}


// <script>
//     var month = ['Jan','Feb','Mar', 'April', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Aug', 'Nov', 'Dec'];

    

//     var candidates = [8,9,12,7]
//     var employer = [5,8,15,5]

//     var barChartData = {
//         labels: month,
//         datasets: [{
//             label: 'Candidate',
//             backgroundColor: "rgba(220,220,220,0.5)",
//             data: candidates
//         }, {
//             label: 'Employer',
//             backgroundColor: "rgba(151,187,205,0.5)",
//             data: employer
//         }]
//     };

//     window.onload = function() {
//         var ctx = document.getElementById("monthwise").getContext("2d");
//         window.myBar = new Chart(ctx, {
//             type: 'line',
//             data: barChartData,
//             options: {
//                 elements: {
//                     rectangle: {
//                         borderWidth: 2,
//                         borderColor: 'rgb(0, 255, 0)',
//                         borderSkipped: 'bottom'
//                     }
//                 },
//                 responsive: true,
//                 title: {
//                     display: true,
//                     text: 'Yearly Accountswale Registers'
//                 }
//             }
//         });

//     };

// </script>


@endsection