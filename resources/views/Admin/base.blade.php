<!DOCTYPE html>
<html lang="en">
    
<!-- Mirrored from thememinister.com/materialdesign/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Jul 2019 09:54:45 GMT -->
<head><meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{$title}}</title>
        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="assets/dist/img/ico/fav.png">
        <!-- Start Global Mandatory Style
             =====================================================================-->
        <!-- jquery-ui css -->
        <link href="{{URL::asset('Admin/assets/plugins/jquery-ui-1.12.1/jquery-ui.min.css')}}" rel="stylesheet" type="text/css" />
        <!-- materialize css -->
        <link href="{{URL::asset('Admin/assets/plugins/materialize/css/materialize.min.css')}}" rel="stylesheet">
        <!-- Bootstrap css-->
        <link href="{{URL::asset('Admin/assets/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
        <!-- Animation Css -->
        <link href="{{URL::asset('Admin/assets/plugins/animate/animate.css')}}" rel="stylesheet" />
        <!-- Material Icons CSS -->
        <link href="{{URL::asset('Admin/fonts.googleapis.com/icone91f.css?family=Material+Icons')}}" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="{{URL::asset('Admin/assets/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" />
        <!-- Monthly css -->
        <link href="{{URL::asset('Admin/assets/plugins/monthly/monthly.css')}}" rel="stylesheet" type="text/css" />
        <!-- simplebar scroll css -->
        <link href="{{URL::asset('Admin/assets/plugins/simplebar/dist/simplebar.css')}}" rel="stylesheet" type="text/css" />
        <!-- mCustomScrollbar css -->
        <link href="{{URL::asset('Admin/assets/plugins/malihu-custom-scrollbar/jquery.mCustomScrollbar.css')}}" rel="stylesheet" type="text/css" />
        <!-- custom CSS -->
        <link href="{{URL::asset('Admin/assets/dist/css/stylematerial.css')}}" rel="stylesheet">

        {{-- Custom CSS --}}
        <link href="{{URL::asset('Admin/assets/dist/css/custom.css')}}" rel="stylesheet">

        <!-- notify alert -->
        <link rel="stylesheet" type="text/css" href="{{URL::asset('Admin/assets/plugins/notify/css/notify.css')}}">
        <link rel="stylesheet" type="text/css" href="{{URL::asset('Admin/assets/plugins/notify/css/prettify.css')}}">

        <!-- sweetalert2 -->
        <link rel="stylesheet" type="text/css" href="{{URL::asset('Admin/assets/plugins/sweetalert2/dist/sweetalert2.min.css')}}">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
    </head>

    <body>
        <div id="wrapper">
            <!--navbar top-->
            @include('Admin.common.nav')
            <!-- Sidebar -->
            @include('Admin.common.sidebar')
            <!-- ./sidebar-wrapper -->
            <!-- Page content -->
            @yield('Content')
            <!-- ./page-content-wrapper -->
        </div>
        <!-- ./page-wrapper -->
        <!-- Preloader -->
        <div id="preloader">
            <div class="preloader-position">
                <div class="preloader-wrapper big active">
                    <div class="spinner-layer spinner-teal">
                        <div class="circle-clipper left">
                            <div class="circle"></div>
                        </div>
                        <div class="gap-patch">
                            <div class="circle"></div>
                        </div>
                        <div class="circle-clipper right">
                            <div class="circle"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Preloader -->

        <!-- Start Core Plugins
             =====================================================================-->
        <!-- jQuery -->
        <script src="{{URL::asset('Admin/assets/plugins/jQuery/jquery-3.2.1.min.js')}}" type="text/javascript"></script>
        <!-- jquery-ui -->
        <script src="{{URL::asset('Admin/assets/plugins/jquery-ui-1.12.1/jquery-ui.min.js')}}" type="text/javascript"></script>
        <!-- Bootstrap -->
        <script src="{{URL::asset('Admin/assets/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
        <!-- materialize  -->
        <script src="{{URL::asset('Admin/assets/plugins/materialize/js/materialize.min.js')}}" type="text/javascript"></script>
        <!-- metismenu-master -->
        <script src="{{URL::asset('Admin/assets/plugins/metismenu-master/dist/metisMenu.min.js')}}" type="text/javascript"></script>
        <!-- SlimScroll -->
        <script src="{{URL::asset('Admin/assets/plugins/slimScroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
        <!-- m-custom-scrollbar -->
        <script src="{{URL::asset('Admin/assets/plugins/malihu-custom-scrollbar/jquery.mCustomScrollbar.concat.min.js')}}" type="text/javascript"></script>
        <!-- scroll -->
        <script src="{{URL::asset('Admin/assets/plugins/simplebar/dist/simplebar.js')}}" type="text/javascript"></script>
        <!-- custom js -->
        <script src="{{URL::asset('Admin/assets/dist/js/custom.js')}}" type="text/javascript"></script>
        <!-- End Core Plugins
             =====================================================================-->
        <!-- Start Page Lavel Plugins
             =====================================================================-->
        <!-- Sparkline js -->
        <script src="{{URL::asset('Admin/assets/plugins/sparkline/sparkline.min.js')}}" type="text/javascript"></script>
        <!-- Counter js -->
        <script src="{{URL::asset('Admin/assets/plugins/counterup/jquery.counterup.min.js')}}" type="text/javascript"></script>
        <!-- ChartJs JavaScript -->
        <script src="{{URL::asset('Admin/assets/plugins/chartJs/Chart.min.js')}}" type="text/javascript"></script>
        <!-- Monthly js -->
        <script src="{{URL::asset('Admin/assets/plugins/monthly/monthly.js')}}" type="text/javascript"></script>

        <!-- End Page Lavel Plugins
             =====================================================================-->
        <!-- Start Theme label Script
             =====================================================================-->
        <!-- main js-->
        <script src="{{URL::asset('Admin/assets/dist/js/main.js')}}" type="text/javascript"></script>
        <!-- End Theme label Script
             =====================================================================-->
       


        {{--  Custom  --}}
        <script src="{{URL::asset('Admin/assets/dist/js/custom/authendication.js')}}" type="text/javascript"></script>

        <script src="{{URL::asset('Admin/assets/dist/js/custom/admins.js')}}" type="text/javascript"></script>
        <script src="{{URL::asset('Admin/assets/dist/js/custom/jobs.js')}}"></script>

        <script src="{{URL::asset('Admin/assets/dist/js/custom/job_attributes.js')}}"></script>

        <script src="{{URL::asset('Admin/assets/dist/js/custom/faculty.js')}}"></script>

        <script src="{{URL::asset('Admin/assets/dist/js/custom/institute.js')}}"></script>

        <script src="{{URL::asset('Admin/assets/dist/js/custom/users.js')}}"></script>

        {{--  PNotify  --}}
        <script src="{{URL::asset('Admin/assets/plugins/pnotify/pnotify.custom.min.js')}}" type="text/javascript"></script>

        <!-- Notify -->
        <script src="{{URL::asset('Admin/assets/plugins/notify/js/notify.js')}}" type="text/javascript"></script>
        <script src="{{URL::asset('Admin/assets/plugins/notify/js/prettify.js')}}" type="text/javascript"></script>

        <!-- sweet alert2 -->
        <script src="{{URL::asset('Admin/assets/plugins/sweetalert2/dist/sweetalert2.min.js')}}" type="text/javascript"></script>

        <script src="https://cdn.ckeditor.com/4.12.1/basic/ckeditor.js"></script>

        <script>
        CKEDITOR.replace( 'web_description', {
            height: 250
        } );
        </script>
    
        
        @yield('JSScript')
    </body>


</html>