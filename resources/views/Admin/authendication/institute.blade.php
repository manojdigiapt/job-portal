<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
<title>{{$title}}</title>
    <!-- Favicon and touch icons -->
    <link rel="shortcut icon" href="assets/img/ico/favicon.html">
    <!-- Start Global Mandatory Style
         =====================================================================-->
    <!-- Material Icons CSS -->
    <link href="{{URL::asset('Admin/fonts.googleapis.com/icone91f.css?family=Material+Icons')}}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{URL::asset('Admin/assets/font-awesome/css/font-awesome.min.css" rel="stylesheet')}}" type="text/css" />
    <!-- Animation Css -->
    <link href="{{URL::asset('Admin/assets/plugins/animate/animate.css')}}" rel="stylesheet" />
    <!-- materialize css -->
    <link href="{{URL::asset('Admin/assets/plugins/materialize/css/materialize.min.css')}}" rel="stylesheet">
    <!-- custom CSS -->
    <link href="{{URL::asset('Admin/assets/dist/css/stylematerial.css')}}" rel="stylesheet">

    <!-- notify alert -->
    <link rel="stylesheet" type="text/css" href="{{URL::asset('Admin/assets/plugins/notify/css/notify.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('Admin/assets/plugins/notify/css/prettify.css')}}">

    <style>
    
.pull-right{
    float: right!important;
}
    </style>
</head>

<body class="sign-section">
    <div class="container sign-cont animated zoomIn" id="LoginForm">
    <div class="row sign-row">
            <div class="sign-title">
                <h2 class="teal-text"><i class="fa fa-user-circle-o"></i></h2>
                <h2 class="teal-text">Login</h2>
            </div>
            <form class="col s12" id="reg-form">
                <div class="row sign-row">
                    <div class="input-field col s12">
                        <input id="institute_email" type="text" class="validate" required>
                        <label for="u_name">User Name</label>
                    </div>
                </div>
                <div class="row sign-row">
                    <div class="input-field col s12">
                        <input id="institute_password" type="password" class="validate" required>
                        <label for="password">Password</label>
                    </div>
                </div>
                <div class="row sign-row">
                     <div class="input-field col s12 m-b-30">
                        <label class="pull-left"><a class='pink-text' href='javascript:void(0);' onclick="InstituteForgotPassword()"><b>Forgot Password?</b></a></label>
                    </div> 
                </div>
                <div class="row sign-row">
                    {{--  <div class="input-field col s6">
                        <div class="sign-confirm">
                            <input type="checkbox" id="sign-confirm" />
                            <label for="sign-confirm">Remember me!</label>
                        </div>
                    </div>  --}}
                    <div class="input-field col s6 pull-right">
                        <button class="btn btn-large btn-register waves-effect waves-light green" type="button" id="InstituteLogin" name="action">Login
                            <i class="material-icons right">done_all</i>
                        </button>
                    </div>
                </div>
            </form>

        </div>
        <a title="Login" class="sign-btn btn-floating btn-large waves-effect waves-light green">
            <i class="material-icons">perm_identity</i></a>
    </div>


    <div class="container sign-cont animated bounceIn" id="ForgotPassword" style="display:none;">
        <div class="row sign-row">
            <div class="sign-title text-center">
                <h2 class="teal-text"><i class="fa fa-lock fa-4x"></i></h2>
                <h2 class="teal-text">Reset Password</h2>
            </div>
            <form class="col s12" id="reg-form">
                <div class="row">
                    <div class="input-field col s12">
                        <input id="forgot_email" type="email" class="validate" required>
                        <label for="email">Enter Your Email</label>
                    </div>
                </div>

                <div class="row sign-row">
                    <div class="input-field col s12 m-b-30">
                       <label class="pull-left"><a class='pink-text' href='javascript:void(0);' onclick="BacktoLogin()"><b>Back to login?</b></a></label>
                   </div> 
               </div>

                <div class="row pull-right">
                    <div class="input-field col s12">
                        <button class="btn btn-large btn-register waves-effect waves-light teal" id="ForgotPasswordBtn" type="button" name="action">Submit
                            <i class="material-icons right">send</i>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <!-- Start Core Plugins
         =====================================================================-->
    <!-- jQuery -->
    <script src="{{URL::asset('Admin/assets/plugins/jQuery/jquery-3.2.1.min.js')}}" type="text/javascript"></script>
    <!-- materialize  -->
    <script src="{{URL::asset('Admin/assets/plugins/materialize/js/materialize.min.js')}}" type="text/javascript"></script>

    {{--  Authendication   --}}
    <script src="{{URL::asset('Admin/assets/dist/js/custom/authendication.js')}}" type="text/javascript"></script>

    <script src="{{URL::asset('Admin/assets/dist/js/custom/institute.js')}}" type="text/javascript"></script>

    <!-- Notify -->
    <script src="{{URL::asset('Admin/assets/plugins/notify/js/notify.js')}}" type="text/javascript"></script>
    <script src="{{URL::asset('Admin/assets/plugins/notify/js/prettify.js')}}" type="text/javascript"></script>
    <!-- End Core Plugins
         =====================================================================-->
    <script>
        $(document).ready(function() {
            $('select').material_select();
        });
    </script>
</body>

</html>