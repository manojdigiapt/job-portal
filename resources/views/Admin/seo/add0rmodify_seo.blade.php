@extends('Admin.base')

@section('Content')
<div id="page-content-wrapper">
        <div class="page-content">
            <!-- Content Header (Page header) -->
            {{-- <section class="content-header z-depth-1">
                <div class="header-icon">
                    <i class="fa fa-table"></i>
                </div>
                <div class="header-title">
                    <h1> Admin Users List</h1>
                    <ul class="link hidden-xs">
                        <li><a href="/Admin/Dashboard"><i class="fa fa-home"></i>Home</a></li>
                        <li><a href="table.html">Table</a></li>
                    </ul>
                </div>
            </section> --}}
            <!-- page section -->
            <br>
            <div class="container-fluid">
                <div class="row">
                    <!-- bootstrap table -->
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="card-header">
                                <i class="fa fa-file-o fa-lg"></i>
                                <h2>SEO</h2>
                            </div>
                            <div class="card-content">
                                <form class="form-horizontal" method="post">
                                    <h2 class="text-center">Update SEO Keywords</h2>
                                    <fieldset>
                                        <!-- Text input-->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label">Meta Author Name:</label>
                                            <div class="col-md-4">
                                                <div class="input-field">
                                                <input type="hidden" id="Id" value="@if($GetSEO->id){{$GetSEO->id}} @endif">

                                                    <input name="first_name" id="name" type="text" class="validate" placeholder="Author Name" value="{{$GetSEO->seo_name}}">
                                                </div>
                                            </div>
                                        </div>
                                        <!-- ./Text input-->
                                        <!-- Text input-->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label">Meta Description:</label>
                                            <div class="col-md-4">
                                                <div class="input-field">
                                                    <input name="Last_name" id="description" type="text" class="validate" placeholder="Description" value="{{$GetSEO->seo_description}}">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-4 control-label">Meta Keywords:</label>
                                            <div class="col-md-4">
                                                <div class="input-field">
                                                    {{-- <input name="Last_name" type="text" class="validate" placeholder="Last Name"> --}}
                                                    <textarea id="keywords" class="materialize-textarea" placeholder="Keywords">{{$GetSEO->seo_keywords}}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- ./Text input-->
                                        <!-- Button -->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label"></label>
                                            <div class="col-md-4">
                                                <button type="button" id="AddOrModifySeo" class="btn btn-success">Submit <span class="glyphicon glyphicon-send"></span></button>
                                            </div>
                                        </div>

                                    </fieldset>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- ./bootstrap table -->
                    <!-- ./data table -->
                </div>
                <!-- ./row -->
            </div>
            <!-- ./cotainer -->

        </div>
        <!-- ./page-content -->
    </div>




    {{-- Add user Modal --}}
@endsection
