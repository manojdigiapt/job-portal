@extends('Admin.base')

@section('Content')
<div id="page-content-wrapper">
        <div class="page-content">
            <!-- Content Header (Page header) -->
            {{-- <section class="content-header z-depth-1">
                <div class="header-icon">
                    <i class="fa fa-table"></i>
                </div>
                <div class="header-title">
                    <h1> Admin Users List</h1>
                    <ul class="link hidden-xs">
                        <li><a href="/Admin/Dashboard"><i class="fa fa-home"></i>Home</a></li>
                        <li><a href="table.html">Table</a></li>
                    </ul>
                </div>
            </section> --}}
            <!-- page section -->
            <br>
            <div class="container-fluid">
                <div class="row">
                    {{-- <div class="fixed-action-btn">
                        <a href="javascript:void(0);" class="btn-floating btn-large red" data-toggle="modal" data-target="#AddUsers" title="Add new admin users">
                            <i class="large material-icons">add</i>
                        </a>
                    </div> --}}
                    <!-- bootstrap table -->
                    {{-- @if($message = Session::get('success'))
                        {{ $message }}
                    @endif --}}

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="card-header">
                                <i class="fa fa-table fa-lg"></i>
                                <h2 class="pull-left">{{$title}}</h2>
                            <h4 class="text-center">Training Name: <b>{{$GetJobs->title}}</b></h4>
                               
                            </div>
                            <div class="card-content">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                {{-- <th>Select</th> --}}
                                                <th>Name</th>
                                                <th>Email </th>
                                                {{-- <th>Status </th> --}}
                                                <th>Mobile</th>
                                                <th>User Type</th>
                                                <th>Created Date</th>
                                                <th>View Details</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($GetUsers as $Users)
                                            <tr>
                                                <td>{{$Users->name}}</td>
                                                <td>{{$Users->email}}</td>
                                                <td>{{$Users->mobile}}</td>
                                                <td>@if($Users->type == 1)
                                                    Experienced
                                                    @elseif($Users->type == 2)
                                                    Fresher
                                                    @else
                                                    CA 
                                                    @endif
                                                </td>
                                                <td>{{date('d M Y', strtotime($Users->created_at))}}</td>
                                                <td><a href="javascript:void(0);" onclick="ViewUserProfile({{$Users->id}})" class="btn btn-sm" data-toggle="tooltip" data-placement="left" title="Info"><i class="fa fa-info" aria-hidden="true"></i></a></td>
                                                
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ./bootstrap table -->
                    <!-- ./data table -->
                </div>
                <!-- ./row -->
            </div>
            <!-- ./cotainer -->

        </div>
        <!-- ./page-content -->
    </div>




    {{-- Add user Modal --}}
    <div id="AddUsers" class="modal fade width100" role="dialog">
            <div class="modal-dialog modal-lg">
          
              <!-- Modal content-->
              <div class="modal-content">
                {{-- <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">View Profile</h4>
                </div> --}}
                <div class="modal-body" id="GetUsersInfo">
                    
                </div>
                {{--  <div class="modal-footer">
                  <button type="button" id="AddAdminUsers" class="btn btn-success modal-save-btn-mar-right">Save</button>  --}}
                </div>
              </div>
          
            </div>
          </div>

@endsection

@section('JSScript')
<script>
    function myFunction() {
      document.getElementById("myForm").submit();
    }
    </script>
@endsection