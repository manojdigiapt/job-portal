@extends('Admin.base')

@section('Content')
<div id="page-content-wrapper">
        <div class="page-content">
            <!-- Content Header (Page header) -->
            {{-- <section class="content-header z-depth-1">
                <div class="header-icon">
                    <i class="fa fa-table"></i>
                </div>
                <div class="header-title">
                    <h1> Admin Users List</h1>
                    <ul class="link hidden-xs">
                        <li><a href="/Admin/Dashboard"><i class="fa fa-home"></i>Home</a></li>
                        <li><a href="table.html">Table</a></li>
                    </ul>
                </div>
            </section> --}}
            <!-- page section -->
            <br>
            <div class="container-fluid">
                <div class="row">
                    {{-- <div class="fixed-action-btn">
                        <a href="javascript:void(0);" class="btn-floating btn-large red" data-toggle="modal" data-target="#AddUsers" title="Add new admin users">
                            <i class="large material-icons">add</i>
                        </a>
                    </div> --}}
                    <!-- bootstrap table -->
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="card-header">
                                <i class="fa fa-table fa-lg"></i>
                            <h2>{{$title}}</h2>
                            </div>
                            <div class="card-content">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                {{-- <th>Select</th> --}}
                                                <th>Company Name</th>
                                                <th>Email </th>
                                                {{-- <th>Status </th> --}}
                                                <th>Mobile</th>
                                                
                                                <th>Created Date</th>
                                                <th>View Jobs</th>
                                                <th>View Internships</th>
                                                <th>View CA Articleships</th>
                                                <th>PAN Number</th>
                                                <th>GST Number</th>
                                                {{-- <th>Action</th> --}}
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($GetUsers as $Users)
                                            <tr>
                                                <td>{{$Users->company_name}}</td>
                                                <td>{{$Users->email}}</td>
                                                <td>{{$Users->mobile}}</td>
                                                
                                                <td>{{date('d M Y', strtotime($Users->created_at))}}</td>

                                            <td><a href="/Admin/EmployerJobList/{{$Users->id}}" class="btn btn-sm" data-toggle="tooltip" data-placement="left" title="View Job Details" target="_blank"> <i class="fa fa-briefcase" aria-hidden="true"></i></a> <span class="badge badge-notification">{{
                                                $GetJobsCount = DB::table('jobs')
                    ->join('employer', 'jobs.employer_id', '=', 'employer.id')
                    ->join('job_title', 'jobs.title', '=', 'job_title.id')
                    ->where("jobs.employer_id", $Users->id)
                    ->where('jobs.employee_type', '!=', 3 )
                    ->count()
                                                
                                                }}</span> </td>

                                                <td><a href="/Admin/EmployerTrainingList/{{$Users->id}}" class="btn btn-sm" data-toggle="tooltip" data-placement="left" title="View Training Details" target="_blank"><i class="fa fa-graduation-cap" aria-hidden="true"></i></a> <span class="badge badge-notification-internships">{{
                                                        $GetInternshipsCount = DB::table('training')
                    ->join('employer', 'training.employer_id', '=', 'employer.id')
                    ->join('training_title', 'training.title', '=', 'training_title.id')
                    ->where("training.employer_id", $Users->id)
                    ->count()
                                                        
                                                        }}</span></td>

                                                <td><a href="/Admin/EmployerCAArticleshipsList/{{$Users->id}}" class="btn btn-sm" data-toggle="tooltip" data-placement="left" title="View Training Details" target="_blank"><i class="fa fa-trophy" aria-hidden="true"></i></a> <span class="badge badge-notification-internships">{{
                                                       $GetCAinternships = DB::table('jobs')
                        ->join('employer', 'jobs.employer_id', '=', 'employer.id')
                        ->where("jobs.employer_id", $Users->id)
                        ->where('jobs.employee_type', 3)
                        ->count()
                                                        
                                                        }}</span></td>

                                        <td>{{$Users->pan_number}}</td>
                                        <td>{{$Users->gst_number}}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ./bootstrap table -->
                    <!-- ./data table -->
                </div>
                <!-- ./row -->
            </div>
            <!-- ./cotainer -->

        </div>
        <!-- ./page-content -->
    </div>




    {{-- Add user Modal --}}
    <div id="AddUsers" class="modal fade width100" role="dialog">
            <div class="modal-dialog modal-lg">
          
              <!-- Modal content-->
              <div class="modal-content">
                {{-- <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">View Profile</h4>
                </div> --}}
                <div class="modal-body" id="GetUsersInfo">
                    
                </div>
                {{--  <div class="modal-footer">
                  <button type="button" id="AddAdminUsers" class="btn btn-success modal-save-btn-mar-right">Save</button>  --}}
                </div>
              </div>
          
            </div>
          </div>

@endsection

