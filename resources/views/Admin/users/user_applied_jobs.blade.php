@extends('Admin.base')

@section('Content')
<div id="page-content-wrapper">
        <div class="page-content">
            <!-- page section -->
            <br>
            <div class="container-fluid">
                <div class="row">
                    {{-- <div class="fixed-action-btn">
                        <a href="javascript:void(0);" class="btn-floating btn-large red" data-toggle="modal" data-target="#AddJobTitleModal" title="Add new gender users">
                            <i class="large material-icons">add</i>
                        </a>
                    </div> --}}
                    <!-- bootstrap table -->
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="card-header">
                                <i class="fa fa-table fa-lg"></i>
                            <h2 class="pull-left">{{$title}}</h2>
                            <h4 class="text-center">Candidate Name: <b>{{$GetCandidate['name']}}</b></h4>
                            </div>
                            <div class="card-content">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                {{-- <th>Select</th> --}}
                                                <th>Job Name</th>
                                                <th>Employer Name </th>
                                                <th>Applied Date </th>
                                                <th>JobDetails</th>
                                                {{-- <th>Action</th> --}}
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($GetJobs as $Jobs)
                                            <tr>
                                                <td>{{$Jobs->JobTitle}}</td>
                                                <td>{{$Jobs->company_name}}</td>
                                                <td>{{date('d M Y', strtotime($Jobs->created_at))}}</td>
                                                <td><a href="/JobDetails/{{$Jobs->CompanySlug}}/{{$Jobs->id}}/{{$Jobs->slug}}" target="_blank">Click here view jobs</a></td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ./bootstrap table -->
                    <!-- ./data table -->
                </div>
                <!-- ./row -->
            </div>
            <!-- ./cotainer -->

        </div>
        <!-- ./page-content -->
    </div>




    {{-- Add gender Modal --}}
    <div id="AddJobTitleModal" class="modal fade" role="dialog">
            <div class="modal-dialog modal-md">
          
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Add Title</h4>
                </div>
                <div class="modal-body">
                        <div class="card-content" id="color-button">
                                <div class="row">
                                    <form class="col s11 m-t-20">
                                        <div class="input-field form-input col s12">
                                            <i class="material-icons prefix">account_circle</i>
                                            <input id="title" type="text" class="validate" placeholder="Title name">
                                            {{--  <label for="icon_prefix">Name</label>  --}}
                                        </div>
                                        <a href="javascript:void(0);" id="AddTitle" class="waves-effect waves-light btn teal pull-right"><i class="material-icons left">save</i>submit</a>

                                    </form>
                                </div>

                            </div>
                </div>
                {{--  <div class="modal-footer">
                  <button type="button" id="AddAdminUsers" class="btn btn-success modal-save-btn-mar-right">Save</button>  --}}
                </div>
              </div>
          
            </div>
          </div>


          {{-- Edit gender Modal --}}
    <div id="EditTitle" class="modal fade" role="dialog">
            <div class="modal-dialog modal-md">
          
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Edit Title</h4>
                </div>
                <div class="modal-body">
                        <div class="card-content" id="color-button">
                                <div class="row">
                                    <form class="col s11 m-t-20">
                                        <div class="input-field form-input col s12">
                                            <i class="material-icons prefix">account_circle</i>
                                            <input id="EditId" type="hidden" class="validate" placeholder="Admin name">
                                            <input id="Editname" type="text" class="validate" placeholder="Title name">
                                            {{--  <label for="icon_prefix">Name</label>  --}}
                                        </div>
                                        <a href="javascript:void(0);" id="UpdateTitle" class="waves-effect waves-light btn teal pull-right"><i class="material-icons left">refresh</i>Update</a>

                                    </form>
                                </div>

                            </div>
                </div>
                {{--  <div class="modal-footer">
                  <button type="button" id="AddAdminUsers" class="btn btn-success modal-save-btn-mar-right">Save</button>  --}}
                </div>
              </div>
          
            </div>
          </div>
@endsection


@section('JSScript')
<script>
    "use strict";
    $(function () {
        $('select').material_select();
        Materialize.updateTextFields();

        //datepicker
        $('.datepicker').pickadate({
            selectMonths: true, // Creates a dropdown to control month
            selectYears: 15 // Creates a dropdown of 15 years to control year
        });
    });
</script>
@endsection