@extends('Admin.base')

@section('Content')
<div id="page-content-wrapper">
    <div class="page-content">
        <!-- Content Header (Page header) -->
        {{-- <section class="content-header z-depth-1">
            <div class="header-icon">
                <i class="fa fa-image"></i>
            </div>
            <div class="header-title">
                <h1> Webinars list</h1>
                <ul class="link hidden-xs">
                    <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
                    <li><a href="#">Webinars</a></li>
                </ul>
            </div>
        </section> --}}
        <!-- page section -->
        <div class="container-fluid">
            <div class="row">
                <!-- Image gellary -->
                <!-- ./iamge gellay -->
                <!-- Image gellary -->
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card mar-top20">
                        <div class="card-header pad-bottom0">
                            <div class="row card-headings mar-bottom0">
                                <div class="col-xs-12 col-sm-6">
                                    <h2>Webinars list</h2>
                                </div>
                                <div class="col-xs-12 col-sm-6 ">
                                        <a class="btn icon-btn btn-info m-r-5 mar-left30 pull-right mar-bottom10" href="/Faculty/AddWebinars" target="_blank"><span class="glyphicon btn-glyphicon glyphicon-education img-circle text-info"></span>Add Webinars</a>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            @foreach($GetWebinars as $Webinars)
                            <div class="card-padding50">
                                   
                                        <div class="card webinars-height">
                                            <div class="col-md-3">
                                                <img src="{{URL::asset('webinar_featured_image/')}}/{{$Webinars->featured_image}}" class="webinar-featured-image">
                                            </div>
                        <div class="col-md-9">
                          <div class="card-block pad-top20">
                          <h3 class="card-title card-head-pad-left-right pull-left">{{$Webinars->topic_name}}</h3>
                        
                          <h3 class="pull-right">
                              <a href="javascript:void(0);" onclick="CheckParticipants({{$Webinars->id}})"><span class="glyphicon btn-glyphicon glyphicon-user img-circle text-info pull-left"></span> <span>{{ \App\Model\Admin\WebinarsAppliedModel::where(['webinar_id' => $Webinars->id])->count() }}     
                            </span></h3>
                            </a>
                        </div>  

                        <div class="card-block pad-top20">
                            <br>
                            <br>
                            <div class="row mar-bottom0">
                                    <div class="col-md-6">
                                            <h4 class="card-head-pad-left-right">Speaker: {{$Webinars->speaker_name}}</h4>
                                        </div>
                                        <div class="col-md-6">
                                            <p class=" card-head-pad-left-right pull-left">Date: {{date('d M Y', strtotime($Webinars->webinar_date))}}</p>
                                        </div>
                            </div>
                            <div class="row mar-bottom0">
                                    <div class="col-md-6">
                                            <p class=" card-head-pad-left-right pull-left">Duration: {{$Webinars->duration}}</p>
                                        </div>
                                        <div class="col-md-6">
                                                <p class=" card-head-pad-left-right pull-left">Timing: {{$Webinars->start_time}} to {{$Webinars->end_time}}</p>
                                            </div>
                                </div>

                            </div>

                            <h3 class="card-head-pad-left-right webinar-price">Price: @if($Webinars->webinar_type == 2)
                                    ₹ {{$Webinars->price}}
                                @else
                                    Free
                                @endif</h3>

                            {{-- <div id="circle-btn" class="text-left pull-left pad-top6percentage">
                            <a class="btn icon-btn btn-info m-r-5 mar-left30" href="/Admin/GetViewParticipants/{{$Webinars->id}}" target="_blank"><span class="glyphicon btn-glyphicon glyphicon-user img-circle text-info"></span>View Participants</a>
                                </div> --}}

                          <div id="circle-btn" class="text-left pull-left pad-top6percentage mar-left10">
                                <a class="btn icon-btn btn-info m-r-5 mar-left30" href="/Faculty/EditWebinars/{{$Webinars->id}}" target="_blank"><span class="glyphicon btn-glyphicon glyphicon-pencil img-circle text-info"></span>Edit</a>
                            </div>
            
                            <div id="circle-btn" class="pull-left pad-top6percentage pad-left15 mar-left5">
                                <a class="btn icon-btn btn-danger m-r-5 mar-right30" href="javascript:void(0);" onclick="DeleteWebinars({{$Webinars->id}})"><span class="glyphicon btn-glyphicon glyphicon-trash img-circle text-danger"></span>Delete</a>
                            </div>

                            @php 
                            date_default_timezone_set('Asia/Kolkata');
                            $GetCurrentTime = date('h:i A', time());
                            $GetCurrentDate = date('Y-m-d', time());

                            if($Webinars->webinar_date == $GetCurrentDate){
                                if($Webinars->start_time <= $GetCurrentTime && $Webinars->end_time >= $GetCurrentTime) {
                                    echo "<div id='circle-btn' class='text-left pull-left pad-top6percentage pull-right'>
                                <a class='btn icon-btn btn-info m-r-5' href=$Webinars->host_url target='_blank'><span class='glyphicon btn-glyphicon glyphicon-facetime-video img-circle text-info'></span>Join Now</a>
                            </div>";
                                }
                            }
                            @endphp
                            
                            
                        </div>
                        </div>
                                            
                            </div>
                                @endforeach
            
                            </div>
                    </div>
                </div>
                <!-- ./image gellary -->
            </div><!-- ./row -->
        </div><!-- ./cotainer -->
    </div><!-- ./page-content -->
</div>




<div id="ViewParticipants" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">
  
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">All participates</h4>
        </div>
        <div class="modal-body">
            <div class="card-content" id="color-button">
                <div class="row">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email </th>
                                <th>Mobile </th>
                                {{-- <th>Action</th> --}}
                            </tr>
                        </thead>
                        <tbody id="ShowParticipates">
                            <tr>
                            <td id="Name"></td>
                            <td id="Email"></td>
                            <td id="Mobile"></td>
                                {{-- <td></td> --}}
                            </tr>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
        {{--  <div class="modal-footer">
          <button type="button" id="AddAdminUsers" class="btn btn-success modal-save-btn-mar-right">Save</button>  --}}
        </div>
      </div>
  
    </div>
  </div>
@endsection