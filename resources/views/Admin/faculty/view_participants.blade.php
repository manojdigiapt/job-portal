@extends('Admin.base')

@section('Content')
<div id="page-content-wrapper">
        <div class="page-content">
            <!-- page section -->
            <br>
            <div class="container-fluid">
                <div class="row">
                    {{-- <div class="fixed-action-btn">
                        <a href="javascript:void(0);" class="btn-floating btn-large red" data-toggle="modal" data-target="#AddJobTitleModal" title="Add new gender users">
                            <i class="large material-icons">add</i>
                        </a>
                    </div> --}}
                    <!-- bootstrap table -->
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="card-header">
                                <i class="fa fa-table fa-lg"></i>
                            <h2>{{$title}}</h2>
                            </div>
                            <div class="card-content">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                {{-- <th>Select</th> --}}
                                                {{-- <th>Profile Pic</th> --}}
                                                <th>Name</th>
                                                <th>Email </th>
                                                <th>Mobile </th>
                                                {{-- <th>Action</th> --}}
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($GetParticipants as $Participants)
                                            <tr>
                                                {{-- <td>
                                                    @if(Auth::guard('candidate')->check())
                                                    <img src="{{URL::asset('candidate_profile/')}}/{{$Participants->profile_pic}}" class="participants-profile_pic" alt="" />
                                                    @elseif(Auth::guard('trainee')->check())
                                                    <img src="{{URL::asset('trainee_profile/')}}/{{$Participants->profile_pic}}" class="participants-profile_pic" alt="" />
                                                    @elseif(Auth::guard('ca_article')->check())
                                                    <img src="{{URL::asset('trainee_profile/')}}/{{$Participants->profile_pic}}" class="participants-profile_pic" alt="" />
                                                    @endif
                                                </td> --}}
                                            <td>{{$Participants->name}}</td>
                                            <td> {{substr_replace($Participants->email, 'xxxxx.com', -20)}}</td>
                                                <td>{{substr_replace($Participants->mobile, 'xxxxxxxx', -8)}}</td>
                                                {{-- <td></td> --}}
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ./bootstrap table -->
                    <!-- ./data table -->
                </div>
                <!-- ./row -->
            </div>
            <!-- ./cotainer -->

        </div>
        <!-- ./page-content -->
    </div>


@endsection
