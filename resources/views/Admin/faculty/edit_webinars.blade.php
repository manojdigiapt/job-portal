@extends('Admin.base')

@section('Content')
<div id="page-content-wrapper">
        <div class="page-content">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="header-icon">
                    <i class="fa fa-tachometer"></i>
                </div>
                <div class="header-title">
                    <h1> Dashboard</h1>
                    {{--  <small> Dashboard features</small>  --}}
                    <ul class="link hidden-xs">
                        <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
                        <li><a href="#">Dashboard</a></li>
                    </ul>
                </div>
            </section>
            <!-- page section -->
            <div class="container-fluid">
                <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="card">
                                    <div class="card-header">
                                        <i class="fa fa-file-o fa-lg"></i>
                                        <h2>Edit Webinars</h2>
                                    </div>
                                    <div class="card-content">
                                        <form class="form-horizontal" method="post">
                                            {{--  <h2 class="text-center">Basic forms Example</h2>  --}}
                                            <fieldset>
                                                <!-- Text input-->
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Webinar Topic</label>
                                                    <div class="col-md-7">
                                                        <div class="input-field">
                                                                <input name="first_name" type="hidden" id="WebId" class="validate" placeholder="Webinar Topic" value="{{$GetWebinarsById->id}}">

                                                        <input name="first_name" type="text" id="web_topic" class="validate" placeholder="Webinar Topic" value="{{$GetWebinarsById->topic_name}}">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Speaker Name</label>
                                                    <div class="col-md-7">
                                                        <div class="input-field">
                                                            <input name="first_name" type="text" id="speaker_name" class="validate" placeholder="Speaker Name" value="{{$GetWebinarsById->speaker_name}}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- ./Text input-->
                                                <!-- Text input-->
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Webinar Date</label>
                                                    <div class="col-md-7">
                                                        <div class="input-field">
                                                            <input name="Last_name" type="date" id="web_date" class="validate" placeholder="Webinar Date" value="{{$GetWebinarsById->webinar_date}}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Text input-->
                                                 <!-- Text input-->
                                                 <div class="form-group">
                                                        <label class="col-md-3 control-label">Webinar Type</label>
                                                        <div class="col-md-7">
                                                            <select class="form-control" id="web_type" onchange="CheckPaymentType()">
                                                            @if($GetWebinarsById->webinar_type == 1)
                                                                <option value="1" selected>Free</option>
                                                                <option value="2">Paid</option>
                                                            @else
                                                                <option value="1">Free</option>
                                                                <option value="2" selected>Paid</option>
                                                            @endif
                                                                
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <div class="form-group" id="ShowPrice" style="
                                                    @if($GetWebinarsById->webinar_type == 1)
                                                    display:none;
                                                    @else

                                                    @endif
                                                    ">
                                                            <label class="col-md-3 control-label">Price</label>
                                                            <div class="col-md-7">
                                                                <div class="input-field">
                                                                <input name="price" type="text" id="price" class="validate" placeholder="Eg: 500" value="{{$GetWebinarsById->price }}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <br>
                                                    <!-- Text input-->
                                                     <!-- Text input-->
                                                <div class="form-group">
                                                        <label class="col-md-3 control-label">Webinar Category</label>
                                                        <div class="col-md-7">
                                                            <select class="form-control" id="web_category">
                                                            @if($GetWebinarsById->category == 1)
                                                                <option value="1" selected>Experienced</option>
                                                                <option value="2">Freshers</option>
                                                                <option value="3">CA Articleship</option>
                                                            @elseif($GetWebinarsById->category == 2)
                                                                <option value="1" >Experienced</option>
                                                                <option value="2" selected>Freshers</option>
                                                                <option value="3">CA Articleship</option>
                                                            @else
                                                                <option value="1" >Experienced</option>
                                                                <option value="2" >Freshers</option>
                                                                <option value="3" selected>CA Articleship</option>
                                                            @endif
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <!-- Text input-->
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Webinar Timing</label>
                                                        <div class="col-md-3">
                                                                <div class="input-field">
                                                                    <input name="Last_name" type="text" id="start_timing" class="validate" value="{{$GetWebinarsById->start_time}}" placeholder="Start Time">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="input-field">
                                                                <input name="Last_name" type="text" id="end_timing" class="validate" value="{{$GetWebinarsById->end_time}}"  placeholder="End Time">
                                                            </div>
                                                        </div>
                                                    </div>
                                                     <!-- Text input-->
                                                <div class="form-group">
                                                        <label class="col-md-3 control-label">Total Duration</label>
                                                        <div class="col-md-7">
                                                            <div class="input-field">
                                                                <input name="Last_name" type="text" id="web_duration" class="validate" placeholder="Total Duration" value="{{$GetWebinarsById->duration}}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">What participant will learn from this webinar</label>
                                                    <div class="col-md-7">
                                                        <div class="input-field">
                                                            <textarea id="web_description">
                                                                {{$GetWebinarsById->description}}
                                                            </textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Set as featured image</label>
                                                    <div class="col-md-3">
                                                        <div class="input-field">
                                                            <input  type="file" id="inputFile" class="validate">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6 text-center">
                                                        <p class="text-center">Preview</p>
                                                        <br>
                                                        <img src="{{URL::asset('webinar_featured_image/')}}/{{$GetWebinarsById->featured_image}}" id="image_upload_preview" class="webinar-featured-img-width" alt="">
                                                    </div>
                                                </div>
                                                    <!-- Text input-->
                                                <!-- Button -->
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label"></label>
                                                    <div class="col-md-4">
                                                        <button type="button" id="UpdateWebinars" class="btn btn-success">Submit <span class="glyphicon glyphicon-floppy-disk"></span></button>
                                                    </div>
                                                </div>

                                            </fieldset>
                                        </form>
                                    </div>
                                </div>
                            </div>
                </div>
                <!-- ./row -->
            </div>
            <!-- ./cotainer -->
        </div>
        <!-- ./page-content -->
    </div>
@endsection

@section('JSScript')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>

    <script>
        $(function() {
        $('#start_timing').timepicker();
        $('#end_timing').timepicker();
    });
    </script>
@endsection