<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CandidateProfileViewedByEmployer extends Mailable
{
    use Queueable, SerializesModels;
    public $GetCandidateName;
    public $GetEmpName;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($GetCandidateName, $GetEmpName)
    {
        $this->GetCandidateName = $GetCandidateName;
        $this->GetEmpName = $GetEmpName;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Employer Has Been Viewed Your Profile - ACCOUNTSWALE')->view('UI.email.employerviewedprofile');
    }
}
