<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class JobAppliedCandidateToEmployer extends Mailable
{
    use Queueable, SerializesModels;
    public $GetJobTitle;
    public $GetEmpName;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($GetEmpName, $GetJobTitle)
    {
        $this->GetEmpName = $GetEmpName;
        $this->GetJobTitle = $GetJobTitle;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Job Application Alerts - ACCOUNTSWALE')->view('UI.email.jobappliedcandidatetoemployer');
    }
}
