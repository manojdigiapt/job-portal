<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class InstituteVerifyEmail extends Mailable
{
    use Queueable, SerializesModels;
    public $GetToken;
    public $Id;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($Id, $GetToken)
    {
        $this->Id = $Id;
        $this->GetToken = $GetToken;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('Admin.institute.reset_email');
    }
}
