<?php

namespace App\Http\Controllers\UI;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Session;
use App\Model\UI\CandidateProfileModel;
use App\Model\UI\CandidateModel;
use App\Model\UI\ShortlistCandidatesModel;
use App\Model\UI\JobsModel;
use App\Model\UI\FavouriteModel;
use App\Model\UI\EmployerModel;
use App\Model\UI\CandidateResume;

use App\Mail\VerifyMail;
use App\Mail\GetUsers;
use App\Mail\EmailOtp;
use DB;

// Admin Model
use App\Model\Admin\JobTypeModel;
use App\Model\Admin\IndustriesModel;
use App\Model\Admin\QualificationModel;
use App\Model\Admin\FunctionalAreasModel;
use App\Model\Admin\JobTitleModel;

use App\Http\Controllers\Textlocal;

class CandidateController extends Controller
{
    public function __construct(){
        // $this->middleware('auth');
        $this->middleware('guest:candidate')->except('logout', 'CandidateLogin', 'AddCandidate', 'CheckLoginAfterRegister', 'CheckForgotEmail', 'SearchCandidatesPositions', 'SearchCandidates', 'SendOtp', 'SortByCandidates', 'ResetPassword', 'CheckResetPassword', 'ViewAllCandidates', 'ResendLink', 'ResendEmailOTP', 'TraineeFirstRegister', 'ArticleshipFirstRegister');
        
    }

    public function logout(Request $request) {
        // Auth::logout();
        Auth::guard('candidate')->logout();
        return redirect('/');
    }

    public function CandidateDashboard(){
        $title = "Candidate Dashboard";
        $GetEmail = Session::get('CandidateEmail');
        $GetCandidateId = Session::get('CandidateId');
        $CandidateProfile = CandidateProfileModel::where("candidate_id", $GetCandidateId)->first();
        if($CandidateProfile){
            $GetCandidateId = Session::get('CandidateId');
        }else{
            $GetCandidateId = "0";
        }
        // echo $GetCandidateId;
        // exit;
        $Profile = CandidateModel::where("id", $GetCandidateId)->first();
        
        $GetJobsBySlug = DB::select("SELECT DISTINCT jobs.id, job_title.name AS title, employer.website, employer.mobile, employer.email, employer.profile_pic, jobs.total_positions, employer.company_name, jobs.city , jobs.salary_min, jobs.salary_max, jobs.job_type, jobs.created_at, jobs.job_description, jobs.experience, jobs.qualification, jobs.slug, employer.slug AS CompanySlug FROM jobs, employer, job_title WHERE jobs.employer_id = employer.id AND job_title.id = jobs.title AND jobs.employee_type != 1 AND jobs.status = 0 ORDER BY jobs.created_at DESC");

        $GetFavouriteJobs = DB::select("SELECT employer.profile_pic, jobs.title, employer.slug AS CompanySlug, jobs.slug, employer.company_name FROM favourite_jobs, jobs, employer WHERE favourite_jobs.candidate_id = $GetCandidateId AND favourite_jobs.jobs_id = jobs.id AND jobs.employer_id = employer.id AND favourite_jobs.status = 1 AND favourite_jobs.type = 1");

        $GetExperiencePosition = DB::table('resume')
                                ->select('job_title.name', 'resume.name AS CompanyName')
                                ->where('candidate_id', $GetCandidateId)
                                ->join('job_title', 'job_title.id', '=', 'resume.department')
                                ->where('worked_till', 1) 
                                ->where('type', 2)
                                ->first();
                                        
        $GetProfessionalDetails = DB::table('candidate')
                                ->select('resume.name', 'job_title.name AS JobTitle')
                                ->join('resume', 'candidate.id', '=', 'resume.candidate_id')
                                ->join('job_title', 'job_title.id', '=', 'resume.department')
                                ->where('resume.worked_till', '=', 1)
                                ->where('resume.candidate_id', $GetCandidateId)
                                ->first();

        $GetJobType = JobTypeModel::get();
        $GetIndustries = IndustriesModel::get();
        $GetQualification = QualificationModel::get();
        $GetJobTitle = JobTitleModel::get();

        $GetCityByJobPosted =  DB::select("SELECT DISTINCT city FROM jobs");

        $GetFavouritesCount = FavouriteModel::where('candidate_id', $GetCandidateId)
                                ->count();
        // echo $GetFavouritesCount;
        // exit;

        return view("UI.layouts.candidate_dashboard", compact('title', "Profile", 'CandidateProfile', 'GetFavouriteJobs', 'GetExperiencePosition', 'GetJobsBySlug', 'GetJobType', 'GetIndustries', 'GetQualification', 'GetProfessionalDetails', 'GetJobTitle', 'GetCityByJobPosted', 'GetFavouritesCount'));
    }


    public function CandidateFavouriteJobs(){
        $title = "Favourite Jobs";
        $GetEmail = Session::get('CandidateEmail');
        $GetCandidateId = Session::get('CandidateId');
        $CandidateProfile = CandidateProfileModel::where("candidate_id", $GetCandidateId)->first();
        if($CandidateProfile){
            $GetCandidateId = Session::get('CandidateId');
        }else{
            $GetCandidateId = "0";
        }
        // echo $GetCandidateId;
        // exit;
        $Profile = CandidateModel::where("email", $GetEmail)->first();
        
        $GetAddedFavouriteJobs = DB::select("SELECT jobs.id, job_title.name AS title, employer.website, employer.mobile, employer.email, employer.profile_pic, jobs.total_positions, employer.company_name, jobs.city , jobs.salary_min, jobs.salary_max, jobs.job_type, jobs.created_at, jobs.job_description, jobs.experience, jobs.qualification, jobs.slug, employer.slug AS CompanySlug FROM jobs, employer, favourite_jobs, job_title WHERE jobs.employer_id = employer.id AND favourite_jobs.jobs_id = jobs.id AND job_title.id = jobs.title AND favourite_jobs.candidate_id = $GetCandidateId ORDER BY favourite_jobs.created_at DESC");

        // echo json_encode($GetAddedFavouriteJobs);
        // exit;
        $GetFavouriteJobs = DB::select("SELECT employer.profile_pic, jobs.title, employer.slug AS CompanySlug, jobs.slug, employer.company_name FROM favourite_jobs, jobs, employer WHERE favourite_jobs.candidate_id = $GetCandidateId AND favourite_jobs.jobs_id = jobs.id AND jobs.employer_id = employer.id AND favourite_jobs.status = 1 AND favourite_jobs.type = 1");

        $GetExperiencePosition = DB::table('resume')
                                ->select('job_title.name', 'resume.name AS CompanyName')
                                ->where('candidate_id', $GetCandidateId)
                                ->join('job_title', 'job_title.id', '=', 'resume.department')
                                ->where('worked_till', 1) 
                                ->where('type', 2)
                                ->first();
                                        
                                $GetProfessionalDetails = DB::table('candidate')
                                ->select('resume.name', 'job_title.name AS JobTitle')
                                ->join('resume', 'candidate.id', '=', 'resume.candidate_id')
                                ->join('job_title', 'job_title.id', '=', 'resume.department')
                                ->where('resume.worked_till', '=', 1)
                                ->where('candidate.id', $GetCandidateId)
                                ->first();

        $GetJobType = JobTypeModel::get();
        $GetIndustries = IndustriesModel::get();
        $GetQualification = QualificationModel::get();

        $GetFavouritesCount = FavouriteModel::where('candidate_id', $GetCandidateId)
                                ->count();

        return view("UI.layouts.candidate_favourites", compact('title', "Profile", 'CandidateProfile', 'GetFavouriteJobs', 'GetExperiencePosition', 'GetAddedFavouriteJobs', 'GetJobType', 'GetIndustries', 'GetQualification', 'GetProfessionalDetails', 'GetFavouritesCount'));
    }



    public function CandidateChangePassword(){
        $title = "Candidate Change Password";
        $GetEmail = Session::get('CandidateEmail');
        $GetCandidateId = Session::get('CandidateId');
        $CandidateProfile = CandidateProfileModel::where("candidate_id", $GetCandidateId)->first();
        if($CandidateProfile){
            $GetCandidateId = Session::get('CandidateId');
        }else{
            $GetCandidateId = "0";
        }
        $GetProfessionalDetails = DB::table('candidate')
                                ->select('resume.name', 'job_title.name AS JobTitle')
                                ->join('resume', 'candidate.id', '=', 'resume.candidate_id')
                                ->join('job_title', 'job_title.id', '=', 'resume.department')
                                ->where('resume.worked_till', '=', 1)
                                ->where('candidate.id', $GetCandidateId)
                                ->first();
        // echo $GetCandidateId;
        // exit;
        $Profile = CandidateModel::where("email", $GetEmail)->first();
        
        $GetFavouriteJobs = DB::select("SELECT employer.profile_pic, jobs.title, employer.slug AS CompanySlug, jobs.slug, employer.company_name FROM favourite_jobs, jobs, employer WHERE favourite_jobs.candidate_id = $GetCandidateId AND favourite_jobs.jobs_id = jobs.id AND jobs.employer_id = employer.id AND favourite_jobs.status = 1 AND favourite_jobs.type = 1");

        $GetFavouritesCount = FavouriteModel::where('candidate_id', $GetCandidateId)
                                ->count();
        
        return view("UI.layouts.candidate_change_password", compact('title', "Profile", 'CandidateProfile', 'GetFavouriteJobs', 'GetProfessionalDetails', 'GetFavouritesCount'));
    }

    public function CandidateProfile(){
        $title = "Candidate Profile";
        // $GetEmail = Auth::guard('candidate')->user()->email;
        $GetEmail = Session::get('CandidateEmail');
        $GetCandidateId = Session::get('CandidateId');

        // echo $GetCandidateId;
        // exit;

        $CandidateProfile = CandidateProfileModel::where("candidate_id", $GetCandidateId)                              
                            ->first();
        $CandidateProfileCareer = DB::table('candidate_profile')
                                ->join('industries', 'industries.id', '=', 'candidate_profile.industry')
                                ->join('functional_areas', 'functional_areas.id', '=', 'candidate_profile.functional_area')
                                ->select('candidate_profile.*', 'industries.name AS IndustryName', 'functional_areas.functional_areas AS FuntionAreas')
                                ->where('candidate_profile.candidate_id', '=', $GetCandidateId)
                                ->first();

        // echo json_encode($CandidateProfileCareer);
        // exit;
        if($CandidateProfile){
            $GetCandidateId = Session::get('CandidateId');
        }else{
            $GetCandidateId = "0";
        }
        // echo $GetCandidateId;
        // exit;
        $Profile = CandidateModel::where("id", $GetCandidateId)->first();
        
        $GetFavouriteJobs = DB::select("SELECT employer.profile_pic, jobs.title, employer.slug AS CompanySlug, jobs.slug, employer.company_name FROM favourite_jobs, jobs, employer WHERE favourite_jobs.candidate_id = $GetCandidateId AND favourite_jobs.jobs_id = jobs.id AND jobs.employer_id = employer.id AND favourite_jobs.status = 1 AND favourite_jobs.type = 1");

        $GetCV = CandidateResume::where('candidate_id', $GetCandidateId)
        ->where('type', 4)->first();

        $GetExperience =    DB::table('resume')
                            ->select('resume.*', 'job_title.name AS JobTitle')
                            ->where('candidate_id', $GetCandidateId)
                            ->where('type', 2)
                            ->join('job_title', 'job_title.id', '=', 'resume.department')
                            ->orderBy('id', 'DESC')
                            ->get();

        // $GetEducation = CandidateResume::where('candidate_id', $GetCandidateId)
        //                                 ->select('resume.*', 'qualifications.qualification')
        //                                 ->join('qualifications', 'qualifications.id', '=', 'resume.title')
        //                                 ->where('type', 1)
        //                                 ->orderBy('id', 'DESC')
        //                                 ->get();
        $GetEducation = DB::table('resume')
                            ->select('resume.*', 'qualifications.qualification')
                            ->join('qualifications', 'qualifications.id', '=', 'resume.department')
                            ->where('type', 1)
                            ->where('resume.candidate_id', $GetCandidateId)
                            ->orderBy('id', 'DESC')
                            ->get();

        // echo json_encode($GetEducation);
        // exit;

        $GetExperiencePosition = DB::table('resume')
                                ->select('job_title.name', 'resume.name AS CompanyName')
                                ->where('candidate_id', $GetCandidateId)
                                ->join('job_title', 'job_title.id', '=', 'resume.department')
                                ->where('worked_till', 1) 
                                ->where('type', 2)
                                ->first();


        $GetIndustries = DB::select("SELECT * FROM industries ORDER BY CHAR_LENGTH(name) DESC");

        // echo json_encode($GetIndustries);
        // exit;

        $GetFunctionalAreas = FunctionalAreasModel::get();
        $GetQualification = QualificationModel::get();
        $GetJobTitle = JobTitleModel::get();


        $GetSkillNames = "";

        // foreach($CandidateProfile as $key => $value){
        //     $GetSkillNames = explode(',', $value['skills']);
        // }
        $GetSkillNames = explode(',', $CandidateProfile->skills);
        // echo json_encode($GetSkillNames);
        // exit;
        $GetProfessionalDetails = DB::table('candidate')
                                ->select('resume.name', 'job_title.name AS JobTitle')
                                ->join('resume', 'candidate.id', '=', 'resume.candidate_id')
                                ->join('job_title', 'job_title.id', '=', 'resume.department')
                                ->where('resume.worked_till', '=', 1)
                                ->where('candidate.id', $GetCandidateId)
                                ->first();

                                $GetFavouritesCount = FavouriteModel::where('candidate_id', $GetCandidateId)
                                ->count();

        return view("UI.layouts.candidate_profile", compact('title', "Profile", 'CandidateProfile', 'GetFavouriteJobs', 'GetCV', 'GetIndustries', 'GetFunctionalAreas', 'GetExperience', 'CandidateProfileCareer', 'GetEducation', 'GetExperiencePosition', 'GetSkillNames', 'GetProfessionalDetails', 'GetQualification', 'GetJobTitle', 'GetFavouritesCount'));
    }

    public function AddCandidate(Request $request){
        $Candidate = new CandidateModel();
        $CandidateProfile = new CandidateProfileModel(); 

        $CandidateEmail = $request->candidateemail;
        $CandidateMobile = $request->candidatephonenumber;
        $CandidateName = $request->candidatename;
        $CandidateType = $request->candidatetype;
        $CandidateMobileOTP = $request->MobileOTP;
        $CandidateEmailOTP = $request->EmailOTP;
        $PromoCode = $request->promocode;

        $GetMobileOTP = Session::get("GetMobileOtp");

        $GetEmailOTP = Session::get("GetEmailOtp");

        $CheckEmail = $Candidate::where("email", $CandidateEmail)->first();
        $CheckUserName = $Candidate::where("name", $CandidateName)
                                    ->where('type', $CandidateType)
                                    ->first();

        $CheckMobile = $Candidate::where("mobile", $CandidateMobile)->first();
        $CandidatePassword = Hash::make($request->candidatepassword);

        if($GetMobileOTP != $CandidateMobileOTP){
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Mobile OTP Verification Failed."
            ));
            
        }
        // else if($GetEmailOTP != $CandidateEmailOTP){
        //     return response()->json(array(
        //         "error"=>TRUE,
        //         "message"=>"Email OTP Verification Failed."
        //     ));
            
        // }
        else{
            $Candidate->name = $CandidateName;
            $Candidate->password = $CandidatePassword;
            $Candidate->slug = strtolower($request->candidatename);
            $Candidate->email = $CandidateEmail;
            $Candidate->type = $CandidateType;
            $Candidate->mobile = $CandidateMobile;
            $Candidate->mobile_otp_verify = 1;
            $Candidate->email_verify = 1;

            $InsertCandidate = $Candidate->save();

            $CandidateProfile->candidate_id = $Candidate->id;
            $CandidateProfile->profile_completion = 5;
            $CandidateProfile->institute_id = $PromoCode;
            $CandidateProfile->save();
            if($InsertCandidate){
                // Auth::guard('candidate')->attempt(['name' => $request->candidatename, 'password' => $CandidatePassword]);

                $request->session()->put('CandidateName', $request->candidatename);
                $request->session()->put('CandidateEmail', $CandidateEmail);

                $GetAdminMail = "admin@accountswale.in";

                $name = $request->candidatename;
                $email = $request->candidateemail;
                $mobile = $request->candidatephonenumber;

                Mail::to($GetAdminMail)->send(new GetUsers($name, $email, $mobile));

                return $this->CheckLoginAfterRegister($request, $CandidateEmail, $request->candidatepassword);
                return response()->json(array(
                    "error"=>FALSE,
                    "message"=>"Registered successfully"
                ));
            }else{
                return response()->json(array(
                    "error"=>TRUE,
                    "message"=>"Registeration failed"
                ));
            }
        }

        
    }


    public function CheckLoginAfterRegister(Request $request, $candidateusername, $candidatepassword){
        $CheckCandidateOrTrainee = CandidateModel::where('email', $candidateusername)
                                ->first();

        $CheckHashPassword = Hash::check($candidatepassword, $CheckCandidateOrTrainee->password);
        // echo json_encode($CheckHashPassword);
        // exit;

        if($CheckHashPassword == true){
            $CheckCandidateOrTraineeType = CandidateModel::where('email', $candidateusername)
                                ->first();
            // echo json_encode($CheckCandidateOrTraineeType->type);
            // exit;
            if($CheckCandidateOrTraineeType->type == 1){
                
                if (Auth::guard('candidate')->attempt(['email' => $candidateusername, 'password' => $candidatepassword])) {
                    //Authentication passed...
                    $request->session()->put('CandidateName', Auth::guard('candidate')->user()->name);
                    $request->session()->put('CandidateEmail', Auth::guard('candidate')->user()->email);
                    $request->session()->put('CandidateId', Auth::guard('candidate')->user()->id);
                    return response()->json(array(
                        "error"=>FALSE,
                        "message"=>"Login successfully",
                        "type" => "1"
                    ));
                }else{
                    return response()->json(array(
                        "error"=>TRUE,
                        "message"=>"Please Check your credentials..."
                    ));
                }   
            }elseif($CheckCandidateOrTraineeType->type == 2){
                if (Auth::guard('trainee')->attempt(['email' => $candidateusername, 'password' => $candidatepassword])) {
                    //Authentication passed...
                    $request->session()->put('TraineeName', Auth::guard('trainee')->user()->name);
                    $request->session()->put('TraineeEmail', Auth::guard('trainee')->user()->email);
                    $request->session()->put('TraineeId', Auth::guard('trainee')->user()->id);
                    return response()->json(array(
                        "error"=>FALSE,
                        "message"=>"Login successfully",
                        "type" => "2"
                    ));
                }else{
                    return response()->json(array(
                        "error"=>TRUE,
                        "message"=>"Please Check your credentials..."
                    ));
                }  
            }elseif($CheckCandidateOrTraineeType->type == 3){
                if (Auth::guard('ca_article')->attempt(['email' => $candidateusername, 'password' => $candidatepassword])) {
                    //Authentication passed...
                    $request->session()->put('CaArticleName', Auth::guard('ca_article')->user()->name);
                    $request->session()->put('CaArticleEmail', Auth::guard('ca_article')->user()->email);
                    $request->session()->put('CaArticleId', Auth::guard('ca_article')->user()->id);
                    return response()->json(array(
                        "error"=>FALSE,
                        "message"=>"Login successfully",
                        "type" => "3"
                    ));
                }else{
                    return response()->json(array(
                        "error"=>TRUE,
                        "message"=>"Please Check your credentials..."
                    ));
                }  
            }
        }
    }


    public function CandidateLogin(Request $request){
        $CheckCandidateOrTrainee = CandidateModel::where('email', $request->candidateusername)
                                ->first();
        // echo json_encode($CheckCandidateOrTrainee);
        // exit;
        $CheckHashPassword = Hash::check($request->candidatepassword, $CheckCandidateOrTrainee["password"]);

        if($CheckHashPassword == true){
            $CheckCandidateOrTraineeType = CandidateModel::where('email', $request->candidateusername)
                                ->first();

            if($CheckCandidateOrTraineeType->type == 1){
                
                if (Auth::guard('candidate')->attempt(['email' => $request->candidateusername, 'password' => $request->candidatepassword])) {
                    //Authentication passed...
                    $request->session()->put('CandidateName', Auth::guard('candidate')->user()->name);
                    $request->session()->put('CandidateEmail', Auth::guard('candidate')->user()->email);
                    $request->session()->put('CandidateId', Auth::guard('candidate')->user()->id);
                    return response()->json(array(
                        "error"=>FALSE,
                        "message"=>"Login successfully",
                        "type" => "1"
                    ));
                }else{
                    return response()->json(array(
                        "error"=>TRUE,
                        "message"=>"Please Check your credentials..."
                    ));
                }   
            }elseif($CheckCandidateOrTraineeType->type == 2){
                if (Auth::guard('trainee')->attempt(['email' => $request->candidateusername, 'password' => $request->candidatepassword])) {
                    //Authentication passed...
                    $request->session()->put('TraineeName', Auth::guard('trainee')->user()->name);
                    $request->session()->put('TraineeEmail', Auth::guard('trainee')->user()->email);
                    $request->session()->put('TraineeId', Auth::guard('trainee')->user()->id);
                    return response()->json(array(
                        "error"=>FALSE,
                        "message"=>"Login successfully",
                        "type" => "2"
                    ));
                }else{
                    return response()->json(array(
                        "error"=>TRUE,
                        "message"=>"Please Check your credentials..."
                    ));
                }  
            }elseif($CheckCandidateOrTraineeType->type == 3){
                if (Auth::guard('ca_article')->attempt(['email' => $request->candidateusername, 'password' => $request->candidatepassword])) {
                    //Authentication passed...
                    $request->session()->put('CaArticleName', Auth::guard('ca_article')->user()->name);
                    $request->session()->put('CaArticleEmail', Auth::guard('ca_article')->user()->email);
                    $request->session()->put('CaArticleId', Auth::guard('ca_article')->user()->id);
                    return response()->json(array(
                        "error"=>FALSE,
                        "message"=>"Login successfully",
                        "type" => "3"
                    ));
                }else{
                    return response()->json(array(
                        "error"=>TRUE,
                        "message"=>"Please Check your credentials..."
                    ));
                }  
            }
        }else{
            return response()->json(array(
                        "error"=>TRUE,
                        "message"=>"Please Check your credentials..."
                    ));
        }
    }

    public function CheckForgotEmail(Request $request){
        $Candidate = new CandidateModel();
        $CandidateEmail = $request->candidateemail;
        $CheckEmail = $Candidate::where("email", $CandidateEmail)->first();

        if(!$CheckEmail){
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Email id is not registered."
            ));
        }else{
            $token = str_random(64);
            $request->session()->put('GetToken', $token);
            $GetToken = Session::get('GetToken');
            $CheckDataById = $Candidate::findOrFail($CheckEmail->id);
            $CheckDataById->_token = $GetToken;
            $CheckDataById->save();
            $Id = $CheckEmail->id;
            Mail::to($CandidateEmail)->send(new VerifyMail($Id, $GetToken));
            // echo json_encode($CheckDataById->_token);
            // exit;
            if($CheckDataById){
                return response()->json(array(
                    "error"=>FALSE,
                    "message"=>"Email send successfully. Please check your email."
                ));
            }else{
                return response()->json(array(
                    "error"=>TRUE,
                    "message"=>"Email failed..."
                ));
            }
        }
        
    }

    public function CheckResetPassword($id, $token){
        $title = "Reset Password";
        $GetId = $id;

        return view("UI.email.reset_password", compact('title', 'GetId'));
    }

    public function ResetPassword(Request $request){
        $GetId = $request->id;
        $GetNewPassword = $request->new_password;
        $GetConfirmPassword = $request->confirm_password;
        // echo json_encode($GetToken);
        // exit;
        if($GetNewPassword != $GetConfirmPassword){
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Password mismatch"
            ));
        }else{
            $ChangePassword = CandidateModel::findorFail($GetId);
            $ChangePassword->password = Hash::make($GetConfirmPassword);
            $ChangePassword->save();
            return response()->json(array(
                "error"=>FALSE,
                "message"=>"Password changed successfully"
            ));
        }
    }

    public function SendOtp(Request $request){
        // $GetMobile = $request->mobile;

        $GenerateMobileOtp = rand(5, 897645);
        $request->session()->put('GetMobileOtp', $GenerateMobileOtp);
        $GetMobileOTP = Session::get("GetMobileOtp");

        $GenerateEmailOtp = rand(7, 8976457);
        $request->session()->put('GetEmailOtp', $GenerateEmailOtp);
        $GetEmailOTP = Session::get("GetEmailOtp");

        $CandidateEmail = $request->candidateemail;
        $CandidateMobile = $request->candidatephonenumber;
        $CandidateName = $request->candidatename;
        $CandidateType = $request->candidatetype;

        $CheckEmail = CandidateModel::where("email", $CandidateEmail)->first();
        $CheckUserName = CandidateModel::where("name", $CandidateName)
                                    ->where('type', $CandidateType)
                                    ->first();

        $CheckMobile = CandidateModel::where("mobile", $CandidateMobile)->first();

        
        
        
        if($CheckUserName){
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"User name is already registered."
            ));
        }else if($CheckEmail){
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Email id is already registered."
            ));
        }
        else if($CheckMobile){
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Mobile number is already registered."
            ));
        }else if($GenerateMobileOtp && $GenerateEmailOtp){
            $apiKey = urlencode('XutSvsJBNnI-sVkiqAhNlXHhDaDkICxtxiITNpcjBn');

            // Message details
            $numbers = array($CandidateMobile);
            $sender = urlencode('ACCWLE');
            $message = rawurlencode('Thanks for registering accountswale. Your OTP is: '.$GetMobileOTP);
        
            $numbers = implode(',', $numbers);
        
            // Prepare data for POST request
            $data = array('apikey' => $apiKey, 'numbers' => $numbers, "sender" => $sender, "message" => $message);
        
            // Send the POST request with cURL
            $ch = curl_init('https://api.textlocal.in/send/');
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($ch);
            curl_close($ch);

            // Mail::to($CandidateEmail)->send(new EmailOtp($GetEmailOTP));
            return response()->json(array(
                "error"=>FALSE,
                "message"=>"OTP Send Successfully."
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Failed"
            ));
        }
        
    }


    public function SortByCandidates(Request $request){
        $GetSortBy = $request->sortby;

        if($GetSortBy == 1){
            $GetDataByDatePosted = DB::select("SELECT * FROM candidate, candidate_profile WHERE candidate.id = candidate_profile.candidate_id AND candidate.type = 1 ORDER BY candidate.id ASC LIMIT 1");
        }elseif($GetSortBy == 2){
            $GetDataByDatePosted = DB::select("SELECT * FROM candidate, candidate_profile WHERE candidate.id = candidate_profile.candidate_id AND candidate.type = 1 ORDER BY candidate.id ASC LIMIT 2");
        }elseif($GetSortBy == 3){
            $GetDataByDatePosted = DB::select("SELECT * FROM candidate, candidate_profile WHERE candidate.id = candidate_profile.candidate_id AND candidate.type = 1 ORDER BY candidate.id ASC LIMIT 3");
        }elseif($GetSortBy == 4){
            $GetDataByDatePosted = DB::select("SELECT * FROM candidate, candidate_profile WHERE candidate.id = candidate_profile.candidate_id AND candidate.type = 1 ORDER BY candidate.id ASC LIMIT 3");
        }

        $GetFilterResultData = array();
        $CheckJobType = "";

        foreach($GetDataByDatePosted as $DatePostedData){

            $GetFilterResultData[] = "<div class='emply-resume-list square'><div class='row'>
            <div class='col-md-8'><div class='emply-resume-thumb'><img src='/candidate_profile/".$DatePostedData->profile_pic."' alt=''/></div><div class='emply-resume-info'><h3><a href='#' title=''>".$DatePostedData->name."</a></h3><span><i>".$DatePostedData->role."</i> at Atract Solutions</span><p><i class='la la-map-marker'></i>".$DatePostedData->address." / ".$DatePostedData->city."</p></div></div><div class='col-md-4'><div class='shortlists'><a href='/CandidateDetails/".$DatePostedData->slug."' id='' title='' target='_blank'>View Profile <i class='la la-plus'></i></a></div></div></div>";
        }

        echo json_encode($GetFilterResultData);
    }

    
    public function ViewAllCandidates(){
        $title = "View all candidates";

        $GetEmployerId = Session::get("EmployerId");
        $EmployerProfile = EmployerModel::where("id", $GetEmployerId)->first();

        $GetAppliedCandidates = DB::select("SELECT candidate.name, candidate_profile.profile_pic,jobs_applied.created_at, jobs.title  FROM jobs_applied, candidate, candidate_profile, jobs WHERE jobs_applied.candidate_id = candidate.id AND candidate_profile.candidate_id = candidate.id AND jobs.id = jobs_applied.jobs_id AND jobs_applied.employer_id = $GetEmployerId");

        return view("UI.layouts.view_all_candidates", compact('title', 'EmployerProfile', 'GetAppliedCandidates'));
    }



    public function ResendLink(Request $request){
        $CandidateMobile = $request->candidatephonenumber;
        // echo $CandidateMobile;
        // exit;

        $GenerateMobileOtp = rand(5, 897645);
        $request->session()->put('GetMobileOtp', $GenerateMobileOtp);
        $GetMobileOTP = Session::get("GetMobileOtp");

        $apiKey = urlencode('XutSvsJBNnI-sVkiqAhNlXHhDaDkICxtxiITNpcjBn');

            // Message details
            $numbers = array($CandidateMobile);
            $sender = urlencode('ACCWLE');
            $message = rawurlencode('Thanks for registering accountswale. Your OTP is: '.$GetMobileOTP);
        
            $numbers = implode(',', $numbers);
        
            // Prepare data for POST request
            $data = array('apikey' => $apiKey, 'numbers' => $numbers, "sender" => $sender, "message" => $message);
        
            // Send the POST request with cURL
            $ch = curl_init('https://api.textlocal.in/send/');
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($ch);
            curl_close($ch);
//         $sms = new Textlocal();
// $sms->send('test', '9698631624','sender'); //sender is optional

        return response()->json(array(
                        "error"=>FALSE,
                        "message"=>"Otp Send Successfully"
        ));
    }


    public function ResendEmailOTP(Request $request){
        $GenerateEmailOtp = rand(7, 8976457);
        $request->session()->put('GetEmailOtp', $GenerateEmailOtp);
        $GetEmailOTP = Session::get("GetEmailOtp");
        
        $CandiateEmail = $request->candidateremail;

        Mail::to($CandiateEmail)->send(new EmailOtp($GetEmailOTP));

        return response()->json(array(
            "error"=>FALSE,
            "message"=>"Otp Send Successfully"
        ));
    }






    // Profile First registration
    public function CandidateFirstRegister(){
        $title = "Candidate Register";
        $GetEmail = Session::get('CandidateEmail');
        $GetCandidateId = Session::get('CandidateId');
        
        $CandidateProfile = CandidateProfileModel::where("candidate_id", $GetCandidateId)->first();

        $Profile = CandidateModel::where("id", $GetCandidateId)->first();

        $GetProfessionalDetails = DB::table('candidate')
                                ->select('resume.name', 'job_title.name AS JobTitle')
                                ->join('resume', 'candidate.id', '=', 'resume.candidate_id')
                                ->join('job_title', 'job_title.id', '=', 'resume.department')
                                ->where('resume.worked_till', '=', 1)
                                ->where('resume.candidate_id', $GetCandidateId)
                                ->first();

        $GetIndustries = IndustriesModel::get();
        $GetJobTitle = JobTitleModel::get();
        $GetQualification = QualificationModel::get();

        $GetExperience =    DB::table('resume')
                            ->select('resume.*', 'job_title.name AS JobTitle')
                            ->where('candidate_id', $GetCandidateId)
                            ->where('type', 2)
                            ->join('job_title', 'job_title.id', '=', 'resume.department')
                            ->orderBy('id', 'DESC')
                            ->get();

        $GetEducation = DB::table('resume')
                            ->select('resume.*', 'qualifications.qualification')
                            ->join('qualifications', 'qualifications.id', '=', 'resume.department')
                            ->where('type', 1)
                            ->where('resume.candidate_id', $GetCandidateId)
                            ->orderBy('id', 'DESC')
                            ->get();

        return view("UI.register.candidate_first_register", compact('title', 'CandidateProfile', 'GetProfessionalDetails', 'Profile', 'GetIndustries', 'GetJobTitle', 'GetQualification', 'GetExperience', 'GetEducation'));
    }


    // Profile First registration
    public function TraineeFirstRegister(){
        if(Auth::guard('trainee')->check()){
            
        $title = "Trainee Register";
        $GetEmail = Session::get('TraineeEmail');
        $GetCandidateId = Session::get('TraineeId');
        
        $TraineeProfile = CandidateProfileModel::where("candidate_id", $GetCandidateId)->first();

        $Profile = CandidateModel::where("id", $GetCandidateId)->first();

        $GetProfessionalDetails = DB::table('candidate')
                                ->select('resume.name', 'job_title.name AS JobTitle')
                                ->join('resume', 'candidate.id', '=', 'resume.candidate_id')
                                ->join('job_title', 'job_title.id', '=', 'resume.department')
                                ->where('resume.worked_till', '=', 1)
                                ->where('resume.candidate_id', $GetCandidateId)
                                ->first();

        $GetIndustries = IndustriesModel::get();
        $GetJobTitle = JobTitleModel::get();
        $GetQualification = QualificationModel::get();

        $GetExperience =    DB::table('resume')
                            ->select('resume.*', 'job_title.name AS JobTitle')
                            ->where('candidate_id', $GetCandidateId)
                            ->where('type', 2)
                            ->join('job_title', 'job_title.id', '=', 'resume.department')
                            ->orderBy('id', 'DESC')
                            ->get();

        $GetEducation = DB::table('resume')
                            ->select('resume.*', 'qualifications.qualification')
                            ->join('qualifications', 'qualifications.id', '=', 'resume.department')
                            ->where('type', 1)
                            ->where('resume.candidate_id', $GetCandidateId)
                            ->orderBy('id', 'DESC')
                            ->get();

        return view("UI.register.internship_first_register", compact('title', 'TraineeProfile', 'GetProfessionalDetails', 'Profile', 'GetIndustries', 'GetJobTitle', 'GetQualification', 'GetExperience', 'GetEducation'));
        }
    }


    // Profile First registration
    public function ArticleshipFirstRegister(){
        if(Auth::guard('ca_article')->check()){
            
        $title = "Articleship Register";
        $GetEmail = Session::get('CaArticleEmail');
        $GetCandidateId = Session::get('CaArticleId');
        
        $ArticleProfile = CandidateProfileModel::where("candidate_id", $GetCandidateId)->first();

        $Profile = CandidateModel::where("id", $GetCandidateId)->first();

        $GetProfessionalDetails = DB::table('candidate')
                                ->select('resume.name', 'job_title.name AS JobTitle')
                                ->join('resume', 'candidate.id', '=', 'resume.candidate_id')
                                ->join('job_title', 'job_title.id', '=', 'resume.department')
                                ->where('resume.worked_till', '=', 1)
                                ->where('resume.candidate_id', $GetCandidateId)
                                ->first();

        $GetIndustries = IndustriesModel::get();
        $GetJobTitle = JobTitleModel::get();
        $GetQualification = QualificationModel::get();

        $GetExperience =    DB::table('resume')
                            ->select('resume.*', 'job_title.name AS JobTitle')
                            ->where('candidate_id', $GetCandidateId)
                            ->where('type', 2)
                            ->join('job_title', 'job_title.id', '=', 'resume.department')
                            ->orderBy('id', 'DESC')
                            ->get();

        $GetEducation = DB::table('resume')
                            ->select('resume.*', 'qualifications.qualification')
                            ->join('qualifications', 'qualifications.id', '=', 'resume.department')
                            ->where('type', 1)
                            ->where('resume.candidate_id', $GetCandidateId)
                            ->orderBy('id', 'DESC')
                            ->get();

        return view("UI.register.ca_articleship_first_register", compact('title', 'ArticleProfile', 'GetProfessionalDetails', 'Profile', 'GetIndustries', 'GetJobTitle', 'GetQualification', 'GetExperience', 'GetEducation'));
        }
    }
}
