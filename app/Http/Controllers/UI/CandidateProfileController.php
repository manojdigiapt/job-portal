<?php

namespace App\Http\Controllers\UI;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\UI\CandidateProfileModel;
use App\Model\UI\CandidateModel;
use App\Model\UI\CandidateResume;
use App\Model\UI\FavouriteModel;
use App\Model\UI\EmployerModel;
use App\Model\UI\JobsApplied;

use App\Model\Admin\WebinarsModel;
use App\Model\Admin\WebinarsAppliedModel;
use Illuminate\Support\Facades\Hash;
use Session;
use DB;
use Auth;

class CandidateProfileController extends Controller
{
    public function __construct(){
        $this->middleware('guest:candidate')->except('AddCandidateProfile', 'InsertEducation', 'GetEducationById', 'UpdateEducation', 'DeleteEducation', 'InsertExperience', 'GetExperienceById', 'UpdateExperience', 'DeleteExperience', 'InsertSkills', 'InsertOrUpdateCV', 'AddToFavourite', 'AddToTrainingFavourite', 'ChangeJobChange', 'UpdateProfilePic', 'GetPersonalDetailsById', 'ResumeHeadline', 'UpdatePersonalDetails', 'WebinarsDetails', 'GetAppliedWebinars', 'ApplyWebinar');
    }

    public function CandidateEducation(){
        $title = "Candidate Education";
        $GetCandidateId = Session::get('CandidateId');

        $GetEducation = CandidateResume::where('candidate_id', $GetCandidateId)
                                        ->where('type', 1)->get();

        return view("UI.layouts.candidate_education", compact('GetEducation', 'title'));
    }

    public function UpdateProfilePic(Request $request){
        $GetCandidateId = Session::get('CandidateId');

        $UpdateCandidateProfile = CandidateProfileModel::where('candidate_id', $GetCandidateId)->first();   
        
        if($UpdateCandidateProfile->profile_pic == null){
            $UpdateCandidateProfile->profile_completion = $UpdateCandidateProfile->profile_completion+5;

            // echo "test";
        }
        // exit;

        $extension = $request->file('file')->getClientOriginalExtension();
        $dir = 'candidate_profile/';
        $filename = uniqid() . '_' . time() . '.' . $extension;
        $request->file('file')->move($dir, $filename);

        $UpdateCandidateProfile->profile_pic = $filename;

        

        $UpdatedCandidateProfile = $UpdateCandidateProfile->save();
        if($UpdatedCandidateProfile){
            return response()->json(array(
                            "error"=>FALSE,
                            "message"=>"Profile updated successfully"
                        ));
        }else{
            return response()->json(array(
                            "error"=>TRUE,
                            "message"=>"Failed"
                        ));
        }
    }


    public function AddCandidateProfile(Request $request){
        $GetCandidateId = Session::get('CandidateId');
        $CandidateProfile = new CandidateProfileModel();

        $UpdateCandidateProfile = CandidateProfileModel::where('candidate_id', $GetCandidateId)->first();
        
        if($UpdateCandidateProfile != null ){
            // echo json_encode($UpdateCandidateProfile->experience);
            // exit;
            if($UpdateCandidateProfile->experience ==null){
                $UpdateCandidateProfile->profile_completion = $UpdateCandidateProfile->profile_completion+5;
            }

            if($UpdateCandidateProfile->gross_salary ==null){
                $UpdateCandidateProfile->profile_completion = $UpdateCandidateProfile->profile_completion+5;
            }

            // if($UpdateCandidateProfile->desired_location ==null){
            //     $UpdateCandidateProfile->profile_completion = $UpdateCandidateProfile->profile_completion+5;
            // }
            $UpdateCandidateProfile->experience = $request->experience;
            $UpdateCandidateProfile->gross_salary = $request->current_salary;
            $UpdateCandidateProfile->desired_location = $request->desired_location;
            
            
            $UpdatedCandidateProfile = $UpdateCandidateProfile->save();
            if($UpdatedCandidateProfile){
                return response()->json(array(
                                "error"=>FALSE,
                                "message"=>"Profile updated successfully"
                            ));
            }else{
                return response()->json(array(
                                "error"=>TRUE,
                                "message"=>"Failed"
                            ));
            }
        }else{
            $CandidateProfile->candidate_id = $GetCandidateId;
            $CandidateProfile->experience = $request->experience;
            $CandidateProfile->gross_salary = $request->current_salary;
            $CandidateProfile->desired_location = $request->desired_location;
    
            $UpdateCandidateProfile = $CandidateProfile->save();
            if($UpdateCandidateProfile){
                return response()->json(array(
                                "error"=>FALSE,
                                "message"=>"Profile updated successfully"
                            ));
            }else{
                return response()->json(array(
                                "error"=>TRUE,
                                "message"=>"Failed"
                            ));
            }
        }
        
    }

    // Candidate Resume Page
    public function CandidateResume(){
        $title = "Candidate Resume";
        $GetCandidateId = Session::get('CandidateId');

        $GetEducation = CandidateResume::where('candidate_id', $GetCandidateId)
                                        ->where('type', 1)->get();
        $GetExperience = CandidateResume::where('candidate_id', $GetCandidateId)
        ->where('type', 2)->get();

        $GetSkills = CandidateResume::where('candidate_id', $GetCandidateId)
        ->where('type', 3)->get();

        $GetSkillsByinput = CandidateResume::where('candidate_id', $GetCandidateId)
        ->where('type', 3)->first();
        

        $CandidateProfile = CandidateProfileModel::where("candidate_id", $GetCandidateId)->first();

        $GetFavouriteJobs = DB::select("SELECT employer.profile_pic, jobs.title, employer.slug AS CompanySlug, jobs.slug, employer.company_name FROM favourite_jobs, jobs, employer WHERE favourite_jobs.candidate_id = $GetCandidateId AND favourite_jobs.jobs_id = jobs.id AND jobs.employer_id = employer.id AND favourite_jobs.status = 1 AND favourite_jobs.type = 1");

        $GetSkillNames = "";

        foreach($GetSkills as $key => $value){
            $GetSkillNames = explode(',', $value->skill_name);
        }
        
        // echo json_encode($GetSkills);
        // exit;
        return view("UI.layouts.candidate_myresume", compact('title', 'GetEducation', 'GetExperience', 'GetSkills', 'CandidateProfile', 'GetFavouriteJobs', 'GetSkillNames', 'GetSkillsByinput'));
    }



    public function InsertEducation(Request $request){
        $GetCandidateId = Session::get('CandidateId');
        $CandidateResumeById = CandidateResume::where('candidate_id', $GetCandidateId)
                                ->where('type', 1)->first();

        $UpdateCandidateProfile = CandidateProfileModel::where('candidate_id', $GetCandidateId)->first();

        if($CandidateResumeById == null){
            $UpdateCandidateProfile->profile_completion = $UpdateCandidateProfile->profile_completion+15;

            $UpdateCandidateProfile->save();
        }

        
        $CandidateResume = new CandidateResume();
        $CandidateResume->candidate_id = Session::get('CandidateId');
        $CandidateResume->department = $request->course_name;
        $CandidateResume->department = $request->board;
        $CandidateResume->name = $request->school_clg;
        $CandidateResume->from_year = $request->passed_out_year;
        $CandidateResume->percentage = $request->percentage;
        $CandidateResume->type = "1";

        $InsertEducation = $CandidateResume->save();
        if($InsertEducation){
            return response()->json(array(
                "error"=>FALSE,
                "message"=>"Education updated successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Failed"
            ));
        }
    }

    public function GetEducationById($id){
        $GetEducationById = CandidateResume::where('id', $id)->first();
        echo json_encode($GetEducationById);
    }

    public function UpdateEducation(Request $request){
        $CandidateId = $request->id;
        $CandidateResume = CandidateResume::find($CandidateId);
        // echo $request->course_name;
        // exit;
        // $CandidateResume->candidate_id = Session::get('CandidateId');
        $CandidateResume->department = $request->course_name;
        // $CandidateResume->department = $request->board;
        $CandidateResume->name = $request->school_clg;
        $CandidateResume->from_year = $request->passed_out_year;
        $CandidateResume->percentage = $request->percentage;
        $CandidateResume->type = "1";

        $InsertEducation = $CandidateResume->save();
        if($InsertEducation){
            return response()->json(array(
                "error"=>FALSE,
                "message"=>"Education updated successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Failed"
            ));
        }
    }

    public function DeleteEducation($Id){
        $DeleteEducation = CandidateResume::where('id', $Id)->delete();

        $GetCandidateId = Session::get('CandidateId');

        $CheckExperience = CandidateResume::where('candidate_id', $GetCandidateId)
                                            ->where('type', 1)
                                            ->first();

        $UpdateCandidateProfile = CandidateProfileModel::where('candidate_id', $GetCandidateId)->first();

        
        if($CheckExperience == null){
            $UpdateCandidateProfile->profile_completion = $UpdateCandidateProfile->profile_completion-15;

            $UpdateCandidateProfile->save();
        }

        
        if($DeleteEducation){
            return response()->json(array(
                "error"=>FALSE,
                "message"=>"Education deleted successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Failed"
            ));
        }
    }

    // Experience

    public function InsertExperience(Request $request){
        $GetCandidateId = Session::get('CandidateId');
        $CandidateResumeById = CandidateResume::where('candidate_id', $GetCandidateId)
                                ->where('type', 2)->first();

        $UpdateCandidateProfile = CandidateProfileModel::where('candidate_id', $GetCandidateId)->first();

        

        $CandidateResume = new CandidateResume();
        $CandidateResume->candidate_id = Session::get('CandidateId');
        $CandidateResume->name = $request->company_name;
        $CandidateResume->department = $request->designation;
        $CandidateResume->from_year = $request->exp_from_year;
        $CandidateResume->from_month = $request->exp_from_month;

        if($request->worked_till == 1){
            $CandidateResume->to_year = date('Y');
            $CandidateResume->to_month = date('m');
        }else{
            $CandidateResume->to_year = $request->exp_to_year;
            $CandidateResume->to_month = $request->exp_to_month;   
        }

        $CandidateResume->worked_till = $request->worked_till;
        $CandidateResume->current_salary_lacs = $request->current_salary_month;
        // $CandidateResume->current_salary_thousand = $request->current_salary_thousand;
        $CandidateResume->description = $request->description;
        $CandidateResume->notice_period = $request->notice_period;
        $CandidateResume->type = "2";

        $InsertExperience = $CandidateResume->save();

        if($CandidateResumeById == null){
            $UpdateCandidateProfile->profile_completion = $UpdateCandidateProfile->profile_completion+20;

            $UpdateCandidateProfile->save();
        }
        
        if($InsertExperience){
            return response()->json(array(
                "error"=>FALSE,
                "message"=>"Experience updated successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Failed"
            ));
        }

    }

    public function GetExperienceById($id){
        $GetExperienceById = CandidateResume::where('id', $id)->first();
        echo json_encode($GetExperienceById);
    }

    public function UpdateExperience(Request $request){
        $CandidateId = $request->exp_id;
        $CandidateResume = CandidateResume::find($CandidateId);

        $CandidateResume->candidate_id = Session::get('CandidateId');
        $CandidateResume->name = $request->company_name;
        $CandidateResume->department = $request->designation;
        $CandidateResume->from_year = $request->exp_from_year;
        $CandidateResume->from_month = $request->exp_from_month;
        // echo $request->worked_till;
        // exit;
        if($request->worked_till == 1){
            $CandidateResume->to_year = date('Y');
            $CandidateResume->to_month = date('m');
        }else{
            $CandidateResume->to_year = $request->exp_to_year;
            $CandidateResume->to_month = $request->exp_to_month;   
        }

        $CandidateResume->worked_till = $request->worked_till;
        $CandidateResume->current_salary_lacs = $request->current_salary_month;
        // $CandidateResume->current_salary_thousand = $request->current_salary_thousand;
        $CandidateResume->description = $request->description;
        $CandidateResume->notice_period = $request->notice_period;
        $CandidateResume->type = "2";

        $InsertExperience = $CandidateResume->save();
        if($InsertExperience){
            return response()->json(array(
                "error"=>FALSE,
                "message"=>"Experience updated successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Failed"
            ));
        }
    }

    public function DeleteExperience($Id){
        $DeleteEducation = CandidateResume::where('id', $Id)->delete();

        $GetCandidateId = Session::get('CandidateId');

        $CheckExperience = CandidateResume::where('candidate_id', $GetCandidateId)
                                            ->where('type', 2)
                                            ->first();

        $UpdateCandidateProfile = CandidateProfileModel::where('candidate_id', $GetCandidateId)->first();

        if($CheckExperience == null){
            $UpdateCandidateProfile->profile_completion = $UpdateCandidateProfile->profile_completion-20;

            $UpdateCandidateProfile->save();
        }
        
        if($DeleteEducation){
            return response()->json(array(
                "error"=>FALSE,
                "message"=>"Experience deleted successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Failed"
            ));
        }
    }


    // Skills

    public function InsertSkills(Request $request){
        $CandidateResume = new CandidateResume();
        $GetCandidateId = Session::get('CandidateId');
        $GetSkillId = $request->SkillId;
        $UpdateSkills = CandidateResume::where('id', $GetSkillId)
                        ->where('type', 3)->first();
        // echo $request->skill_name;
        // exit;
        if($UpdateSkills != null){
            $UpdateSkills->skill_name = $request->skill_name;
            $UpdatedSkill = $UpdateSkills->save();

            if($UpdatedSkill){
                return response()->json(array(
                    "error"=>FALSE,
                    "message"=>"Skills updated successfully"
                ));
            }else{
                return response()->json(array(
                    "error"=>TRUE,
                    "message"=>"Failed"
                ));
            }
        }else{
            $CandidateResume->candidate_id = $GetCandidateId;
            $CandidateResume->skill_name = $request->skill_name;
            // $CandidateResume->percentage = $request->percentage;
            $CandidateResume->type = "3";
    
            $InsertSkill = $CandidateResume->save();
            if($InsertSkill){
                return response()->json(array(
                    "error"=>FALSE,
                    "message"=>"Skills updated successfully"
                ));
            }else{
                return response()->json(array(
                    "error"=>TRUE,
                    "message"=>"Failed"
                ));
            }
        }
        

    }

    // Upload CV
    public function UploadCv(){
        $title = "Upload CV";
        $GetCandidateId = Session::get('CandidateId');

        $GetCV = CandidateResume::where('candidate_id', $GetCandidateId)
        ->where('type', 4)->first();

        $CandidateProfile = CandidateProfileModel::where("candidate_id", $GetCandidateId)->first();

        $GetFavouriteJobs = DB::select("SELECT employer.profile_pic, jobs.title, employer.slug AS CompanySlug, jobs.slug, employer.company_name FROM favourite_jobs, jobs, employer WHERE favourite_jobs.candidate_id = $GetCandidateId AND favourite_jobs.jobs_id = jobs.id AND jobs.employer_id = employer.id AND favourite_jobs.status = 1 AND favourite_jobs.type = 1");
        // echo json_encode($GetCV);
        // exit;
        return view("UI.layouts.candidate_cv", compact('title', 'GetCV', 'CandidateProfile', 'GetFavouriteJobs'));
    }

    public function InsertOrUpdateCV(Request $request){
        $GetCandidateId = Session::get('CandidateId');
        $CandidateCV = new CandidateResume();

        $GetCVId = $request->cv_id;
        $UpdateCandidateCV = CandidateResume::where('id', $GetCVId)->first();

        $UpdateCandidateProfile = CandidateProfileModel::where('candidate_id', $GetCandidateId)->first();

        if($UpdateCandidateCV["cv"] == null){
            $UpdateCandidateProfile->profile_completion = $UpdateCandidateProfile->profile_completion+15;

            $UpdateCandidateProfile->save();
        }

        if($UpdateCandidateCV != null){
            $File = $request->cv;
            // $UpdateCandidateProfile = CandidateProfileModel::findOrFail($GetCandidateId);
            // echo json_encode($File);
            // exit;
            if($File == null){
                $filename = $UpdateCandidateCV->cv;
            }else{
                $extension = $request->file('cv')->getClientOriginalExtension();
                $dir = 'candidate_cv/';
                $filename = uniqid() . '_' . time() . '.' . $extension;
                $request->file('cv')->move($dir, $filename);

               
            }
            // echo json_encode($filename);
            // exit;

            $UpdateCandidateCV->cv = $filename;
            
            $UpdatedCandidateCV = $UpdateCandidateCV->save();
            if($UpdatedCandidateCV){
                return response()->json(array(
                    "error"=>FALSE,
                    "message"=>"CV updated successfully"
                ));
            }else{
                return response()->json(array(
                    "error"=>TRUE,
                    "message"=>"Failed."
                ));
            }
        }else{
           
            $extension = $request->file('cv')->getClientOriginalExtension();
            $dir = 'candidate_cv/';
            $filename = uniqid() . '_' . time() . '.' . $extension;
            $request->file('cv')->move($dir, $filename);

            $CandidateCV->candidate_id = $GetCandidateId;
            $CandidateCV->cv = $filename;
            $CandidateCV->type = "4";
    
            $AddCandidateCV = $CandidateCV->save();
            if($AddCandidateCV){
                return response()->json(array(
                    "error"=>FALSE,
                    "message"=>"CV updated successfully"
                ));
            }else{
                return response()->json(array(
                    "error"=>TRUE,
                    "message"=>"Failed."
                ));
            }
        }
    }


    public function InsertOrUpdateCVTitle(Request $request){
        $GetCandidateId = Session::get('CandidateId');
        $CandidateCV = new CandidateResume();

        $GetCVId = $request->cv_id;
        $UpdateCandidateCV = CandidateResume::where('id', $GetCVId)->first();

        $UpdateCandidateProfile = CandidateProfileModel::where('candidate_id', $GetCandidateId)->first();

        if($UpdateCandidateCV["title"] == null){
            $UpdateCandidateProfile->profile_completion = $UpdateCandidateProfile->profile_completion+5;

            $UpdateCandidateProfile->save();
        }

        if($UpdateCandidateCV != null){

            $UpdateCandidateCV->title = $request->resume_title;
    
            $UpdatedCandidateCV = $UpdateCandidateCV->save();
            if($UpdatedCandidateCV){
                return response()->json(array(
                    "error"=>FALSE,
                    "message"=>"CV title updated successfully"
                ));
            }else{
                return response()->json(array(
                    "error"=>TRUE,
                    "message"=>"Failed."
                ));
            }
        }else{
            $UpdateCandidateProfile = CandidateProfileModel::where('candidate_id', $GetCandidateId)->first();
            
            $UpdateCandidateProfile->profile_completion = $UpdateCandidateProfile->profile_completion+5;

            $UpdateCandidateProfile->save();

            $CandidateCV->candidate_id = $GetCandidateId;
            $CandidateCV->title = $request->resume_title;
    
            $AddCandidateCV = $CandidateCV->save();
            if($AddCandidateCV){
                return response()->json(array(
                    "error"=>FALSE,
                    "message"=>"CV title updated successfully"
                ));
            }else{
                return response()->json(array(
                    "error"=>TRUE,
                    "message"=>"Failed."
                ));
            }
        }
    }

    public function ResumeHeadline($id){
        $GetResumeHeadline = CandidateResume::findorFail($id);
        echo json_encode($GetResumeHeadline);
    }


    public function UpdateCareerProfile(Request $request){
        $GetCandidateId = Session::get('CandidateId');

        $CandidateProfile = CandidateProfileModel::where("candidate_id", $GetCandidateId)->first();
        
        // if($CandidateProfile->industry == null){
        //     $CandidateProfile->profile_completion = $CandidateProfile->profile_completion+5;
        // }
        // if($CandidateProfile->functional_area == null){
        //     $CandidateProfile->profile_completion = $CandidateProfile->profile_completion+5;
        // }
        // if($CandidateProfile->role == null){
        //     $CandidateProfile->profile_completion = $CandidateProfile->profile_completion+5;
        // }
        if($CandidateProfile->skills == null){
            $CandidateProfile->profile_completion = $CandidateProfile->profile_completion+15;
        }

        // $CandidateProfile->industry = $request->industry;
        // $CandidateProfile->functional_area = $request->functional_area;
        // $CandidateProfile->role = $request->role;
        $CandidateProfile->skills = $request->skills;

        $UpdateCareerProfile = $CandidateProfile->save();
        if($UpdateCareerProfile){
            return response()->json(array(
                "error"=>FALSE,
                "message"=>"Career profile updated successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Failed."
            ));
        }

    }

    public function GetPersonalDetailsById($id){
        $GetPersonalDetailsById = CandidateProfileModel::where('id', $id)->first();
        echo json_encode($GetPersonalDetailsById);
    }

    public function UpdatePersonalDetails(Request $request){
        $CandidateProfile = new CandidateProfileModel();
        $GetCandidateId = Session::get('CandidateId');

        $UpdateCandidateProfile = CandidateProfileModel::where("candidate_id", $GetCandidateId)->first();

        if($UpdateCandidateProfile->date_of_birth == null AND $UpdateCandidateProfile->gender == null AND $UpdateCandidateProfile->marital_status == null AND $UpdateCandidateProfile->father_name == null AND $UpdateCandidateProfile->address == null){
            $UpdateCandidateProfile->profile_completion = $UpdateCandidateProfile->profile_completion+15;
        }


        if($UpdateCandidateProfile != null){
            $UpdateCandidateProfile->candidate_id = $GetCandidateId;
            $UpdateCandidateProfile->date_of_birth = date('Y-m-d', strtotime($request->dob));
            $UpdateCandidateProfile->gender = $request->gender;
            $UpdateCandidateProfile->marital_status = $request->marital_status;
            $UpdateCandidateProfile->father_name = $request->father_name;
            $UpdateCandidateProfile->address = $request->address;
            $UpdateCandidateProfile->state = $request->state;
            $UpdateCandidateProfile->city = $request->city;
            $UpdateCandidateProfile->zip_code = $request->zipcode;
            
            
            $UpdatedCareerProfile = $UpdateCandidateProfile->save();
            if($UpdatedCareerProfile){
                return response()->json(array(
                    "error"=>FALSE,
                    "message"=>"Personal details updated successfully"
                ));
            }else{
                return response()->json(array(
                    "error"=>TRUE,
                    "message"=>"Failed."
                ));
            }
        }else{
            $CandidateProfile->candidate_id = $GetCandidateId;
            $CandidateProfile->date_of_birth = date('Y-m-d', strtotime($request->dob));
            $CandidateProfile->gender = $request->gender;
            $CandidateProfile->marital_status = $request->marital_status;
            $CandidateProfile->father_name = $request->father_name;
            $CandidateProfile->address = $request->address;
            $InsertCandidateProfile = $CandidateProfile->save();
            if($InsertCandidateProfile){
                return response()->json(array(
                    "error"=>FALSE,
                    "message"=>"Personal details updated successfully"
                ));
            }else{
                return response()->json(array(
                    "error"=>TRUE,
                    "message"=>"Failed."
                ));
            }
        }
        

    }


    public function GetAppliedJobs(){
        $title = "Applied Jobs";
        $GetCandidateId = Session::get('CandidateId');

        $GetEmail = Session::get('CandidateEmail');

        $CandidateProfile = CandidateProfileModel::where("candidate_id", $GetCandidateId)->first();
        $Profile = CandidateModel::where("email", $GetEmail)->first();

        $GetAppliedJobs = DB::select("SELECT job_title.name AS title, jobs.experience, jobs.job_type, employer.company_name, jobs.location, jobs.city, jobs_applied.created_at, jobs_applied.recruiter_status, employer.slug AS CompanySlug, jobs.slug, jobs_applied.download_cv_status, jobs_applied.application_view_date , jobs_applied.download_cv_date FROM jobs_applied, jobs, employer, job_title WHERE jobs_applied.jobs_id = jobs.id AND jobs.employer_id = employer.id AND jobs_applied.candidate_id = $GetCandidateId AND job_title.id = jobs.title ORDER BY jobs_applied.created_at DESC");

        $GetRecruiterViewed = JobsApplied::where('candidate_id', $GetCandidateId)
                                        ->where('recruiter_status', 1)
                                        ->count();

        $GetApplicationDownload = JobsApplied::where('candidate_id', $GetCandidateId)
                                        ->where('download_cv_status', 1)
                                        ->count();

        $GetFavouriteJobs = DB::select("SELECT employer.profile_pic, jobs.title, employer.slug AS CompanySlug, jobs.slug, employer.company_name FROM favourite_jobs, jobs, employer WHERE favourite_jobs.candidate_id = $GetCandidateId AND favourite_jobs.jobs_id = jobs.id AND jobs.employer_id = employer.id AND favourite_jobs.status = 1 AND favourite_jobs.type = 1");

        $GetProfessionalDetails = DB::table('candidate')
                                ->select('resume.name', 'job_title.name AS JobTitle')
                                ->join('resume', 'candidate.id', '=', 'resume.candidate_id')
                                ->join('job_title', 'job_title.id', '=', 'resume.department')
                                ->where('resume.worked_till', '=', 1)
                                ->first();
        // echo json_encode($GetAppliedJobs);p
        // exit;
        $GetFavouritesCount = FavouriteModel::where('candidate_id', $GetCandidateId)
                                ->count();

        return view("UI.layouts.candidates_applied_jobs", compact('title', 'GetAppliedJobs', 'CandidateProfile', 'GetFavouriteJobs', 'GetRecruiterViewed', 'GetApplicationDownload', 'GetProfessionalDetails', 'Profile', 'GetFavouritesCount'));
    }

    public function GetShortlistedJobs(){
        $title = "Shortlisted Jobs";
        $GetCandidateId = Session::get('CandidateId');
        $CandidateProfile = CandidateProfileModel::where("candidate_id", $GetCandidateId)->first();

        $GetEmail = Session::get('CandidateEmail');

        $Profile = CandidateModel::where("email", $GetEmail)->first();

        $GetShortlistedCandidates = DB::select("SELECT employer.profile_pic, employer.company_name, employer.city, candidate_profile.role, shortlist_candidates.created_at FROM shortlist_candidates, employer, candidate_profile WHERE shortlist_candidates.employer_id = employer.id AND  shortlist_candidates.candidate_id = $GetCandidateId AND candidate_profile.candidate_id = $GetCandidateId ORDER BY shortlist_candidates.created_at DESC");

        $GetProfessionalDetails = DB::table('candidate')
                                ->select('resume.name', 'job_title.name AS JobTitle')
                                ->join('resume', 'candidate.id', '=', 'resume.candidate_id')
                                ->join('job_title', 'job_title.id', '=', 'resume.department')
                                ->where('resume.worked_till', '=', 1)
                                ->first();

        $GetFavouriteJobs = DB::select("SELECT employer.profile_pic, jobs.title, employer.slug AS CompanySlug, jobs.slug, employer.company_name FROM favourite_jobs, jobs, employer WHERE favourite_jobs.candidate_id = $GetCandidateId AND favourite_jobs.jobs_id = jobs.id AND jobs.employer_id = employer.id AND favourite_jobs.status = 1 AND favourite_jobs.type = 1");
        // echo json_encode($GetShortlistedCandidates);
        // exit;
        $GetFavouritesCount = FavouriteModel::where('candidate_id', $GetCandidateId)
                                ->count();

        return view("UI.layouts.candidates_shortlisted", compact('title', 'GetShortlistedCandidates', 'CandidateProfile', 'GetFavouriteJobs', 'GetProfessionalDetails', 'Profile', 'GetFavouritesCount'));
    }


    public function AddToFavourite(Request $request){
        $GetCandidateId = Session::get('CandidateId');
        $GetJobsId = $request->JobsId;

        $CheckFavourites = FavouriteModel::where('candidate_id', $GetCandidateId)
                                        ->where('jobs_id', $GetJobsId)
                                        ->where('type', 1)
                                        ->first();
        if($CheckFavourites != null){
            if($CheckFavourites["status"] == 0){
                $CheckFavourites->status = 1;
                $CheckFavourites->save();
            }else{
                $CheckFavourites->status = 0;
                $CheckFavourites->save();
            }
        }else{
            $AddFavourite = new FavouriteModel();

            $AddFavourite->candidate_id = $GetCandidateId;
            $AddFavourite->jobs_id = $GetJobsId;
            $AddFavourite->type = 1;
            $AddFavourite->status = 1;
            $AddFavourite->save();
            
        }
    
    }

    public function AddToTrainingFavourite(Request $request){
        $GetTraineeId = Session::get('TraineeId');
        $GetTrainingId = $request->TrainingId;

        
        $CheckFavourites = FavouriteModel::where('candidate_id', $GetTraineeId)
                                        ->where('training_id', $GetTrainingId)
                                        ->where('type', 2)
                                        ->first();

        if($CheckFavourites != null){
            if($CheckFavourites["status"] == 0){
                $CheckFavourites->status = 1;
                $CheckFavourites->save();
            }else{
                $CheckFavourites->status = 0;
                $CheckFavourites->save();
            }
        }else{
            $AddFavourite = new FavouriteModel();
            $AddFavourite->candidate_id = $GetTraineeId;
            $AddFavourite->training_id = $GetTrainingId;
            $AddFavourite->type = 2;
            $AddFavourite->status = 1;
            $AddFavourite->save();
            
        }
    
    }

    public function ChangeJobChange(){
        $GetCandidateId = Session::get('CandidateId');
        // $GetStatus = $request->status;

        $ChangeJobChangeStatus = CandidateProfileModel::where("candidate_id", $GetCandidateId)
                                ->first();
        // echo json_encode($ChangeJobChangeStatus);
        // exit;
        if($ChangeJobChangeStatus->allow_search == 0){
            $ChangeJobChangeStatus->allow_search = 1;
            $ChangeJobChangeStatus->save();
            return response()->json(array(
                    "error"=>FALSE,
                    "message"=>"Enabled"
            ));
        }else{
            $ChangeJobChangeStatus->allow_search = 0;
            $ChangeJobChangeStatus->save();
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Disabled"
            ));
        }

    }


    public function LoadMoreJobs(Request $request){
        $GetJobsId = $request->JobsId;
        
        $GetResults = "";
        
        $GetCandidateId = Session::get("CandidateId");

        
        $GetJobs = DB::select("SELECT employer.profile_pic, jobs.id, jobs.title, jobs.slug, employer.company_name, jobs.location, jobs.job_type, employer.slug AS CompanySlug FROM jobs, employer WHERE jobs.employer_id = employer.id AND jobs.id > $GetJobsId ORDER BY jobs.created_at ASC LIMIT 1");

        $GetJobType = "";
        if ($GetJobs != null){
            foreach($GetJobs as $Jobs){
                if($Jobs->job_type == 1){
                    $GetJobType = "<span class='job-is ft'>Full time</span>";
                }else{
                    $GetJobType = "<span class='job-is pt'>Part time</span>";
                }
                $GetResults .= "<div class='job-listing wtabs'><div class='job-title-sec'><div class='c-logo'> <img src='/employer_profile/".$Jobs->profile_pic."' alt='' /> </div><h3><a href='/JobDetails/".$Jobs->CompanySlug."/".$Jobs->slug."' target='_blank' title=''>".$Jobs->title."</a></h3><span>".$Jobs->company_name."</span><div class='job-lctn'><i class='la la-map-marker'></i>".$Jobs->location."</div></div><div class='job-style-bx'>".$GetJobType."<i>5 months ago</i></div></div>";
            }
            
            $GetResults .= "<div class='col-lg-12' id='remove-row'><div class='browse-all-cat'><div class='text-center' id='ApplyBtnLoader' style='display: none;'><img class='applyJobsLoader' src='{{URL::asset('UI/apply_btn_loader.gif')}}' alt=''></div><a href='javascript:void(0);' data-id='".$Jobs->id ."' id='LoadmoreJobs'title='' class='style2'>Load more jobs</a></div></div>";
    
            echo $GetResults;
        }
    }


    public function EmployerDetails($Slug){
        $title = "Employer Details";

        $GetEmployerDetails = EmployerModel::where('slug', $Slug)->first();

        $GetJobs = DB::select("SELECT jobs.title, employer.company_name, jobs.slug AS JobsSlug, employer.slug AS CompanySlug, jobs.state, jobs.city, jobs.job_type FROM jobs, employer WHERE jobs.employer_id = employer.id AND employer.slug = '$Slug'");
        return view("UI.layouts.employer_details", compact('title', 'GetEmployerDetails', 'GetJobs'));
    }



    public function UpdatePassword(Request $request){
        $GetCandidateId = Session::get('CandidateId');
        $GetOldPassword = CandidateModel::where('id', $GetCandidateId)->first();

        $CheckPassword = Hash::check($request->OldPassword, $GetOldPassword->password);
        // echo json_encode($CheckPassword);
        // exit;
        $GetNewPassword = $request->NewPassword;
        $GetVerifyPassword = $request->VerifyPassword;


        if($GetNewPassword != $GetVerifyPassword){
            return response()->json(array(
                "error"=>TRUE,
                "message" => "Password mismatch"
            ));
        }else if($CheckPassword == false){
            return response()->json(array(
                "error"=>TRUE,
                "message" => "Old password is wrong"
            ));
        }else if($CheckPassword == true){
            $GetOldPassword->password = Hash::make($request->VerifyPassword);
            $GetOldPassword->save();
            return response()->json(array(
                "error"=>FALSE,
                "message" => "Password updated successfully"
            ));
        }
    }


    public function Webinars(){
        $title = "Webinars";
        $GetCandidateId = Session::get('CandidateId');
        
        $CandidateProfile = CandidateProfileModel::where("candidate_id", $GetCandidateId)->first();
        $Profile = CandidateModel::where("id", $GetCandidateId)->first();

        $GetFavouritesCount = FavouriteModel::where('candidate_id', $GetCandidateId)
                                ->count();

        $GetProfessionalDetails = DB::table('candidate')
                                ->select('resume.name', 'job_title.name AS JobTitle')
                                ->join('resume', 'candidate.id', '=', 'resume.candidate_id')
                                ->join('job_title', 'job_title.id', '=', 'resume.department')
                                ->where('resume.worked_till', '=', 1)
                                ->where('candidate.id', $GetCandidateId)
                                ->first();

        // $GetWebinars = WebinarsModel::where('category', 1)->get();
        $GetWebinars = DB::table('webinars')
                        ->select('webinars.*', 'admin.name')
                        ->join('admin', 'webinars.faculty_id', '=', 'admin.id')
                        ->where('category', 1)
                        ->get();

        return view('UI.candidates.webinars', compact('title', 'CandidateProfile', 'GetFavouritesCount', 'GetProfessionalDetails', 'Profile', 'GetWebinars'));
    }


    public function WebinarsDetails($Id){
        $title = "Webinars Details";
        // $GetCandidateId = Session::get('CandidateId');
        if(Auth::guard('trainee')->check()){
            $GetCandidateId = Session::get('TraineeId');
        }elseif(Auth::guard('ca_article')->check()){
            $GetCandidateId = Session::get('CaArticleId');
        }elseif(Auth::guard('candidate')->check()){
            $GetCandidateId = Session::get('CandidateId');
        }
        
        
        $Profile = CandidateModel::where("id", $GetCandidateId)->first();

        $GetFavouritesCount = FavouriteModel::where('candidate_id', $GetCandidateId)
                                ->count();

        $GetProfessionalDetails = DB::table('candidate')
                                ->select('resume.name', 'job_title.name AS JobTitle')
                                ->join('resume', 'candidate.id', '=', 'resume.candidate_id')
                                ->join('job_title', 'job_title.id', '=', 'resume.department')
                                ->where('resume.worked_till', '=', 1)
                                ->where('candidate.id', $GetCandidateId)
                                ->first();

        

        if(Auth::guard('candidate')->check()){
            $CandidateProfile = CandidateProfileModel::where("candidate_id", $GetCandidateId)->first();

            $GetWebinars = DB::table('webinars')
                        ->select('webinars.*', 'admin.name')
                        ->join('admin', 'webinars.faculty_id', '=', 'admin.id')
                        ->where('webinars.category', 1)
                        ->where('webinars.id', $Id)
                        ->first();
        }elseif(Auth::guard('trainee')->check()){
            $TraineeProfile = CandidateProfileModel::where("candidate_id", $GetCandidateId)->first();

            $GetWebinars = DB::table('webinars')
                        ->select('webinars.*', 'admin.name')
                        ->join('admin', 'webinars.faculty_id', '=', 'admin.id')
                        ->where('webinars.category', 2)
                        ->where('webinars.id', $Id)
                        ->first();
        }elseif(Auth::guard('ca_article')->check()){
            $ArticleProfile = CandidateProfileModel::where("candidate_id", $GetCandidateId)->first();

            $GetWebinars = DB::table('webinars')
                        ->select('webinars.*', 'admin.name')
                        ->join('admin', 'webinars.faculty_id', '=', 'admin.id') 
                        ->where('webinars.category', 3)
                        ->where('webinars.id', $Id)
                        ->first();
        }

        // echo json_encode($TraineeProfile);
        // exit;

        if(Auth::guard('candidate')->check()){
            $GetSimilarWebinars = DB::table('webinars')
            ->join('admin', 'webinars.faculty_id', '=', 'admin.id')
            ->where('category', 1)
            // ->orderBy('webinars.created_at', 'desc')
            ->inRandomOrder()
            ->take(3)
            ->get();
        }elseif(Auth::guard('trainee')->check()){
            $GetSimilarWebinars = DB::table('webinars')
            ->join('admin', 'webinars.faculty_id', '=', 'admin.id')
            ->where('category', 2)
            // ->orderBy('webinars.created_at', 'desc')
            ->inRandomOrder()
            ->take(3)
            ->get();
        }elseif(Auth::guard('ca_article')->check()){
            $GetSimilarWebinars = DB::table('webinars')
            ->join('admin', 'webinars.faculty_id', '=', 'admin.id')
            ->where('category', 3)
            // ->orderBy('webinars.created_at', 'desc')
            ->inRandomOrder()
            ->take(3)
            ->get();
        }

        $CheckWebinarApplied = WebinarsAppliedModel::where('candidate_id', $GetCandidateId)
                                        ->where('webinar_id', $Id)
                                        ->first();

        // echo json_encode($GetWebinars);
        // exit;

        return view('UI.candidates.webinars_details', compact('title', 'CandidateProfile', 'GetFavouritesCount', 'GetProfessionalDetails', 'Profile', 'GetWebinars', 'GetSimilarWebinars', 'ArticleProfile', 'TraineeProfile', 'CheckWebinarApplied'));
    }


    public function ThankyouPage(){
        $title = "Webinars Details";
        $GetCandidateId = Session::get('CandidateId');
        
        
        $Profile = CandidateModel::where("id", $GetCandidateId)->first();

        $GetFavouritesCount = FavouriteModel::where('candidate_id', $GetCandidateId)
                                ->count();

        $GetProfessionalDetails = DB::table('candidate')
                                ->select('resume.name', 'job_title.name AS JobTitle')
                                ->join('resume', 'candidate.id', '=', 'resume.candidate_id')
                                ->join('job_title', 'job_title.id', '=', 'resume.department')
                                ->where('resume.worked_till', '=', 1)
                                ->where('candidate.id', $GetCandidateId)
                                ->first();

        

        if(Auth::guard('candidate')->check()){
            $CandidateProfile = CandidateProfileModel::where("candidate_id", $GetCandidateId)->first();
        }elseif(Auth::guard('trainee')->check()){
            $TraineeProfile = CandidateProfileModel::where("candidate_id", $GetCandidateId)->first();
        }elseif(Auth::guard('ca_article')->check()){
            $ArticleProfile = CandidateProfileModel::where("candidate_id", $GetCandidateId)->first();

        }



        // echo json_encode($CheckWebinarApplied);
        // exit;

        return view('UI.candidates.thankyou_page_order', compact('title', 'CandidateProfile', 'GetFavouritesCount', 'GetProfessionalDetails', 'Profile', 'GetWebinars', 'GetSimilarWebinars', 'ArticleProfile', 'TraineeProfile', 'CheckWebinarApplied'));
    }


    public function GetAppliedWebinars(){
        $title = "Applied Webinars";
        // $GetCandidateId = Session::get('CandidateId');

        // $GetCandidateId = "";

        $GetEmail = Session::get('CandidateEmail');

        if(Auth::guard('candidate')->check()){
            $GetCandidateId = Session::get("CandidateId");

            $CandidateProfile = CandidateProfileModel::where("candidate_id", $GetCandidateId)->first();
            

        }elseif(Auth::guard('trainee')->check()){
            $GetCandidateId = Session::get("TraineeId"); 

            $TraineeProfile = CandidateProfileModel::where("candidate_id", $GetCandidateId)->first();

        }elseif(Auth::guard('ca_article')->check()){
            $GetCandidateId = Session::get("CaArticleId");

            $ArticleProfile = CandidateProfileModel::where("candidate_id", $GetCandidateId)->first();
            
        }

        $Profile = CandidateModel::where("email", $GetEmail)->first();

        // $GetAppliedJobs = DB::select("SELECT job_title.name AS title, jobs.experience, jobs.job_type, employer.company_name, jobs.location, jobs.city, jobs_applied.created_at, jobs_applied.recruiter_status, employer.slug AS CompanySlug, jobs.slug, jobs_applied.download_cv_status, jobs_applied.application_view_date , jobs_applied.download_cv_date FROM jobs_applied, jobs, employer, job_title WHERE jobs_applied.jobs_id = jobs.id AND jobs.employer_id = employer.id AND jobs_applied.candidate_id = $GetCandidateId AND job_title.id = jobs.title ORDER BY jobs_applied.created_at DESC");

        $GetAppliedWebinars = DB::table('webinars_applied')
                            ->select('admin.name AS FacultyName', 'webinars.*')
                            ->join('admin', 'webinars_applied.faculty_id', '=', 'admin.id')
                            ->join('candidate', 'webinars_applied.candidate_id', '=', 'candidate.id')
                            ->join('webinars', 'webinars_applied.webinar_id', '=', 'webinars.id')
                            ->where('webinars_applied.candidate_id', $GetCandidateId)
                            ->get();


        $GetFavouriteJobs = DB::select("SELECT employer.profile_pic, jobs.title, employer.slug AS CompanySlug, jobs.slug, employer.company_name FROM favourite_jobs, jobs, employer WHERE favourite_jobs.candidate_id = $GetCandidateId AND favourite_jobs.jobs_id = jobs.id AND jobs.employer_id = employer.id AND favourite_jobs.status = 1 AND favourite_jobs.type = 1");

        $GetProfessionalDetails = DB::table('candidate')
                                ->select('resume.name', 'job_title.name AS JobTitle')
                                ->join('resume', 'candidate.id', '=', 'resume.candidate_id')
                                ->join('job_title', 'job_title.id', '=', 'resume.department')
                                ->where('resume.worked_till', '=', 1)
                                ->first();
        // echo json_encode($GetAppliedJobs);p
        // exit;
        $GetFavouritesCount = FavouriteModel::where('candidate_id', $GetCandidateId)
                                ->count();

        // $GetCurrentTime = date("h:i A");
        date_default_timezone_set('Asia/Kolkata');
        $GetCurrentDate = date('Y-m-d', time());
        $GetCurrentTime = date('h:i A', time());
        // echo $GetCurrentDate;
        // exit;

        // $CheckStartTime = DB::table('webinars')
        //                     ->where('webinars.webinar_date', '=', $GetCurrentDate)
        //                     ->where('webinars.start_time', '>=', $GetCurrentTime)
        //                     ->where('webinars.end_time', '<=', $GetCurrentTime)

        //                     ->get();

        $CheckStartTime = DB::select("SELECT * FROM webinars WHERE webinars.webinar_date = '$GetCurrentDate' AND start_time >= '$GetCurrentTime' AND end_time <= '$GetCurrentTime'");

        // echo json_encode($CheckStartTime);
        // exit;

        return view("UI.candidates.applied_webinars", compact('title', 'GetAppliedWebinars', 'CandidateProfile', 'GetFavouriteJobs', 'GetRecruiterViewed', 'GetApplicationDownload', 'GetProfessionalDetails', 'Profile', 'GetFavouritesCount', 'TraineeProfile', 'ArticleProfile', 'CheckStartTime'));
    }


    public function ApplyWebinar(Request $request){
        

        if(Auth::guard('candidate')->check()){
            $GetCandidateId = Session::get("CandidateId");
        }elseif(Auth::guard('trainee')->check()){
            $GetCandidateId = Session::get("TraineeId");   
        }elseif(Auth::guard('ca_article')->check()){
            $GetCandidateId = Session::get("CaArticleId");   
        }

        $GetWebinarId = $request->WebinarId;
        $GetFacultyId = $request->FacultyId;

        $CheckWebinarApplied = WebinarsAppliedModel::where('candidate_id', $GetCandidateId)
                                        ->where('webinar_id', $GetWebinarId)
                                        ->first();
        // echo json_encode($GetCandidateId);
        // exit;
        if($CheckWebinarApplied != null){
            return response()->json(array(
                "error" => TRUE,
                "message" => "You have already applied this webinar..."
            ));
        }else{
            $JobsApplied = new WebinarsAppliedModel();
            $JobsApplied->candidate_id = $GetCandidateId;
            $JobsApplied->webinar_id = $GetWebinarId;
            $JobsApplied->faculty_id = $GetFacultyId;
            
    
            $ApplyWebinar = $JobsApplied->save();
            if($ApplyWebinar){
                return response()->json(array(
                    "error" => FALSE,
                    "message" => "Webinar applied successfully"
                ));
            }else{
                return response()->json(array(
                    "error" => TRUE,
                    "message" => "Failed"
                ));
            }
        }
    }





    
}
