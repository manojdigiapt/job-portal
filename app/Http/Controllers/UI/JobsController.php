<?php

namespace App\Http\Controllers\UI;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

use App\Model\UI\EmployerModel;
use App\Model\UI\JobsModel;
use App\Model\UI\TotalViews;
use App\Model\UI\JobsApplied;
use App\Model\UI\FavouriteModel;
use App\Model\UI\CandidateProfileModel;
use App\Model\UI\CandidateModel;

use Session;
use DB;
use DateTime;

use App\Mail\JobAppliedCandidateToEmployer;


use App\Model\Admin\IndustriesModel;
use App\Model\Admin\TrainingCompletionModel;
use App\Model\Admin\JobTitleModel;
use App\Model\Admin\QualificationModel;
use App\Model\Admin\TrainingTitleModel;

class JobsController extends Controller
{   
    public function __construct(){
        if(Auth::guard('employer')->check()){
            $this->middleware('guest:employer')->except('PostJobs', 'UpdateJobs', 'SearchJobs', 'SearchJobsTitle', 'ApplyJobs', 'FilterJobs', 'FilterJobType');
        }elseif(Auth::guard('candidate')->check()){
            $this->middleware('guest:candidate')->except('SearchJobs', 'SearchJobsTitle', 'ApplyJobs', 'FilterJobs', 'FilterJobType');
        }
        
    }


    public function PostJobs(Request $request){
        $GetEmployerId = Session::get("EmployerId");

        if($request->method() == 'POST'){
            //do something if the value is not empty
            $Jobs = new JobsModel();

            $Jobs->employer_id = $GetEmployerId;
            $Jobs->jobs_id = rand(4, 7645);
            $Jobs->title = $request->title;
            $Jobs->slug = strtolower(str_replace(' ', '_', $request->title));
            $Jobs->zip_code = $request->zipcode;
            $Jobs->country = $request->country;
            $Jobs->state = $request->state;
            $Jobs->city = $request->city;
            $Jobs->location = $request->address;
            $Jobs->job_description = $request->description;
            $Jobs->job_type = $request->job_type;
            $Jobs->employee_type = $request->employee_type;
            $Jobs->salary_min = $request->salary_min;
            $Jobs->salary_max = $request->salary_max;
            $Jobs->salary_type = $request->salary_type;
            $Jobs->experience = $request->experience;
            $Jobs->qualification = $request->qualification;
            $Jobs->total_positions = $request->positions;

            $AddJobs = $Jobs->save();
            if($AddJobs){
                return response()->json(array(
                    "error" => FALSE,
                    "message" => "Jobs posted successfully"
                ));
            }else{
                return response()->json(array(
                    "error" => TRUE,
                    "message" => "Failed"
                ));
            }
        }else{
            return response()->json(array(
                "error" => TRUE,
                "message" => "Something went wrong. Please try again later."
            ));   
        }
    }

    public function GetJobsById($id){
        $GetJobsById = JobsModel::where('id', $id)->first();
        // $GetJobsById = DB::table('jobs')
        //             ->select('jobs.*', 'qualifications.qualification AS Qualify')
        //             ->join('qualifications', 'qualifications.id', '=', 'jobs.qualification')
        //             ->where('jobs.id', $id)
        //             ->first();

        echo json_encode($GetJobsById);
    }

    public function UpdateJobs(Request $request){
        $GetEmployerId = Session::get("EmployerId");

        $id = $request->id;

        $Jobs = JobsModel::where('id', $id)->first();
        // echo json_encode($request->description);
        // exit;
        $Jobs->title = $request->title;
        $Jobs->slug = strtolower(str_replace(' ', '_', $request->title));
        $Jobs->employee_type = $request->EmployeeType;
        $Jobs->location = $request->address;
        $Jobs->zip_code = $request->zipcode;
        $Jobs->country = $request->country;
        $Jobs->state = $request->state;
        $Jobs->city = $request->city;
        $Jobs->job_description = $request->description;
        $Jobs->job_type = $request->job_type;
        $Jobs->industry = $request->industry;
        $Jobs->salary_min = $request->salary_min;
        $Jobs->salary_max = $request->salary_max;
        $Jobs->salary_type = $request->salary_type;
        $Jobs->experience = $request->experience;
        $Jobs->qualification = $request->qualification;
        $Jobs->total_positions = $request->positions;

        $AddJobs = $Jobs->save();
        if($AddJobs){
            return response()->json(array(
                "error" => FALSE,
                "message" => "Jobs updated successfully"
            ));
        }else{
            return response()->json(array(
                "error" => TRUE,
                "message" => "Failed"
            ));
        }

    }

    
    public function UpdateCAJobs(Request $request){
        $GetEmployerId = Session::get("EmployerId");

        $id = $request->id;

        $Jobs = JobsModel::where('id', $id)->first();
        // echo json_encode($request->description);
        // exit;
        $Jobs->title = "CA Article";
        $Jobs->slug = strtolower(str_replace(' ', '_', $Jobs->title));
        $Jobs->stipend_type = $request->StipendType;
        $Jobs->location = $request->address;
        $Jobs->zip_code = $request->zipcode;
        $Jobs->country = $request->country;
        $Jobs->state = $request->state;
        $Jobs->city = $request->city;
        $Jobs->job_description = $request->description;
        $Jobs->salary_min = $request->salary_min;
        $Jobs->salary_max = $request->salary_max;
        $Jobs->salary_type = $request->salary_type;
        $Jobs->qualification = $request->qualification;
        $Jobs->total_positions = $request->positions;

        $AddJobs = $Jobs->save();
        if($AddJobs){
            return response()->json(array(
                "error" => FALSE,
                "message" => "Jobs updated successfully"
            ));
        }else{
            return response()->json(array(
                "error" => TRUE,
                "message" => "Failed"
            ));
        }

    }

    

    public function JobDetails($company_name, $JobId, $slug){
        $title = "Jobs Details";
        $GetCandidateId = Session::get('CandidateId');
        $GetEmail = Session::get('CandidateEmail');

        $GetEmployerId = Session::get("EmployerId");
        // $GetEmployerId = 1;
        $EmployerProfile = EmployerModel::where("id", $GetEmployerId)->first();

        $CandidateProfile = CandidateProfileModel::where("candidate_id", $GetCandidateId)->first();
    
        $CheckPercentage = DB::table('candidate_profile')
                            ->where('candidate_id', $GetCandidateId)
                            ->where('candidate_profile.profile_completion', '>=', 90)
                            ->first();
        // echo json_encode($CheckPercentage);
        // exit;

        $GetJobsId = JobsModel::where('id', $JobId)->first();
        // $GetJobsId = DB::table('jobs')
        //             ->select('jobs.*', 'job_title.*')
        //             ->join('job_title', 'job_title.id', '=', 'jobs.title')
        //             ->where('jobs.id', $JobId)
        //             ->first();

        // echo json_encode($GetJobsId);
        // exit;
        $GetIndustrySegment = request()->segment(4);

        

        $Profile = CandidateModel::where("email", $GetEmail)->first();

        $GetRoles = JobsModel::where('id', $GetJobsId->id)->get();

        $GetJobTitle = JobTitleModel::get();
        $GetQualificationAdmin = QualificationModel::get();
        $GetTrainingTitle = TrainingTitleModel::get();
        $GetIndustries = IndustriesModel::get();

        if(Auth::guard('candidate')->check()){
            $CheckTotalViews = DB::select("SELECT * FROM total_views WHERE total_views.candidate_id = '$GetCandidateId' AND total_views.jobs_id = $GetJobsId->id");
            if($CheckTotalViews != null){
                $GetJobsBySlug = DB::select("SELECT jobs.id, job_title.name AS title, employer.website, employer.mobile, employer.id AS EmployerId, employer.email, employer.profile_pic, jobs.total_positions, employer.company_name, jobs.city , jobs.salary_min, jobs.salary_max, jobs.job_type, jobs.created_at, jobs.job_description,  jobs.industry, jobs.experience, jobs.qualification, employer.slug AS CompanySlug FROM jobs, employer, job_title WHERE jobs.employer_id = employer.id AND job_title.id = jobs.title AND jobs.id = '$GetJobsId->id'");

                

                $GetTotalViewsCount = TotalViews::where('jobs_id', $GetJobsId->id)
                                            ->get();

                $CheckJobsApplied = JobsApplied::where('candidate_id', $GetCandidateId)
                                                ->where('jobs_id', $GetJobsId->id)
                                                ->first();

                $CheckApplicantsCount = JobsApplied::where('jobs_id', $GetJobsId->id)
                                                    ->get();

                $CheckFavourites = FavouriteModel::where('candidate_id', $GetCandidateId)
                ->where('jobs_id', $GetJobsId->id)
                ->where('type', 1)
                ->first();

                $GetSimilarJobs = DB::select("SELECT jobs.id, job_title.name AS title, employer.website, employer.mobile, employer.email, employer.profile_pic, jobs.total_positions, employer.company_name, jobs.city , jobs.salary_min, jobs.salary_max, jobs.job_type, jobs.created_at, jobs.job_description,  jobs.industry, jobs.experience,  jobs.qualification,jobs.slug, jobs.slug, employer.slug AS CompanySlug FROM jobs, employer, job_title WHERE jobs.industry = '$GetJobsId->industry' AND jobs.slug != '$GetIndustrySegment' AND job_title.id = jobs.title AND jobs.employer_id = employer.id");

                $GetFavouriteJobs = DB::select("SELECT employer.profile_pic, jobs.title, employer.slug AS CompanySlug, jobs.slug, employer.company_name FROM favourite_jobs, jobs, employer WHERE favourite_jobs.candidate_id = $GetCandidateId AND favourite_jobs.jobs_id = jobs.id AND jobs.employer_id = employer.id AND favourite_jobs.status = 1 AND favourite_jobs.type = 1");
                // echo json_encode($GetSimilarJobs);
                // exit;    
                $GetProfessionalDetails = DB::table('candidate')
                                ->select('resume.name', 'job_title.name AS JobTitle')
                                ->join('resume', 'candidate.id', '=', 'resume.candidate_id')
                                ->join('job_title', 'job_title.id', '=', 'resume.department')
                                ->where('resume.worked_till', '=', 1)
                                ->first();

                $GetRole = "";

                foreach($GetRoles as $key => $value){
                    $GetRole = explode(',', $value->role);
                }

                $GetQualification = "";

                foreach($GetRoles as $key => $value){
                    $GetQualification = explode(',', $value->qualification);
                }

                // echo json_encode($GetJobsBySlug);
                // exit;
                $GetFavouritesCount = FavouriteModel::where('candidate_id', $GetCandidateId)
                                ->count();

                return view("UI.layouts.job_details", compact('title', 'GetJobsBySlug', 'GetTotalViewsCount', 'CheckJobsApplied', 'CheckApplicantsCount', 'CheckFavourites', 'GetSimilarJobs', 'CandidateProfile', 'GetFavouriteJobs', 'GetRole', 'GetQualification', 'GetProfessionalDetails', 'Profile', 'EmployerProfile', 'GetJobTitle', 'GetQualificationAdmin', 'GetTrainingTitle', 'GetIndustries', 'CheckPercentage', 'GetFavouritesCount'));
            }else{
                $TotalViews = new TotalViews();
                $TotalViews->candidate_id = $GetCandidateId;
                $TotalViews->jobs_id = $GetJobsId->id;
                $TotalViews->total_views = 1;
                $TotalViews->views_type = 1;

                $TotalViews->save();
            }
        }
        
        

        $GetJobsBySlug = DB::select("SELECT jobs.id, job_title.name AS title, employer.website, employer.mobile, employer.id AS EmployerId, employer.email, employer.profile_pic, jobs.total_positions, employer.company_name, jobs.city , jobs.salary_min, jobs.salary_max, jobs.job_type, jobs.created_at, jobs.job_description,  jobs.industry, jobs.experience,jobs.qualification, employer.slug AS CompanySlug FROM jobs, employer, job_title WHERE jobs.employer_id = employer.id AND job_title.id = jobs.title AND jobs.id = '$GetJobsId->id'");
        
        $GetTotalViewsCount = TotalViews::where('jobs_id', $GetJobsId->id)
                                        ->get();

        $CheckJobsApplied = JobsApplied::where('candidate_id', $GetCandidateId)
                                        ->where('jobs_id', $GetJobsId->id)
                                        ->first();

        $CheckApplicantsCount = JobsApplied::where('jobs_id', $GetJobsId->id)
                                            ->get();

        $CheckFavourites = FavouriteModel::where('candidate_id', $GetCandidateId)
                                        ->where('jobs_id', $GetJobsId->id)
                                        ->where('type', 1)
                                        ->first();
        
        $GetSimilarJobs = DB::select("SELECT jobs.id, job_title.name AS title, employer.website, employer.mobile, employer.email, employer.profile_pic, jobs.total_positions, employer.company_name, jobs.city , jobs.salary_min, jobs.salary_max, jobs.job_type, jobs.created_at, jobs.job_description,  jobs.industry, jobs.experience,  jobs.qualification,jobs.slug, jobs.slug, employer.slug AS CompanySlug FROM jobs, employer, job_title WHERE jobs.industry = '$GetJobsId->industry' AND jobs.slug != '$GetIndustrySegment' AND job_title.id = jobs.title AND jobs.employer_id = employer.id");

        if(Auth::guard('candidate')->check()){
            $GetFavouriteJobs = DB::select("SELECT employer.profile_pic, jobs.title, employer.slug AS CompanySlug, jobs.slug, employer.company_name FROM favourite_jobs, jobs, employer WHERE favourite_jobs.candidate_id = $GetCandidateId AND favourite_jobs.jobs_id = jobs.id AND jobs.employer_id = employer.id AND favourite_jobs.status = 1 AND favourite_jobs.type = 1");
        }
        // echo json_encode($GetSimilarJobs);
        // exit;
        // $GetRole = "";

        // foreach($GetRoles as $key => $value){
        //     $GetRole = explode(',', $value->role);
        // }

        $GetProfessionalDetails = DB::table('candidate')
                                ->select('resume.name', 'job_title.name AS JobTitle')
                                ->join('resume', 'candidate.id', '=', 'resume.candidate_id')
                                ->join('job_title', 'job_title.id', '=', 'resume.department')
                                ->where('resume.worked_till', '=', 1)
                                ->first();
                                $GetFavouritesCount = FavouriteModel::where('candidate_id', $GetCandidateId)
                                ->count();

        return view("UI.layouts.job_details", compact('title', 'GetJobsBySlug', 'GetTotalViewsCount', 'CheckJobsApplied', 'CheckApplicantsCount', 'CheckFavourites', 'GetSimilarJobs', 'CandidateProfile', 'GetFavouriteJobs', 'GetRole', 'GetQualification', 'GetProfessionalDetails', 'Profile', 'EmployerProfile', 'GetJobTitle', 'GetQualificationAdmin', 'GetTrainingTitle', 'GetIndustries', 'CheckPercentage', 'GetFavouritesCount'));
    }



    // Jobs Lists & Search
    public function GetAllJobs(){
        if(Auth::guard('candidate')->check()){
            $title = "All Jobs";
            $GetCandidateId = Session::get("CandidateId");
            $CandidateProfile = CandidateProfileModel::where("candidate_id", $GetCandidateId)->first();

            $GetJobs = DB::select("SELECT employer.profile_pic, jobs.id, jobs.title, jobs.slug, employer.company_name, jobs.location, jobs.job_type, employer.slug AS CompanySlug FROM jobs, employer WHERE jobs.employer_id = employer.id ORDER BY jobs.id ASC LIMIT 1");
            
            $GetFavouriteJobs = DB::select("SELECT employer.profile_pic, jobs.title, employer.slug AS CompanySlug, jobs.slug, employer.company_name FROM favourite_jobs, jobs, employer WHERE favourite_jobs.candidate_id = $GetCandidateId AND favourite_jobs.jobs_id = jobs.id AND jobs.employer_id = employer.id AND favourite_jobs.status = 1 AND favourite_jobs.type = 1");

            return view("UI.layouts.jobs_list", compact('title', 'GetJobs', 'CandidateProfile', 'GetFavouriteJobs'));
        }else{
            return redirect('/');
        }
    }

    public function SearchJobsTitle(Request $request){
        $title = $request->term;

        $GetJobs = DB::select("SELECT jobs.title FROM jobs, employer WHERE employer.id = jobs.employer_id AND title LIKE '%$title%' ORDER BY title");
        echo json_encode($GetJobs);

    }

    public function SearchJobs(Request $request){
        $Title = $request->title;
        $City = $request->city;
        // echo $Title;
        // exit;
        // $GetTrainingsBySearch = DB::select("SELECT employer.profile_pic, jobs.title, jobs.slug, employer.company_name, jobs.location, jobs.job_type FROM jobs, employer WHERE jobs.employer_id = employer.id AND jobs.title LIKE '%{$Title}%' AND jobs.city = '$City'");
        $GetJobsBySearch = DB::select("SELECT DISTINCT jobs.id, job_title.name AS title, employer.website, employer.mobile, employer.email, employer.profile_pic, jobs.total_positions, employer.company_name, jobs.city, jobs.job_type, jobs.created_at, jobs.job_description, jobs.experience, jobs.qualification, jobs.slug, employer.slug AS CompanySlug FROM jobs, employer, job_title WHERE jobs.employer_id = employer.id AND job_title.id = jobs.title AND job_title.name LIKE '%{$Title}%' AND jobs.city = '$City'");

        // echo json_encode($GetJobsBySearch);
        // exit;

        $GetSearchResults = "";
        $GetJobType = "";
        $GetPostedOn = "";
        $GetDescription ="";

        foreach($GetJobsBySearch as $Results){
            if($Results->job_type == 1){
                $GetJobType = "Part time";
            }else{
                $GetJobType = "Full time";
            }
            
            $now = new DateTime();
            $full = false;	
            $ago = new DateTime($Results->created_at);
            $diff = $now->diff($ago);

            $diff->w = floor($diff->d / 7);
            $diff->d -= $diff->w * 7;

            $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
            );
            foreach ($string as $k => &$v) {
            if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
            unset($string[$k]);
            }
            }

            if (!$full) $string = array_slice($string, 0, 1);
            $GetPostedOn =  $string ? implode(', ', $string) . ' ago' : 'just now';
            
            $GetDescription = implode(' ', array_slice(explode(' ', $Results->job_description), 0, 20));
            
            $GetSearchResults = "<div class='job-listing wtabs recommented-jobs'>
            <div class='job-title-sec recommended-jobs-pad-left'>
                <h3><a href='/JobDetails/".$Results->CompanySlug."/".$Results->id."/".$Results->slug."' target='_blank'>".$Results->title."</a></h3>
                <p class='recommended-jobs-company-name'>".$Results->company_name."</p>
                <div class='job-lctn job-attributes-recommended-jobs'><i class='fa fa-briefcase'></i>".$Results->experience." Years</div>
                <div class='job-lctn job-attributes-recommended-jobs'><i class='fa fa-user-circle'></i>".$GetJobType."
                 </div>
                <div class='job-lctn job-attributes-recommended-jobs'><i class='fa fa-map-marker' ></i>".$Results->city."</div>
                
             </div>
             <p class='recommended-jobs-description'>".$GetDescription."</p>
             <p class='recommended-jobs-posted-on'>Posted on:
                     ".$GetPostedOn."
             </p>
        </div>";

            if($GetSearchResults){
                echo $GetSearchResults;
            }else{
                echo json_encode("No Results Found...");
            }
        }

        
        
    }

    public function ApplyJobs(Request $request){
        

        if(Auth::guard('candidate')->check()){
            $GetCandidateId = Session::get("CandidateId");
        }elseif(Auth::guard('trainee')->check()){
            $GetCandidateId = Session::get("TraineeId");   
        }elseif(Auth::guard('ca_article')->check()){
            $GetCandidateId = Session::get("CaArticleId");   
        }

        $GetJobsId = $request->JobsId;

        $GetEmployerId = $request->GetEmployerId;

        $GetJobTitle = $request->GetJobTitle;

        $CheckJobsApplied = JobsApplied::where('candidate_id', $GetCandidateId)
                                        ->where('jobs_id', $GetJobsId)
                                        ->first();
        
        $GetEmployerMail = EmployerModel::where('id', $GetEmployerId)->first();
        
        $GetEmpName = $GetEmployerMail->company_name;
        // $GetJobTitle = 

        

        // echo json_encode($GetEmpName);
        // exit;

        if($CheckJobsApplied != null){
            return response()->json(array(
                "error" => TRUE,
                "message" => "You have already applied this job..."
            ));
        }else{
            $JobsApplied = new JobsApplied();
            $JobsApplied->candidate_id = $GetCandidateId;
            $JobsApplied->jobs_id = $GetJobsId;
    
            $ApplyJobs = $JobsApplied->save();

            Mail::to($GetEmployerMail->email)->send(new JobAppliedCandidateToEmployer($GetEmpName, $GetJobTitle));            

            if($ApplyJobs){
                return response()->json(array(
                    "error" => FALSE,
                    "message" => "Jobs applied successfully"
                ));
            }else{
                return response()->json(array(
                    "error" => TRUE,
                    "message" => "Failed"
                ));
            }
        }
    }

    public function FilterDatePostedJobs(Request $request){
        $GetDatePosted = $request->date_posted;
        

        $GetDataByDatePosted = [];
        
            if($GetDatePosted == 1){
                $GetDataByDatePosted[] = DB::select("SELECT jobs.id, job_title.name AS title, employer.website, employer.mobile, employer.email, employer.profile_pic, jobs.total_positions, employer.company_name, jobs.city , jobs.salary_min, jobs.salary_max, jobs.job_type, jobs.created_at, jobs.job_description,  jobs.experience, jobs.qualification, jobs.slug, employer.slug AS CompanySlug FROM jobs, employer, job_title WHERE job_title.id = jobs.title AND jobs.employer_id = employer.id AND jobs.status = 0 AND jobs.employee_type = 2 AND jobs.created_at >= DATE_SUB(CURDATE(), INTERVAL 1 HOUR) ORDER BY jobs.created_at DESC");
                
            }elseif($GetDatePosted == 2){
                $GetDataByDatePosted[] = DB::select("SELECT jobs.id, job_title.name AS title, employer.website, employer.mobile, employer.email, employer.profile_pic, jobs.total_positions, employer.company_name, jobs.city , jobs.salary_min, jobs.salary_max, jobs.job_type, jobs.created_at, jobs.job_description,  jobs.experience, jobs.qualification, jobs.slug, employer.slug AS CompanySlug FROM jobs, employer, job_title WHERE job_title.id = jobs.title AND jobs.employer_id = employer.id AND jobs.status = 0 AND jobs.employee_type = 2 AND jobs.created_at >= (CURRENT_TIMESTAMP - INTERVAL '24' HOUR) ORDER BY jobs.created_at DESC");
            }elseif($GetDatePosted == 3){
                $GetDataByDatePosted[] = DB::select("SELECT jobs.id, job_title.name AS title, employer.website, employer.mobile, employer.email, employer.profile_pic, jobs.total_positions, employer.company_name, jobs.city , jobs.salary_min, jobs.salary_max, jobs.job_type, jobs.created_at, jobs.job_description,  jobs.experience, jobs.qualification, jobs.slug, employer.slug AS CompanySlug FROM jobs, employer, job_title WHERE job_title.id = jobs.title AND  jobs.employer_id = employer.id AND jobs.status = 0 AND jobs.employee_type = 2 AND jobs.created_at >= DATE_ADD(CURDATE(), INTERVAL -7 DAY) ORDER BY jobs.created_at DESC");
            }elseif($GetDatePosted == 4){
                $GetDataByDatePosted[] = DB::select("SELECT jobs.id, job_title.name AS title, employer.website, employer.mobile, employer.email, employer.profile_pic, jobs.total_positions, employer.company_name, jobs.city , jobs.salary_min, jobs.salary_max, jobs.job_type, jobs.created_at, jobs.job_description,  jobs.experience, jobs.qualification, jobs.slug, employer.slug AS CompanySlug FROM jobs, employer, job_title WHERE job_title.id = jobs.title AND jobs.employee_type = 2 AND jobs.status = 0 AND jobs.employer_id = employer.id ORDER BY jobs.created_at DESC");
            }
        
        // echo json_encode($GetDataByDatePosted);
        // exit;

        $GetSearchResults = "";
        $GetJobType = "";
        $GetPostedOn = "";
        $GetDescription ="";

        if($GetDataByDatePosted){
            foreach($GetDataByDatePosted as $GetData){
                foreach($GetData as $Results){
                    if($Results->job_type == 1){
                        $GetJobType = "Part time";
                    }else{
                        $GetJobType = "Full time";
                    }
                    
                    $now = new DateTime();
                    $full = false;	
                    $ago = new DateTime($Results->created_at);
                    $diff = $now->diff($ago);
    
                    $diff->w = floor($diff->d / 7);
                    $diff->d -= $diff->w * 7;
    
                    $string = array(
                    'y' => 'year',
                    'm' => 'month',
                    'w' => 'week',
                    'd' => 'day',
                    'h' => 'hour',
                    'i' => 'minute',
                    's' => 'second',
                    );
                    foreach ($string as $k => &$v) {
                    if ($diff->$k) {
                    $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
                    } else {
                    unset($string[$k]);
                    }
                    }
    
                    if (!$full) $string = array_slice($string, 0, 1);
                    $GetPostedOn =  $string ? implode(', ', $string) . ' ago' : 'just now';
                    
                    $GetDescription = implode(' ', array_slice(explode(' ', $Results->job_description), 0, 20));

                    $GetSearchResults = "<div class='job-listing wtabs recommented-jobs'>
                    <div class='job-title-sec recommended-jobs-pad-left'>
                        <h3><a href='/JobDetails/".$Results->CompanySlug."/".$Results->id."/".$Results->slug."' target='_blank'>".$Results->title."</a></h3>
                        <p class='recommended-jobs-company-name'>".$Results->company_name."</p>
                        <div class='job-lctn job-attributes-recommended-jobs'><i class='fa fa-briefcase'></i>".$Results->experience." Years</div>
                        <div class='job-lctn job-attributes-recommended-jobs'><i class='fa fa-user-circle'></i>".$GetJobType."
                        </div>
                        <div class='job-lctn job-attributes-recommended-jobs'><i class='fa fa-map-marker' ></i>".$Results->city."</div>
                    </div>
                    <p class='recommended-jobs-description'>".$GetDescription."</p>
                    <p class='recommended-jobs-posted-on'>Posted on:
                            ".$GetPostedOn."
                    </p>
                </div>";
    
                    if($GetSearchResults){
                        echo $GetSearchResults;
                    }
                }
            }
        }
        

    }



    public function FilterJobs(Request $request){
        $GetDatePosted = $request->date_posted;
        $GetLocation = $request->location;
        // $GetQualification = $request->qualification;
        // $GetJobType = $request->JobType;
        $GetTitle = $request->Title;
        
        $GetDataByDatePosted = [];

        // echo json_encode($GetJobType);
        // exit;
        // if(count($GetQualification)>0){
        //     $GetDataByDatePosted[] = DB::select("SELECT jobs.id, jobs.title, employer.website, employer.mobile, employer.email, employer.profile_pic, jobs.total_positions, employer.company_name, jobs.city , jobs.salary_min, jobs.salary_max, jobs.job_type, jobs.created_at, jobs.job_description, jobs.industry, jobs.experience, jobs.qualification, jobs.slug, employer.slug AS CompanySlug FROM jobs, employer WHERE jobs.employer_id = employer.id AND jobs.qualification IN (".implode(',', $GetQualification).")");
        // }elseif(count($GetJobType)>0){
        //     $GetDataByDatePosted[] = DB::select("SELECT jobs.id, jobs.title, employer.website, employer.mobile, employer.email, employer.profile_pic, jobs.total_positions, employer.company_name, jobs.city , jobs.salary_min, jobs.salary_max, jobs.job_type, jobs.created_at, jobs.job_description, jobs.industry, jobs.experience, jobs.qualification, jobs.slug, employer.slug AS CompanySlug FROM jobs, employer WHERE jobs.employer_id = employer.id AND jobs.job_type IN (".implode(',', $GetJobType).")");
            
        // }elseif(count($GetIndustry)>0){
        //     $GetDataByDatePosted[] = DB::select("SELECT jobs.id, jobs.title, employer.website, employer.mobile, employer.email, employer.profile_pic, jobs.total_positions, employer.company_name, jobs.city , jobs.salary_min, jobs.salary_max, jobs.job_type, jobs.created_at, jobs.job_description, jobs.industry, jobs.experience, jobs.qualification, jobs.slug, employer.slug AS CompanySlug FROM jobs, employer WHERE jobs.employer_id = employer.id AND jobs.industry IN (".implode(',', $GetIndustry).")");

        // }
        // if($GetQualification != null && $GetJobType !=null){
            
        //     $GetDataByDatePosted[] = DB::select("SELECT jobs.id, jobs.title, employer.website, employer.mobile, employer.email, employer.profile_pic, jobs.total_positions, employer.company_name, jobs.city , jobs.salary_min, jobs.salary_max, jobs.job_type, jobs.created_at, jobs.job_description, jobs.industry, jobs.experience, jobs.qualification, jobs.slug, employer.slug AS CompanySlug FROM jobs, employer WHERE  (jobs.qualification, jobs.job_type) IN (".implode(',', $GetQualification)."), (".implode(',', $GetJobType).")");
        // }
        
        // if($GetDatePosted == 1){
        //     $GetDataByDatePosted[] = DB::select("SELECT jobs.id, jobs.title, employer.website, employer.mobile, employer.email, employer.profile_pic, jobs.total_positions, employer.company_name, jobs.city , jobs.salary_min, jobs.salary_max, jobs.job_type, jobs.created_at, jobs.job_description, jobs.industry, jobs.experience, jobs.qualification, jobs.slug, employer.slug AS CompanySlug FROM jobs, employer WHERE jobs.employer_id = employer.id AND jobs.created_at >= CURRENT_TIMESTAMP - INTERVAL '1' HOUR");
        // }elseif($GetDatePosted == 2){
        //     $GetDataByDatePosted[] = DB::select("SELECT jobs.id, jobs.title, employer.website, employer.mobile, employer.email, employer.profile_pic, jobs.total_positions, employer.company_name, jobs.city , jobs.salary_min, jobs.salary_max, jobs.job_type, jobs.created_at, jobs.job_description, jobs.industry, jobs.experience, jobs.qualification, jobs.slug, employer.slug AS CompanySlug FROM jobs, employer WHERE jobs.employer_id = employer.id AND jobs.created_at >= CURRENT_TIMESTAMP - INTERVAL '24' HOUR");
        // }elseif($GetDatePosted == 3){
        //     $GetDataByDatePosted[] = DB::select("SELECT jobs.id, jobs.title, employer.website, employer.mobile, employer.email, employer.profile_pic, jobs.total_positions, employer.company_name, jobs.city , jobs.salary_min, jobs.salary_max, jobs.job_type, jobs.created_at, jobs.job_description, jobs.industry, jobs.experience, jobs.qualification, jobs.slug, employer.slug AS CompanySlug FROM jobs, employer WHERE jobs.employer_id = employer.id AND jobs.created_at >= DATE_ADD(CURDATE(), INTERVAL -7 DAY)");
        // }else{
            
            $GetDataByDatePosted[] =DB::table('jobs')
            ->select('jobs.id', 'job_title.name AS title', 'employer.website', 'employer.mobile', 'employer.email', 'employer.profile_pic', 'jobs.total_positions', 'employer.company_name', 'jobs.city' , 'jobs.salary_min', 'jobs.salary_max', 'jobs.job_type', 'jobs.created_at', 'jobs.job_description', 'jobs.experience', 'jobs.qualification', 'jobs.slug', 'employer.slug AS CompanySlug')
            ->join('employer', 'employer.id', '=', 'jobs.employer_id')
            ->join('job_title', 'job_title.id', '=', 'jobs.title')
            ->where('employee_type', 2)
            ->where('jobs.status', 0)
            // ->whereIn('job_type', $request->categories)
            ->when($GetLocation, function ($query) use ($GetLocation) {
                return $query->whereIn('jobs.city', $GetLocation);
            })

            // ->when($GetQualification, function ($query) use ($GetQualification) {
            //     return $query->whereIn('qualification', $GetQualification);
            // })
            //         // ->WhereIn('job_role', $request->role)
            // ->when($GetJobType, function ($query) use ($GetJobType) {
            //     return $query->whereIn('job_type', $GetJobType);
            // })
    
            ->when($GetTitle, function ($query) use ($GetTitle) {
                return $query->whereIn('jobs.title', $GetTitle);
            })
            ->get();
        // }

        


        // echo json_encode($GetDataByDatePosted);
        // exit;

        $GetSearchResults = "";
        $GetJobType = "";
        $GetPostedOn = "";
        $GetDescription = "";

        foreach($GetDataByDatePosted as $GetData){
            foreach($GetData as $Results){
                if($Results->job_type == 1){
                    $GetJobType = "Part time";
                }else{
                    $GetJobType = "Full time";
                }
                
                $now = new DateTime();
                $full = false;	
                $ago = new DateTime($Results->created_at);
                $diff = $now->diff($ago);

                $diff->w = floor($diff->d / 7);
                $diff->d -= $diff->w * 7;

                $string = array(
                'y' => 'year',
                'm' => 'month',
                'w' => 'week',
                'd' => 'day',
                'h' => 'hour',
                'i' => 'minute',
                's' => 'second',
                );
                foreach ($string as $k => &$v) {
                if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
                } else {
                unset($string[$k]);
                }
                }

                if (!$full) $string = array_slice($string, 0, 1);
                $GetPostedOn =  $string ? implode(', ', $string) . ' ago' : 'just now';

                $GetDescription = implode(' ', array_slice(explode(' ', $Results->job_description), 0, 20));

                $GetSearchResults = "<div class='job-listing wtabs recommented-jobs'>
                <div class='job-title-sec recommended-jobs-pad-left'>
                    <h3><a href='/JobDetails/".$Results->CompanySlug."/".$Results->id."/".$Results->slug."' target='_blank'>".$Results->title."</a></h3>
                    <p class='recommended-jobs-company-name'>".$Results->company_name."</p>
                    <div class='job-lctn job-attributes-recommended-jobs'><i class='fa fa-briefcase'></i>".$Results->experience." Years</div>
                    <div class='job-lctn job-attributes-recommended-jobs'><i class='fa fa-user-circle'></i>".$GetJobType."
                    </div>
                    <div class='job-lctn job-attributes-recommended-jobs'><i class='fa fa-map-marker' ></i>".$Results->city."</div>
                </div>
                <p class='recommended-jobs-description'>".$GetDescription."</p>
                <p class='recommended-jobs-posted-on'>Posted on:
                        ".$GetPostedOn."
                </p>
            </div>";

                if($GetSearchResults){
                    echo $GetSearchResults;
                }else{
                    echo json_encode("No Results Found...");
                }
            }
        }
    }


    public function SortByJobs(Request $request){
        $GetSortBy = $request->sortby;
        
        $GetDataByDatePosted = [];
        
            if($GetSortBy == 1){
                $GetDataByDatePosted[] = DB::select("SELECT jobs.id, jobs.title, employer.website, employer.mobile, employer.email, employer.profile_pic, jobs.total_positions, employer.company_name, jobs.city , jobs.salary_min, jobs.salary_max, jobs.job_type, jobs.created_at, jobs.job_description, industries.name, jobs.experience, jobs.qualification, jobs.slug, employer.slug AS CompanySlug FROM jobs, employer, industries WHERE jobs.employer_id = employer.id AND industries.id = jobs.industry ORDER BY jobs.created_at DESC");
            }elseif($GetSortBy == 2){
                $GetDataByDatePosted[] = DB::select("SELECT jobs.id, jobs.title, employer.website, employer.mobile, employer.email, employer.profile_pic, jobs.total_positions, employer.company_name, jobs.city , jobs.salary_min, jobs.salary_max, jobs.job_type, jobs.created_at, jobs.job_description, industries.name, jobs.experience, jobs.qualification, jobs.slug, employer.slug AS CompanySlug FROM jobs, employer, industries WHERE jobs.employer_id = employer.id AND industries.id = jobs.industry");
            }

        $GetSearchResults = "";
        $GetJobType = "";
        $GetPostedOn = "";
        
        $GetDescription = "";

        if($GetDataByDatePosted){
            foreach($GetDataByDatePosted as $GetData){
                foreach($GetData as $Results){
                    if($Results->job_type == 1){
                        $GetJobType = "Part time";
                    }else{
                        $GetJobType = "Full time";
                    }
                    
                    $now = new DateTime();
                    $full = false;	
                    $ago = new DateTime($Results->created_at);
                    $diff = $now->diff($ago);
    
                    $diff->w = floor($diff->d / 7);
                    $diff->d -= $diff->w * 7;
    
                    $string = array(
                    'y' => 'year',
                    'm' => 'month',
                    'w' => 'week',
                    'd' => 'day',
                    'h' => 'hour',
                    'i' => 'minute',
                    's' => 'second',
                    );
                    foreach ($string as $k => &$v) {
                    if ($diff->$k) {
                    $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
                    } else {
                    unset($string[$k]);
                    }
                    }
    
                    if (!$full) $string = array_slice($string, 0, 1);
                    $GetPostedOn =  $string ? implode(', ', $string) . ' ago' : 'just now';
                    
                    $GetDescription = implode(' ', array_slice(explode(' ', $Results->job_description), 0, 20));

                    $GetSearchResults = "<div class='job-listing wtabs recommented-jobs'>
                    <div class='job-title-sec recommended-jobs-pad-left'>
                        <h3><a href='/JobDetails/".$Results->CompanySlug."/".$Results->slug."' target='_blank'>".$Results->title."</a></h3>
                        <p class='recommended-jobs-company-name'>".$Results->company_name."</p>
                        <div class='job-lctn job-attributes-recommended-jobs'><i class='fa fa-briefcase'></i>".$Results->experience." Years</div>
                        <div class='job-lctn job-attributes-recommended-jobs'><i class='fa fa-user-circle'></i>".$GetJobType."
                        </div>
                        <div class='job-lctn job-attributes-recommended-jobs'><i class='fa fa-map-marker' ></i>".$Results->city."</div>
                        <div class='job-lctn job-attributes-recommended-jobs'><i class='fa fa-industry'></i>".$Results->name."</div>
                    </div>
                    <p class='recommended-jobs-description'>".$GetDescription."</p>
                    <p class='recommended-jobs-posted-on'>Posted on:
                            ".$GetPostedOn."
                    </p>
                </div>";
    
                    if($GetSearchResults){
                        echo $GetSearchResults;
                    }
                }
            }
        }
        


    }



    public function ClearAllFilter(Request $request){
        $GetDataByDatePosted[] = DB::select("SELECT DISTINCT jobs.id, job_title.name AS title, employer.website, employer.mobile, employer.email, employer.profile_pic, jobs.total_positions, employer.company_name, jobs.city , jobs.salary_min, jobs.salary_max, jobs.job_type, jobs.created_at, jobs.job_description, jobs.experience, jobs.qualification, jobs.slug, employer.slug AS CompanySlug FROM jobs, employer, job_title WHERE jobs.employer_id = employer.id AND job_title.id = jobs.title AND jobs.employee_type = 2 AND jobs.status = 0 ORDER BY jobs.created_at DESC");


        $GetSearchResults = "";
        $GetJobType = "";
        $GetPostedOn = "";
        
        $GetDecription = "";

        if($GetDataByDatePosted){
            foreach($GetDataByDatePosted as $GetData){
                foreach($GetData as $Results){
                    if($Results->job_type == 1){
                        $GetJobType = "Part time";
                    }else{
                        $GetJobType = "Full time";
                    }
                    
                    $now = new DateTime();
                    $full = false;	
                    $ago = new DateTime($Results->created_at);
                    $diff = $now->diff($ago);
    
                    $diff->w = floor($diff->d / 7);
                    $diff->d -= $diff->w * 7;
    
                    $string = array(
                    'y' => 'year',
                    'm' => 'month',
                    'w' => 'week',
                    'd' => 'day',
                    'h' => 'hour',
                    'i' => 'minute',
                    's' => 'second',
                    );
                    foreach ($string as $k => &$v) {
                    if ($diff->$k) {
                    $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
                    } else {
                    unset($string[$k]);
                    }
                    }
    
                    if (!$full) $string = array_slice($string, 0, 1);
                    $GetPostedOn =  $string ? implode(', ', $string) . ' ago' : 'just now';
                    

                    $GetDescription = implode(' ', array_slice(explode(' ', $Results->job_description), 0, 20));


                    $GetSearchResults = "<div class='job-listing wtabs recommented-jobs'>
                    <div class='job-title-sec recommended-jobs-pad-left'>
                        <h3><a href='/JobDetails/".$Results->CompanySlug."/".$Results->id."/".$Results->slug."' target='_blank'>".$Results->title."</a></h3>
                        <p class='recommended-jobs-company-name'>".$Results->company_name."</p>
                        <div class='job-lctn job-attributes-recommended-jobs'><i class='fa fa-briefcase'></i>".$Results->experience." Years</div>
                        <div class='job-lctn job-attributes-recommended-jobs'><i class='fa fa-user-circle'></i>".$GetJobType."
                        </div>
                        <div class='job-lctn job-attributes-recommended-jobs'><i class='fa fa-map-marker' ></i>".$Results->city."</div>
                    </div>
                    <p class='recommended-jobs-description'>".$GetDescription."</p>
                    <p class='recommended-jobs-posted-on'>Posted on:
                            ".$GetPostedOn."
                    </p>
                </div>";
    
                    if($GetSearchResults){
                        echo $GetSearchResults;
                    }
                }
            }
        }
        


    }


    public function FilterJobType(Request $request){
        
        $GetJobType = $request->job_type;
        $GetDataByDatePosted = array();
        if(is_array($GetJobType)){
            foreach($GetJobType as $JobType){
                $GetDataByDatePosted[] = DB::select("SELECT employer.profile_pic, jobs.title, jobs.slug, employer.company_name, jobs.location, jobs.job_type FROM jobs, employer WHERE jobs.employer_id = employer.id AND jobs.job_type IN ($JobType)");
            }
            
            $GetFilterResultData = array();
            $CheckJobType = "";
    
            foreach($GetDataByDatePosted as $DatePostedData){
                foreach($DatePostedData as $FilterJobType){
                    if($FilterJobType->job_type == 1){
                        $CheckJobType = "<span class='job-is ft'>Full time</span>";
                    }else{
                        $CheckJobType = "<span class='job-is pt'>Part time</span>";
                    }
        
                    $GetFilterResultData[] = "<div class='job-listing wtabs'><div class='job-title-sec'><div class='c-logo'> <img src='employer_profile/".$FilterJobType->profile_pic."' alt='' /> </div><h3><a href='/JobDetails/".$FilterJobType->slug."' target='_blank' title=''>".$FilterJobType->title."</a></h3><span>".$FilterJobType->company_name."</span><div class='job-lctn'><i class='la la-map-marker'></i>".$FilterJobType->location."</div></div><div class='job-style-bx'>".$CheckJobType."<span class='fav-job'><i class='la la-heart-o'></i></span><i>5 months ago</i></div></div>";
                }
                
            }
            echo json_encode($GetFilterResultData);
        }else{
            $GetDataByDatePosted = DB::select("SELECT employer.profile_pic, jobs.title, jobs.slug, employer.company_name, jobs.location, jobs.job_type FROM jobs, employer WHERE jobs.employer_id = employer.id");

            $GetFilterResultData = array();
            $CheckJobType = "";
    
            foreach($GetDataByDatePosted as $DatePostedData){
                    if($DatePostedData->job_type == 1){
                        $CheckJobType = "<span class='job-is ft'>Full time</span>";
                    }else{
                        $CheckJobType = "<span class='job-is pt'>Part time</span>";
                    }
        
                    $GetFilterResultData[] = "<div class='job-listing wtabs'><div class='job-title-sec'><div class='c-logo'> <img src='employer_profile/".$DatePostedData->profile_pic."' alt='' /> </div><h3><a href='/JobDetails/".$DatePostedData->slug."' target='_blank' title=''>".$DatePostedData->title."</a></h3><span>".$DatePostedData->company_name."</span><div class='job-lctn'><i class='la la-map-marker'></i>".$DatePostedData->location."</div></div><div class='job-style-bx'>".$CheckJobType."<span class='fav-job'><i class='la la-heart-o'></i></span><i>5 months ago</i></div></div>";
                
            }
            
            echo json_encode($GetFilterResultData);
        }
    }

    // public function SortByJobs(Request $request){
    //     $GetSortBy = $request->sortby;

    //     if($GetSortBy == 1){
    //         $GetDataByDatePosted = DB::select("SELECT employer.profile_pic, jobs.title, jobs.slug, employer.company_name, jobs.location, jobs.job_type FROM jobs, employer WHERE jobs.employer_id = employer.id ORDER BY jobs.id ASC LIMIT 30");
    //     }elseif($GetSortBy == 2){
    //         $GetDataByDatePosted = DB::select("SELECT employer.profile_pic, jobs.title, jobs.slug, employer.company_name, jobs.location, jobs.job_type FROM jobs, employer WHERE jobs.employer_id = employer.id ORDER BY jobs.id ASC LIMIT 40");
    //     }elseif($GetSortBy == 3){
    //         $GetDataByDatePosted = DB::select("SELECT employer.profile_pic, jobs.title, jobs.slug, employer.company_name, jobs.location, jobs.job_type FROM jobs, employer WHERE jobs.employer_id = employer.id ORDER BY jobs.id ASC LIMIT 50");
    //     }elseif($GetSortBy == 4){
    //         $GetDataByDatePosted = DB::select("SELECT employer.profile_pic, jobs.title, jobs.slug, employer.company_name, jobs.location, jobs.job_type FROM jobs, employer WHERE jobs.employer_id = employer.id ORDER BY jobs.id ASC LIMIT 60");
    //     }

    //     $GetSortByResultData = array();
    //     $CheckJobType = "";

    //     foreach($GetDataByDatePosted as $DatePostedData){
    //         if($DatePostedData->job_type == 1){
    //             $CheckJobType = "<span class='job-is ft'>Full time</span>";
    //         }else{
    //             $CheckJobType = "<span class='job-is pt'>Part time</span>";
    //         }

    //         $GetSortByResultData[] = "<div class='job-listing wtabs'><div class='job-title-sec'><div class='c-logo'> <img src='employer_profile/".$DatePostedData->profile_pic."' alt='' /> </div><h3><a href='/JobDetails/".$DatePostedData->slug."' target='_blank' title=''>".$DatePostedData->title."</a></h3><span>".$DatePostedData->company_name."</span><div class='job-lctn'><i class='la la-map-marker'></i>".$DatePostedData->location."</div></div><div class='job-style-bx'>".$CheckJobType."<span class='fav-job'><i class='la la-heart-o'></i></span><i>5 months ago</i></div></div>";
    //     }

    //     echo json_encode($GetSortByResultData);
    // }

    public function SearchQualification(Request $request){
        // $title = $request->term;
        // // echo $title;
        // // exit;
        
        // $GetTrainings = DB::select("SELECT qualification FROM qualifications WHERE qualification LIKE '%$title%' ORDER BY qualification ASC");

        // echo json_encode($GetTrainings);
        $query = $request->get('term','');
        
        $products=QualificationModel::where('qualification','LIKE',$query.'%')
                                    ->orderBy('qualification', 'ASC')
                                    ->get();
        
        $data=array();
        foreach ($products as $product) {
                $data[]=array('value'=>$product->qualification,'id'=>$product->id);
        }
        if(count($data))
             return $data;
        else
            return ['value'=>'No Result Found','id'=>''];
    }



    // public function SearchJobsByKeyword(Request $request){
    //     $query = $request->get('term','');
        
    //     $products = DB::table('jobs')
    //                 ->join('job_title', 'job_title.id', '=', 'jobs.title')
    //                 ->select('job_title.id', 'job_title.name')
    //                 ->distinct()
    //                 ->where('job_title.name','LIKE','%'.$query.'%')
    //                 ->where('jobs.job_type', 2)
    //                 ->orderBy('title', 'ASC')
    //                 ->get();
        
    //     $data=array();
    //     foreach ($products as $product) {
    //             $data[]=array('value'=>$product->name,'id'=>$product->id);
    //     }
    //     if(count($data))
    //          return $data;
    //     else
    //         return ['value'=>'No Result Found','id'=>''];
    // }
}
