<?php

namespace App\Http\Controllers\UI;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Razorpay\Api\Api;
use App\Model\UI\PaymentsModel;

use Auth;
use Session;

class RazorpayController extends Controller
{
    public function pay() {
        return view('pay');
    }

    public function dopayment(Request $request) {
        //Input items of form

        $Payments = new PaymentsModel();

        if(Auth::guard('trainee')->check()){
            $GetCandidateId = Session::get('TraineeId');
        }elseif(Auth::guard('ca_article')->check()){
            $GetCandidateId = Session::get('CaArticleId');
        }elseif(Auth::guard('candidate')->check()){
            $GetCandidateId = Session::get('CandidateId');
        }
        // echo $request->razorpay_payment_id;
        // exit;
        if($request->all()){
            $Payments->payment_id = $request->razorpay_payment_id;
            $Payments->payment_date = $request->payment_date;
            $Payments->candidate_id = $GetCandidateId;
            $Payments->webinar_id = $request->webinar_id;
            $Payments->status = "1";
    
            $CheckPaymentStatus = $Payments->save();
            if($CheckPaymentStatus){
                return response()->json(array(
                    "error"=>FALSE,
                    "message"=>"Payment Successfully"
                ));
            }else{
                return response()->json(array(
                    "error"=>TRUE,
                    "message"=>"Failed"
                ));
            }
        }
        

        // $input = $request->all();

        // Please check browser console.
        // print_r($input);
        // exit;
    }
}
