<?php

namespace App\Http\Controllers\UI;
// namespace Rahulreghunath\Textlocal;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\UI\EmployerModel;
use Illuminate\Support\Facades\Mail;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Session;
use App\Mail\VerifyMail;
use App\Mail\EmailOtp;
use App\Mail\ResetPassword;
use App\Mail\EmployerRegistration;

use App\Model\UI\CandidateProfileModel;
use App\Model\UI\CandidateModel;
use DB;

use App\Mail\ContactUs;




// Admin Model

use App\Model\Admin\IndustriesModel;
use App\Model\Admin\TrainingCompletionModel;
use App\Model\Admin\JobTitleModel;
use App\Model\Admin\QualificationModel;
use App\Model\Admin\TrainingTitleModel;



class EmployerController extends Controller
{   
    public function __construct(){
        // $this->middleware('guest:employer')->except('logout', 'AddEmployer', 'CheckEmployerLoginAfterRegister', 'EmployerLogin', 'CheckEmployerForgotEmail', 'SendEmployerOtp');
    }

    public function logout(Request $request) {
        // Auth::logout();
        Auth::guard('employer')->logout();
        return redirect('/');
    }


    public function EmployerProfile(){
        $title = "Employer Profile";

        $GetEmployerId = Session::get('EmployerId');
        $EmployerProfile = EmployerModel::where("id", $GetEmployerId)->first();
        if($EmployerProfile){
            $GetEmployerId = Session::get('EmployerId');
        }else{
            $GetEmployerId = "0";
        }

        $GetStipend = TrainingCompletionModel::get();
        $GetJobTitle = JobTitleModel::get();
        $GetQualification = QualificationModel::get();
        $GetTrainingTitle = TrainingTitleModel::get();

        // echo json_encode($EmployerProfile->company_name);
        // exit;
        return view("UI.layouts.employer_profile", compact('title', 'EmployerProfile', 'GetJobTitle', 'GetQualification', 'GetTrainingTitle'));
    }

    public function EmployerChangePassword(){
        $title = "Employer ChangePassword";

        $GetEmployerId = Session::get('EmployerId');
        $EmployerProfile = EmployerModel::where("id", $GetEmployerId)->first();
        if($EmployerProfile){
            $GetEmployerId = Session::get('EmployerId');
        }else{
            $GetEmployerId = "0";
        }

        $GetJobTitle = JobTitleModel::get();
        $GetQualification = QualificationModel::get();
        $GetTrainingTitle = TrainingTitleModel::get();

        return view("UI.layouts.employer_change_password", compact('title', 'EmployerProfile', 'GetJobTitle', 'GetQualification', 'GetTrainingTitle'));
    }

    public function Dashboard(){
        if(Auth::guard('employer')->check()){
            $title = "Employer Dashboard";

            $GetIndustries = IndustriesModel::get();
            $GetStipend = TrainingCompletionModel::get();
            $GetJobTitle = JobTitleModel::get();
            $GetQualification = QualificationModel::get();
            $GetTrainingTitle = TrainingTitleModel::get();

            $GetEmployerId = Session::get('EmployerId');
            $EmployerProfile = EmployerModel::where("id", $GetEmployerId)->first();
            if($EmployerProfile){
                $GetEmployerId = Session::get('EmployerId');
            }else{
                $GetEmployerId = "0";
            }

            $GetCandidates = DB::table('candidate')
                            ->select('candidate.name', 'job_title.name AS title', 'candidate.created_at', 'candidate.type')
                            ->join('jobs_applied', 'jobs_applied.candidate_id', '=', 'candidate.id')
                            ->join('jobs', 'jobs_applied.jobs_id', '=', 'jobs.id')
                            ->join('job_title', 'jobs.title', '=', 'job_title.id')
                            ->join('employer', 'employer.id', '=', 'jobs.employer_id')
                            ->where('candidate.type', 1)
                            ->where('employer.id', $GetEmployerId)
                            ->orderBy('candidate.created_at', 'DESC')->take(4)
                            ->get();

            $GetTrainees = DB::table('candidate')
                            ->select('candidate.name', 'training_title.title', 'candidate.created_at', 'candidate.type')
                            ->join('jobs_applied', 'jobs_applied.candidate_id', '=', 'candidate.id')
                            ->join('training', 'jobs_applied.training_id', '=', 'training.id')
                            ->join('training_title', 'training_title.id', '=', 'training.title')
                            ->join('employer', 'employer.id', '=', 'training.employer_id')
                            ->where('candidate.type', 2)
                            ->where('employer.id', $GetEmployerId)
                            ->orderBy('candidate.created_at', 'DESC')->take(4)
                            ->get();

            $GetTotalApplicants = count($GetCandidates) + count($GetTrainees);

            $CheckShortlisted = DB::select("SELECT * FROM candidate, candidate_profile, shortlist_candidates, employer WHERE candidate.id = candidate_profile.candidate_id AND employer.id = shortlist_candidates.employer_id AND candidate.type = 1 AND employer.id = '$GetEmployerId' ORDER BY candidate.id ASC");

            $GetResumeDownloadStatus = DB::table('jobs_applied')
                                        ->join('employer', 'employer.id', '=', 'jobs_applied.employer_id')
                                        ->where('employer.id', $GetEmployerId)
                                        ->where('download_cv_status', 1)
                                        ->count();

            $GetTotalPositions = DB::table('jobs')
                                ->where('jobs.employer_id', $GetEmployerId)
                                ->sum('total_positions');

            // echo $GetTotalPositions;
            // exit;
            return view("UI.layouts.employer_dashboard", compact('title', 'EmployerProfile', 'GetCandidates', 'GetTrainees', 'GetTotalApplicants', 'CheckShortlisted', 'GetResumeDownloadStatus', 'GetTotalPositions', 'GetIndustries', 'GetStipend', 'GetJobTitle', 'GetQualification', 'GetTrainingTitle'));
        }else{
            return redirect('/');
        }

    }

    public function AddEmployer(Request $request){
        $Employer = new EmployerModel();

        // $EmployerUsername = $request->username;
        $EmployerEmail = $request->employeremail;
        $EmployerMobile = $request->employerphonenumber;
        $EmployerPassword = Hash::make($request->employerpassword);
        $CheckEmail = $Employer::where("email", $EmployerEmail)->first();
        $CheckMobile = $Employer::where("mobile", $EmployerMobile)->first();
        // $CheckUsername = $Employer::where("user_name", $EmployerUsername)->first();
        $GetMobileOTP = Session::get("GetEmployerMobileOtp");

        $GetEmailOTP = Session::get("GetEmployerEmailOtp");

        $CandidateMobileOTP = $request->MobileOTP;
        $CandidateEmailOTP = $request->EmailOTP;

        // if($GetMobileOTP != $CandidateMobileOTP){
        //     return response()->json(array(
        //         "error"=>TRUE,
        //         "message"=>"Mobile OTP Verification Failed."
        //     ));
        // }

        // else if($GetEmailOTP != $CandidateEmailOTP){
        //     return response()->json(array(
        //         "error"=>TRUE,
        //         "message"=>"Email OTP Verification Failed."
        //     ));
            
        // else{
            // $Employer->user_name = $EmployerUsername;
            $Employer->company_name = $request->companyname;
            $Employer->password = $EmployerPassword;
            $Employer->slug = strtolower($request->companyname);
            $Employer->email = $EmployerEmail;
            $Employer->mobile = $EmployerMobile;
    
            $InsertEmployer = $Employer->save();
            // Mail::to($EmployerEmail)->send(new EmployerRegistration($request->companyname));

            $template_data = ['Name' => $request->companyname];

            Mail::send(['html' => 'UI.email.employer_register_confirmation'], $template_data,
                function ($message) use ($EmployerEmail) {
                   $message->to($EmployerEmail)
                   ->from('services@accountswale.in') //not sure why I have to add this
                   ->subject('Congratulations ! You are registered successfully at ACCOUNTSWALE');
      });
            
            return $this->CheckEmployerLoginAfterRegister($request, $EmployerEmail, $request->employerpassword);
            if($InsertEmployer){
                return response()->json(array(
                    "error"=>FALSE,
                    "message"=>"Registered successfully"
                ));
            }else{
                return response()->json(array(
                    "error"=>TRUE,
                    "message"=>"Registeration failed"
                ));
            }
        // }
       
    }

    public function CheckEmployerLoginAfterRegister(Request $request, $EmployerEmail, $EmployerPassword){
        if (Auth::guard('employer')->attempt(['email' => $EmployerEmail, 'password' => $EmployerPassword])) {
            //Authentication passed...
            $request->session()->put('EmployerName', Auth::guard('employer')->user()->user_name);
            $request->session()->put('EmployerCompanyName', Auth::guard('employer')->user()->company_name);
            $request->session()->put('EmployerEmail', Auth::guard('employer')->user()->email);
            $request->session()->put('EmployerId', Auth::guard('employer')->user()->id);
            $request->session()->put('EmployerMobile', Auth::guard('employer')->user()->mobile);
            return response()->json(array(
                "error"=>FALSE,
                "message"=>"Login successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Please Check your credentials..."
            ));
        }
    }


    public function EmployerLogin(Request $request){
        
        if (Auth::guard('employer')->attempt(['email' => $request->employerusername, 'password' => $request->employerpassword])) {
            //Authentication passed...
            $request->session()->put('EmployerName', Auth::guard('employer')->user()->user_name);
            $request->session()->put('EmployerEmail', Auth::guard('employer')->user()->email);
            $request->session()->put('EmployerId', Auth::guard('employer')->user()->id);
            $request->session()->put('EmployerMobile', Auth::guard('employer')->user()->mobile);
            return response()->json(array(
                "error"=>FALSE,
                "message"=>"Login successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Please Check your credentials..."
            ));
        }
    }

    public function CheckEmployerForgotEmail(Request $request){
        $Employer = new EmployerModel();
        $EmployerEmail = $request->employeremail;
        $CheckEmail = $Employer::where("email", $EmployerEmail)->first();

        if(!$CheckEmail){
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Email id is not registered."
            ));
        }else{
            $token = str_random(64);
            $CheckDataById = $Employer::findOrFail($CheckEmail->id);
            $CheckDataById->_token = $token;
            $CheckDataById->save();
            
            // echo json_encode($CheckDataById->_token);
            // exit;
            if($CheckDataById){
                return response()->json(array(
                    "error"=>FALSE,
                    "message"=>"Email send..."
                ));
            }else{
                return response()->json(array(
                    "error"=>TRUE,
                    "message"=>"Email failed..."
                ));
            }
        }
        
    }


    public function SendEmployerOtp(Request $request){
        $GetMobile = $request->employerphonenumber;

        $GenerateMobileOtp = rand(5, 897645);
        $request->session()->put('GetEmployerMobileOtp', $GenerateMobileOtp);
        $GetMobileOTP = Session::get("GetEmployerMobileOtp");

        $GenerateEmailOtp = rand(7, 8976457);
        $request->session()->put('GetEmployerEmailOtp', $GenerateEmailOtp);
        $GetEmailOTP = Session::get("GetEmployerEmailOtp");

        $EmployerEmail = $request->employeremail;
        $EmployerMobile = $request->employerphonenumber;
        $EmployerName = $request->companyname;

        $CheckEmail = EmployerModel::where("email", $EmployerEmail)->first();
        $CheckMobile = EmployerModel::where("mobile", $EmployerMobile)->first();

        
        
        
        if($CheckEmail){
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Email id is already registered."
            ));
        }elseif($CheckMobile){
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Mobile number is already registered."
            ));
        }else if($GenerateMobileOtp && $GenerateEmailOtp){
            $apiKey = urlencode('XutSvsJBNnI-sVkiqAhNlXHhDaDkICxtxiITNpcjBn');

            // Message details
            $numbers = array($GetMobile);
            $sender = urlencode('ACCWLE');
            $message = rawurlencode('Thanks for registering accountswale. Your OTP is: '.$GetMobileOTP);
        
            $numbers = implode(',', $numbers);
        
            // Prepare data for POST request
            $data = array('apikey' => $apiKey, 'numbers' => $numbers, "sender" => $sender, "message" => $message);
        
            // Send the POST request with cURL
            $ch = curl_init('https://api.textlocal.in/send/');
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($ch);
            curl_close($ch);

            // Mail::to($EmployerEmail)->send(new EmailOtp($GetEmailOTP));
            
            // Mail::to($EmployerEmail)->send(new EmployerRegistration($EmployerName));

    //         $template_data = ['Name' => $EmployerName];

    //         Mail::send(['html' => 'UI.email.employer_register_confirmation'], $template_data,
    //             function ($message) use ($EmployerEmail) {
    //                $message->to($EmployerEmail)
    //                ->from('services@accountswale.in') //not sure why I have to add this
    //                ->subject('Thanks Form Register');
    //   });

            return response()->json(array(
                "error"=>FALSE,
                "message"=>"OTP Send Successfully."
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Failed"
            ));
        }
        
    }

    public function CheckForgotEmail(Request $request){
        $Employer = new EmployerModel();
        $EmployerEmail = $request->employeremail;
        $CheckEmail = $Employer::where("email", $EmployerEmail)->first();

        if(!$CheckEmail){
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Email id is not registered."
            ));
        }else{
            $token = str_random(64);
            $request->session()->put('GetToken', $token);
            $GetToken = Session::get('GetToken');
            $CheckDataById = $Employer::findOrFail($CheckEmail->id);
            $CheckDataById->_token = $GetToken;
            $CheckDataById->save();
            $Id = $CheckEmail->id;
            Mail::to($EmployerEmail)->send(new ResetPassword($Id, $GetToken));
            // echo json_encode($CheckDataById->_token);
            // exit;
            if($CheckDataById){
                return response()->json(array(
                    "error"=>FALSE,
                    "message"=>"Email send successfully. Please check your email."
                ));
            }else{
                return response()->json(array(
                    "error"=>TRUE,
                    "message"=>"Email failed..."
                ));
            }
        }
        
    }
    
    public function CheckResetPassword($id, $token){
        $title = "Reset Password";
        $GetId = $id;

        return view("UI.email.reset_employer_password", compact('title', 'GetId'));
    }

    public function ResetPassword(Request $request){
        $GetId = $request->id;
        $GetNewPassword = $request->new_password;
        $GetConfirmPassword = $request->confirm_password;
        // echo json_encode($GetToken);
        // exit;
        if($GetNewPassword != $GetConfirmPassword){
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Password mismatch"
            ));
        }else{
            $ChangePassword = EmployerModel::findorFail($GetId);
            $ChangePassword->password = Hash::make($GetConfirmPassword);
            $ChangePassword->save();
            return response()->json(array(
                "error"=>FALSE,
                "message"=>"Password changed successfully"
            ));
        }
    }


    public function ResendLink(Request $request){
        $GetMobile = $request->employerphonenumber;
        
        $GenerateMobileOtp = rand(5, 897645);
        $request->session()->put('GetEmployerMobileOtp', $GenerateMobileOtp);
        $GetMobileOTP = Session::get("GetEmployerMobileOtp");

        $apiKey = urlencode('XutSvsJBNnI-sVkiqAhNlXHhDaDkICxtxiITNpcjBn');

            // Message details
            $numbers = array($GetMobile);
            $sender = urlencode('ACCWLE');
            $message = rawurlencode('Thanks for registering accountswale. Your OTP is: '.$GetMobileOTP);
        
            $numbers = implode(',', $numbers);
        
            // Prepare data for POST request
            $data = array('apikey' => $apiKey, 'numbers' => $numbers, "sender" => $sender, "message" => $message);
        
            // Send the POST request with cURL
            $ch = curl_init('https://api.textlocal.in/send/');
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($ch);
            curl_close($ch);
//         $sms = new Textlocal();
// $sms->send('test', '9698631624','sender'); //sender is optional

        return response()->json(array(
                        "error"=>FALSE,
                        "message"=>"Otp Send Successfully"
        ));
    }


    public function ResendEmailOTP(Request $request){
        $GenerateEmailOtp = rand(7, 8976457);
        $request->session()->put('GetEmployerEmailOtp', $GenerateEmailOtp);
        $GetEmailOTP = Session::get("GetEmployerEmailOtp");
        
        $EmployerEmail = $request->employeremail;

        Mail::to($EmployerEmail)->send(new EmailOtp($GetEmailOTP));

        return response()->json(array(
            "error"=>FALSE,
            "message"=>"Otp Send Successfully"
        ));
    }

    public function ContactUsMail(Request $request)
    {
        $name = $request->name;
        $email = $request->email;
        $subject = $request->subject;
        $messages = $request->message;

        $SendMail = Mail::to('admin@accountswale.in')->send(new ContactUs($name, $email, $subject, $messages));
        
        return response()->json(array(
            "error"=>FALSE,
            "message"=>"Mail Send Successfully"
        ));
    }


    // Profile First registration
    public function EmployerFirstRegister(){
        if(Auth::guard('employer')->check()){
            
        $title = "Employer Register";
        $GetEmail = Session::get('EmployerEmail');
        $GetCandidateId = Session::get('EmployerId');
        
        $EmployerProfile = EmployerModel::where("id", $GetCandidateId)->first();

        $CandidateProfile = EmployerModel::where("id", $GetCandidateId)->first();

        $Profile = EmployerModel::where("id", $GetCandidateId)->first();

        // $GetProfessionalDetails = DB::table('candidate')
        //                         ->select('resume.name', 'job_title.name AS JobTitle')
        //                         ->join('resume', 'candidate.id', '=', 'resume.candidate_id')
        //                         ->join('job_title', 'job_title.id', '=', 'resume.department')
        //                         ->where('resume.worked_till', '=', 1)
        //                         ->where('resume.candidate_id', $GetCandidateId)
        //                         ->first();

        // $GetIndustries = IndustriesModel::get();
        // $GetJobTitle = JobTitleModel::get();
        // $GetQualification = QualificationModel::get();

        // $GetExperience =    DB::table('resume')
        //                     ->select('resume.*', 'job_title.name AS JobTitle')
        //                     ->where('candidate_id', $GetCandidateId)
        //                     ->where('type', 2)
        //                     ->join('job_title', 'job_title.id', '=', 'resume.department')
        //                     ->orderBy('id', 'DESC')
        //                     ->get();

        // $GetEducation = DB::table('resume')
        //                     ->select('resume.*', 'qualifications.qualification')
        //                     ->join('qualifications', 'qualifications.id', '=', 'resume.department')
        //                     ->where('type', 1)
        //                     ->where('resume.candidate_id', $GetCandidateId)
        //                     ->orderBy('id', 'DESC')
        //                     ->get();

        return view("UI.register.employer_first_register", compact('title', 'CandidateProfile', 'GetProfessionalDetails', 'Profile', 'GetIndustries', 'GetJobTitle', 'GetQualification', 'GetExperience', 'GetEducation', 'EmployerProfile'));
        }
    }

}
