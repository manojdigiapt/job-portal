<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\WebinarsModel;
use App\Model\UI\CandidateProfileModel;
use App\Model\UI\CandidateModel;

use GuzzleHttp\Client;

use Session;
use DB;

class FacultyController extends Controller
{
    public function Dashboard(){
        $title = "Faculty Dashboard";

        return view("Admin.faculty.faculty_dashboard", compact('title'));
    }


    public function AddWebinars(){
        $title = "Add Webinars";

        return view("Admin.faculty.add_webinars", compact('title'));
    }

    public function EditWebinars($Id){
        $title = "Edit Webinars";

        $GetWebinarsById = WebinarsModel::where('id', $Id)->first();

        return view("Admin.faculty.edit_webinars", compact('title', 'GetWebinarsById'));
    }

    public function CandidateWebinarsList(){
        $title = "Webinars list";

        $GetFacultyId = Session::get('FacultyId');

        $GetWebinars = WebinarsModel::where('faculty_id', $GetFacultyId)
                        ->where('category', 1)
                        ->get();

        // echo json_encode($GetWebinars);
        // exit;
        
        return view("Admin.faculty.webinars_list", compact('title', 'GetWebinars'));
    }


    public function TraineeWebinarsList(){
        $title = "Webinars list";

        $GetFacultyId = Session::get('FacultyId');

        $GetWebinars = WebinarsModel::where('faculty_id', $GetFacultyId)
                        ->where('category', 2)
                        ->get();

        // echo json_encode($GetWebinars);
        // exit;
        
        return view("Admin.faculty.webinars_list", compact('title', 'GetWebinars'));
    }


    public function ArticleWebinarsList(){
        $title = "Webinars list";

        $GetFacultyId = Session::get('FacultyId');

        $GetWebinars = WebinarsModel::where('faculty_id', $GetFacultyId)
                        ->where('category', 3)
                        ->get();

        // echo json_encode($GetWebinars);
        // exit;
        
        return view("Admin.faculty.webinars_list", compact('title', 'GetWebinars'));
    }


    public function WebinarsList(){
        $title = "Webinars list";

        $GetFacultyId = Session::get('FacultyId');

        $GetWebinars = WebinarsModel::where('faculty_id', $GetFacultyId)->get();

        // echo json_encode($GetWebinars);
        // exit;
        
        return view("Admin.faculty.webinars_list", compact('title', 'GetWebinars'));
    }

    public function PostWebinars(Request $request){

        // $client = new Client();
            // $GetNews = json_decode(file_get_contents('https://newsapi.org/v2/everything?q=gst&apiKey=514385d001164438a7688424ed2f5e6a'), true);

            // $GetNews = json_decode(file_get_contents('http://api.zoom.us/v2/users/'), true);

//             $url = 'http://api.zoom.us/v2/users/ghw20-7pTFObP89-NjVDpg/meetings';
// $options = array('http' => array(
//     'method'  => 'POST',
//     'header' => 'Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOm51bGwsImlzcyI6IkVsbDRDc2R0VDQyWUEzTXYzZGpMblEiLCJleHAiOjE1NzIzNDI2MjgsImlhdCI6MTU3MTczNzgyOH0.ZKzdn5AQt_tS7l3OfSH2P6hX1ieC1s0WzuXUxMeMn_o',
//     'contentType: application/json',
// ));
// $context  = stream_context_create($options);
// $response = json_decode(file_get_contents($url, false, $context));


// $url = 'http://api.zoom.us/v2/users/ghw20-7pTFObP89-NjVDpg/meetings';
// $data = array (
//     'uuid' => 'n0uAx/XtQre8q7y23zX2xA==',
//     'id' => 965092621,
//     'host_id' => 'ghw20-7pTFObP89-NjVDpg',
//     'topic' => 'Test API',
//     'type' => 2,
//     'status' => 'waiting',
//     'start_time' => '2019-10-23T11:36:50Z',
//     'duration' => 90,
//     'timezone' => 'Asia/Kolkata',
//     'created_at' => '2019-10-23T05:36:50Z',
//     'start_url' => 'https://zoom.us/s/965092621?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJnaHcyMC03cFRGT2JQODktTmpWRHBnIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6ImF3MSIsImNsdCI6MCwic3RrIjoiZXRvRlUwZ0l0elg0djkySS1nbzRJUVo3dGN1VGloOEVMUzcxZEFSZXoydy5CZ1VzTkRobmRDOHliV1kzYnpKRVFrZzVMMmt4VEZWRU1FaEtOR2d4VUdWblRWbHpRMHh3Ym5CRk9EUnFUVDFBTTJFeU1USTFOVFJpWW1JeU9UTXlPR0prTmpKalpqWXlNbUU1T1daaU9HVmlNRGt5TUdFME5UTTVZakUzTnpWak56UTFaVE5qTldFM09UbGpaVGt6T0FBTU0wTkNRWFZ2YVZsVE0zTTlBQU5oZHpFIiwiZXhwIjoxNTcxODE2MjEwLCJpYXQiOjE1NzE4MDkwMTAsImFpZCI6ImxBbXIwYTlPU0FpN3ZSYnZjaTdUbkEiLCJjaWQiOiIifQ.oAxsF-YRx_Ets90BH2PD6xjKHmQbPiUeF6HGdJ6B_2g',
//     'join_url' => 'https://zoom.us/j/965092621',
//     'settings' => 
//     array (
//       'host_video' => true,
//       'participant_video' => true,
//       'cn_meeting' => false,
//       'in_meeting' => false,
//       'join_before_host' => true,
//       'mute_upon_entry' => false,
//       'watermark' => false,
//       'use_pmi' => false,
//       'approval_type' => 2,
//       'audio' => 'both',
//       'auto_recording' => 'none',
//       'enforce_login' => false,
//       'enforce_login_domains' => '',
//       'alternative_hosts' => '',
//       'close_registration' => false,
//       'registrants_confirmation_email' => true,
//       'waiting_room' => false,
//       'global_dial_in_countries' => 
//       array (
//         0 => 'US',
//       ),
//       'global_dial_in_numbers' => 
//       array (
//         0 => 
//         array (
//           'country_name' => 'US',
//           'city' => 'New York',
//           'number' => '+1 6465588656',
//           'type' => 'toll',
//           'country' => 'US',
//         ),
//         1 => 
//         array (
//           'country_name' => 'US',
//           'city' => 'San Jose',
//           'number' => '+1 4086380968',
//           'type' => 'toll',
//           'country' => 'US',
//         ),
//       ),
//     ),
//   );

// $postdata = json_encode($data);

// $ch = curl_init($url);
// curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
// curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
// curl_setopt($ch, CURLOPT_POST, 1);
// curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
// curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
// curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
// curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
// 'method'  => 'POST',
//     'header' => 'Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOm51bGwsImlzcyI6IkVsbDRDc2R0VDQyWUEzTXYzZGpMblEiLCJleHAiOjE1NzIzNDI2MjgsImlhdCI6MTU3MTczNzgyOH0.ZKzdn5AQt_tS7l3OfSH2P6hX1ieC1s0WzuXUxMeMn_o'));
// $result = curl_exec($ch);
// curl_close($ch);

// print_r ($result);

//             // echo json_encode($result);
//             exit;


            

        $Webinars = new WebinarsModel();

        $GetFacultyId = Session::get('FacultyId');

        $GetTopicName = $request->web_topic;

        $CheckWebinars = WebinarsModel::where('topic_name', $GetTopicName)->first();

        if($CheckWebinars == null){
            $Webinars->faculty_id = $GetFacultyId;
            $Webinars->topic_name = $GetTopicName;
            $Webinars->speaker_name = $request->speaker;
            $Webinars->webinar_date = $request->web_date;
            $Webinars->webinar_type = $request->web_type;
            $Webinars->price = $request->price;
            $Webinars->category = $request->web_category;
            $Webinars->duration = $request->web_duration;
            $Webinars->start_time = $request->start_timing;
            $Webinars->end_time = $request->end_timing;
            $Webinars->description = $request->web_description;
            $Webinars->host_url = "https://zoom.us/j/2462442718";

            $extension = $request->file('file')->getClientOriginalExtension();
            $dir = 'webinar_featured_image/';
            $filename = uniqid() . '_' . time() . '.' . $extension;
            $request->file('file')->move($dir, $filename);

            $Webinars->featured_image = $filename;

            $InsertWebinars = $Webinars->save();
            if($InsertWebinars){
                return response()->json(array(
                    "error"=>FALSE,
                    "message"=> "Webinars added successfully"
                ));
            }else{
                return response()->json(array(
                    "error"=>TRUE,
                    "message"=> "failed"
                ));
            }
        }
        
    }


    public function UpdateWebinars(Request $request){

        $GetId = $request->WebId;

        $Webinars = WebinarsModel::where('id', $GetId)->first();

        $GetFacultyId = Session::get('FacultyId');

        $GetTopicName = $request->web_topic;

        $Webinars->faculty_id = $GetFacultyId;
        $Webinars->topic_name = $GetTopicName;
        $Webinars->speaker_name = $request->speaker;
        $Webinars->webinar_date = $request->web_date;
        $Webinars->webinar_type = $request->web_type;
        $Webinars->price = $request->price;
        $Webinars->category = $request->web_category;
        $Webinars->duration = $request->web_duration;
        $Webinars->start_time = $request->start_timing;
        $Webinars->end_time = $request->end_timing;
        $Webinars->description = $request->web_description;

        $File = $request->image;
            // $UpdateCandidateProfile = CandidateProfileModel::findOrFail($GetCandidateId);
            // echo json_encode($File);
            // exit;
        if($File == "undefined" || null){
            $filename = $Webinars->featured_image;
        }else{
            $extension = $request->file('image')->getClientOriginalExtension();
            $dir = 'webinar_featured_image/';
            $filename = uniqid() . '_' . time() . '.' . $extension;
            $request->file('image')->move($dir, $filename);

            
        }

        $Webinars->featured_image = $filename;

        $InsertWebinars = $Webinars->save();
        if($InsertWebinars){
            return response()->json(array(
                "error"=>FALSE,
                "message"=> "Webinars updated successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=> "failed"
            ));
        }
        
    }


    public function DeleteWebinars($Id){
        $DeleteWebinars = WebinarsModel::where('id', $Id)->delete();
        if($DeleteWebinars){
            return response()->json(array(
                "error"=>FALSE,
                "message"=>"Webinars deleted successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Failed"
            ));
        }
    }



    public function GetParticpants($Id){
        $title = "Get Participants";

        $GetParticipants = DB::table('candidate')
                            ->select('candidate.*', 'candidate_profile.profile_pic')
                            ->join('candidate_profile', 'candidate_profile.candidate_id', '=', 'candidate.id')
                            ->join('webinars_applied', 'webinars_applied.candidate_id', '=', 'candidate.id')
                            ->where('webinars_applied.webinar_id', $Id)
                            ->get();

        echo json_encode($GetParticipants);
        // exit;

        // return view("Admin.faculty.view_participants", compact('title', 'GetParticipants'));
    }
}
