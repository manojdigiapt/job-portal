<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\UI\CandidateModel;
use App\Model\UI\EmployerModel;
use Illuminate\Support\Facades\Hash;
use App\Model\Admin\QualificationModel;
use App\Model\UI\CandidateResume;
use App\Model\Admin\JobTitleModel;

use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailtoFresher;

use DB;
use Excel;
use Session;
use DateTime;

class UsersController extends Controller
{
    public function ExperiencedUsersList(){
        $title = "Experienced Users List";

        $GetUsers = DB::table('candidate')
                    ->join('candidate_profile', 'candidate_profile.candidate_id', '=', 'candidate.id')
                    ->where('type', 1)
                    ->orderBy('candidate.created_at', 'desc')
                    ->paginate(15);

        return view("Admin.users.users_list", compact('title', 'GetUsers'));
    }

    public function FresherUsersList(){
        $title = "Fresher Users List";

        $GetUsers = DB::table('candidate')
                    ->join('candidate_profile', 'candidate_profile.candidate_id', '=', 'candidate.id')
                    ->where('type', 2)
                    ->orderBy('candidate.created_at', 'desc')
                    ->paginate(15);

        return view("Admin.users.users_list", compact('title', 'GetUsers'));
    }

    public function CAUsersList(){
        $title = "CA Users List";

        $GetUsers = DB::table('candidate')
                    ->join('candidate_profile', 'candidate_profile.candidate_id', '=', 'candidate.id')
                    ->where('type', 3)
                    ->orderBy('candidate.created_at', 'desc')
                    ->paginate(15);

        return view("Admin.users.users_list", compact('title', 'GetUsers'));
    }

    public function UsersAppliedJobList($Id){
        $title = "Users Applied Job List";

        $GetCandidate = CandidateModel::where('id', $Id)->first();

        $GetJobs = DB::table('jobs_applied')
                    ->select('job_title.name AS JobTitle', 'employer.company_name', 'jobs_applied.created_at', 'jobs.slug', 'employer.slug AS CompanySlug', 'jobs.id')
                    ->join('candidate', 'jobs_applied.candidate_id', '=', 'candidate.id')
                    ->join('jobs', 'jobs_applied.jobs_id', '=', 'jobs.id')
                    ->join('job_title', 'jobs.title', '=', 'job_title.id')
                    ->join('employer', 'jobs.employer_id', '=', 'employer.id')
                    ->where("jobs_applied.candidate_id", $Id)
                    ->get();

        return view("Admin.users.user_applied_jobs", compact('title', 'GetJobs', 'GetCandidate'));
    }


    public function EmployerJobList($Id){
        $title = "CA Firm Job List";

        $GetEmployer = EmployerModel::where('id', $Id)->first();

        $GetJobs = DB::table('jobs')
                    ->select('job_title.name AS JobTitle', 'employer.company_name', 'jobs.created_at', 'jobs.slug', 'employer.slug AS CompanySlug', 'jobs.id', 'jobs.employee_type')
                    ->join('employer', 'jobs.employer_id', '=', 'employer.id')
                    ->join('job_title', 'jobs.title', '=', 'job_title.id')
                    ->where("jobs.employer_id", $Id)
                    ->where('jobs.employee_type', '!=', 3 )
                    ->get();
        // echo json_encode($GetJobs);
        // exit;

        return view("Admin.users.view_employer_jobs", compact('title', 'GetJobs', 'GetEmployer'));
    }

    public function EmployerTrainingList($Id){
        $title = "CA Firm Internships List";

        $GetEmployer = EmployerModel::where('id', $Id)->first();

        $GetTraining = DB::table('training')
                    ->select('training_title.title AS TrainingTitle', 'employer.company_name', 'training.created_at', 'training.slug', 'employer.slug AS CompanySlug', 'training.id')
                    ->join('employer', 'training.employer_id', '=', 'employer.id')
                    ->join('training_title', 'training.title', '=', 'training_title.id')
                    ->where("training.employer_id", $Id)
                    ->get();

        return view("Admin.users.view_employer_training", compact('title', 'GetTraining', 'GetEmployer'));
    }


    public function EmployerCAArticleshipsList($Id){
        $title = "CA Firm CA Articleships List";
        // echo $Id;
        // exit;
        $GetEmployer = EmployerModel::where('id', $Id)->first();

        $GetJobs = DB::table('jobs')
                        ->select('employer.company_name', 'jobs.created_at', 'jobs.slug', 'employer.slug AS CompanySlug', 'jobs.id')
                        ->join('employer', 'jobs.employer_id', '=', 'employer.id')
                        ->where("jobs.employer_id", $Id)
                        ->where('jobs.employee_type', 3)
                        ->get();

        // echo json_encode($GetJobs);
        // exit;

        return view("Admin.users.view_employer_ca_articleships", compact('title', 'GetJobs', 'GetEmployer'));
    }


    public function GetAppliedCandidates($Id){
        $title = "Applied Candidate List";

        // $GetJobs = DB::table('jobs')
        //             ->join('job_title', 'job_title.id', '=', 'jobs.title')
        //             ->where('jobs.id', $Id)
        //             ->first();

        $GetUsers = DB::table('jobs_applied')
                    ->select('candidate.name', 'candidate.email', 'candidate.mobile', 'jobs_applied.created_at', 'candidate.type', 'candidate.id')
                    ->join('candidate', 'jobs_applied.candidate_id', '=', 'candidate.id')
                    // ->join('job_title', 'jobs.title', '=', 'job_title.id')
                    ->where("jobs_applied.jobs_id", $Id)
                    ->get();
        // echo json_encode($GetJobs);
        // exit;

        return view("Admin.users.applied_candidates", compact('title', 'GetUsers'));
    }


    public function GetAppliedTrainees($Id){
        $title = "Applied Trainees List";

        $GetJobs = DB::table('training')
                    ->join('training_title', 'training_title.id', '=', 'training.title')
                    ->where('training.id', $Id)
                    ->first();

        $GetUsers = DB::table('jobs_applied')
                    ->select('candidate.name', 'candidate.email', 'candidate.mobile', 'jobs_applied.created_at', 'candidate.type', 'candidate.id')
                    ->join('candidate', 'jobs_applied.candidate_id', '=', 'candidate.id')
                    // ->join('job_title', 'jobs.title', '=', 'job_title.id')
                    ->where("jobs_applied.training_id", $Id)
                    ->get();

        return view("Admin.users.applied_trainees", compact('title', 'GetUsers', 'GetJobs'));
    }


    public function EmployerList(){
        $title = "CA Firm List";

        $GetUsers = EmployerModel::orderBy('created_at', 'desc')->get();

        return view("Admin.users.employer_list", compact('title', 'GetUsers'));
    }


    public function GetUserInfoById($Id){
        
        $GetUsersInfo = DB::select("SELECT * FROM candidate, candidate_profile WHERE candidate_profile.candidate_id = candidate.id AND candidate.id = $Id");

        $GetExperiencePosition = DB::table('resume')
                                ->select('job_title.name', 'resume.name AS CompanyName')
                                ->where('candidate_id', $Id)
                                ->join('job_title', 'job_title.id', '=', 'resume.department')
                                ->where('worked_till', 1) 
                                ->where('type', 2)
                                ->first();

        $GetGender = "";
        $GetMarialStatus = "";
        $GetExperience = "";
        $GetGrossSalary = "";

        $GetEducationDetails = "";

        $GetPositions = "";
        $ShowExperienceDetails = "";

        foreach($GetUsersInfo as $Users){

            if($Users->type == 1){
                $ShowExperienceDetails = "
                <hr class='user-details-hr'>
                      <br>
                      <h3 class='userprofile-head'>Experience</h3>
                <table class='table table-hover'>
                  <thead>
                    <tr>
                      <th class='userprofile-table-head'>Designation</th>
                      <th class='userprofile-table-head'>Company name</th>
                      <th class='userprofile-table-head'>Experience</th>
                    </tr>
                  </thead>
                  <tbody id='ExperienceDetails'>
                                   
                                </tbody>
                </table>";
            }else{
                $ShowExperienceDetails = "";
            }


            if($Users->type == 1){
                $GetPositions = $GetExperiencePosition->name." <span style='color: #000;'>at</span> ". $GetExperiencePosition->CompanyName;
            }elseif($Users->type == 2){
                $GetPositions = "Fresher";
            }elseif($Users->type == 3){
                $GetPositions = "CA";
            }

            if($Users->gender == 1){
                $GetGender = "Male";
            }else{
                $GetGender = "Fe Male";
            }

            if($Users->marital_status == 1){
                $GetMarialStatus = "Married";
            }else{
                $GetMarialStatus = "Unmarried";
            }

            if(isset($Users->experience)){
                $TotalExperience = $Users->experience;
                $GetTotalExperience = explode(',', $TotalExperience);

                $GetExperience =  $GetTotalExperience[0].' Years '.$GetTotalExperience[1].' Months';
            }
            
            if($Users->gross_salary){
                $GetGrossSalary = "Rs. ".$Users->gross_salary."";
            }else{
                $GetGrossSalary = "";
            }

            // foreach($GetEducation as $Education){
            //     $GetEducationDetails = "<tr><td>".$Education->name."</td></tr>";
            // }
            
            // echo $GetEducationDetails;
            // exit;
            $GetProfilePics = "";

            if(isset($Users->profile_pic)){
                $GetProfilePics = "<img src='/candidate_profile/".$Users->profile_pic."' class='userprofile-details-pic' alt=''/>";
            }else{
                $GetProfilePics = "<img src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS52y5aInsxSm31CvHOFHWujqUx_wWTS9iM6s7BAm21oEN_RiGoog' class='userprofile-details-pic' alt=''/>";
            }

            $GetUsers = "<div class='container emp-profile width100'>
            <form method='post'>
                <div class='row'>
                    <div class='col-md-4'>
                        <div class='profile-img text-left'>
                            ".$GetProfilePics."
                        </div>

                        <div class='profile-work pad-left0per'>
                            <p class='userprofile-details userprofile-details-mar-top'> ".$Users->desired_location."</p>
                            <p class='userprofile-details userprofile-details-mar-top'> ".$GetExperience."</p>
                            <p class='userprofile-details userprofile-details-mar-top'>".$GetGrossSalary."</p>
                            <p class='userprofile-details userprofile-details-mar-top'>".$Users->mobile."</p>
                            <p class='userprofile-details userprofile-details-mar-top'> ".$Users->email."</p>
                        </div>
                    </div>
                    <div class='col-md-7'>
                        <div class='profile-head'>
                                    <h5 class='userprofile-name'>
                                        ".$Users->name."
                                    </h5>
                                    <h6 class='userprofile-position'>
                                        ".$GetPositions."
                                    </h6>
                            <h4 class='userprofile-head'>Personal Details</h4>
                            <div class='row'>
                                <div class='col-md-3'>
                                    <h4 class='candidate-work-experience-head userprofile-subhead'>Date of birth
                            </h4>
                                    <p class='candidate-work-experience-p'>".date('d M Y', strtotime($Users->date_of_birth))."
                          </p>
                                          </div>
                                <div class='col-md-3'>
                                    <h4 class='candidate-work-experience-head userprofile-subhead'>Gender
                            </h4>
                                                <p class='candidate-work-experience-p'>
                                          ".$GetGender."
                                      </p>
                                          </div>
                                <div class='col-md-3'>
                                    <h4 class='candidate-work-experience-head userprofile-subhead'>Marital Status
                            </h4>
                                                <p class='candidate-work-experience-p'>
                                            ".$GetMarialStatus."
                                      </p>
                                          </div>
                                <div class='col-md-3'>
                                    <h4 class='candidate-work-experience-head userprofile-subhead'>Father's Name
                            </h4>
                                                <p class='candidate-work-experience-p'>".$Users->father_name."
                          </p>
                                          </div>
                            </div>

                            <div class='row'>
                                <div class='col-md-6'>
                                    <h4 class='candidate-work-experience-head userprofile-subhead'>Address
                          </h4>
                                              <p class='candidate-work-experience-p'>".$Users->address."
                          </p>
                                          </div>
                      </div>

                      <div class='row'>
                        <div class='col-md-4'>
                          <h4 class='candidate-work-experience-head userprofile-subhead'>State
                          </h4>
                                    <p class='candidate-work-experience-p'>".$Users->state."
                          </p>
                                  </div>
                        <div class='col-md-4'>
                            <h4 class='candidate-work-experience-head userprofile-subhead'>City
                            </h4>
                                        <p class='candidate-work-experience-p'>".$Users->city."
                            </p>
                                      </div>
                          <div class='col-md-4'>
                              <h4 class='candidate-work-experience-head userprofile-subhead'>Zip code
                              </h4>
                                            <p class='candidate-work-experience-p'>".$Users->zip_code."
                              </p>
                                          </div>
                      </div>

                      <h3 class='userprofile-head'>Education</h3>

                      <table class='table table-hover'>
                        <thead>
                          <tr>
                            <th class='userprofile-table-head'>Degree</th>
                            <th class='userprofile-table-head'>University / Board</th>
                            <th class='userprofile-table-head'>Year of passed</th>
                            <th class='userprofile-table-head'>Percentage</th>
                          </tr>
                        </thead>
                        <tbody id='EducationDetails'>
                                
                                            </tbody>
                      </table>
                        
                      

                      ".$ShowExperienceDetails."
                      
                        </div>
                    </div>
                    <div class='col-md-1'>
                        <a class='profile-edit-btn close text-right' name='btnAddMore' data-dismiss='modal' >Close</a>
                    </div>
                </div>
            </form>           
        </div>";

        echo $GetUsers;
        }
        

    }


    public function GetUserEducationDetails($Id){
        $GetEducation = DB::table('resume')
                            ->select('resume.*', 'qualifications.qualification')
                            ->join('qualifications', 'qualifications.id', '=', 'resume.department')
                            ->where('type', 1)
                            ->where('resume.candidate_id', $Id)
                            ->orderBy('id', 'DESC')
                            ->get();

        foreach($GetEducation as $Education){
            $GetEducationDetails = "<tr><td>".$Education->qualification."</td><td>".$Education->name."</td><td>".$Education->from_year."</td><td>".$Education->percentage."</td></tr>";

            echo $GetEducationDetails;
        }

        
    }


    public function GetUserExperienceInfoById($Id){
        $GetExperience =    DB::table('resume')
                            ->select('resume.*', 'job_title.name AS JobTitle')
                            ->where('candidate_id', $Id)
                            ->where('type', 2)
                            ->join('job_title', 'job_title.id', '=', 'resume.department')
                            ->orderBy('id', 'DESC')
                            ->get();

        //   echo json_encode($Id);
        //   exit;                  

        foreach($GetExperience as $Experience){
            $GetExperience = "";

            if($Experience->from_year && $Experience->from_month){
                $d1 = new DateTime($Experience->from_year."-".$Experience->from_month);
  
                if($Experience->to_year && $Experience->to_month){
                $d2 = new DateTime($Experience->to_year."-".$Experience->to_month);
                }
                $diff = $d2->diff($d1);
                        
              }

              if($Experience->from_year && $Experience->from_month){
        if($Experience->to_year && $Experience->to_month){
            $GetExperience = $diff->y." Years ".$diff->m." Months";
        }
    }

            $GetExperienceDetails = "<tr><td>".$Experience->JobTitle."</td><td>".$Experience->name."</td><td>".$GetExperience."</td></tr>";

            echo $GetExperienceDetails;
        }

        
    }


    public function importExcel(Request $request)
    {
        $Candidate = new CandidateModel();
        if($request->hasFile('import_file')){
            Excel::load($request->file('import_file')->getRealPath(), function ($reader) {
                foreach ($reader->toArray() as $key => $row) {
                    $Name = $row['name1']." ". $row['name2'];

                    $Email = $row['email'];
                    $Password = "Test@123";

                    $CheckCandidate = CandidateModel::where('email', $Email)->first();

                    if (!$CheckCandidate) {
                        $data['name'] = $Name;
                        $data['password'] = Hash::make("Test@123");
                        $data['email'] = $row['email'];
                        $data['mobile'] = $row['mobile'];
                        $data['type'] = "2";
                        // $Candidate['name'] = $row['name'];
                        // $Candidate['password'] = $row['password'];
                        // $Candidate['email'] = $row['email'];
                        // $Candidate['mobile'] = $row['mobile'];
    
                        if(!empty($data)) {
                            // $Candidate->save();
                           $Id = DB::table('candidate')->insertGetId($data);
    
                           $GetGender = "";
                           if($row['gender'] == "female"){
                            $GetGender = "2";
                           }else{
                            $GetGender = "1";
                           }
    
                            $data1['candidate_id'] = $Id;
                            $data1['date_of_birth'] = $row['dob'];
                            $data1['gender'] = $GetGender;
                            $data1['state'] = $row['state'];
                            $data1['city'] = $row['location'];
                            $data1['address'] = $row['sub_location'];
                            $data1['profile_completion'] = "35";
    
                            DB::table('candidate_profile')->insert($data1);
    
                           
                            $GetQualification = QualificationModel::where('qualification', $row['qualification'])->first();
    
                            $data2['candidate_id'] = $Id;
                            $data2['title'] = $GetQualification->id;
                            $data2['name'] = $row['university'];
                            $data2['percentage'] = $row['percentage'];
                            $data2['to_year'] = $row['year'];
                            $data2['type'] = "1";
                            // $data2['address'] = $row['sub_location'];
    
                            DB::table('resume')->insert($data2);
    
                            Mail::to($Email)->send(new SendMailtoFresher($Email));
                            
                            // echo $Id;
                        }
                    }else{
                        $user = CandidateModel::where('email', $Email)->first();

                        return $user;
                        // echo "No Users are there";
                        // exit;
                    }

                    
                }
            });
        }

        Session::put('success', 'Youe file successfully import in database!!!');

        return back();
    }
}
