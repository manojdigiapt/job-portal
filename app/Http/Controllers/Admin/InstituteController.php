<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

use App\Model\Admin\InstituteModel;

use App\Mail\InstituteVerifyEmail;

use Session;
use DB;

class InstituteController extends Controller
{
    public function InstituteList(){
        $title = "Institute list";

        $GetInstitute = InstituteModel::get();

        // echo json_encode($GetWebinars);
        // exit;
        
        return view("Admin.institute.institute_list", compact('title', 'GetInstitute'));
    }

    public function InstituteLogin(Request $request){
        $Email = $request->email;
        $Password = $request->password;

        $CheckCredentials = InstituteModel::where('email', $Email)->first();
        
        // echo json_encode($CheckCredentials);
        // exit;

        if($CheckCredentials){
            $CheckAdminPassword = Hash::check($Password, $CheckCredentials->password);
            
            if($CheckAdminPassword == true){
                // $GetRole = AdminModel::where('email', $Email)
                //                         ->where('role_id', 4)
                //                         ->first();
                
                // if($GetRole['role_id'] == 4){
                    if (Auth::guard('institute')->attempt(['email' => $Email, 'password' => $Password])) {
                        $request->session()->put('InstituteName', Auth::guard('institute')->user()->name);
                        $request->session()->put('InstituteEmail', Auth::guard('institute')->user()->email);
                        $request->session()->put('InstituteId', Auth::guard('institute')->user()->id);
                        return response()->json(array(
                            "error"=>FALSE,
                            "message"=> "Login successfully",
                            "type" => 4
                        ));
                    }else{
                        return response()->json(array(
                            "error"=>TRUE,
                            "message"=> "Please check your credentials."
                        ));
                    }
                // }
            }else{
                return response()->json(array(
                    "error"=>TRUE,
                    "message"=> "Please check your credentials."
                ));
            }   
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=> "Please check your credentials."
            ));
        }
    }

    public function Dashboard(){
    //    echo  Auth::guard('institute')->user()->name;
    //    exit;
        $title = "Institute Dashboard";

        return view("Admin.institute.institute_dashboard", compact('title'));
    }

    public function CandidateList(){
        $title = "Candidate list";

        $GetInstituteId = Session::get('InstituteId');

        $GetCandidates = DB::table('institute')
                        ->join('candidate_profile', 'candidate_profile.institute_id', '=', 'institute.id')
                        ->join('candidate', 'candidate.id', '=', 'candidate_profile.candidate_id')
                        ->where('institute.id', $GetInstituteId)
                        ->select('candidate.name', 'candidate.email', 'candidate.mobile','candidate.created_at')
                        ->get();

        // echo json_encode($GetCandidates);
        // exit;
        
        return view("Admin.institute.candidate_list", compact('title', 'GetCandidates'));
    }

    // public function SearchPromoCode(Request $request){
    //     $title = $request->term;

    //     $GetTrainings = DB::select("SELECT institute.referral_code FROM institute WHERE AND referral_code LIKE '%$title%' ORDER BY referral_code");
    //     echo json_encode($GetTrainings);

    // }

    public function SearchPromoCode(Request $request){

        $query = $request->get('term','');
        
        $products=InstituteModel::where('referral_code','LIKE',$query.'%')
                                    ->orderBy('referral_code', 'ASC')
                                    ->get();
        
        $data=array();
        foreach ($products as $product) {
                $data[]=array('value'=>$product->referral_code,'id'=>$product->id);
        }
        if(count($data))
             return $data;
        else
            return ['value'=>'No Result Found','id'=>''];
    }

    public function AddInstitute(Request $request){
        $AdminInstitute = new InstituteModel();

        $Name = $request->name;

        $AdminInstitute->name = $Name;
        

        $GenerateReferralCode = str_replace(' ', '', $Name);
        $AdminInstitute->password = Hash::make($request->password);
        $AdminInstitute->email = $request->email;
        $AdminInstitute->mobile = $request->mobile;
        $AdminInstitute->address = $request->address;
        $AdminInstitute->role = 1;
        $AdminInstitute->referral_code = substr($GenerateReferralCode, 0, 4).rand(10,100);

        // echo substr($GenerateReferralCode, 0, 4).rand(10,100);
        // exit;

        $CheckEmail = InstituteModel::where('email', $request->email)
                        // ->where('role_id', 4)
                        ->first();
        
        if($CheckEmail){
            return response()->json(array(
                "error"=>TRUE,
                "message"=> "Email id is already exist."
            ));
        }else{
            $InsertInstitute = $AdminInstitute->save();
            if($InsertInstitute){
                return response()->json(array(
                    "error"=>FALSE,
                    "message"=> "Institute added successfully."
                ));
            }else{
                return response()->json(array(
                    "error"=>TRUE,
                    "message"=> "Failed"
                ));
            }
        }
        
    }

    public function GetInstitute($id){
        $GetInstitute = InstituteModel::findorFail($id);
        echo json_encode($GetInstitute);
    }

    public function UpdateInstitute(Request $request){
        $Id = $request->id;

        $AdminInstitute = InstituteModel::where('id', $Id)->first();

        $Name = $request->name;

        $AdminInstitute->name = $Name;
        

        $GenerateReferralCode = str_replace(' ', '', $Name);
        $AdminInstitute->password = Hash::make($request->password);
        $AdminInstitute->email = $request->email;
        $AdminInstitute->mobile = $request->mobile;
        $AdminInstitute->address = $request->address;
        $AdminInstitute->referral_code = substr($GenerateReferralCode, 0, 4).rand(10,100);

        // echo substr($GenerateReferralCode, 0, 4).rand(10,100);
        // exit;
        
        $InsertInstitute = $AdminInstitute->save();
        if($InsertInstitute){
            return response()->json(array(
                "error"=>FALSE,
                "message"=> "Institute added successfully."
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=> "Failed"
            ));
        }
        
    }

    public function DeleteInstitute($Id){
        $DeleteInstitute = InstituteModel::where('id', $Id)->delete();
        if($DeleteInstitute){
            return response()->json(array(
                "error"=>FALSE,
                "message"=>"Institute deleted successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Failed"
            ));
        }
    }


    public function InstituteForgotPassword(Request $request){
        $Email = $request->forgotEmail;

        $CheckEmail = InstituteModel::where('email', $Email)->first();

        if($CheckEmail){
            $token = str_random(64);
            $request->session()->put('GetToken', $token);
            $GetToken = Session::get('GetToken');

            $CheckEmail->_token = $GetToken;
            $CheckEmail->save();

            $Id = $CheckEmail->id;
            Mail::to($Email)->send(new InstituteVerifyEmail($Id, $GetToken));

            return response()->json(array(
                "error"=>FALSE,
                "message"=>"Reset Link Send Successfully. Please Check Your Email."
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Email id is not registered."
            ));
        }
    }


    public function CheckResetPassword($id, $token){
        $title = "Reset Password";
        $GetId = $id;

        return view("Admin.institute.reset_password", compact('title', 'GetId'));
    }


    public function ResetPassword(Request $request){
        $GetId = $request->id;
        $GetNewPassword = $request->new_password;
        $GetConfirmPassword = $request->confirm_password;
        // echo json_encode($GetToken);
        // exit;
        if($GetNewPassword != $GetConfirmPassword){
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Password does not match"
            ));
        }else{
            $ChangePassword = InstituteModel::findorFail($GetId);
            $ChangePassword->password = Hash::make($GetConfirmPassword);
            $ChangePassword->save();
            return response()->json(array(
                "error"=>FALSE,
                "message"=>"Password changed successfully"
            ));
        }
    }
}
