<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\AdminModel;
use App\Model\Admin\SEOModel;
use App\Model\Admin\FacultyModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use App\Model\UI\CandidateModel;
use App\Model\UI\EmployerModel;
use App\Model\UI\CandidateProfileModel;

use DB;


class AdminController extends Controller
{
    public function Adminlogout(Request $request) {
        // Auth::logout();
        Auth::guard('super_admin')->logout();
        return redirect('/Admin/Login');
    }

    public function Facultylogout(Request $request) {
        // Auth::logout();
        Auth::guard('faculty')->logout();
        return redirect('/Admin/Login');
    }

    public function Institutelogout(Request $request) {
        // Auth::logout();
        Auth::guard('institute')->logout();
        return redirect('/Institute');
    }

    public function login(){
        $title = "Admin Login";

        return view("Admin.login", compact('title'));
    }

    public function Institute(){
        $title = "Institute Login";

        return view("Admin.authendication.institute", compact('title'));
    }

    public function Dashboard(){
        $title = "Admin Dashboard";

        $GetExperiencedUsersCount = DB::table('candidate')
                        ->join('candidate_profile', 'candidate_profile.candidate_id', '=', 'candidate.id')
                        ->where('candidate.type', 1)
                        ->count();

        $GetFresherUsersCount = DB::table('candidate')
                        ->join('candidate_profile', 'candidate_profile.candidate_id', '=', 'candidate.id')
                        ->where('candidate.type', 2)
                        ->count();

        $GetCAUsersCount = DB::table('candidate')
                        ->join('candidate_profile', 'candidate_profile.candidate_id', '=', 'candidate.id')
                        ->where('candidate.type', 3)
                        ->count();

        $GetJobsAppliedCount = DB::table('jobs_applied')
                        ->select(DB::raw("count(jobs_id) as count"))
                        ->get();

        // echo json_encode($GetJobsAppliedCount);
        // exit;

        $GetInternshipsAppliedCount = DB::table('jobs_applied')
                                    ->select(DB::raw("count(training_id) as count"))
                                    ->get();

                        
        
        $GetEmployerCount = DB::table('employer')
                        ->count();

        $GetFacultyCount = DB::table('faculty')
                        ->count();

        // $employer = EmployerModel::select(DB::raw("count(id) as count"))
        //                 ->orderBy("created_at")
        //                 ->groupBy(DB::raw("year(created_at)"))
        //                 ->get()->toArray();
        
        // $employer = array_column($employer, 'count');


        // $candidates = CandidateModel::select(DB::raw("count(id) as count"))
        //                 ->orderBy("created_at")
        //                 ->groupBy(DB::raw("year(created_at)"))
        //                 ->get()->toArray();
        
        // $candidates = array_column($candidates, 'count');


        // $candidatesmonthwise = CandidateModel::select(DB::raw("count(id) as count"))
        //                 ->orderBy("created_at")
        //                 ->groupBy(DB::raw("month(created_at)"))
        //                 // ->where(DB::raw("MONTH(created_at) = MONTH(CURRENT_DATE())"))
        //                 ->get()->toArray();
        
        // $candidatesmonthwise = array_column($candidatesmonthwise, 'count');
        
        $ExperienceCandidate = CandidateModel::where('type', 1)
                            ->orderBy('created_at', 'desc')
                                ->take(5)->get();

        $FresherCandidate = CandidateModel::where('type', 2)
                            ->orderBy('created_at', 'desc')
                                ->take(5)->get();

        $CACandidate = CandidateModel::where('type', 3)
                            ->orderBy('created_at', 'desc')
                                ->take(5)->get();

        $GetUsers = DB::table('jobs_applied')
                    ->select('candidate.name', 'candidate.email', 'candidate.mobile', 'jobs_applied.created_at', 'candidate.type', 'candidate.id')
                    ->join('candidate', 'jobs_applied.candidate_id', '=', 'candidate.id')
                    // ->join('job_title', 'jobs.title', '=', 'job_title.id')
                    ->orderBy('jobs_applied.created_at', 'desc')
                    ->get();
        // echo json_encode($candidatesmonthwise);
        // exit;

        return view("Admin.layouts.dashboard", compact('title', 'GetExperiencedUsersCount', 'GetFresherUsersCount', 'GetUsers', 'GetInternshipsAppliedCount', 'GetJobsAppliedCount', 'GetCAUsersCount', 'GetEmployerCount', 'GetFacultyCount','ExperienceCandidate', 'FresherCandidate', 'CACandidate'));
        // ->with('employer',json_encode($employer,JSON_NUMERIC_CHECK))->with('candidates',json_encode($candidates,JSON_NUMERIC_CHECK))->with('candidatesmonthwise',json_encode($candidatesmonthwise,JSON_NUMERIC_CHECK));
    }


    public function AdminLogin(Request $request){
        $Email = $request->email;
        $Password = $request->password;

        

        $CheckCredentials = AdminModel::where('email', $Email)->first();
        
        // echo json_encode($CheckCredentials);
        // exit;

        

        if($CheckCredentials){
            $CheckAdminPassword = Hash::check($Password, $CheckCredentials->password);
            
            if($CheckAdminPassword == true){
                $GetRole = AdminModel::where('email', $Email)
                                        // ->where('role_id', 1)
                                        ->first();
                
                if($GetRole['role_id'] == 1){
                    if (Auth::guard('super_admin')->attempt(['email' => $Email, 'password' => $Password])) {
                        $request->session()->put('AdminName', Auth::guard('super_admin')->user()->name);
                        $request->session()->put('AdminEmail', Auth::guard('super_admin')->user()->email);
                        $request->session()->put('AdminId', Auth::guard('super_admin')->user()->id);
                        return response()->json(array(
                            "error"=>FALSE,
                            "message"=> "Login successfully",
                            "type" => 1
                        ));
                    }else{
                        return response()->json(array(
                            "error"=>TRUE,
                            "message"=> "Please check your credentials."
                        ));
                    }
                }else if($GetRole['role_id'] == 2){
                    if (Auth::guard('sub_admin')->attempt(['email' => $Email, 'password' => $Password])) {
                        $request->session()->put('SubAdminName', Auth::guard('sub_admin')->user()->name);
                        $request->session()->put('SubAdminEmail', Auth::guard('sub_admin')->user()->email);
                        $request->session()->put('SubAdminId', Auth::guard('sub_admin')->user()->id);
                        return response()->json(array(
                            "error"=>FALSE,
                            "message"=> "Login successfully",
                            "type" => 2
                        ));
                    }else{
                        return response()->json(array(
                            "error"=>TRUE,
                            "message"=> "Please check your credentials."
                        ));
                    }
                }else if($GetRole['role_id'] == 3){
                    if (Auth::guard('faculty')->attempt(['email' => $Email, 'password' => $Password])) {
                        $request->session()->put('FacultyName', Auth::guard('faculty')->user()->name);
                        $request->session()->put('FacultyEmail', Auth::guard('faculty')->user()->email);
                        $request->session()->put('FacultyId', Auth::guard('faculty')->user()->id);
                        return response()->json(array(
                            "error"=>FALSE,
                            "message"=> "Login successfully",
                            "type" => 3
                        ));
                    }else{
                        return response()->json(array(
                            "error"=>TRUE,
                            "message"=> "Please check your credentials."
                        ));
                    }
                }else{
                    return response()->json(array(
                        "error"=>TRUE,
                        "message"=> "Please check your credentials."
                    ));
                }
            }else{
                return response()->json(array(
                    "error"=>TRUE,
                    "message"=> "Please check your credentials."
                ));
            }   
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=> "Please check your credentials."
            ));
        }
    }


    
    


    public function AddAdmins(Request $request){
        $AdminUsers = new AdminModel();

        $AdminUsers->name = $request->name;
        $AdminUsers->email = $request->email;
        $AdminUsers->role_id = $request->role;
        $AdminUsers->password = Hash::make($request->password);

        $CheckEmail = AdminModel::where('email', $request->email)->first();
        
        if($CheckEmail){
            return response()->json(array(
                "error"=>TRUE,
                "message"=> "Email id is already exist."
            ));
        }else{
            $InsertAdmins = $AdminUsers->save();
            if($InsertAdmins){
                return response()->json(array(
                    "error"=>FALSE,
                    "message"=> "Admin users added successfully."
                ));
            }else{
                return response()->json(array(
                    "error"=>TRUE,
                    "message"=> "Failed"
                ));
            }
        }
        
    }


    public function GetEditAdminUser($id){
        $GetAdminUsers = AdminModel::findorFail($id);
        echo json_encode($GetAdminUsers);
    }

    public function UpdateAdmins(Request $request){
        $AdminUsers = new AdminModel();
        $GetAdmins = AdminModel::where('id', $request->id)->first();

        $GetAdmins->name = $request->name;
        $GetAdmins->email = $request->email;
        $GetAdmins->role_id = $request->role;
        // $GetAdmins->password = Hash::make($request->password);

        $UpdateAdmins = $GetAdmins->save();
        if($UpdateAdmins){
            return response()->json(array(
                "error"=>FALSE,
                "message"=> "Admin users updated successfully."
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=> "Failed"
            ));
        }
        
    }

    public function DeleteAdminUsers($Id){
        $DeleteAdmins = AdminModel::where('id', $Id)->delete();
        if($DeleteAdmins){
            return response()->json(array(
                "error"=>FALSE,
                "message"=>"Admin deleted successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Failed"
            ));
        }
    }

    public function Adminlists(){
        $title = "Admin Users List";

        $GetAdmins = AdminModel::where('role_id', 2)->get();

        return view("Admin.layouts.admin_users", compact('title', 'GetAdmins'));
    }



    public function Facultyists(){
        $title = "Faculty List";

        $GetFaculty = AdminModel::
                        where('role_id', 3)
                        ->get();

        return view("Admin.faculty.faculty_list", compact('title', 'GetFaculty'));
    }


    public function SEO(){
        $title = "SEO";

        $GetSEO = SEOModel::first();

        // echo json_encode($GetSEO->id);
        // exit;

        return view("Admin.seo.add0rmodify_seo", compact('title', 'GetSEO'));
    }

    public function AddOrModifySEO(Request $request){
        $Id = $request->Id;

        // echo $Id;
        // exit;

        $CheckSeo = SEOModel::where('id', $Id)->first();

        if($CheckSeo){

            $CheckSeo->seo_name = $request->name;
            $CheckSeo->seo_description = $request->description;
            $CheckSeo->seo_keywords = $request->keywords;

            $CheckSeo->save();
            // echo "Update";
        }else{
            $SEO = new SEOModel();

            $SEO->seo_name = $request->name;
            $SEO->seo_description = $request->description;
            $SEO->seo_keywords = $request->keywords;

            $SEO->save();
            // echo "Insert";
        }

        return response()->json(array(
                "error"=>FALSE,
                "message" => "SEO Updated"
        ));

    }




    public function AddFaculty(Request $request){
        $AdminUsers = new AdminModel();

        $AdminUsers->name = $request->name;
        $AdminUsers->email = $request->email;
        $AdminUsers->role_id = "3";
        $AdminUsers->password = Hash::make($request->password);

        $CheckEmail = AdminModel::where('email', $request->email)
                            ->where('role_id', 3)
                        ->first();
        
        if($CheckEmail){
            return response()->json(array(
                "error"=>TRUE,
                "message"=> "Email id is already exist."
            ));
        }else{
            $InsertAdmins = $AdminUsers->save();
            if($InsertAdmins){
                return response()->json(array(
                    "error"=>FALSE,
                    "message"=> "Faculty users added successfully."
                ));
            }else{
                return response()->json(array(
                    "error"=>TRUE,
                    "message"=> "Failed"
                ));
            }
        }
        
    }



    public function UpdateFaculty(Request $request){
        $AdminUsers = new AdminModel();
        $GetAdmins = AdminModel::where('id', $request->id)->first();

        $GetAdmins->name = $request->name;
        $GetAdmins->email = $request->email;
        // $GetAdmins->password = Hash::make($request->password);

        $UpdateAdmins = $GetAdmins->save();
        if($UpdateAdmins){
            return response()->json(array(
                "error"=>FALSE,
                "message"=> "Admin users updated successfully."
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=> "Failed"
            ));
        }
        
    }
}
