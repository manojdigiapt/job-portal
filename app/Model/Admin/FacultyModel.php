<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class FacultyModel extends Model
{
    protected $table = 'faculty';

    protected $fillable = ['id', 'name', 'email', 'password', 'status'];
}
