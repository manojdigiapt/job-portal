<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class WebinarsAppliedModel extends Model
{
    protected $table = 'webinars_applied';

    protected $fillable = ['id', 'faculty_id', 'candidate_id', 'webinar_id', 'status'];
}
