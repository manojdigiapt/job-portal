<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class AdminModel extends Authenticatable
{
    protected $guard = 'super_admin';


    protected $table = 'admin';

    protected $fillable = ['id', 'name', 'email', 'password', 'role_id', 'mobile', 'address'];
}
