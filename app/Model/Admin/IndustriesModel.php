<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class IndustriesModel extends Model
{
    protected $table = 'industries';

    protected $fillable = ['id', 'name', 'status'];
}
