<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class LanguagesModel extends Model
{
    protected $table = 'languages';

    protected $fillable = ['id', 'languages', 'status'];
}
