<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class WebinarsModel extends Model
{
    protected $table = 'webinars';

    protected $fillable = ['id', 'faculty_id', 'topic_name', 'speaker_name', 'webinar_date', 'webinar_type', 'category', 'duration', 'timing', 'description', 'featured_image'];
}
