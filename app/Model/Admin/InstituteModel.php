<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Foundation\Auth\User as Authenticatable;

class InstituteModel extends Authenticatable
{
    protected $guard = 'institute';

    protected $table = 'institute';

    protected $fillable = ['id', 'name', 'password', 'email', 'mobile', 'address', 'referral_code'];
}
