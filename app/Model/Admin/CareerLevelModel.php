<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class CareerLevelModel extends Model
{
    protected $table = 'career_level';

    protected $fillable = ['id', 'career_level', 'status'];
}
