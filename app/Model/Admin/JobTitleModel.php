<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class JobTitleModel extends Model
{
    protected $table = 'job_title';

    protected $fillable = ['id', 'name', 'status'];
}
