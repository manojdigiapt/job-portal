<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class SEOModel extends Model
{
    protected $table = 'seo';

    protected $fillable = ['id', 'seo_name', 'seo_keyword', 'seo_description'];
}
