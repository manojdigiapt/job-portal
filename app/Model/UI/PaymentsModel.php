<?php

namespace App\Model\UI;

use Illuminate\Database\Eloquent\Model;

class PaymentsModel extends Model
{
    protected $table = "payments";

    protected $fillable = ['id', 'payment_id', 'payment_date', 'candidate_id', 'webinar_id', 'status'];
}
