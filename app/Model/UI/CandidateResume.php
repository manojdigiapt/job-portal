<?php

namespace App\Model\UI;

use Illuminate\Database\Eloquent\Model;

class CandidateResume extends Model
{
    protected $table = "resume";

    protected $fillable = ['candidate_id', 'title', 'name', 'department', 'from_year', 'from_month', 'to_year', 'to_month', '	worked_till', 'current_salary_lacs', 'current_salary_thousand', 'cv', 'description', 'notice_period', 'course_type', 'type'];
}
