<?php

namespace App\Model\UI;

use Illuminate\Database\Eloquent\Model;

class NewsAndEvents extends Model
{
    protected $table = "news_and_updates";

    protected $fillable = ['id', 'title', 'description', 'images', 'published', 'url'];
}
