<?php

namespace App\Model\UI;

use Illuminate\Database\Eloquent\Model;

class FavouriteModel extends Model
{
    protected $table = "favourite_jobs";

    protected $fillable = ['candidate_id', 'jobs_id', 'status'];
}
