<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailtoCandidate;

class RegisteredUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'registered:users';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send an email of registered users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $totalUsers = \DB::table('candidate')
                        ->select('candidate.email')
                        ->join('candidate_profile', 'candidate_profile.candidate_id', '=', 'candidate.id')
                        // ->whereRaw('Date(created_at) = CURDATE()')
                        ->where('candidate_profile.profile_completion', '<=', 90)
                        ->where('candidate.id', 28)
                        ->get();

        foreach($totalUsers as $Users){
            Mail::to($Users->email)->send(new SendMailtoCandidate());
        }

        
    }
}
