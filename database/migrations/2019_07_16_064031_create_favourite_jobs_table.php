<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFavouriteJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('favourite_jobs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('candidate_id')->unsigned()->nullable();
            $table->bigInteger('jobs_id')->unsigned()->nullable();
            $table->bigInteger('training_id')->unsigned()->nullable();
            $table->boolean('type')->nullable();
            $table->boolean('status')->nullable();
            
            $table->foreign('candidate_id')->references('id')->on('candidate');
            $table->foreign('jobs_id')->references('id')->on('jobs');
            $table->foreign('training_id')->references('id')->on('training');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('favourite_jobs');
    }
}
