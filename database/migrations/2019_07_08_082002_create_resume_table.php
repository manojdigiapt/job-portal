<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResumeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resume', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('candidate_id')->unsigned();
            $table->string('title')->nullable();
            $table->string('name')->nullable();
            $table->string('department')->nullable();
            $table->string('from_year')->nullable();
            $table->string('from_month')->nullable();
            $table->string('to_year')->nullable();
            $table->string('to_month')->nullable();
            $table->boolean('worked_till')->nullable();
            $table->string('current_salary_lacs')->nullable();
            $table->string('current_salary_thousand')->nullable();
            $table->string('cv')->nullable();
            // $table->string('percentage')->nullable();
            $table->longText('description')->nullable();
            $table->string('notice_period')->nullable();
            $table->boolean('course_type')->nullable();
            $table->string('percentage')->nullable();
            $table->boolean('type');

            $table->foreign('candidate_id')->references('id')->on('candidate');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resume');
    }
}
