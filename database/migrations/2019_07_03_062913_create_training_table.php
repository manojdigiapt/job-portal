<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrainingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('training', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('employer_id')->unsigned();
            $table->string('training_id')->nullable();
            $table->string('title');
            $table->string('slug')->nullable();
            $table->date('start_date');
            $table->string('duration');
            $table->string('stipend');
            $table->date('last_date_of_application');
            $table->boolean('training_completion');
            $table->longText('description');
            $table->boolean('training_status')->nullable();
            $table->foreign('employer_id')->references('id')->on('employer');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('training');
    }
}
