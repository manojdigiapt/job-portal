<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWebinarsAppliedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('webinars_applied', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('faculty_id')->unsigned()->nullable();
            $table->bigInteger('candidate_id')->unsigned()->nullable();
            $table->bigInteger('webinar_id')->unsigned()->nullable();
            $table->boolean('status')->nullable();

            $table->foreign('faculty_id')->references('id')->on('admin');
            $table->foreign('candidate_id')->references('id')->on('candidate');
            $table->foreign('webinar_id')->references('id')->on('webinars');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('webinars_applied');
    }
}
