<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('payment_id')->nullable();
            $table->string('payment_date')->nullable();

            $table->bigInteger('candidate_id')->unsigned()->nullable();
            $table->bigInteger('webinar_id')->unsigned()->nullable();
            $table->boolean('status')->nullable();

            $table->foreign('candidate_id')->references('id')->on('candidate');
            $table->foreign('webinar_id')->references('id')->on('webinars');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
