<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShortlistCandidatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shortlist_candidates', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('candidate_id')->unsigned()->nullable();
            $table->bigInteger('employer_id')->unsigned()->nullable();
            $table->boolean('shortlist_status')->nullable();
            
            $table->boolean('shortlisted_type')->nullable();

            $table->foreign('candidate_id')->references('id')->on('candidate');
            $table->foreign('employer_id')->references('id')->on('employer');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shortlist_candidates');
    }
}
