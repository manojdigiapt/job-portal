<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCandidateProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidate_profile', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('candidate_id')->unsigned();
            $table->string('father_name')->nullable();
            $table->string('profile_pic')->nullable();
            $table->string('industry')->nullable();
            $table->string('functional_area')->nullable();
            $table->string('role')->nullable();
            $table->longText('skills')->nullable();
            $table->boolean('allow_search')->nullable();
            $table->string('experience')->nullable();
            // $table->longText('education');
            $table->string('minimum_salary')->nullable();
            $table->string('gender')->nullable();
            $table->string('date_of_birth')->nullable();
            $table->string('age')->nullable();
            $table->string('aadhar')->nullable();
            $table->string('pan')->nullable();
            $table->string('address')->nullable();
            $table->bigInteger('zip_code')->nullable();
            $table->string('country')->nullable();
            $table->string('state')->nullable();
            $table->string('city')->nullable();
            // $table->longText('skills');
            $table->bigInteger('gross_salary')->nullable();
            $table->bigInteger('expected_salary')->nullable();
            $table->string('education_level')->nullable();
            $table->string('languages')->nullable();
            $table->longText('specialisms')->nullable();
            $table->longText('description')->nullable();
            $table->string('website')->nullable();
            $table->string('facebook')->nullable();
            $table->string('twitter')->nullable();
            $table->string('linkedin')->nullable();
            $table->string('googleplus')->nullable();

            $table->string('job_type')->nullable();
            $table->string('employee_type')->nullable();
            $table->string('desired_location')->nullable();
            $table->string('preferred_shift')->nullable();
            $table->string('marital_status')->nullable();
            $table->boolean('profile_status')->nullable();
            $table->string('profile_completion')->nullable();
            $table->bigInteger('institute_id')->nullable();
            // $table->foreign('favourite_jobs')->references('id')->on('jobs');
            $table->foreign('candidate_id')->references('id')->on('candidate');
            $table->foreign('institute_id')->references('id')->on('institute');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('candidate_profile');
    }
}
