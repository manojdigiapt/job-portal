<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsAppliedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs_applied', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('candidate_id')->unsigned();
            $table->bigInteger('employer_id')->unsigned()->nullable();
            $table->bigInteger('jobs_id')->unsigned()->nullable();
            $table->bigInteger('training_id')->unsigned()->nullable();
            $table->boolean('training_status')->nullable();
            $table->string('certificate')->nullable();
            $table->boolean('recruiter_status')->nullable();
            $table->boolean('download_cv_status')->nullable();

            $table->string('application_view_date')->nullable();
            $table->string('download_cv_date')->nullable();

            $table->string('training_end_date')->nullable();

            $table->boolean('training_completion_status')->nullable();

            $table->foreign('training_id')->references('id')->on('training');
            $table->foreign('jobs_id')->references('id')->on('jobs');
            $table->foreign('candidate_id')->references('id')->on('candidate');
            $table->foreign('employer_id')->references('id')->on('employer');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs_applied');
    }
}
